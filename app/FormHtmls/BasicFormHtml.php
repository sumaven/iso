<?php namespace App\FormHtmls;

use App\FormEntities\FormCol;
use App\FormEntities\FormColType;
use App\FormEntities\FormData;
use App\FormEntities\FormRow;
use App\FormEntities\FormRowType;
use App\FormEntities\SelectChoice;

trait BasicFormHtml
{

    public function getFormHTML($formArrayObject)
    {
        $content = "";
        $content .= <<<EOT
<div class="row py-3">\n
EOT;

        foreach ($formArrayObject as $formRow) {

            if (isset($formRow->form_header_title)) {
                $content .= <<<EOT
                    <div class="page-header" style="margin-top: 30px">
                        <h4 style="font-weight: bold"> {$formRow->form_header_title} </h4>
                    </div><!-- /.page-header -->
EOT;
            }

            if ($formRow instanceof FormRow && $formRow->form_data instanceof FormData) {

                if ($formRow->form_row_type == FormRowType::ADDABLE) {

                    $content .= <<<EOT
                    
                    <div class="row py-3">\n
EOT;

                    $content .= <<<EOT

                    <div style="margin-left: 30px; margin-right: 30px">
                        <div id="toolbar" style="margin-left: 15px">
                            <div class="form-inline" role="form">
                                <div class="form-group">
                                    <a id="addRow" onclick="addRow('{$formRow->form_data->table_name}')">
                                        <button type="button" class="btn btn-labeled btn-primary">Add new</button>
                                    </a>
                                </div>
                                <!-- <button id="ok" type="submit" class="btn btn-primary">OK</button> -->
                            </div>
                        </div>

                        <table
                            id="table"
                            data-pagination="true"
                            data-toggle="table"
                            data-toolbar="#toolbar"
                            data-show-refresh="true"
                            data-show-toggle="true"
                            data-tablename="{$formRow->form_data->table_name}">
                            <!-- data-url="https://examples.wenzhixin.net.cn/examples/bootstrap_table/data" -->
                            <thead>
                            <tr>
                                <th data-field="no">No</th>
                                <th data-field="action">Action</th>\n
EOT;
                    foreach ($formRow->form_list as $formCol) {
                        if ($formCol instanceof FormCol && $formCol->form_col_type != null) {
                            $content .= <<<EOT
                                <th data-field="{$formCol->form_name}">{$formCol->form_label}</th>\n
EOT;
                        }
                    }

                    $content .= <<<EOT
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>\n
EOT;

                }
            } else if ($formRow instanceof FormRow) {

                $input_col_width = "";
                if ($formRow->form_row_type == FormRowType::TWO_SPLIT) {
                    $input_col_width = "col-xs-4";
                } else if ($formRow->form_row_type == FormRowType::ONE_SOLO) {
                    $input_col_width = "col-xs-10";
                }

                $formColTitleContent = "";
                foreach ($formRow->form_list as $formCol) {
                    if ($formCol instanceof FormCol) {
                        if (isset($formCol->form_title)) {
                            $formColTitleContent .= <<<EOT
                        
                        <div class="col-xs-6" align="center">
                            <div class="page-header">
                                <h4 style="font-weight: bold"> $formCol->form_title </h4>
                            </div><!-- /.page-header -->
                        </div>
EOT;
                        }
                    }
                }

                if (!empty($formColTitleContent)) {
                    $top = <<<EOT
                    
                    <div class="row">\n
EOT;
                    $bottom = <<<EOT
                    
                    </div>\n
EOT;

                    $top .= $formColTitleContent;
                    $formColTitleContent = $top;
                    $formColTitleContent .= $bottom;
                }

                $content .= $formColTitleContent;

                $content .= <<<EOT
                    
                    <div class="row py-3">\n
EOT;

                foreach ($formRow->form_list as $formCol) {
                    if ($formCol instanceof FormCol && $formCol->form_col_type != null) {

                        $content .= $this->loopFormColHorizontally($formCol, $input_col_width);

                    } else if (is_array($formCol)) {

                        $content .= $this->loopFormColVertically($formCol);

                    } else if ($formCol->form_col_type == null) {
                        $content .= <<<EOT
                        <div class="col-xs-6">
                        </div>
EOT;

                    }
                }

            }

            $content .= <<<EOT
                    </div>\n\n
EOT;
        }

        return $content;
    }

    public function loopFormColHorizontally($formCol, $input_col_width)
    {
        $label_style = "";
        if (isset($formCol->form_label_style)) {
            foreach ($formCol->form_label_style as $property => $value) {
                $label_style .= "$property: $value; ";
            }
        }

        $content = "";

        $content .= <<<EOT
                        <div class="col-xs-2" align="right">
                            <label class="pt-1" style="$label_style"> {$formCol->form_label} </label>
                        </div>
                        <div class="{$input_col_width}">
                            <div class="form-group">\n
EOT;
        if ($formCol->form_col_type == FormColType::FORM_INPUT) {
            $content .= <<<EOT
                                <input name="{$formCol->form_name}" placeholder="{$formCol->form_description}" class="form-control" type="text" />\n
EOT;
        } else if ($formCol->form_col_type == FormColType::FORM_FILE) {
            $content .= <<<EOT
                                <input name="{$formCol->form_name}" type="file" id="file_choose" class="custom-file-input" type="text" />\n
EOT;
        } else if ($formCol->form_col_type == FormColType::FORM_INPUT_DATE) {
            $content .= <<<EOT
                                <input name="{$formCol->form_name}" class="form-control" placeholder="yyyy-mm-dd" />\n
EOT;
        } else if ($formCol->form_col_type == FormColType::FORM_TEXTAREA) {
            $content .= <<<EOT
                                <div class="wysiwyg-editor" name="{$formCol->form_name}"></div>
                                <textarea style="display:none" name="{$formCol->form_name}" placeholder="{$formCol->form_description}"></textarea>\n
EOT;
        } else if ($formCol->form_col_type == FormColType::FORM_SELECT) {
            $content .= <<<EOT
                                <select name="{$formCol->form_name}" class="form-control" style="width: 100%;">
                                    <option value="" selected="selected" class="">{$formCol->form_description}</option>\n
EOT;
            if (isset($formCol->form_data))
                foreach ($formCol->form_data as $selectChoice) {
                    if ($selectChoice instanceof SelectChoice) {
                        $content .= <<<EOT
                                    <option value="{$selectChoice->value}">{$selectChoice->option}</option>\n
EOT;
                    }
                }

            $content .= <<<EOT
                                </select>\n
EOT;

        } else if ($formCol->form_col_type == FormColType::FORM_CHECKBOX) {

            if (isset($formCol->form_data))
                foreach ($formCol->form_data as $selectChoice) {
                    if ($selectChoice instanceof SelectChoice) {
                        $content .= <<<EOT
                                    <div class="form-check" style="margin-left: 20px">
                                      <label class="form-check-label">
                                        <input type="checkbox" name="{$formCol->form_name}[]" class="form-check-input" value="{$selectChoice->value}"> {$selectChoice->option}
                                      </label>
                                    </div>
EOT;
                    }
                }

        } else {
            $content .= "NULL";
        }
        $content .= <<<EOT
                            </div>
                        </div>\n
EOT;
        return $content;
    }

    public function loopFormColVertically($array)
    {

        $label_style = "";
        if (isset($formCol->form_label_style)) {
            foreach ($formCol->form_label_style as $property => $value) {
                $label_style .= "$property: $value; ";
            }
        }

        $content = <<<EOT
                    
                    <div class="col-xs-6">
EOT;

        foreach ($array as $formCol) {
            $content .= <<<EOT
                    
                        <div class="row">
                            <div class="col-xs-4" align="right">
                                <label class="pt-1" style="$label_style"> {$formCol->form_label} </label>
                            </div>
                            <div class="col-xs-8">
                                <div class="form-group">\n
EOT;
            if ($formCol->form_col_type == FormColType::FORM_INPUT) {
                $content .= <<<EOT
                                    <input name="{$formCol->form_name}" placeholder="{$formCol->form_description}" class="form-control" type="text" />\n
EOT;
            } else if ($formCol->form_col_type == FormColType::FORM_FILE) {
                $content .= <<<EOT
                                    <input name="{$formCol->form_name}" type="file" id="file_choose" class="custom-file-input" type="text" />\n
EOT;
            } else if ($formCol->form_col_type == FormColType::FORM_INPUT_DATE) {
                $content .= <<<EOT
                                    <input name="{$formCol->form_name}" class="form-control" placeholder="yyyy-mm-dd" />\n
EOT;
            } else if ($formCol->form_col_type == FormColType::FORM_TEXTAREA) {
                $content .= <<<EOT
                                    <div class="wysiwyg-editor" name="{$formCol->form_name}"></div>
                                    <textarea style="display:none" name="{$formCol->form_name}" placeholder="{$formCol->form_description}"></textarea>\n
EOT;
            } else if ($formCol->form_col_type == FormColType::FORM_SELECT) {
                $content .= <<<EOT
                                    <select name="{$formCol->form_name}" class="form-control" style="width: 100%;">
                                        <option value="" selected="selected" class="">{$formCol->form_description}</option>\n
EOT;
                if (isset($formCol->form_data))
                    foreach ($formCol->form_data as $selectChoice) {
                        if ($selectChoice instanceof SelectChoice) {
                            $content .= <<<EOT
                                        <option value="{$selectChoice->value}">{$selectChoice->option}</option>\n
EOT;
                        }
                    }

                $content .= <<<EOT
                                    </select>\n
EOT;

            } else if ($formCol->form_col_type == FormColType::FORM_CHECKBOX) {

                if (isset($formCol->form_data))
                    foreach ($formCol->form_data as $selectChoice) {
                        if ($selectChoice instanceof SelectChoice) {
                            $content .= <<<EOT
                                    <div class="form-check" style="margin-left: 20px">
                                      <label class="form-check-label">
                                        <input type="checkbox" name="{$formCol->form_name}[]" class="form-check-input" value="{$selectChoice->value}"> {$selectChoice->option}
                                      </label>
                                    </div>
EOT;
                        }
                    }

            } else {
                $content .= "NULL";
            }
            $content .= <<<EOT
                            
                                </div>
                            </div>
                        </div>\n
EOT;
        }

        $content .= <<<EOT
                    
                    </div>\n
EOT;

        return $content;

    }

}