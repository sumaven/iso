<?php
return [
    "action" => "Action",
    "data_table" => "Data Table",
    "nav_dashboard" => "Dashboard",
    "nav_business_process" => "Business Proces",
    "reference" => "Reference",
    "procedure" => "Procedure",
    "document_no" => "Document No",
    "rev_no" => "Rev No.",
    "header_title" => "Header Title",
    "distribusi" => "Distribution",
    "approve_author" => "Approving Authority",
    "approval_status" => "Approval Status",
    "supporting_files" => "Supporting Files",
    
    "create_edit_agenda" => "Create / Edit Agenda",

    # Create Management Review
    "date_of_management_review" => "Date of Management Review",
    "attendee" => "Attendee",
    "agenda" => "Agenda",
    "responsibility" => "Responsibility",
    "document_required" => "Document Required",

    # Management Review Result
    "actual_management_review" => "Actual Management Review",
    "action_required" => "Action Required",
    "target_date" => "Target Date",
    "actual_date" => "Actual Date",

];