<?php
return [
    "action" => "Action",
    "data_table" => "Data Table",
    "nav_dashboard" => "Dashboard",
    "nav_business_process" => "Business Proces",
    "document_no" => "Document No",
    "rev_no" => "Rev No.",
    "header_title" => "Header Title",
    "distribusi" => "Distribution",
    "approve_author" => "Approving Authority",
    "approval_status" => "Approval Status",

    # Risk Opportunity
    "management_system_type" => "Management System Type",
    "risk_opportunity_type" => "Risk and Opportunity Type",
    "risk_impact" => "Risk Impact",
    "risk_probability" => "Risk Probability",
    "assigned_to" => "Assigned To",
    "treatment_type" => "Treatment Type",
    "is_opportunity" => "Is it an Opportunity",
    "proposed_review_date" => "Proposed Review Date",
    "proposed_completion_date" => "Proposed Completion Date",
    "risk_opportunity_source" => "Risk / Opportunity Source or Activity",
    "risk_description" => "Risk Description",
    "actions_proposed" => "Action Proposed",

    # Risk Opportunity Type
    "risk_type" => "Risk Type",

    # Risk Category
    "risk_category" => "Risk Category",

    "remarks" => "Remarks",

];