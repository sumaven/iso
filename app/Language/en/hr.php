<?php
return [
    "action" => "Action",
    "data_table" => "Data Table",
    "nav_dashboard" => "Dashboard",
    "nav_business_process" => "Business Proces",
    "reference" => "Reference",
    "procedure" => "Procedure",
    "document_no" => "Document No",
    "rev_no" => "Rev No.",
    "header_title" => "Header Title",
    "distribusi" => "Distribution",
    "approve_author" => "Approving Authority",
    "approval_status" => "Approval Status",
    "supporting_files" => "Supporting Files",

    # Department
    "department_hod" => "Department Hod",
    "department_name" => "Department Name",
    "profile" => "Profile",

    # Designation
    "designation_name" => "Designation Name",

    # Ideal Competence
    "designation" => "Designation",

    # Training Identification
    "training_topic" => "Training Topic",
    "proposed_trainer" => "Proposed Trainer",
    "department" => "Department",
    "training_type" => "Training Type",
    "planned_date" => "Planned Date",
    "remarks" => "Remarks",

    "attendee_name" => "Attendee name",
    "pre_training_competence" => "Pre Training Competence"

];