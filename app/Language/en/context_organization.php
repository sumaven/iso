<?php
return [
    "action" => "Action",
    "data_table" => "Data Table",
    "nav_dashboard" => "Dashboard",
    "nav_business_process" => "Business Proces",
    "reference" => "Reference",
    "procedure" => "Procedure",
    "document_no" => "Document No",
    "rev_no" => "Rev No.",
    "header_title" => "Header Title",
    "distribusi" => "Distribution",
    "approve_author" => "Approving Authority",
    "approval_status" => "Approval Status",
    "supporting_files" => "Supporting Files",

    # Business Process
    "dept_location" => "Dept / Location Name",
    "performance_indicator" => "Performance Indicator",
    "process_name" => "Process Name",
    "process_type" => "Process Type",
    "monitoring_methodology" => "Monitoring Methodology",
    "input_category" => "Input Category",
    "input" => "Input",
    "output_category" => "Output Category",
    "output" => "Output",
    "measurement" => "Measurement",
    "resources" => "Resources",
    "process_documented_information" => "Process / Documented Information",
    "person_in_charge" => "Person in Charge",


    # Context Organization
    "site" => "Site",
    "department" => "Department",
    "where_do_we_aim" => "Where Do We Aim To Be?",
    "why_good_business" => "Why This Business Is Good Business To Be In?",
    "what_do_we_do" => "What Do We Do? What Makes Us Different?",
    "swot_opportunity" => "Opportunity",
    "swot_strength" => "Strength",
    "swot_weakness" => "Weakness",
    "swot_threat" => "Threat",
    "pestel_political" => "Political",
    "pestel_social" => "Social",
    "pestel_legal" => "Legal",
    "pestel_economic" => "Economic",
    "pestel_technological" => "Technological",
    "pestel_environmental" => "Environmental",

    # Understanding the Organization and its Context
    "goals_vision_mission" => "Define Goals, Vision, Mission Your Organization",
    "product_service" => "Define Product / Service Your Organization",

    # Context organization - Issue Type
    "management_system_type" => "Management System Type",
    "scope" => "Scope of Management System",
    "stages_of_product" => "Stages of Product",
    "issue_type" => "Issue Type",

    # Context organization - Internal / External Issue
    "how_it_affects_your_organization" => "How it affects your organization",
    "risk_description" => "Risk Description",
    "risk_impact" => "Risk Impact",
    "risk_probability" => "Risk Probability",

    # Context Organization - Scope Management System
    "scope_of_management_system" => "Scope of Management System",
    "exclusion_justification" => "Justification for Exclusion",
    "exclusion" => "Exclusion",

    # Context Organization - Internal Issue
    "how_affect" => "How It Affects Your Organization",
    "action_plan" => "Action Plan",
    "risk_opportunity" => "Risk / Opportunity",

    "risk_description" => "Risk Description",
    "risk_impact" => "Risk Impact",
    "risk_possibility" => "Risk Possibility",
    "risk_category" => "Risk Category",

    # Context Organization - Interested Party
    "stakeholder_interestedparty" => "Stakeholder / Interestedparty",
    "requirement_need_expectation" => "Requirements / Needs / Expectations",
    "fulfillment" => "Fulfillment of Requirement in the Management System",
    "impact" => "Impact on Organization",

    # Context Organization - Lifecycle
    "product_service_project" => "Product / Service / Project",
    "stages_of_product" => "Stages of Product",
    "description" => "Description",
    "control_measure" => "Control Measure",
];