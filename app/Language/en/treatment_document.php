<?php
return [
    "action" => "Action",
    "data_table" => "Data Table",
    "nav_dashboard" => "Dashboard",
    "nav_business_process" => "Business Proces",
    "document_no" => "Document No",
    "rev_no" => "Rev No.",
    "header_title" => "Header Title",
    "distribusi" => "Distribution",
    "approve_author" => "Approving Authority",
    "approval_status" => "Approval Status",

    "procedure" => [
        // Kenapa pakai name prosedure, dan bukan procedure :
        // karna di database mysql gak bisa pakai nama kolom procedure

        # Procedure

        "header_title" => "Procedure Title",
        "header_title_description" => "Untitled Procedure",

        "department" => "Department",
        "revision" => "Revision",
        "date_issued" => "Date Issued",

        "number" => "Procedure Number",

        "scope" => "Scope",
        "goal" => "Goal",
        "definition" => "Definition",
        "general_information" => "General Information",
        "prosedure" => "Procedure",
        "recording" => "Recording",

        "reference" => "Reference",
        "approval" => "Approval",
        "distribution" => "Distribution",

        "scope_description" => "Describe this procedure is related to what",
        "goal_description" => "Provide a 2-3 sentence description of what the procedure is about and its purposed. Why is this procedure important?",
        "definition_description" => "Describe all the terms used including extension",
        "general_information_description" => "Information is submitted and explained as a guide, to avoid misperseption",
        "prosedure_description" => "Communicate the role off all related section including duties and responbilities",
        "recording_description" => "Communicate all document and supporting facilities",
    ],

    "work_instruction" => [

        # Work Instruction

        "header_title" => "Work Instruction Title",
        "header_title_description" => "Untitled Work Instruction",

        "department" => "Department",
        "revision" => "Revision",
        "date_issued" => "Date Issued",

        "number" => "Work Instruction Number",

        "scope" => "Scope",
        "goal" => "Goal",
        "work_instruction" => "Work Instruction",

        "reference" => "Reference",
        "approval" => "Approval",
        "distribution" => "Distribution",

        "scope_description" => "Describe this procedure is related to what",
        "goal_description" => "Provide a 2-3 sentence description of what the procedure is about and its purposed. Why is this procedure important?",
        "work_instruction_description" => "Communicate how the step to do a job",
    ],

    # Notulen
    "hari" => "Day",
    "tanggal" => "Date",
    "waktu" => "Time",
    "tempat" => "Place",
    "status" => "Status",
    "pic" => "PIC",
    "konseptor" => "Created by",
    "topik" => "Topics",
    "uraian" => "Description",
    "status_panjang" => "Status",

    # Rekaman / Recordings
    "referensi" => "Reference",
    "header_title" => "Header Title",
    "class_project" => "Class / Project",
    "date_time_location" => "Date / Time / Location",
    "goal" => "Goal",
    "attendees" => "Attendees",
    "description_activity" => "Description Activity",
    "result" => "Result",
    "distribusi_1" => "Distribution 1",
    "distribusi_2" => "Distribution 2",
    "approval_1" => "Approval 1",
    "approval_2" => "Approval 2",

    # Target
    "id_department" => "Location / Department",
    "periode" => "Period",
    "mengetahui" => "Knowing",
    "persetujuan" => "Submission of Approval",
    "target" => "Target & Quality Objectives",
    "sumber_daya" => "Resource",
    "action" => "Step / Action",
    "parameter" => "Parameter",
    "jadwal" => "Completion Time Schedule",
    "tanggung_jawab" => "Person in Charge",

    # Usulan Dokumen
    "header_doc_no" => "Document No. (Header)",
    "header_rev_no" => "Revision No. (Header)",
    "nama_pengusul" => "The Name of Proposer",
    "nama_bagian" => "The Name of Part",
    "judul_dokumen" => "Document Title",
    "doc_no" => "Doc No.",
    "versi" => "Version",

    // Usulan Dokumen Items
    "no_halaman" => "Halaman",
    "no_bagian" => "Bagian",
    "klausul_awal_1" => "Klausul Awal 1",
    "klausul_awal_2" => "Klausul Awal 2",
    "klausul_perubahan_1" => "Klausul Ubah 1",
    "klausul_perubahan_2" => "Klausul Ubah 2",
];