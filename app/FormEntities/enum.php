<?php namespace App\FormEntities;

abstract class enum
{
    // Enumeration constructor
    final public function __construct($value)
    {
        $this->value = $value;
    }

    // String representation
    final public function __toString()
    {
        return $this->value;
    }
}