<?php namespace App\FormEntities;

class DependentSelectChoice
{
    public $dependentToValueOf;
    public $dependantChoices;

    public function setDependentToValueOf($formName){
        $this->dependentToValueOf = $formName;
    }
}