<?php namespace App\FormEntities;

class FormRow
{
    public $form_row_type;
    public $form_header_title;
    public $form_data;
    public $form_list;

    public function __construct($form_row_type, $form_header_title, ...$form_list)
    {
        $this->form_row_type = $form_row_type;
        $this->form_header_title = $form_header_title;
        $this->form_list = $form_list;
    }

}