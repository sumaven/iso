<?php namespace App\FormEntities;

class FormCol
{
    public $form_col_type;
    public $form_label;
    public $form_name;
    public $form_data;
    public $form_title;
    public $form_label_style;
    public $form_description;

    public function __construct()
    {
        $this->form_col_type = "";
        $this->form_label = "";
        $this->form_name = "";
        $this->form_label_style = array();
        $this->form_data = array();
    }

    public static function createInstance($form_col_type = "", $form_label = "", $form_name = "", $form_data = array()){
        $formCol = new FormCol();
        $formCol->form_col_type = $form_col_type;
        $formCol->form_label = $form_label;
        $formCol->form_name = $form_name;
        $formCol->form_data = $form_data;
        return $formCol;
    }

    public function setFormColType($arg = "") {
        $this->form_col_type = $arg;
        return $this;
    }

    public function setFormLabel($arg = ""){
        $this->form_label = $arg;
        return $this;
    }

    public function setFormName($arg = ""){
        $this->form_name = $arg;
        return $this;
    }

    public function setFormData($arg = array()){
        $this->form_data = $arg;
        return $this;
    }

    public function setFormTitle($arg = array()){
        $this->form_title = $arg;
        return $this;
    }

    public function setFormDescription($arg = ""){
        $this->form_description = $arg;
        return $this;
    }

    public function setFormLabelStyle($arg = array()){
        $this->form_label_style = $arg;
        return $this;
    }
}