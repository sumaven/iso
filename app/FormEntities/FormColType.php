<?php namespace App\FormEntities;

class FormColType extends enum
{
    const FORM_SELECT = 'form_select';
    const FORM_INPUT = 'form_input';
    const FORM_INPUT_DATE = 'form_input_date';
    const FORM_TEXTAREA = 'form_textarea';
    const FORM_CHECKBOX = 'form_checkbox';
    const FORM_FILE = "form_file";
}