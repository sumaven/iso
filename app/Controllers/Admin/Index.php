<?php namespace App\Controllers;

class Index extends BaseController {

	function __construct(){
		parent::__construct();
		
		$this->load->model('HR/EmployeeModel');
	}

    public function index(){
		$data = array(
			'title' => "",
			"rows" => $this->EmployeeModel->read()->result()
		);
		echo view('hr/employee_read', $data);
	}
}
