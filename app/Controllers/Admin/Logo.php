<?php namespace App\Controllers\Admin;

class Logo extends BaseController {

	function __construct(){
		parent::__construct();
		
		$this->load->model('Admin/LogoModel');
	}

    public function list(){
		$data = array(
			'title' => "",
			"rows" => $this->LogoModel->read()->result()
		);
		echo view('admin/logo_read', $data);
    }

    public function create(){
		$data = array(
			"title"			=> "Add Logo",
		);
		echo view('admin/logo_create', $data);
    }

    public function process_add(){
		$errors         = array();      // array to hold validation errors
		$data           = array();      // array to pass back data
		$data_pass		= array(
				"active"        		=> $_POST['active'],
		 );

        if (!empty($errors)) {

            // if there are items in our errors array, return those errors
            $response_data["success"] = false;
            $response_data["errors"]  = $errors;

        } else {

            // if there are no errors process our form, then return a message
            $isSuccess = $this->LogoModel->insert($data_pass, $_FILES);

            // DO ALL YOUR FORM PROCESSING HERE

            // show a message of success and provide a true success variable
            if ($isSuccess){
                $response_data["success"] = true;
                $response_data["message"] = "Tambah data berhasil";
            } else {
                $errors['email'] = 'This type is already exists';
                $response_data["success"] = false;
                $response_data["errors"]  = $errors;
            }
        }

        echo $response_data["message"];
    }
}
