<?php namespace App\Controllers\Context_organization;

use App\Controllers\BaseTrackingController;
use App\FormHtmls\BasicFormHtml;
use App\FormObjects\context_organization\Internal_issue_form;
use App\Libraries\Tracking;
use App\Models\Context\InternalIssueModel;
use App\Models\Context\IssueTypeModel;
use App\Models\HR\EmployeeModel;
use App\Models\RiskOpportunity\RiskOpportunityCategoryModel;
use CodeIgniter\HTTP\RequestInterface;
use CodeIgniter\HTTP\ResponseInterface;
use Psr\Log\LoggerInterface;

class Internal_Issue extends BaseTrackingController
{
    protected $step_type = 10;

    use Internal_issue_form;
    use BasicFormHtml;

    public function initController(RequestInterface $request, ResponseInterface $response, LoggerInterface $logger)
    {
        // Do Not Edit This Line
        parent::initController($request, $response, $logger);

        $this->tracking = new Tracking();
        $this->model = new InternalIssueModel();
        $this->RiskOpportunityCategoryModel = new RiskOpportunityCategoryModel();
        $this->IssueTypeModel = new IssueTypeModel();
        $this->EmployeeModel = new EmployeeModel();

        $this->module_title = "Context of Organization";
        $this->submodule_title = "Internal Issue";
        $this->controller_path = "context_organization/internal_issue";
        $this->controller_path_url = base_url($this->controller_path);
        $this->hasSupportingFiles = true;

        $this->view_data = array(
            "module_title" => $this->module_title,
            "submodule_title" => $this->submodule_title,
            "controller_path" => $this->controller_path,
            "controller_path_url" => $this->controller_path_url,
            "hasSupportingFiles" => $this->hasSupportingFiles,
        );
    }

    public function list()
    {
        $this->view_data["title"] = $this->submodule_title;
        $this->view_data["rows"] = $this->model->readAll(session()->get("id"));
        echo view("{$this->controller_path}_read", $this->view_data);
    }

    public function create()
    {
        $formArrayObject = $this->getFormObject();
        $formHtml = $this->getFormHTML($formArrayObject);

        $this->view_data["form"] = $formHtml;
        $this->view_data["formArrayObject"] = $formArrayObject;

        $this->view_data["title"] = "Create New {$this->submodule_title}";
        echo view("_base/tracking_create", $this->view_data);
    }

    public function edit($id)
    {
        $formArrayObject = $this->getFormObject();
        $formHtml = $this->getFormHTML($formArrayObject);

        $this->view_data["form"] = $formHtml;
        $this->view_data["formArrayObject"] = $formArrayObject;

        $this->view_data["title"] = "Edit {$this->submodule_title}";
        $this->view_data["row"] = $this->model->read($id);
        echo view("_base/tracking_edit", $this->view_data);
    }

}
