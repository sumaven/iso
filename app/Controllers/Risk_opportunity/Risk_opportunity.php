<?php namespace App\Controllers\Risk_opportunity;

use App\Controllers\BaseController;
use App\FormHtmls\BasicFormHtml;
use App\FormObjects\risk_opportunity\Risk_opportunity_form;
use App\Models\Admin\ManagementSystemTypeModel;
use App\Models\HR\DepartmentModel;
use App\Models\HR\EmployeeModel;
use App\Models\RiskOpportunity\RiskOpportunityCategoryModel;
use App\Models\RiskOpportunity\RiskOpportunityModel;
use App\Models\RiskOpportunity\RiskOpportunityTypeModel;
use CodeIgniter\HTTP\RequestInterface;
use CodeIgniter\HTTP\ResponseInterface;
use Psr\Log\LoggerInterface;

class Risk_Opportunity extends BaseController
{
    use Risk_opportunity_form;
    use BasicFormHtml;

    public function initController(RequestInterface $request, ResponseInterface $response, LoggerInterface $logger)
    {
        // Do Not Edit This Line
        parent::initController($request, $response, $logger);

        $this->model = new RiskOpportunityModel();
        $this->ManagementSystemTypeModel = new ManagementSystemTypeModel();
        $this->EmployeeModel = new EmployeeModel();
        $this->DepartmentModel = new DepartmentModel();
        $this->RiskOpportunityTypeModel = new RiskOpportunityTypeModel();
        $this->RiskOpportunityCategoryModel = new RiskOpportunityCategoryModel();

        $this->module_title = "Risk and Opportunity";
        $this->submodule_title = ".";
        $this->controller_path = "risk_opportunity";
        $this->controller_path_url = base_url($this->controller_path);
        $this->hasSupportingFiles = true;

        $this->view_data = array(
            "module_title" => $this->module_title,
            "submodule_title" => $this->submodule_title,
            "controller_path" => $this->controller_path,
            "controller_path_url" => $this->controller_path_url,
            "hasSupportingFiles" => $this->hasSupportingFiles,
        );
    }

    public function list()
    {
        $this->view_data["title"] = $this->submodule_title;
        $this->view_data["rows"] = $this->model->readAll(session()->get("id"));
        echo view("{$this->controller_path}/read", $this->view_data);
    }

    public function create()
    {
        $formArrayObject = $this->getFormObject();
        $formHtml = $this->getFormHTML($formArrayObject);

        $this->view_data["form"] = $formHtml;
        $this->view_data["formArrayObject"] = $formArrayObject;

        $this->view_data["title"] = "Create New {$this->submodule_title}";
        echo view("_base/create", $this->view_data);
    }

    public function edit($id)
    {
        $formArrayObject = $this->getFormObject();
        $formHtml = $this->getFormHTML($formArrayObject);

        $this->view_data["form"] = $formHtml;
        $this->view_data["formArrayObject"] = $formArrayObject;

        $this->view_data["title"] = "Edit {$this->submodule_title}";
        $this->view_data["row"] = $this->model->find($id);
        echo view("_base/edit", $this->view_data);
    }
}
