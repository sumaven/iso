<?php namespace App\Controllers;

class Rule extends BaseController
{
    private $module_title;
    private $submodule_title;
    private $controller_path;
    private $controller_path_url;

    public function __construct()
    {
        parent::__construct();

        if ($this->request->uri->getSegment(3) != "html") {
            if (!user_access_check()) {
                exit();
            }
        }

        $this->load->model("RuleModel");
        $this->load->model("RuleTypeModel");

        $this->module_title = "Rule";
        $this->submodule_title = ".";
        $this->controller_path = "rule";
        $this->controller_path_url = base_url($this->controller_path);
    }

    public function list()
    {
        $data = array(
            "rows" => $this->RuleModel->read()->result(),
            "module_title" => $this->module_title,
            "submodule_title" => $this->submodule_title,
            "controller_path_url" => $this->controller_path_url,
        );
        echo view("rule/read", $data);
    }

    public function create()
    {
        $data = array(
            "rule_types" => $this->RuleTypeModel->read()->result(),
            "module_title" => $this->module_title,
            "submodule_title" => $this->submodule_title,
            "controller_path_url" => $this->controller_path_url,
        );

        echo view("rule/create", $data);
    }

    public function edit($id)
    {
        $data = array(
            "id" => $id,
            "rows" => $this->RuleModel->readItems($id)->result(),
            "rule_types" => $this->RuleTypeModel->read()->result(),
            "module_title" => $this->module_title,
            "submodule_title" => $this->submodule_title,
            "controller_path_url" => $this->controller_path_url,
        );

        echo view("rule/edit", $data);
    }

    public function process_add()
    {
        $errors = array();      // array to hold validation errors
        $data_pass = array(
            "datas" => $_POST["datas"]
        );

//        foreach ($data_pass["datas"] as $data) {
//            if (empty($data["nama"]) && $data["nama"] != "0") {
//                // if (!("nama" in $errors)){
//                // $errors["nama"] =  "alert("Designation is required");";
//                $errors["nama"] = <<<EOT
//	                $("#designation_group").addClass("has-error"); // add the error class to show red input
//	                $("#designation_group").append(
//	                    "<div class="help-block" style="color:red">" +
//	                        "This field is required"  +
//	                    "</div>");
//EOT;
//                // }
//            }
//        }

        // if there are any errors in our errors array, return a success boolean of false
        if (!empty($errors)) {

            // if there are items in our errors array, return those errors
            $response_data["success"] = false;
            $response_data["errors"] = $errors;

        } else {

            // if there are no errors process our form, then return a message
            $isSuccess = $this->RuleModel->insert($data_pass);

            // DO ALL YOUR FORM PROCESSING HERE

            // show a message of success and provide a true success variable
            if ($isSuccess) {
                $response_data["success"] = true;
                $response_data["message"] = "Tambah data berhasil";
                header("refresh:2; url=" . base_url($this->controller_path . "/list"));
            } else {
                $response_data["success"] = false;
                $response_data["errors"] = $errors;
                $response_data["message"] = "Tambah data gagal";
                echo "<script>setTimeout(function() {history.back()}, 2000)</script>";
            }
        }

        echo $response_data["message"];
    }

    public function process_edit()
    {
        $data = array(
            "id" => $_POST["id"],
            "datas" => $_POST["datas"]
        );

        if ($this->RuleModel->edit($data)) {
            $response_data["success"] = true;
            $response_data["message"] = "Ubah data berhasil";
            header("refresh:2; url=" . base_url($this->controller_path . "/edit/" . $_POST['id']));
        } else {
            $response_data["success"] = false;
            $response_data["message"] = "Ubah data gagal";
            echo "<script>setTimeout(function() {history.back()}, 2000)</script>";
        }

        echo $response_data["message"];
    }

    public function process_delete($id)
    {

        if ($this->RuleModel->delete($id)) {
            $response_data["success"] = true;
            $response_data["message"] = "Hapus data berhasil";
        } else {
            $response_data["success"] = false;
            $response_data["message"] = "Hapus data gagal";
        }
        header("refresh:2; url=" . base_url($this->controller_path . "/list"));

        echo $response_data["message"];
    }
}
