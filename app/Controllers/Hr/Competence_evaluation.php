<?php namespace App\Controllers\Hr;

use App\Controllers\BaseController;

class Competence_Evaluation extends BaseController
{
    private $module_title;
    private $submodule_title;
    private $controller_path;
    private $controller_path_url;

    public function __construct()
    {
        parent::__construct();

        if ($this->request->uri->getSegment(3) != "html") {
            if (!user_access_check()) {
                exit();
            }
        }

        $this->load->model("HR/CompetenceEvaluationModel");
        $this->load->model("HR/DesignationModel");

        $this->module_title = "HR";
        $this->submodule_title = "Competence Evaluation";
        $this->controller_path = "hr/competence_evaluation";
        $this->controller_path_url = base_url($this->controller_path);
    }

    public function index()
    {
        $data = array(
            "title" => "Dashboard - HR",
            "module_title" => $this->module_title,
            "submodule_title" => $this->submodule_title,
            "controller_path_url" => $this->controller_path_url,
        );
        echo view("hr/index", $data);
    }

    public function list()
    {
        $data = array(
            "title" => "Competence Evaluation",
            "rows" => $this->CompetenceEvaluationModel->read()->result(),
            "module_title" => $this->module_title,
            "submodule_title" => $this->submodule_title,
            "controller_path_url" => $this->controller_path_url,
        );
        echo view("hr/competence_evaluation_read", $data);
    }

    public function create()
    {
        $data = array(
            "title" => "Create New Competence Evaluation",
            "designations" => $this->DesignationModel->read()->result(),
            "module_title" => $this->module_title,
            "submodule_title" => $this->submodule_title,
            "controller_path_url" => $this->controller_path_url,
        );

        echo view("hr/competence_evaluation_create", $data);
    }

    public function edit($id)
    {
        $data = array(
            "title" => "Edit Competence Evaluation",
            "designations" => $this->DesignationModel->read()->result(),
            "rows" => $this->CompetenceEvaluationModel->readItems($id)->result(),
            "evaluationRows" => $this->CompetenceEvaluationModel->readEvaluationItems($id)->result(),
            "id" => $id,
            "module_title" => $this->module_title,
            "submodule_title" => $this->submodule_title,
            "controller_path_url" => $this->controller_path_url,
        );

        echo view("hr/competence_evaluation_edit", $data);
    }

    public function process_add()
    {
        $errors = array();      // array to hold validation errors
        $data_pass = array(
            "datas" => $_POST["datas"]
        );

        // if there are any errors in our errors array, return a success boolean of false
        if (!empty($errors)) {

            // if there are items in our errors array, return those errors
            $response_data["success"] = false;
            $response_data["errors"] = $errors;
        } else {

            // if there are no errors process our form, then return a message
            $isSuccess = $this->CompetenceEvaluationModel->insert($data_pass);


            // DO ALL YOUR FORM PROCESSING HERE

            // show a message of success and provide a true success variable
            if ($isSuccess) {
                $response_data["success"] = true;
                $response_data["message"] = "Tambah data berhasil";
            } else {
                $response_data["success"] = false;
                $response_data["errors"] = $errors;
                $response_data["message"] = "Tambah data gagal";
            }
        }

        // return all our data to an AJAX call
        echo json_encode($response_data);
    }

    public function process_edit()
    {
        $data = array(
            "id" => $_POST["id"],
            "datas" => $_POST["datas"]
        );

        if ($this->CompetenceEvaluationModel->edit($data)) {
            $response_data["success"] = true;
            $response_data["message"] = "Ubah data berhasil";
        } else {
            $response_data["success"] = false;
            $response_data["message"] = "Ubah data gagal";
        }

        echo json_encode($response_data);
    }

    public function process_delete($id)
    {

        if ($this->CompetenceEvaluationModel->delete($id)) {
            $response_data["success"] = true;
            $response_data["message"] = "Hapus data berhasil";
        } else {
            $response_data["success"] = false;
            $response_data["message"] = "Hapus data gagal";
        }
        header("refresh:2; url=" . base_url($this->controller_path . "/list"));

        echo $response_data["message"];
    }
}
