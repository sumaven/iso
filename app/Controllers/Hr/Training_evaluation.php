<?php namespace App\Controllers\Hr;

use App\Controllers\BaseController;

class Training_Evaluation extends BaseController
{

    private $module_title;
    private $submodule_title;
    private $controller_path;
    private $controller_path_url;

	public function __construct(){
		parent::__construct();

        if ($this->request->uri->getSegment(3) != "html") {
            if (!user_access_check()) {
                exit();
            }
        }

        $this->load->model('HR/TrainingEvaluationModel');

        $this->module_title = "HR";
        $this->submodule_title = "Training Evaluation";
        $this->controller_path = "hr/training_evaluation";
        $this->controller_path_url = base_url($this->controller_path);
    }

    public function list()
    {
        $data = array(
            "title"			=> "Training Evaluation",
            "rows" 			=> $this->TrainingEvaluationModel->read()->result(),
            "module_title" => $this->module_title,
            "submodule_title" => $this->submodule_title,
            "controller_path_url" => $this->controller_path_url,
        );
        echo view('hr/training_evaluation_read', $data);
    }

    public function create()
    {
        $data = array(
            "title"			=> "Create New Training Evaluation",
            "module_title" => $this->module_title,
            "submodule_title" => $this->submodule_title,
            "controller_path_url" => $this->controller_path_url,
        );

        echo view('hr/training_evaluation_create', $data);
    }

    public function edit($id)
    {
        $data = array(
            "title"			=> "Edit Training Evaluation",
            'row'			=> $this->TrainingEvaluationModel->readItem($id)->row(),
            'rows'  		=> $this->TrainingEvaluationModel->readItems($id)->result(),
            'id' 			=> $id,
            "module_title" => $this->module_title,
            "submodule_title" => $this->submodule_title,
            "controller_path_url" => $this->controller_path_url,
        );

        echo view('hr/training_evaluation_edit', $data);
    }

    public function process_edit()
    {
        $data		= array(
            "id"					=>  $_POST['id'],
            "datas"					=> 	$_POST['datas'],
            "data"					=>  $_POST['data']
        );

        if ($this->TrainingEvaluationModel->edit($data)) {
            $response_data["success"] = true;
            $response_data["message"] = "Ubah data berhasil";
            header("refresh:2; url=" . base_url($this->controller_path . "/edit/" . $_POST['id']));
        } else {
            $response_data["success"] = false;
            $response_data["message"] = "Ubah data gagal";
            echo "<script>setTimeout(function() {history.back()}, 2000)</script>";
        }

        echo json_encode($response_data);
    }

    public function process_delete($id)
    {
        if ($this->TrainingEvaluationModel->delete($id)) {
            $response_data["success"] = true;
            $response_data["message"] = "Hapus data berhasil";
            header("refresh:2; url=" . base_url($this->controller_path . "/list"));
        } else {
            $response_data["success"] = false;
            $response_data["message"] = "Hapus data gagal";
            header("refresh:2; url=" . base_url($this->controller_path . "/list"));
        }

        echo $response_data["message"];
    }
}
