<?php namespace App\Controllers\Hr;

use App\Controllers\BaseController;
use App\FormHtmls\BasicFormHtml;
use App\FormObjects\documented_information\External_document_form;
use App\Models\DocumentedInformation\ExternalDocumentModel;
use App\Models\HR\DepartmentModel;
use App\Models\HR\EmployeeModel;
use CodeIgniter\HTTP\RequestInterface;
use CodeIgniter\HTTP\ResponseInterface;
use Psr\Log\LoggerInterface;

class Employee extends BaseController
{
    use External_document_form;
    use BasicFormHtml;

    public function initController(RequestInterface $request, ResponseInterface $response, LoggerInterface $logger)
    {
        // Do Not Edit This Line
        parent::initController($request, $response, $logger);

        $this->model = new EmployeeModel();
        $this->DepartmentModel = new DepartmentModel();

        $this->module_title = "HR";
        $this->submodule_title = "Employee";
        $this->controller_path = "hr/employee";
        $this->controller_path_url = base_url($this->controller_path);
        $this->hasSupportingFiles = true;

        $this->view_data = array(
            "module_title" => $this->module_title,
            "submodule_title" => $this->submodule_title,
            "controller_path" => $this->controller_path,
            "controller_path_url" => $this->controller_path_url,
            "hasSupportingFiles" => $this->hasSupportingFiles,
        );
    }

    public function list()
    {
        $data = array(
            "title" => "Employee",
            "rows" => $this->model->findAll(),
            "module_title" => $this->module_title,
            "submodule_title" => $this->submodule_title,
            "controller_path_url" => $this->controller_path_url,
        );
        echo view('hr/employee_read', $data);
    }

    public function create()
    {
        $data = array(
            "title" => "Create New Employee",
            "departments" => $this->DepartmentModel->findAll(),
            "designations" => $this->DesignationModel->findAll(),
            "jabatans" => $this->DesignationModel->findAll(),
            "module_title" => $this->module_title,
            "submodule_title" => $this->submodule_title,
            "controller_path_url" => $this->controller_path_url,
        );
        echo view('hr/employee_create', $data);
    }

    public function edit($id)
    {
        $data = array(
            "title" => "Edit Employee",
            'row' => $this->model->readItem($id),
            "departments" => $this->DepartmentModel->findAll(),
            "designations" => $this->DesignationModel->findAll(),
            "jabatans" => $this->DesignationModel->findAll(),
            "module_title" => $this->module_title,
            "submodule_title" => $this->submodule_title,
            "controller_path_url" => $this->controller_path_url,
        );

        echo view('hr/employee_edit', $data);
    }

//    public function create()
//    {
//        $formArrayObject = $this->getFormObject();
//        $formHtml = $this->getFormHTML($formArrayObject);
//
//        $this->view_data["form"] = $formHtml;
//        $this->view_data["formArrayObject"] = $formArrayObject;
//
//        $this->view_data["title"] = "Create New {$this->submodule_title}";
//        echo view("_base/create", $this->view_data);
//    }
//
//    public function edit($id)
//    {
//        $formArrayObject = $this->getFormObject();
//        $formHtml = $this->getFormHTML($formArrayObject);
//
//        $this->view_data["form"] = $formHtml;
//        $this->view_data["formArrayObject"] = $formArrayObject;
//
//        $this->view_data["title"] = "Edit {$this->submodule_title}";
//        $this->view_data["row"] = $this->model->find($id);
//        echo view("_base/edit", $this->view_data);
//    }

    public function test()
    {
        header('HTTP/1.1 403 Your access is denied');
        header('Status: 403 Your access is denied');
        header('Retry-After: 300');
        echo "Your access is denied";
    }
}
