<?php namespace App\Controllers\Procurement;

use App\Controllers\BaseController;
use App\FormHtmls\BasicFormHtml;
use App\FormObjects\procurement\Vendor_registration_form;
use App\Models\Planning\RegulationRegisterModel;
use App\Models\Procurement\VendorRegistrationModel;
use CodeIgniter\HTTP\RequestInterface;
use CodeIgniter\HTTP\ResponseInterface;
use Psr\Log\LoggerInterface;

class Vendor_registration extends BaseController
{
    use Vendor_registration_form;
    use BasicFormHtml;

    public function initController(RequestInterface $request, ResponseInterface $response, LoggerInterface $logger)
    {
        // Do Not Edit This Line
        parent::initController($request, $response, $logger);

        $this->model = new VendorRegistrationModel();

        $this->module_title = "Procurement";
        $this->submodule_title = "Vendor Registration";
        $this->controller_path = "procurement/vendor_registration";
        $this->controller_path_url = base_url($this->controller_path);
        $this->hasSupportingFiles = false;

        $this->view_data = array(
            "module_title" => $this->module_title,
            "submodule_title" => $this->submodule_title,
            "controller_path" => $this->controller_path,
            "controller_path_url" => $this->controller_path_url,
            "hasSupportingFiles" => $this->hasSupportingFiles,
        );
    }

    public function list()
    {
        $this->view_data["title"] = $this->submodule_title;
        $this->view_data["rows"] = $this->model->readAll(session()->get("id"));
        echo view("{$this->controller_path}_read", $this->view_data);
    }

    public function create()
    {
        $formArrayObject = $this->getFormObject();
        $formHtml = $this->getFormHTML($formArrayObject);

        $this->view_data["form"] = $formHtml;
        $this->view_data["formArrayObject"] = $formArrayObject;

        $this->view_data["title"] = "Create New {$this->submodule_title}";
        echo view("_base/create", $this->view_data);
    }

    public function edit($id)
    {
        $formArrayObject = $this->getFormObject();
        $formHtml = $this->getFormHTML($formArrayObject);

        $this->view_data["form"] = $formHtml;
        $this->view_data["formArrayObject"] = $formArrayObject;

        $this->view_data["title"] = "Edit {$this->submodule_title}";
        $this->view_data["row"] = $this->model->find($id);
        echo view("_base/edit", $this->view_data);
    }

}
