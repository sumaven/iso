<?php namespace App\Controllers;

use App\Controllers\BaseController;

class Business_process extends BaseController
{

    private $module_title;
    private $submodule_title;
    private $controller_path;
    private $controller_path_url;

	public function __construct(){
		parent::__construct();

        if ($this->uri->segment(3) != "html") {
            if (!user_access_check()) {
                exit();
            }
        }
    }

    public function index()
    {
        $data = array(
            "title"         => lang("nav_business_process")
        );
        $this->load->view('business_process_view', $data);
    }
}
