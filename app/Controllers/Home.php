<?php namespace App\Controllers;

use App\Models\CalendarOfEventModel;
use App\Models\HR\EmployeeModel;
use App\Models\InternalAudit\InternalAuditReportModel;
use App\Models\InternalAudit\NonConformanceModel;
use App\Models\LoginModel;
use App\Models\Planning\ObjectiveEvaluationModel;
use App\Models\Smk3\ChecklistAuditEksternalModel;
use App\Models\SustainablePalmoil\ISPOModel;
use App\Models\SustainablePalmoil\RSPOModel;
use App\Models\TreatmentDocument\ProcedureModel;
use App\Models\TreatmentDocument\WorkInstructionModel;
use CodeIgniter\Config\Services;
use CodeIgniter\HTTP\RequestInterface;
use CodeIgniter\HTTP\ResponseInterface;
use Psr\Log\LoggerInterface;

class Home extends BaseController
{

    public function initController(RequestInterface $request, ResponseInterface $response, LoggerInterface $logger)
    {
        // Do Not Edit This Line
        parent::initController($request, $response, $logger);
        $this->model = new LoginModel();
        $this->EmployeeModel = new EmployeeModel();
    }

    public function index()
    {
        if (session()->get('logged_in') == TRUE) {
            $this->view_data["title"] = "General Dashboard";
            $this->view_data["calendarOfEvents"] = (new CalendarOfEventModel())->findAll();
            $this->view_data["calendarOfEventCount"] = (new CalendarOfEventModel())->countAll();
            $this->view_data["internalAuditReportCount"] = (new InternalAuditReportModel())->countAll();
            $this->view_data["nonConformanceCount"] = (new NonConformanceModel())->countAll();
            $this->view_data["wiCount"] = (new WorkInstructionModel())->countAll();
            $this->view_data["sopCount"] = (new ProcedureModel())->countAll();

            // ISPO, RSPO, SMK3 Percentages
            $this->view_data["percentages_ispo"] = (new ISPOModel())->getPercentages();
            $this->view_data["percentages_rspo"] = (new RSPOModel())->getPercentages();
            $this->view_data["percentages_smk3"] = (new ChecklistAuditEksternalModel())->getPercentages();

            $objectiveEvaluationModel = new ObjectiveEvaluationModel();
            $objectiveEvaluationList = $objectiveEvaluationModel->getPercentageByDeptLocation();
            foreach ($objectiveEvaluationList as $item){
                $unformatted = $item->percentage;
                $formatted = str_replace("%", "", $unformatted);
                $formatted = trim($formatted);
                $formatted = str_replace(",", ".", $formatted);
                $item->percentage = $formatted;
            }

            $this->view_data["objectiveEvaluationList"] = $objectiveEvaluationList;

            echo view('index', $this->view_data);
        } else {
            return redirect()->to(base_url("login"));
        }
    }

    public function login()
    {
        if (session()->get("logged_in") == TRUE) {
            return redirect()->to(base_url());
        }

        echo view('login_view');
    }

    public function process_auth()
    {

        $email = Services::request()->getPost("email");
        $password = md5(Services::request()->getPost("password"));

        $user = $this->model->authenticate($email, $password);

        if (isset($user) > 0) {

            $email = $user->user_email;
            $employee = $this->EmployeeModel->find($user->employee_id);

            $sesdata = array(
                'email' => $email,
                'id' => $user->employee_id,
                'first_name' => $employee->first_name,
                'last_name' => $employee->last_name,
                'logged_in' => TRUE
            );

            session()->set($sesdata);

            header("refresh:1; url=" . base_url());
            echo "Successfully logged in ";
        } else {
            header("refresh:2; url=" . base_url("login"));
            echo 'Username or Password is Wrong';
        }
    }

    public function logout()
    {
        session()->destroy();
        return redirect()->to(base_url());
    }

}
