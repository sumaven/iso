<?php namespace App\Controllers\Smk3\Checklist_audit_eksternal;

use App\Controllers\BaseController;
use App\FormHtmls\BasicFormHtml;
use App\FormObjects\planning\Regulation_register_form;
use App\Models\Planning\RegulationRegisterModel;
use App\Models\Smk3\ChecklistAuditEksternalModel;
use CodeIgniter\HTTP\RequestInterface;
use CodeIgniter\HTTP\ResponseInterface;
use Psr\Log\LoggerInterface;

class Sub_main extends BaseController
{
    use Regulation_register_form;
    use BasicFormHtml;

    public function initController(RequestInterface $request, ResponseInterface $response, LoggerInterface $logger)
    {
        // Do Not Edit This Line
        parent::initController($request, $response, $logger);

        $this->model = new ChecklistAuditEksternalModel();

        $this->module_title = "Checklist Audit Eksternal";
        $this->submodule_title = "";
        $this->controller_path = "smk3/checklist_audit_eksternal/sub_main";
        $this->controller_path_url = base_url($this->controller_path);
        $this->hasSupportingFiles = true;

        $this->view_data = array(
            "module_title" => $this->module_title,
            "submodule_title" => $this->submodule_title,
            "controller_path" => $this->controller_path,
            "controller_path_url" => $this->controller_path_url,
            "hasSupportingFiles" => $this->hasSupportingFiles,
        );
    }

    public function list()
    {
        $this->view_data["title"] = $this->submodule_title;
        $this->view_data["rows"] = $this->model->readAll(session()->get("id"));

        helper("smk3/checklist_audit_eksternal_submain");
        $mains = get_submain();
        $main = $_GET['main'];

        if (empty($_GET['sub_main'])) {
            $sub_main_list = $mains[$main];
            $this->view_data["submain_list"] = $sub_main_list;
        } else {
            $sub_main = $_GET['sub_main'];
            $sub_main_tree = explode("-", $sub_main);
            $sub_main_list = $mains[$main];

            $countdown = count($sub_main_tree);
            $counttop = 0;
            while ($countdown > 0){
                $sub_main_list = $sub_main_list[$sub_main_tree[$counttop]];
                $sub_main_list = $sub_main_list["sub"];

                $countdown--;
                $counttop++;
            }

            $this->view_data["submain_list"] = $sub_main_list;
        }

        $this->view_data["submain_percentages"] = $this->model->getSubmainPercentages($main);

        echo view("{$this->controller_path}_read", $this->view_data);
    }

}
