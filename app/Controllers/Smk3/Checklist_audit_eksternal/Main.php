<?php namespace App\Controllers\Smk3\Checklist_audit_eksternal;

use App\Controllers\BaseController;
use App\FormHtmls\BasicFormHtml;
use App\FormObjects\planning\Regulation_register_form;
use App\Models\Smk3\ChecklistAuditEksternalModel;
use CodeIgniter\HTTP\RequestInterface;
use CodeIgniter\HTTP\ResponseInterface;
use Psr\Log\LoggerInterface;

class Main extends BaseController
{
    use Regulation_register_form;
    use BasicFormHtml;

    public function initController(RequestInterface $request, ResponseInterface $response, LoggerInterface $logger)
    {
        // Do Not Edit This Line
        parent::initController($request, $response, $logger);

        $this->model = new ChecklistAuditEksternalModel();

        $this->module_title = "Checklist Audit Eksternal";
        $this->submodule_title = "";
        $this->controller_path = "smk3/checklist_audit_eksternal/main";
        $this->controller_path_url = base_url($this->controller_path);
        $this->hasSupportingFiles = true;

        $this->view_data = array(
            "module_title" => $this->module_title,
            "submodule_title" => $this->submodule_title,
            "controller_path" => $this->controller_path,
            "controller_path_url" => $this->controller_path_url,
            "hasSupportingFiles" => $this->hasSupportingFiles,
        );
    }

    public function list()
    {
        $this->view_data["title"] = $this->submodule_title;
        $this->view_data["percentages"] = $this->model->getPercentages();
        echo view("{$this->controller_path}_read", $this->view_data);
    }

}
