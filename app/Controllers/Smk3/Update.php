<?php namespace App\Controllers\Smk3;

use App\Controllers\BaseController;
use App\FormHtmls\BasicFormHtml;
use App\FormObjects\smk3\Smk3_form;
use App\Models\Smk3\ChecklistAuditEksternalModel;
use CodeIgniter\HTTP\RequestInterface;
use CodeIgniter\HTTP\ResponseInterface;
use Psr\Log\LoggerInterface;

class Update extends BaseController
{
    use Smk3_form;
    use BasicFormHtml;

    public function initController(RequestInterface $request, ResponseInterface $response, LoggerInterface $logger)
    {
        // Do Not Edit This Line
        parent::initController($request, $response, $logger);

        $this->model = new ChecklistAuditEksternalModel();

        $this->module_title = "Input Panduan";
        $this->submodule_title = "Input Panduan SMK3";
        $this->controller_path = "smk3/update";
        $this->controller_path_url = base_url($this->controller_path);
        $this->hasSupportingFiles = false;

        $this->view_data = array(
            "module_title" => $this->module_title,
            "submodule_title" => $this->submodule_title,
            "controller_path" => $this->controller_path,
            "controller_path_url" => $this->controller_path_url,
            "hasSupportingFiles" => $this->hasSupportingFiles,
        );
    }

    public function edit($id)
    {
        $formArrayObject = $this->getFormObject();
        $formHtml = $this->getFormHTML($formArrayObject);

        $this->view_data["form"] = $formHtml;
        $this->view_data["formArrayObject"] = $formArrayObject;

        $this->view_data["title"] = "Edit {$this->submodule_title}";
        $this->view_data["row"] = $this->model->find($id);
        echo view("{$this->controller_path}_edit", $this->view_data);
    }

}
