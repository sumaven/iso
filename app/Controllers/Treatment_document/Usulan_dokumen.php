<?php namespace App\Controllers\Treatment_document;

use App\Controllers\BaseController;
use App\FormHtmls\BasicFormHtml;
use App\FormObjects\treatment_document\Usulan_dokumen_form;
use App\Models\TreatmentDocument\UsulanDokumenModel;
use CodeIgniter\HTTP\RequestInterface;
use CodeIgniter\HTTP\ResponseInterface;
use Psr\Log\LoggerInterface;

class Usulan_dokumen extends BaseController
{
    use Usulan_dokumen_form;
    use BasicFormHtml;

    public function initController(RequestInterface $request, ResponseInterface $response, LoggerInterface $logger)
    {
        // Do Not Edit This Line
        parent::initController($request, $response, $logger);

        $this->model = new UsulanDokumenModel();

        $this->module_title = "Treatment Document";
        $this->submodule_title = "Proposed Document";
        $this->controller_path = "treatment_document/usulan_dokumen";
        $this->controller_path_url = base_url($this->controller_path);
        $this->hasSupportingFiles = true;

        $this->view_data = array(
            "module_title" => $this->module_title,
            "submodule_title" => $this->submodule_title,
            "controller_path" => $this->controller_path,
            "controller_path_url" => $this->controller_path_url,
            "hasSupportingFiles" => $this->hasSupportingFiles,
        );
    }

    public function list()
    {
        $this->view_data["title"] = $this->submodule_title;
        $this->view_data["rows"] = $this->model->readAll(session()->get("id"));
        echo view("{$this->controller_path}_read", $this->view_data);
    }

    public function create()
    {
        $formArrayObject = $this->getFormObject();
        $formHtml = $this->getFormHTML($formArrayObject);

        $this->view_data["form"] = $formHtml;
        $this->view_data["formArrayObject"] = $formArrayObject;

        $this->view_data["title"] = "Create New {$this->submodule_title}";
        echo view("_base/create", $this->view_data);
    }

    public function edit($id)
    {
        $formArrayObject = $this->getFormObject();
        $formHtml = $this->getFormHTML($formArrayObject);

        $this->view_data["form"] = $formHtml;
        $this->view_data["formArrayObject"] = $formArrayObject;

        $this->view_data["title"] = "Edit {$this->submodule_title}";
        $this->view_data["row"] = $this->model->find($id);

        $items = array();
        $items["treatment_usulan_dokumen_items"] = $this->model->getItems("treatment_usulan_dokumen_items", $id);
        $this->view_data["items_list"] = $items;
        echo view("_base/edit", $this->view_data);
    }

}
