<?php namespace App\Controllers\Treatment_document;

use App\Controllers\BaseTrackingController;
use App\FormHtmls\BasicFormHtml;
use App\FormObjects\treatment_document\Procedure_form;
use App\Libraries\Tracking;
use App\Models\HR\DepartmentModel;
use App\Models\TreatmentDocument\ProcedureModel;
use CodeIgniter\HTTP\RequestInterface;
use CodeIgniter\HTTP\ResponseInterface;
use Psr\Log\LoggerInterface;

class Procedure extends BaseTrackingController
{
    protected $step_type = 2;

    use Procedure_form;
    use BasicFormHtml;

    public function initController(RequestInterface $request, ResponseInterface $response, LoggerInterface $logger)
    {
        // Do Not Edit This Line
        parent::initController($request, $response, $logger);

        $this->tracking = new Tracking();
        $this->model = new ProcedureModel();
        $this->DepartmentModel = new DepartmentModel();

        $this->module_title = "Treatment Document";
        $this->submodule_title = "Procedure";
        $this->controller_path = "treatment_document/procedure";
        $this->controller_path_url = base_url($this->controller_path);
        $this->hasSupportingFiles = true;

        $this->view_data = array(
            "module_title" => $this->module_title,
            "submodule_title" => $this->submodule_title,
            "controller_path" => $this->controller_path,
            "controller_path_url" => $this->controller_path_url,
            "hasSupportingFiles" => $this->hasSupportingFiles,
        );
    }

    public function list()
    {
        $this->view_data["title"] = $this->submodule_title;
        $this->view_data["rows"] = $this->model->readAll(session()->get("id"));
        echo view("{$this->controller_path}_read", $this->view_data);
    }

    public function create()
    {
        $formArrayObject = $this->getFormObject();
        $formHtml = $this->getFormHTML($formArrayObject);

        $this->view_data["form"] = $formHtml;
        $this->view_data["formArrayObject"] = $formArrayObject;

        $this->view_data["title"] = "Create New {$this->submodule_title}";
        echo view("_base/tracking_create", $this->view_data);
    }

    public function edit($id)
    {
        $formArrayObject = $this->getFormObject();
        $formHtml = $this->getFormHTML($formArrayObject);

        $this->view_data["form"] = $formHtml;
        $this->view_data["formArrayObject"] = $formArrayObject;

        $this->view_data["title"] = "Edit {$this->submodule_title}";
        $this->view_data["row"] = $this->model->read($id);
        echo view("_base/tracking_edit", $this->view_data);
    }

}


//    public function distribution($id)
//    {
//        $data = array(
//            "title" => "Treatment Document",
//            "row" => $this->model->dist($id)->row(),
//            "module_title" => $this->module_title,
//            "submodule_title" => $this->submodule_title,
//            "controller_path_url" => $this->controller_path_url,
//        );
//        echo view('treatment_document/distribusi_read', $data);
//    }
//
//    public function list_dist()
//    {
//        $id = $this->tracking->dpt(session()->get('id'));
//        $data = array(
//            "title" => "Distribusi Document",
//            "rows" => $this->model->readDist($id)->result(),
//            "module_title" => $this->module_title,
//            "submodule_title" => $this->submodule_title,
//            "controller_path_url" => $this->controller_path_url,
//        );
//        echo view('treatment_document/procedure_dist', $data);
//    }
