<?php namespace App\Models\_base;

use CodeIgniter\Model;

class BaseTrackingModel extends Model
{
    protected $step_type = -1;

    // Ini khusus untuk modul yang memiliki tracking / flow
//    public function readAll($id)
//    {
//        $arrayResultObject = $this->db->query("SELECT tp.* FROM {$this->table} tp
//        							JOIN tracking t ON tp.step = t.step AND tp.id = t.refid
//    									WHERE t.type = $this->step_type AND t.id_employee = $id")->getResultObject();
//        return $arrayResultObject;
//    }

    public function readAll($id)
    {
        $select_for_staff_viewable = $this->db->query("SELECT t.id_employee, tp.*, tp.step as actual_step, t.step as this_step 
                                    FROM {$this->table} tp 
                                    JOIN tracking t ON tp.id = t.refid
                                    WHERE t.type = {$this->step_type} AND t.step = 0 AND t.id_employee = $id");

        $select = $this->db->query("SELECT t.id_employee, tp.*, tp.step as actual_step, t.step as this_step 
                                    FROM {$this->table} tp 
                                    JOIN tracking t ON tp.id = t.refid AND tp.step = t.step
                                    WHERE t.type = {$this->step_type} AND (t.id_employee = $id)");

        if (count($select->getResultObject()) > 0){
            return $select->getResultObject();
        } else {
            return $select_for_staff_viewable->getResultObject();
        }

        // return array_merge($select->getResultObject(), $select_for_staff_viewable->getResultObject());
    }

    // Ini khusus untuk modul yang memiliki tracking / flow
    public function read($id)
    {
        $result = $this->db->query("SELECT tp.*, t.id_employee FROM {$this->table} tp
        							JOIN tracking t ON tp.step = t.step AND tp.id = t.refid
									WHERE t.type = $this->step_type  AND tp.id = " . $id)->getRow();
        return $result;
    }

    function remove($id)
    {
        $this->where("id", $id);
        $isSuccess = $this->delete();

        if ($isSuccess) {
            $builder = $this->db->table("tracking");
            $builder->where([
                "type" => $this->step_type,
                "refid" => $id
            ]);
            $isSuccess = $builder->delete();
        }

        if ($isSuccess) {
            $builder = $this->db->table("history");
            $builder->where([
                "type" => $this->step_type,
                "refid" => $id
            ]);
            $isSuccess = $builder->delete();
        }

        return $isSuccess;
    }

    public function insertTracking($data)
    {
        $data = array(
            "refid" => $data["refid"],
            "type" => $data["type"],
            "step" => $data["step"],
            "id_employee" => $data["id_employee"],
        );

        $builder = $this->db->table("tracking");
        $builder->insert($data);
        return $this->db->insertID();
    }

    public function insertKomentar($data)
    {
        $data = array(
            "refid" => $data["refid"],
            "type" => $data["type"],
            "id_employee" => $data["id_employee"],
            "komentar" => $data["komentar"],
        );

        $builder = $this->db->table("history");
        $builder->insert($data);
        return $this->db->insertID();
    }

    public function approve($id)
    {
        $row = $this->db->query("SELECT t.step
					FROM {$this->table} twi
					JOIN tracking t ON twi.id = t.refid AND twi.step = t.step
					WHERE t.type = $this->step_type AND twi.id =  " . $id)->getRow();

        $data = array(
            "step" => $row->step + 1,
        );

        if ($data["step"] == 2) {
            $data["approval_status"] = "Menunggu persetujuan VP";
        } else if ($data["step"] == 3) {
            $data["approval_status"] = "Disetujui";
        } else {
            $data["approval_status"] = "Menunggu persetujuan";
        }

        return $this->update($id, $data);
    }

    public function cancel_approve($id)
    {
        $row = $this->db->query("SELECT t.step
					FROM {$this->table} twi
					JOIN tracking t ON twi.id = t.refid AND twi.step = t.step
					WHERE t.type = $this->step_type AND twi.id =  " . $id)->getRow();

        $data = array(
            "step" => $row->step - 1,
        );

        if ($data["step"] == 0) {
            $data["approval_status"] = "Dibatalkan oleh MGR";
        } else if ($data["step"] == 1) {
            $data["approval_status"] = "Dibatalkan oleh VP";
        } else {
            $data["approval_status"] = "Dibatalkan";
        }

        return $this->update($id, $data);
    }
}