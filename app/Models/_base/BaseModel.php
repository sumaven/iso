<?php namespace App\Models\_base;

use CodeIgniter\Model;

class BaseModel extends Model
{

    public function readAll($id)
    {
        $select = $this->db->query("SELECT * FROM {$this->table}");
        //WHERE (t.id_employee = $id)
        return $select->getResultObject();
    }
}