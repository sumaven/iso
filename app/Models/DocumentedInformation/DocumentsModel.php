<?php namespace App\Models\DocumentedInformation;

use App\Models\_base\BaseTrackingModel;

class DocumentsModel extends BaseTrackingModel
{
    protected $table = "documented_documents";
    protected $primaryKey = "id";
    protected $returnType = "object";
    protected $allowedFields = ["management_system_type", "department",
        "document_name", "revision_number", "document_category", "document_type",
        "approve_author", "document", "next_revision_date",
        "file_url", "step", "approval_status"];

    protected $step_type = 14;

}

?>
