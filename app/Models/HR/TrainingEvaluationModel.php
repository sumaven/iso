<?php namespace App\Models\HR;

use CodeIgniter\Model;

class TrainingEvaluationModel extends Model
{



    function insert($data_pass)
    {

        $datas = $data_pass['datas'];
        $data = $data_pass['data'];

        $data = array(
            "id" => NULL,
            "training_topic" => $data['training_topic'],
            "training_type" => $data['training_type'],
            "proposed_trainer" => $data['proposed_trainer'],
            "planned_date" => $data['planned_date'],
            "department" => $data['department'],
            "remarks" => $data['remarks'],
            "created_at" => "",
        );

        $this->db->insert("hr_training_identification", $data);
        $inserted_id = $this->db->insert_id();

        $data = array(
            "id" => NULL,
            "training_id" => $inserted_id,
            "remarks" => "",
            "actual_date" => "",
            "actual_trainer" => "",
            "proposed_evaluation_date" => "",
            "proposed_evaluation_date" => ""
        );

        $this->db->insert("hr_training_attendance", $data);

        foreach ($datas as $data) {

            $array = array(
                "id" => NULL,
                "training_id" => $inserted_id,
                "no" => $data['no'],
                "attendee_name" => $data['attendee_name'],
                "pre_training_competence" => $data['pre_training_competence'],
                "post_training_competence" => 0,
                "attended" => "false",
                "remarks" => ""
            );

            $this->db->insert("hr_trainee_items", $array);
        }

        return true;
    }

    function read()
    {
        $select = $this->db->query("SELECT b.id, b.training_id, a.training_topic, a.training_type, a.proposed_trainer, a.planned_date, a.department,
            b.remarks, a.created_at, b.actual_evaluation_date, b.actual_evaluation_methodology, c.actual_trainer, c.actual_date
            FROM hr_training_identification AS a
            JOIN hr_training_evaluation AS b ON a.id = b.training_id
            JOIN hr_training_attendance AS c ON a.id = c.training_id");
        return $select;
    }

    function readItem($id)
    {
        $select = $this->db->query("SELECT *  FROM
            (SELECT b.training_id, a.training_topic, a.training_type, a.proposed_trainer, a.planned_date, a.department,
                b.remarks, a.created_at, b.actual_evaluation_date, b.actual_evaluation_methodology, c.actual_trainer, c.actual_date
            FROM hr_training_identification AS a
            JOIN hr_training_evaluation AS b ON a.id = b.training_id
            JOIN hr_training_attendance AS c ON a.id = c.training_id) AS D WHERE D.training_id=$id");
        // $select = $this->db->query("SELECT * EXCLUDE b.id FROM hr_training_identification AS a JOIN hr_training_attendance AS b ON a.id = b.training_id");
        return $select;
    }

    function readItems($id)
    {
        // $query = $this->db->query("SELECT * FROM hr_trainee_items where items_id='$id'");
        $query = $this->db->query("SELECT b.training_topic, b.training_type, b.proposed_trainer,
            b.planned_date, b.department, b.remarks, b.created_at,
            a.training_id, a.no, a.attendee_name, a.pre_training_competence, a.post_training_competence, a.remarks
            FROM hr_trainee_items AS a
            JOIN hr_training_identification AS b
            ON a.training_id = b.id
            WHERE b.id = $id");
        // $row = $query->row();
        return $query;
    }

    function edit($data_pass)
    {
        $id = $data_pass['id'];
        $datas = $data_pass['datas'];
        $data = $data_pass['data'];

        $this->db->where('training_id', $id);
        $this->db->update('hr_training_evaluation', $data);

        foreach ($datas as $data) {
            $array = array(
                "post_training_competence" => $data['post_training_competence']
            );

            $this->db->where(array(
                'training_id' => $id,
                'no' => $data['no'],
            ));

            $this->db->update('hr_trainee_items', $array);
        }

        return true;
    }

    function delete($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('hr_training_identification');

        $this->db->where('training_id', $id);
        $this->db->delete('hr_training_attendance');

        $this->db->where('training_id', $id);
        $result = $this->db->delete('hr_trainee_items');
        return $result;
    }

}

?>
