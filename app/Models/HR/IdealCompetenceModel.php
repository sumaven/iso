<?php namespace App\Models\HR;

use App\Models\_base\BaseModel;

class IdealCompetenceModel extends BaseModel
{

    protected $table = "hr_ideal_competence";
    protected $primaryKey = "id";
    protected $returnType = "object";
    protected $allowedFields = ["designation"];

    public function getItems($table_name, $ref_id)
    {
        return $this->db->query("SELECT * FROM $table_name WHERE ref_id=$ref_id")->getResultArray();
    }

    public function insertItems($data, $table_name)
    {
        $this->db->table($table_name)->insert($data);
    }

    public function deleteItems($ref_id, $table_name)
    {
        return $this->db->table($table_name)->delete(array("ref_id" => $ref_id));
    }

//    function delete($id){
//        $this->db->where('id', $id);
//        $this->db->delete('hr_ideal_competence');
//
//        $this->db->where('items_id', $id);
//        $result = $this->db->delete('hr_ideal_competence_items');
//
//        $this->db->where('items_id', $id);
//        $result = $this->db->delete('hr_competence_evaluation_items');
//        return $result;
//    }

}

?>
