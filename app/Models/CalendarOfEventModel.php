<?php namespace App\Models;

use App\Models\_base\BaseModel;

class CalendarOfEventModel extends BaseModel
{
    protected $table = "calendar_of_event";
    protected $primaryKey = "id";
    protected $returnType = 'object';
    protected $allowedFields = ["agenda", "waktu_pelaksanaan", "file_url", "approval_status"];

}

?>
