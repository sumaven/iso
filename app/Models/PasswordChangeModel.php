<?php namespace App\Models;

use CodeIgniter\Model;

class PasswordChangeModel extends Model{

    function process($email, $oldPassword, $newPassword){
        $this->db->where('user_email',$email);
        $this->db->where('user_password',$oldPassword);
        $result = $this->db->get('tbl_users',1);

        if ($result->num_rows() > 0){

            $data = array(
                'user_password' 	=> $newPassword,
            );

            $this->db->where('user_email', $email);
            return $this->db->update('tbl_users', $data);
        } else {
            return false;
        }
    }

}
