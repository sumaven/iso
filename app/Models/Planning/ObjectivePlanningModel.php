<?php namespace App\Models\Planning;

use App\Models\_base\BaseModel;
use App\Models\_base\BaseTrackingModel;
use CodeIgniter\Model;

class ObjectivePlanningModel extends BaseTrackingModel
{

    protected $table = "planning_objective_planning";
    protected $primaryKey = "id";
    protected $returnType = "object";
    protected $allowedFields = ["dept_location", "performance_indicator", "process_name",
        "monitoring_frequency", "objective", "resources", "objective_plan", "remark",
        "approve_author", "monitoring_methodology", "risk_description", "risk_impact",
        "risk_category", "risk_possibility",
        "file_url", "step", "approval_status"];

    protected $step_type = 30;

    public function getItems($table_name, $ref_id)
    {
        return $this->db->query("SELECT document_type, document_name FROM $table_name WHERE ref_id=$ref_id")->getResultArray();
    }

    public function insertItems($data, $table_name)
    {
        $this->db->table($table_name)->insert($data);
    }

    public function deleteItems($ref_id, $table_name)
    {
        return $this->db->table($table_name)->delete(array("ref_id" => $ref_id));
    }

}

//    "dept_location",
//    "performance_indicator",
//    "process_name",
//    "monitoring_frequency",
//    "objective",
//    "resources",
//    "objective_plan",
//    "remark",
//    "approve_author",
//    "monitoring_methodology",
//    "risk_description",
//    "risk_impact",
//    "risk_category",
//    "risk_possibility",

?>
