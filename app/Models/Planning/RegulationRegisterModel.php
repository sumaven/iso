<?php namespace App\Models\Planning;

use App\Models\_base\BaseModel;
use CodeIgniter\Model;

class RegulationRegisterModel extends BaseModel
{

    protected $table = "planning_regulation_register";
    protected $primaryKey = "id";
    protected $returnType = "object";
    protected $allowedFields = ["type_list", "release_date", "regulation_title",
        "institution_owner", "regulation_type", "file_url"];

}

?>
