<?php namespace App\Models\Planning;

use App\Models\_base\BaseTrackingModel;

class ObjectiveEvaluationModel extends BaseTrackingModel
{

    protected $table = "objective_planning_objective_evaluation";
    protected $primaryKey = "id";
    protected $returnType = "object";
    protected $allowedFields = ["dept_location", "process_name", "objective",
        "step_action", "parameter", "evidance", "level_of_achievement",
        "percentage", "obstacle_factor", "approve_author", "remark",
        "improvement_suggestion", "monitoring_frequency",
        "file_url", "step", "approval_status"];

    protected $step_type = 31;

    public function getPercentageByDeptLocation(){
        return $this->db->query("SELECT dept_location, percentage FROM {$this->table}")->getResult();
    }

    public function getItems($table_name, $ref_id)
    {
        return $this->db->query("SELECT document_type, document_name FROM $table_name WHERE ref_id=$ref_id")->getResultArray();
    }

    public function insertItems($data, $table_name)
    {
        $this->db->table($table_name)->insert($data);
    }

    public function deleteItems($ref_id, $table_name)
    {
        return $this->db->table($table_name)->delete(array("ref_id" => $ref_id));
    }

}

//    "dept_location",
//    "performance_indicator",
//    "process_name",
//    "monitoring_frequency",
//    "objective",
//    "resources",
//    "objective_plan",
//    "remark",
//    "approve_author",
//    "monitoring_methodology",
//    "risk_description",
//    "risk_impact",
//    "risk_category",
//    "risk_possibility",

?>
