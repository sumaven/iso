<?php namespace App\Models\Planning;

use App\Models\_base\BaseTrackingModel;

class RegulationEvaluationModel extends BaseTrackingModel
{

    protected $table = "planning_regulation_evaluation";
    protected $primaryKey = "id";
    protected $returnType = "object";
    protected $allowedFields = [
        "regulation_title",
        "clause",
        "aspect",
        "fulfillment",
        "evidance",
        "remark",
        "file_url", "step", "approval_status"
    ];
    protected $step_type = 32;

    public function readAll($id)
    {
        return $this->db->query("SELECT *  FROM {$this->table} ORDER BY regulation_title ASC, clause ASC")->getResultObject();
    }

}

?>
