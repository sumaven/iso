<?php namespace App\Models\Planning;

use App\Models\_base\BaseTrackingModel;

class HiaroEnvironmentAspectModel extends BaseTrackingModel
{

    protected $table = "planning_hiaro_environment_aspect";
    protected $primaryKey = "id";
    protected $returnType = "object";
    protected $allowedFields = ["dept_location", "qhse_potential_issue_description", "process_name",
        "operational_condition", "activity", "risk_opportunity", "qhse_risk", "qhse_potential_impact", "ro_impact",
        "category", "ro_impact_company", "qhse_issue_category", "interested_parties", "compliance_description",
        "pre_consequences_benefit_initial_rating", "post_consequences_benefit_initial_rating",
        "pre_likelihood_initial_rating", "post_likelihood_initial_rating", "initial_risk_rating",
        "final_risk_rating", "existing_control", "additional_control", "remarks", "remarks_2", "approve_author",
        "file_url", "step", "approval_status"];


    protected $step_type = 28;


}

?>
