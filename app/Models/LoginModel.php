<?php namespace App\Models;

use CodeIgniter\Model;

class LoginModel extends Model {

    protected $table = "tbl_users";
    protected $primaryKey = "id";
    protected $returnType = 'object';
    protected $allowedFields = ["employee_id", "user_email", "user_password"];

    public function authenticate($email,$password){
        return $this->where("user_email", $email)
            ->where("user_password", $password)
            ->first();
    }

}
