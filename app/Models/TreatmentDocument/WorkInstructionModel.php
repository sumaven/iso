<?php namespace App\Models\TreatmentDocument;

use App\Models\_base\BaseTrackingModel;

class WorkInstructionModel extends BaseTrackingModel
{
    protected $table = "treatment_work_instruction";
    protected $primaryKey = "id";
    protected $returnType = "object";
    protected $allowedFields = ["header_title", "department", "revision", "date_issued", "scope",
        "goal", "work_instruction", "reference", "approval", "distribution",
        "file_url", "step", "approval_status"];

    protected $step_type = 1;

}

?>
