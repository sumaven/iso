<?php namespace App\Models\TreatmentDocument;

use App\Models\_base\BaseModel;

class UsulanDokumenModel extends BaseModel
{

    protected $table = "treatment_usulan_dokumen";
    protected $primaryKey = "id";
    protected $returnType = "object";
    protected $allowedFields = ["header_doc_no", "header_rev_no", "doc_no",
        "versi", "nama_pengusul", "nama_bagian", "judul_dokumen", "file_url"];

    public function getItems($table_name, $ref_id)
    {
        return $this->db->query("SELECT no_halaman, no_bagian, klausul_awal_1, klausul_awal_2, klausul_perubahan_1, klausul_perubahan_2 FROM $table_name WHERE ref_id=$ref_id")->getResultArray();
    }

    public function insertItems($data, $table_name)
    {
        $this->db->table($table_name)->insert($data);
    }

    public function deleteItems($ref_id, $table_name)
    {
        return $this->db->table($table_name)->delete(array("ref_id" => $ref_id));
    }

}

?>
