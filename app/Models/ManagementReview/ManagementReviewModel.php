<?php namespace App\Models\ManagementReview;

use App\Models\_base\BaseTrackingModel;

class ManagementReviewModel extends BaseTrackingModel
{
    protected $table = "internal_audit_management_review";
    protected $primaryKey = "id";
    protected $returnType = "object";
    protected $allowedFields = ["date_of_management_review", "approve_author", "attendee",
        "file_url", "step", "approval_status"];

    protected $step_type = 22;

    public function getItems($table_name, $ref_id)
    {
        return $this->db->query("SELECT agenda, responsibility, document_required FROM $table_name WHERE ref_id=$ref_id")->getResultArray();
    }

    public function insertItems($data, $table_name)
    {
        $this->db->table($table_name)->insert($data);
    }

    public function deleteItems($ref_id, $table_name)
    {
        return $this->db->table($table_name)->delete(array("ref_id" => $ref_id));
    }

}