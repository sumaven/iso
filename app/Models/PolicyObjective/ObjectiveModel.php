<?php namespace App\Models\PolicyObjective;

use App\Models\_base\BaseModel;

class ObjectiveModel extends BaseModel
{
    protected $table = "objective";
    protected $primaryKey = "id";
    protected $returnType = "object";
    protected $allowedFields = [
        "management_system_type", "department", "objective",
        "objective_start_date", "objective_end_date",
        "monitoring_frequency", "monitoring_methodology",
        "remarks", "approve_author", "access_control", "file_url"
    ];

}

?>
