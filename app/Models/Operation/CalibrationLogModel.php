<?php namespace App\Models\Operation;

use App\Models\_base\BaseTrackingModel;

class CalibrationLogModel extends BaseTrackingModel
{
    protected $table = "operation_calibration_log";
    protected $primaryKey = "id";
    protected $returnType = "object";
    protected $allowedFields = ["equipment_name", "equipment_owner", "equipment_location",
        "model_name", "identification_number", "asset_number", "date_of_last_calibration", "next_calibration_date",
        "file_url", "step", "approval_status"];

    protected $step_type = 18;

}
