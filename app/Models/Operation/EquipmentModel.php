<?php namespace App\Models\Operation;

use App\Models\_base\BaseTrackingModel;
use CodeIgniter\Model;

class EquipmentModel extends BaseTrackingModel
{
    protected $table = "operation_equipment";
    protected $primaryKey = "id";
    protected $returnType = "object";
    protected $allowedFields = ["equipment_name", "equipment_owner", "equipment_location",
        "model_name", "identification_number", "asset_number", "date_of_purchase", "status",
        "file_url", "step", "approval_status"];

    protected $step_type = 19;

}