<?php namespace App\Models\Procurement;

use App\Models\_base\BaseTrackingModel;

class PurchaseOrderModel extends BaseTrackingModel
{
    protected $table = "procurement_purchase_order";
    protected $primaryKey = "id";
    protected $returnType = "object";
    protected $allowedFields = ["file_url", "date", "po_number",
        "npwp", "alamat", "quantity", "unit", 'description',
        "price", "delivery_date", "term_of_payment",
        "delivery_location", "step", "approval_status"];

    protected $step_type = 5;

}
