<?php namespace App\Models\Procurement;

use App\Models\_base\BaseTrackingModel;

class VendorEvaluationModel extends BaseTrackingModel
{
    protected $table = "procurement_vendor_evaluation";
    protected $primaryKey = "id";
    protected $returnType = "object";
    protected $allowedFields = ["doc", "form_no", "rev", "company",
        "address", "phone", "fax", "product", "npwp", "tdp", "pkp",
        "tdr", "siup",
        "quality_score", "quality_evaluation", "quality_remarks",
        "k3_apd_score", "k3_apd_evaluation", "k3_apd_remarks",
        "quantity_score", "quantity_evaluation", "quantity_remarks",
        "delivery_score", "delivery_evaluation", "delivery_remarks",
        "respon_to_complains_score", "respon_to_complains_evaluation", "respon_to_complains_remarks",
        "accuracy_in_invoicing_score", "accuracy_in_invoicing_evaluation", "accuracy_in_invoicing_remarks",
        "respon_to_order_score", "respon_to_order_evaluation", "respon_to_order_remarks",
        "recommendation", "step", "approval_status"];

    protected $step_type = 6;

    // ada readItem
}
