<?php namespace App\Models\InternalAudit;

use App\Models\_base\BaseTrackingModel;

class AuditPlanningModel extends BaseTrackingModel
{
    protected $table = "internal_audit_audit_planning";
    protected $primaryKey = "id";
    protected $returnType = "object";
    protected $allowedFields = ["dept_location", "schedule_month", "auditor_name", "auditee_name",
        "location_audit", "approve_author", "file_url", "step", "approval_status"];

    protected $step_type = 24;

}