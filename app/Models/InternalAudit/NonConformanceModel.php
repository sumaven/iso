<?php namespace App\Models\InternalAudit;

use App\Models\_base\BaseTrackingModel;

class NonConformanceModel extends BaseTrackingModel
{
    protected $table = "internal_audit_non_conformance";
    protected $primaryKey = "id";
    protected $returnType = "object";
    protected $allowedFields = [
        "header_title", 'departemen', 'no', 'audit',
        'lokasi', 'tanggal', 'penjelasan_ketidaksesuaian',
        'tindakan_perbaikan', 'tanggal_penyelesaian', 'penjelasan_perbaikan',
        'tindakan_perbaikan_selesai', 'step', "approval_status"
    ];

    protected $step_type = 3;
}
