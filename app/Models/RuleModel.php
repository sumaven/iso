<?php namespace App\Models;

use CodeIgniter\Model;

class RuleModel extends Model {

	function insert($data_pass){

        $datas = $data_pass['datas'];

        $data = array(
                "id"                => NULL,
                "nama"    => $datas[0]['nama'],
        );

        $this->db->insert("rule", $data);
        $inserted_id = $this->db->insert_id();

        foreach ($datas as $data) {

            $array = array(
                    "id"                => NULL,
                    "rule_id"           => $inserted_id,
                    "no"                => $data['no'],
                    "rule_type"         => $data['rule_type'],
            );

            $this->db->insert("rule_items", $array);
        }

        return true;
    }

    function read(){
        $select = $this->db->query("SELECT * FROM rule");
        return $select;
    }

    function readItems($id){
        $query = $this->db->query("SELECT a.*, b.* FROM rule_items AS a JOIN rule AS b ON a.rule_id = b.id WHERE b.id = $id");
        return $query;
    }

    function edit($data_pass){
        $id = $data_pass['id'];
        $datas = $data_pass['datas'];
        $nama = $datas[0]['nama'];

        $data = array(
                "nama"    => $nama,
        );

        $this->db->where('id', $id);
        $this->db->update('rule', $data);


        foreach ($datas as $data) {
            $array = array(
                    "rule_type"   => $data['rule_type'],
            );

            $this->db->where(array(
                'rule_id'           => $id,
                'no'                => $data['no'],
            ));

            $this->db->update('rule_items', $array);
        }

        return true;
    }

    function delete($id){
        $this->db->where('id', $id);
        $this->db->delete('rule');

        $this->db->where('rule_id', $id);
        $result = $this->db->delete('rule_items');
        return $result;
    }

}
?>
