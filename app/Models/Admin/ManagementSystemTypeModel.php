<?php namespace App\Models\Admin;

use App\Models\_base\BaseModel;
use CodeIgniter\Model;

class ManagementSystemTypeModel extends BaseModel
{

    protected $table = "admin_management_system_type";
    protected $primaryKey = "id";
    protected $returnType = "object";
    protected $allowedFields = ["management_system_short_name", "management_system_full_name"];

}

?>
