<?php namespace App\Models\Context;

use App\Models\_base\BaseModel;

class IssueTypeModel extends BaseModel
{
    protected $table = "context_issue_type";
    protected $primaryKey = "id";
    protected $returnType = "object";
    protected $allowedFields = ["management_system_type", "issue_type", "scope", "stages_of_product", "created_at"];

//    function read(){
//
//        if (func_num_args() == 1){
//            $scope = func_get_arg(0);
//            $select = $this->db->query("SELECT * FROM context_issue_type where scope='$scope'");
//        } else {
//            $select = $this->db->query("SELECT * FROM context_issue_type");
//        }
//
//        return $select;
//    }

}

?>
