<?php namespace App\Models\Context;

use App\Models\_base\BaseModel;

class UnderstandingContextOrganizationModel extends BaseModel
{

    protected $table = "context_organization_understanding";
    protected $primaryKey = "id";
    protected $returnType = "object";
    protected $allowedFields = ["goals_vision_mission", "product_service", "approve_author",
        "swot_strength", "swot_weakness", "swot_opportunity", "swot_threat", "file_url"];

}

?>
