<?php namespace App\Models\Context;

use App\Models\_base\BaseTrackingModel;

class ExternalIssueModel extends BaseTrackingModel
{
    protected $table = "context_external_issue";
    protected $primaryKey = "id";
    protected $returnType = "object";
    protected $allowedFields = ["issue_type", "how_affect", "approve_author", "risk_opportunity", "action_plan",
        "risk_category", "risk_description", "risk_impact", "risk_possibility",
        "created_at", "file_url", "step", "approval_status"];

    protected $step_type = 9;

}

?>
