<?php namespace App\Models\RiskOpportunity;

use App\Models\_base\BaseModel;

class RiskOpportunityModel extends BaseModel
{
    protected $table = "risk_opportunity";
    protected $primaryKey = "id";
    protected $returnType = "object";
    protected $allowedFields = [
        "risk_opportunity_type", "management_system_type", "risk_opportunity_source",
        "risk_category", "risk_description", "risk_impact", "risk_probability",
        "department", "assigned_to", "treatment_type", "actions_proposed",
        "proposed_review_date", "proposed_completion_date",
        "next_revision_date", "is_opportunity", "approve_author", "created_at", "file_url"
    ];

    # RiskOpportunity CRUD
    # --------------------------------------------------------------------------------

//    function insertRiskOpportunity($data, $file)
//    {
//        $select = $this->db->query("SELECT * FROM risk_opportunity where management_system_type='{$data['management_system_type']}'");
//
//        if ($select->num_rows() != 0) {
//            return false;
//        }
//
//        if (!empty($file["file_upload"]['name'])) {
//            $file_upload = upload_file($file);
//
//            if ($file_upload['status'] == "success") {
//                // $data['upload_file'] = $file_upload['message'] . " " . $file_upload['filename'];
//                // $data_pass['file_url'] = base_url("assets/uploaded_files/") . $file_upload['filename'];
//                $file_url = base_url("assets/uploaded_files/") . $file_upload['filename'];
//            } else if ($file_upload['status'] == "failed") {
//                // $data['upload_file'] = $file_upload['message'] . " " . $file_upload['filename'];
//                return false;
//            }
//        }
//
//        $data = array(
//            "id" => NULL,
//            "risk_opportunity_type" => $data['risk_opportunity_type'],
//            "management_system_type" => $data['management_system_type'],
//            "risk_opportunity_source" => $data['risk_opportunity_source'],
//            "risk_category" => $data['risk_category'],
//            "risk_description" => $data['risk_description'],
//            "risk_impact" => $data['risk_impact'],
//            "risk_probability" => $data['risk_probability'],
//            "department" => $data['department'],
//            "assigned_to" => $data['assigned_to'],
//            "treatment_type" => $data['treatment_type'],
//            "actions_proposed" => $data['actions_proposed'],
//            "proposed_review_date" => $data['proposed_review_date'],
//            "proposed_completion_date" => $data['proposed_completion_date'],
//            "next_revision_date" => $data['next_revision_date'],
//            "is_opportunity" => $data['is_opportunity'],
//            "approve_author" => $data['approve_author'],
//            "created_at" => "",
//            "file_url" => empty($file_url) ? "" : $file_url
//        );
//
//        $this->db->insert("risk_opportunity", $data);
//
//        return true;
//    }
//
//    function readRiskOpportunity()
//    {
//        $select = $this->db->query("SELECT * FROM risk_opportunity");
//        return $select;
//    }
//
//    function editRiskOpportunity($data_pass, $file)
//    {
//
//        $row = $this->readRiskOpportunityItem($data_pass['id']);
//        if (!empty($file["file_upload"]['name'])) {
//            $file_upload = upload_file($file);
//
//            if ($file_upload['status'] == "success") {
//                delete_file_url($row->file_url);
//                $file_url = base_url("assets/uploaded_files/") . $file_upload['filename'];
//            } else if ($file_upload['status'] == "failed") {
//                return false;
//            }
//        }
//
//        $data = array(
//            'risk_opportunity_type' => $data_pass['risk_opportunity_type'],
//            'management_system_type' => $data_pass['management_system_type'],
//            'risk_opportunity_source' => $data_pass['risk_opportunity_source'],
//            'risk_category' => $data_pass['risk_category'],
//            'risk_description' => $data_pass['risk_description'],
//            'risk_impact' => $data_pass['risk_impact'],
//            'risk_probability' => $data_pass['risk_probability'],
//            'department' => $data_pass['department'],
//            'assigned_to' => $data_pass['assigned_to'],
//            'treatment_type' => $data_pass['treatment_type'],
//            'actions_proposed' => $data_pass['actions_proposed'],
//            'proposed_review_date' => $data_pass['proposed_review_date'],
//            'proposed_completion_date' => $data_pass['proposed_completion_date'],
//            "next_revision_date"  => $data_pass['next_revision_date'],
//            'is_opportunity' => $data_pass['is_opportunity'],
//            'approve_author' => $data_pass['approve_author'],
//            'created_at' => "",
//            "file_url" => empty($file_url) ? $row->file_url : $file_url
//        );
//
//        $this->db->where('id', $data_pass['id']);
//        return $this->db->update('risk_opportunity', $data);
//    }
//
//    function readRiskOpportunityItem($id)
//    {
//        $query = $this->db->query("SELECT * FROM risk_opportunity where id='$id'");
//        $row = $query->row();
//        return $row;
//    }
//
//    function deleteRiskOpportunity($id)
//    {
//
//        $row = $this->readRiskOpportunityItem($id);
//        if (!empty($row->file_url)) {
//            delete_file_url($row->file_url);
//        }
//
//        $this->db->where('id', $id);
//        $result = $this->db->delete('risk_opportunity');
//        return $result;
//    }

}

?>
