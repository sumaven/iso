<?php namespace App\Models\RiskOpportunity;

use App\Models\_base\BaseModel;

class RiskOpportunityCategoryModel extends BaseModel
{

    protected $table = "risk_opportunity_category";
    protected $primaryKey = "id";
    protected $returnType = "object";
    protected $allowedFields = ["risk_category", "remarks", "created_at"];

}

?>
