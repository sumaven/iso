<?php namespace App\Models\RiskOpportunity;

use App\Models\_base\BaseModel;

class RiskOpportunityTypeModel extends BaseModel
{
    protected $table = "risk_opportunity_type";
    protected $primaryKey = "id";
    protected $returnType = "object";
    protected $allowedFields = [
        "risk_type", "remarks", "created_at",
    ];

}

?>
