<?php namespace App\Libraries;

use App\Models\HR\EmployeeModel;

class Tracking
{
    private $EmployeeModel;
    private $db;

	 public function __construct()
    {
        $this->db = db_connect();
        $this->EmployeeModel = new EmployeeModel();
    }

	public function insertTracking($refid, $type, $step, $id_employee){
		$data = array(
                "refid"         => $refid,
                "type"          => $type,
                "step"    		=> $step,
                "id_employee"   => $id_employee,
           );

         return $data;
	}

	// Keterangan ID jabatan
	// 4 -> untuk staff
	// 2 -> itu manager
	// 3 -> itu vp
	// 1 -> itu mr untuk admin atau entah apa

	public function mgr($id){

		$row = $this->db->query("select * from hr_employee where id= ".$id)->getRow();
		$mgr = $this->db->query("SELECT id from hr_employee where jabatan = 2 and department = ".$row->department)->getRow();
        return $mgr->id;
	}

	public function vp($id){
		$row = $this->db->query("select * from hr_employee where id= ".$id)->getRow();
		$mgr = $this->db->query("SELECT id from hr_employee where jabatan = 3 and department = ".$row->department)->getRow();
        return $mgr->id;
	}

	public function mr($id){
		$row = $this->db->query("select * from hr_employee where id= ".$id)->getRow();
		$mgr = $this->db->query("SELECT id from hr_employee where jabatan = 1 and department = ".$row->department)->getRow();
        return $mgr->id;
	}

	public function dpt($id){
		$row = $this->db->query("select * from hr_employee where id= ".$id)->getRow();
		return $row->department;
	}

	public function loadDepartmen($id){
		return $this->EmployeeModel->getDepartment($id);
	}
}
