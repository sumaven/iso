<?php namespace App\Libraries;

class HtmlToPdf
{

    public function display_pdf($htmlUrl)
    {
        $data = [
            "url" => $htmlUrl,
            "apiKey" => getenv("HTML_TO_PDF_API_KEY"),
        ];

        $dataString = json_encode($data);

        $ch = curl_init("https://api.html2pdf.app/v1/generate");
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $dataString);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, [
            "Content-Type: application/json",
        ]);

        $response = curl_exec($ch);
        $err = curl_error($ch);

        curl_close($ch);

        if ($err) {
            echo "Error #:" . $err;
        } else {
            echo "<head><title>PDF Document</title></head>";
            header("Content-Type: application/pdf");
            header("Content-Disposition: inline; filename=exported.pdf");
            header("Content-Transfer-Encoding: binary");
            header("Accept-Ranges: bytes");
            echo $response;
        }

    }
}

/*
        $html2pdf = new pdflayer();
        //set the URL to convert
        $html2pdf->set_param(
            "document_url", $htmlUrl
        );

        //start the conversion
        $html2pdf->convert();
        $html2pdf->display_pdf();
 */
