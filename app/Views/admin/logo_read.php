<?php
  echo view('_partials/header');
?>

    <!-- Bootstrap DataTables -->
    <link rel="stylesheet" href="https://unpkg.com/bootstrap-table@1.15.3/dist/bootstrap-table.min.css">
    <!-- SweetAlert2 -->
    <link rel="stylesheet" href="<?=base_url("assets")?>/plugins/sweetalert2/sweetalert2.min.css">
    <!-- Toastr -->
    <link rel="stylesheet" href="<?=base_url("assets")?>/plugins/toastr/toastr.min.css">

<style>
  .btn-label {position: relative;left: -12px;display: inline-block;padding: 6px 12px;background: rgba(0,0,0,0.15);border-radius: 3px 0 0 3px;}
  .btn-labeled {padding-top: 0;padding-bottom: 0;}
  .btn { margin-bottom:10px; }
</style>

<?php
  echo view('_partials/navbar');
  echo view('_partials/sidebar');
?>


  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h5><?= $title ?></h5>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Inline Charts</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-12">

          <div class="card">
            <div class="card-header">
              <h3 class="card-title">Logo</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">


              <div id="toolbar">
                <div class="form-inline" role="form">
                  <div class="form-group">
                    <!-- <input name="search" class="form-control" type="text" placeholder="Search"> -->
                    <a href='create'>
                        <button type="button" class="btn btn-labeled btn-primary"><span class="btn-label"><i class="fa fa-plus"></i></span>Add new</button>
                    </a>
                  </div>
                  <!-- <button id="ok" type="submit" class="btn btn-primary">OK</button> -->
                </div>
              </div>

              <table
                id="table"
                data-pagination="true"
                data-toggle="table">
                <!-- data-url="https://examples.wenzhixin.net.cn/examples/bootstrap_table/data" -->
                <thead>
                <tr>
                <th style="width: 20px;text-align:center">No</th>
                  <th>Action</th>
                  <th>Logo</th>
                </tr>
                </thead>
                <tbody>

                                <?php
$count = 0;
foreach ($rows as $row){
    ++$count;
?>
                <?=
"
                <tr>
                  <td style='text-align:center'>$count</td>
                  <td>
                    <a onclick='deleteItem($row->id)' style='cursor: pointer; margin: 5px' title='Remove'><i class='fa fa-trash'></i></a>
                  </td>
                  <td><img src='{$row->url}'</td>
                </tr>
"
                ?>
<?php
}
?>
                </tbody>
              </table>


            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->


      <div class="modal fade" id="modal-default">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">Delete Data</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <p>Are you sure to delete item ?</p>
            </div>
            <div class="modal-footer justify-content-between">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              <a class="btn btn-danger btn-ok" data-dismiss="modal">Delete</a>
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->
    </section>
    <!-- /.content -->


  </div>
  <!-- /.content-wrapper -->



<?php
  echo view('_partials/footer');
?>



<!-- jQuery -->
<script src="<?php echo base_url(); ?>assets/plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="<?php echo base_url(); ?>assets/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- FastClick -->
<script src="<?php echo base_url(); ?>assets/plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url(); ?>assets/dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?php echo base_url(); ?>assets/dist/js/demo.js"></script>
<!-- page script -->

<!-- SweetAlert2 -->
<script src="<?php echo base_url(); ?>assets/plugins/sweetalert2/sweetalert2.min.js"></script>
<!-- Toastr -->
<script src="<?php echo base_url(); ?>assets/plugins/toastr/toastr.min.js"></script>

<!-- Bootstrap DataTables -->
<script src="https://unpkg.com/bootstrap-table@1.15.3/dist/bootstrap-table.min.js"></script>
<script src="https://unpkg.com/tableexport.jquery.plugin/tableExport.min.js"></script>
<script src="https://unpkg.com/tableexport.jquery.plugin/libs/jsPDF/jspdf.min.js"></script>
<script src="https://unpkg.com/tableexport.jquery.plugin/libs/jsPDF-AutoTable/jspdf.plugin.autotable.js"></script>
<!-- <script src="extensions/print/bootstrap-table-print.js"></script> -->
<script>;

function deleteItem(itemId){
    $('#modal-default').on('show.bs.modal', function(e) {
        $('.btn-ok', this).data('recordId', itemId);
    });

    $('#modal-default').on('click', '.btn-ok', function(e) {

        var id = $(this).data('recordId');

        var formData = {
            'id'   : id
        };

        $.ajax({
            type        : 'POST',
            url         : '<?=base_url("admin/logo/process_delete")?>',
            data        : formData,     // data object
            dataType    : 'json',       // what type of data do we expect back from the server
            encode      : true,
            error: function (request, status, error) {
                alert(request.responseText);
            }
        })

        // using the done promise callback
        .done(function(data) {

            // here we will handle errors and validation messages
            if (!data.success) {

                Toast.fire({
                    type: 'error',
                    title: data.message
                });

            } else {

                // ALL GOOD! just show the success message!
                alert(data.message);

                setTimeout(function(){
                    location.reload();
                }, 1000);

            }
        });
    });

    $('#modal-default').modal('show');
}

function editItem(itemId){
    window.location = "edit/" + itemId;
}

var $table = $('#table')
var $ok = $('#ok')

$ok.click(function () {
    $table.bootstrapTable('refresh')
})

$table.bootstrapTable({
    showExport: 'true'
})

function queryParams() {
    var params = {}
    $('#toolbar').find('input[name]').each(function () {
        params[$(this).attr('name')] = $(this).val()
    })
    return params
}

function responseHandler(res) {
    return res.rows
}
</script>
</body>
</html>
