<?php
  echo view('_partials/header');
?>

    <link href="<?=base_url("assets")?>/plugins/summernote/summernote-bs4.css" rel="stylesheet">
    <!-- SweetAlert2 -->
    <link rel="stylesheet" href="<?=base_url("assets")?>/plugins/sweetalert2/sweetalert2.min.css">
    <!-- Toastr -->
    <link rel="stylesheet" href="<?=base_url("assets")?>/plugins/toastr/toastr.min.css">
    <!-- Bootstrap Datepicker -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css"/>

<?php
  echo view('_partials/navbar');
  echo view('_partials/sidebar');
?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h5><?= $title ?></h5>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Inline Charts</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-12">
            <div class="card card-outline card-primary" style="border-top: 0px solid #86909b;">

                <div class="card-header" style="background-color: #86909b; color: #fff;">
                        <h3 class="card-title">
                            <?= $title ?>
                            <!-- <small>Simple and fast</small> -->
                        </h3>
                        <!-- tools box -->
                        <div class="card-tools">
                            <button type="button" class="btn btn-tool btn-sm" data-widget="collapse" data-toggle="tooltip"
                                    title="Collapse" style="color: rgba(255,255,255,.8);">
                            <i class="fas fa-minus"></i></button>
                            <button type="button" class="btn btn-tool btn-sm" data-widget="remove" data-toggle="tooltip"
                                    title="Remove" style="color: rgba(255,255,255,.8);">
                            <i class="fas fa-times"></i></button>
                        </div>
                        <!-- /. tools -->
                </div>
                <!-- /.card-header -->

            <form role="form" id="form">

                <div class="card-body">

                        <div class="row" style="margin-top: 30px;">
                            <div class="col">
                                <div class="card card-outline card-primary" style="border-top: 3px solid #86909b">
                                    <div class="card-header">
                                    <h3 class="card-title">Data Form</h3>

                                    <div class="card-tools">
                                        <button type="button" class="btn btn-tool" data-widget="collapse">
                                            <i class="fas fa-minus"></i>
                                        </button>
                                    </div>
                                    <!-- /.card-tools -->
                                    </div>
                                    <!-- /.card-header -->
                                    <div class="card-body">



                                        <div class="row" style="margin-top: 30px;margin-right: 40px;">


                                            <div class="col-lg">
                                            <div class="card-body">
                                            <h1>Upload Logo</h1>
                                                <div class="input-group">
                                                    <div class="custom-file">
                                                        <input name="file_upload" type="file" id="file_choose" class="custom-file-input">
                                                        <label class="custom-file-label" for="exampleInputFile">Choose file</label>
                                                    </div>
                                                    <div class="input-group-append">
                                                        <!-- <span class="input-group-text" id="btn_upload">Upload</span> -->
                                                    </div>
                                                </div>
                                            </div>
                                                <div class="row">
                                                    <div class="col-lg-1">
                                                    </div>
                                                    <div class="col-lg-2">
                                                        <p style="">Active</p>
                                                    </div>
                                                    <div class="col-lg-8">
                                                        <!-- Date input -->
                                                        <div id="site_name_group" class="form-group">
                                                            <select name="active" class="form-control select2" style="width: 100%;">
                                                                <option value="0" selected="selected" class="">Inactive</option>
                                                                <option value="1" selected="selected" class="">Active</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>


                                                
                                            </div>


                                        </div>




                                    </div>
                                    <!-- /.card-body -->
                                </div>
                            </div>
                        </div>


                </div>

                <div class="card-footer">
                    <button type="submit" class="btn btn-primary" style="float:right;">Submit</button>
                </div>

            </form>
            </div>
        </div>
        <!-- /.col-->
      </div>
      <!-- ./row -->
    </section>
    <!-- /.content -->


  </div>
  <!-- /.content-wrapper -->



<?php
  echo view('_partials/footer');
?>


<!-- jQuery -->
<script src="<?php echo base_url(); ?>assets/plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="<?php echo base_url(); ?>assets/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- Bootstrap Date-Picker Plugin -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>
<!-- FastClick -->
<script src="<?php echo base_url(); ?>assets/plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url(); ?>assets/dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?php echo base_url(); ?>assets/dist/js/demo.js"></script>
<!-- SweetAlert2 -->
<script src="<?php echo base_url(); ?>assets/plugins/sweetalert2/sweetalert2.min.js"></script>
<!-- Toastr -->
<script src="<?php echo base_url(); ?>assets/plugins/toastr/toastr.min.js"></script>

<!-- Summernote -->
<script src="<?php echo base_url(); ?>assets/plugins/summernote/summernote-bs4.min.js"></script>
<script>
$('#file_choose').on('change',function(){
    //get the file name
    var fileName = $(this).val();
    //replace the "Choose a file" label
    $(this).next('.custom-file-label').html(fileName.split("C:\\fakepath\\"));
})

function uploadImage(image, nameString) {
    var data = new FormData();
    data.append("image", image);
    $.ajax({
        url: "<?= base_url() ?>summernote/upload_image",
        cache: false,
        contentType: false,
        processData: false,
        data: data,
        type: "POST",
        success: function(url) {
            // $('.textarea').summernote("insertImage", url);
            $('textarea[name='+nameString+']').summernote("insertImage", url);
        },

        error: function(data) {
            // console.log(data);
        }
    });
}

function deleteImage(src) {
    $.ajax({
        data: {src : src},
        type: "POST",
        url: "<?= base_url() ?>summernote/delete_image",
        cache: false,
        success: function(response) {
            console.log(response);
        }
    });
}
  $(function () {
    // Summernote
    $('.textarea').summernote({
        height: 200
    })
  });

$('#form').submit(function (e) {
    e.preventDefault();

    $('.form-group').removeClass('has-error'); // remove the error class
    $('.help-block').remove(); // remove the error text
    $('.alert-success').remove();

    // var formData = {
    //         'management_system_short_name'            : $('input[name=management_system_short_name').val(),
    //         'management_system_full_name'             : $('input[name=management_system_full_name').val(),
    // };

    var formData = new FormData(this);

    // $(this).prop("checked", attendedValues[index]);

    // process the form
    $.ajax({
            type        : 'POST',
            url         : '<?=base_url("admin/logo/process_add")?>',
            data        : formData, // data object
            dataType    : 'json', // what type of data do we expect back from the server
            encode      : true,
            processData : false,
            contentType : false,
            cache       : false,
            async       : false,
            error       : function(data){
                alert("AJAX ERROR")
                alert(JSON.stringify(data));
            }
        })

        // using the done promise callback
        .done(function(data) {

            // alert(JSON.stringify(data));

            // log data to the console so we can see
            console.log(data);

            // here we will handle errors and validation messages
            if ( ! data.success) {

                alert('Please complete the form');

                for (var key in data.errors) {
                  eval(data.errors[key]);
                }

            } else {

                // ALL GOOD! just show the success message!
                // $('form[id=form]').prepend('<div class="alert alert-success">' + data.message + '</div>');
                alert(data.message);

                // usually after form submission, you'll want to redirect
                // window.location = '/thank-you'; // redirect a user to another page
                // alert('success'); // for now we'll just alert the user

                $("#form").trigger("reset");
                $('.textarea').summernote('code', '');

            }
        });
});
</script>
</body>
</html>
