<?php

?><!DOCTYPE html>
<html lang="en">

<?php echo view('_partials/header'); ?>

<body class="no-skin">

<?php echo view('_partials/navbar'); ?>

<div class="main-container ace-save-state" id="main-container">
    <script type="text/javascript">
        try {
            ace.settings.loadState('main-container')
        } catch (e) {
        }
    </script>

    <?php echo view('_partials/sidebar'); ?>

    <div class="main-content">
        <div class="main-content-inner">
            <div class="breadcrumbs ace-save-state" id="breadcrumbs">
                <ul class="breadcrumb">
                    <li>
                        <i class="ace-icon fa fa-home home-icon"></i>
                        <a href="#">Home</a>
                    </li>
                    <li class="active"><?= $module_title ?></li>
                </ul><!-- /.breadcrumb -->
            </div>

            <div class="page-content">
                <div class="ace-settings-container" id="ace-settings-container">
                    <div class="btn btn-app btn-xs btn-warning ace-settings-btn" id="ace-settings-btn">
                        <i class="ace-icon fa fa-cog bigger-130"></i>
                    </div>

                    <div class="ace-settings-box clearfix" id="ace-settings-box">
                        <div class="pull-left width-50">
                            <div class="ace-settings-item">
                                <div class="pull-left">
                                    <select id="skin-colorpicker" class="hide">
                                        <option data-skin="no-skin" value="#438EB9">#438EB9</option>
                                        <option data-skin="skin-1" value="#222A2D">#222A2D</option>
                                        <option data-skin="skin-2" value="#C6487E">#C6487E</option>
                                        <option data-skin="skin-3" value="#D0D0D0">#D0D0D0</option>
                                    </select>
                                </div>
                                <span>&nbsp; Choose Skin</span>
                            </div>

                            <div class="ace-settings-item">
                                <input type="checkbox" class="ace ace-checkbox-2 ace-save-state"
                                       id="ace-settings-navbar" autocomplete="off"/>
                                <label class="lbl" for="ace-settings-navbar"> Fixed Navbar</label>
                            </div>

                            <div class="ace-settings-item">
                                <input type="checkbox" class="ace ace-checkbox-2 ace-save-state"
                                       id="ace-settings-sidebar" autocomplete="off"/>
                                <label class="lbl" for="ace-settings-sidebar"> Fixed Sidebar</label>
                            </div>

                            <div class="ace-settings-item">
                                <input type="checkbox" class="ace ace-checkbox-2 ace-save-state"
                                       id="ace-settings-breadcrumbs" autocomplete="off"/>
                                <label class="lbl" for="ace-settings-breadcrumbs"> Fixed Breadcrumbs</label>
                            </div>

                            <div class="ace-settings-item">
                                <input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-rtl"
                                       autocomplete="off"/>
                                <label class="lbl" for="ace-settings-rtl"> Right To Left (rtl)</label>
                            </div>

                            <div class="ace-settings-item">
                                <input type="checkbox" class="ace ace-checkbox-2 ace-save-state"
                                       id="ace-settings-add-container" autocomplete="off"/>
                                <label class="lbl" for="ace-settings-add-container">
                                    Inside
                                    <b>.container</b>
                                </label>
                            </div>
                        </div><!-- /.pull-left -->

                        <div class="pull-left width-50">
                            <div class="ace-settings-item">
                                <input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-hover"
                                       autocomplete="off"/>
                                <label class="lbl" for="ace-settings-hover"> Submenu on Hover</label>
                            </div>

                            <div class="ace-settings-item">
                                <input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-compact"
                                       autocomplete="off"/>
                                <label class="lbl" for="ace-settings-compact"> Compact Sidebar</label>
                            </div>

                            <div class="ace-settings-item">
                                <input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-highlight"
                                       autocomplete="off"/>
                                <label class="lbl" for="ace-settings-highlight"> Alt. Active Item</label>
                            </div>
                        </div><!-- /.pull-left -->
                    </div><!-- /.ace-settings-box -->
                </div><!-- /.ace-settings-container -->

                <div class="page-header">
                    <h1>
                        <a href="<?php echo base_url("sustainable_palmoil/ispo/sub_main/list?main={$_GET['main']}") ?>"><i
                                    class="fa fa-arrow-circle-left"></i></a>
                        <?= $module_title ?>
                        <small>
                            <i class="ace-icon fa fa-angle-double-right"></i>
                            <?= $submodule_title ?>
                        </small>
                    </h1>
                </div><!-- /.page-header -->

                <div class="row">
                    <div class="col-xs-12">

                        <div class="table-header">
                            Indicators
                        </div>

                        <div>
                            <div class="x_content">
                                <div class="" role="tabpanel" data-example-id="togglable-tabs">
                                    <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
                                        <li role="presentation" class="active">
                                            <a href="#tab_content1" id="home-tab" role="tab" data-toggle="tab"
                                               aria-expanded="true">
                                                <i class="fa fa-file-o"></i> Draft (<?= count($rows_draft) ?>
                                                /<?= $totalCount ?>)</a>
                                        </li>
                                        <li role="presentation" class="">
                                            <a href="#tab_content2" id="home-tab" role="tab" data-toggle="tab"
                                               aria-expanded="false">
                                                <i class="fa fa-info-circle"></i>Revision (<?= count($rows_revision) ?>)</a>
                                        </li>
                                        <li role="presentation" class="">
                                            <a href="#tab_content3" id="home-tab" role="tab" data-toggle="tab"
                                               aria-expanded="true">
                                                <i class="fa fa-check-circle"></i> Approved
                                                (<?= count($rows_approved) ?>/<?= $totalCount ?>)</a>
                                        </li>
                                        <li role="presentation" style="float:right;">
                                            <a href="#" data-toggle="modal" data-target=".adddoc">
                                                <i class="fa fa-plus-circle"
                                                   style="color:blue; font-size:19px;"></i></a>
                                        </li>
                                    </ul>

                                    <div id="myTabContent" class="tab-content">
                                        <div role="tabpanel" class="tab-pane fade active in" id="tab_content1"
                                             aria-labelledby="home-tab">
                                            <table id="datatable" class="table table-striped dataTable no-footer">
                                                <thead>
                                                <tr style="display:none;">
                                                    <th>a</th>
                                                    <th>b</th>
                                                    <th>c</th>
                                                    <th>d</th>
                                                    <th>e</th>
                                                </tr>
                                                </thead>
                                                <tbody>

                                                <?php foreach ($rows_draft as $row): ?>

                                                    <tr>
                                                        <td>
                                                            <span class="badge badge-primary">Indikator <?= $row->indicator_no ?></span>
                                                        </td>
                                                        <td>
                                                            <h3>
                                                                <?php if (!empty($row->doc_title)): ?>
                                                                    <?= $row->doc_title ?>
                                                                <?php endif; ?>
                                                                <?php if (empty($row->file_url)): ?>
                                                                    <a href="<?php echo base_url("sustainable_palmoil/ispo/indicator_upload/edit/{$row->id}?main={$_GET['main']}&sub_main={$_GET['sub_main']}") ?>"
                                                                       class="btn btn-info">
                                                                        <i class="fa fa-upload"></i>
                                                                        Upload Dokumen Indikator
                                                                    </a>
                                                                <?php endif; ?>
                                                            </h3>
                                                            <p>
                                                                <?= $row->description ?>
                                                            </p>
                                                        </td>
                                                        <td style="width: 125px">
                                                            <h1>
                                                                <?php if (!empty($row->file_url)): ?>
                                                                    <a href="<?= $row->file_url ?>"
                                                                       target="_BLANK"><i class="fa fa-download"></i>
                                                                    </a>
                                                                    <a href="<?php echo base_url("sustainable_palmoil/ispo/indicator_upload/edit/{$row->id}?main={$_GET['main']}&sub_main={$_GET['sub_main']}") ?>">
                                                                        <i class="fa fa-edit"></i>
                                                                    </a>
                                                                <?php endif; ?>
                                                            </h1>
                                                        </td>
                                                    </tr>

                                                <?php endforeach; ?>

                                                </tbody>
                                            </table>
                                        </div>

                                        <div role="tabpanel" class="tab-pane fade" id="tab_content2"
                                             aria-labelledby="home-tab">
                                            <table id="datatable" class="datatable table table-striped">
                                                <thead>
                                                <tr style="display:none;">
                                                    <th>a</th>
                                                    <th>b</th>
                                                    <th>c</th>
                                                    <th>d</th>
                                                    <th>e</th>
                                                </tr>
                                                </thead>
                                                <tbody>

                                                <?php foreach ($rows_revision as $row): ?>

                                                    <tr>
                                                        <td>
                                                            <span class="badge badge-primary">Indikator <?= $row->indicator_no ?></span>
                                                        </td>
                                                        <td>
                                                            <h3>
                                                                <?php if (!empty($row->doc_title)): ?>
                                                                    <?= $row->doc_title ?>
                                                                <?php endif; ?>
                                                                <?php if (empty($row->file_url)): ?>
                                                                    <a href="<?php echo base_url("sustainable_palmoil/ispo/indicator_upload/edit/{$row->id}?main={$_GET['main']}&sub_main={$_GET['sub_main']}") ?>"
                                                                       class="btn btn-info">
                                                                        <i class="fa fa-upload"></i>
                                                                        Upload Dokumen Indikator
                                                                    </a>
                                                                <?php endif; ?>
                                                            </h3>
                                                            <p>
                                                                <?= $row->description ?>
                                                            </p>
                                                        </td>
                                                        <td style="width: 125px">
                                                            <h1>
                                                                <?php if (!empty($row->file_url)): ?>
                                                                    <a href="<?= $row->file_url ?>"
                                                                       target="_BLANK"><i class="fa fa-download"></i>
                                                                    </a>
                                                                    <a href="<?php echo base_url("sustainable_palmoil/ispo/indicator_upload/edit/{$row->id}?main={$_GET['main']}&sub_main={$_GET['sub_main']}") ?>">
                                                                        <i class="fa fa-edit"></i>
                                                                    </a>
                                                                <?php endif; ?>
                                                            </h1>
                                                        </td>
                                                    </tr>

                                                <?php endforeach; ?>

                                                </tbody>
                                            </table>
                                        </div>

                                        <div role="tabpanel" class="tab-pane fade in" id="tab_content3"
                                             aria-labelledby="home-tab">
                                            <table id="datatable" class="datatable table table-striped">
                                                <thead>
                                                <tr style="display:none;">
                                                    <th>a</th>
                                                    <th>b</th>
                                                    <th>c</th>
                                                    <th>d</th>
                                                    <th>e</th>
                                                </tr>
                                                </thead>
                                                <tbody>

                                                <?php foreach ($rows_approved as $row): ?>

                                                    <tr>
                                                        <td>
                                                            <span class="badge badge-primary">Indikator <?= $row->indicator_no ?></span>
                                                        </td>
                                                        <td>
                                                            <h3>
                                                                <?php if (!empty($row->doc_title)): ?>
                                                                    <?= $row->doc_title ?>
                                                                <?php endif; ?>
                                                                <?php if (empty($row->file_url)): ?>
                                                                    <a href="<?php echo base_url("sustainable_palmoil/ispo/indicator_upload/edit/{$row->id}?main={$_GET['main']}&sub_main={$_GET['sub_main']}") ?>"
                                                                       class="btn btn-info">
                                                                        <i class="fa fa-upload"></i>
                                                                        Upload Dokumen Indikator
                                                                    </a>
                                                                <?php endif; ?>
                                                            </h3>
                                                            <p>
                                                                <?= $row->description ?>
                                                            </p>
                                                        </td>
                                                        <td style="width: 125px">
                                                            <h1>
                                                                <?php if (!empty($row->file_url)): ?>
                                                                    <a href="<?= $row->file_url ?>"
                                                                       target="_BLANK"><i class="fa fa-download"></i>
                                                                    </a>
                                                                    <a href="<?php echo base_url("sustainable_palmoil/ispo/indicator_upload/edit/{$row->id}?main={$_GET['main']}&sub_main={$_GET['sub_main']}") ?>">
                                                                        <i class="fa fa-edit"></i>
                                                                    </a>
                                                                <?php endif; ?>
                                                            </h1>
                                                        </td>
                                                    </tr>

                                                <?php endforeach; ?>

                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div><!-- /.row -->
            </div><!-- /.page-content -->
        </div>
    </div><!-- /.main-content -->


    <a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
        <i class="ace-icon fa fa-angle-double-up icon-only bigger-110"></i>
    </a>
</div><!-- /.main-container -->

<?php echo view("_partials/read_script"); ?>

</body>
</html>
