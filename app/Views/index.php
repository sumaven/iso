<!DOCTYPE html>
<html lang="en">

<?php echo view('_partials/header'); ?>
<style>
    .middle {
        width: 100%;
        margin: 0;
        position: absolute;
        top: 50%;
        left: 50%;
        -ms-transform: translateX(-50%) translateY(-35%);
        transform: translateX(-50%) translateY(-35%);
    }

    .Marquee {
        background: linear-gradient(-135deg, #008ed9, #00a8db);
        box-sizing: border-box;
        padding: 1em;
        color: white;
        font-weight: 200;
        display: flex;
        align-items: center;
        overflow: hidden;
    }

    .Marquee-content {
        display: flex;
        animation: marquee <?= ($calendarOfEventCount > 1) ? $calendarOfEventCount*5 : 0 ?>s linear infinite running;
    }

    .Marquee-content:hover {
        animation-play-state: paused;
    }

    .Marquee-tag {
        width: 300px;
        height: 150px;
        margin: 0 .5em;
        padding: .5em;
        background: #00a8db;
        display: inline-flex;
        flex-direction: column;
        align-items: center;
        justify-content: center;
        transition: all .2s ease;
    }

    .Marquee-tag:hover {
        background: rgba(255, 255, 255, .5);
        transform: scale(1.1);
        cursor: pointer;
    }

    @keyframes marquee {
        0% {
            transform: translateX(100%)
        }
        100% {
            transform: translate(-100%)
        }
    }

    /*https://codepen.io/JKudla/pen/vyXBpO*/
</style>
<body class="no-skin">

<?php echo view('_partials/navbar'); ?>

<div class="main-container ace-save-state" id="main-container">
    <script type="text/javascript">
        try {
            ace.settings.loadState('main-container')
        } catch (e) {
        }
    </script>

    <?php echo view('_partials/sidebar'); ?>

    <div class="main-content">
        <div class="main-content-inner">
            <div class="breadcrumbs ace-save-state" id="breadcrumbs">
                <ul class="breadcrumb">
                    <li>
                        <i class="ace-icon fa fa-home home-icon"></i>
                        <a href="#">Home</a>
                    </li>
                    <li class="active">Dashboard</li>
                </ul><!-- /.breadcrumb -->

                <div class="nav-search" id="nav-search">
                    <form class="form-search">
								<span class="input-icon">
									<input type="text" placeholder="Search ..." class="nav-search-input"
                                           id="nav-search-input" autocomplete="off"/>
									<i class="ace-icon fa fa-search nav-search-icon"></i>
								</span>
                    </form>
                </div><!-- /.nav-search -->
            </div>

            <div class="page-content">
                <div class="ace-settings-container" id="ace-settings-container">
                    <div class="btn btn-app btn-xs btn-warning ace-settings-btn" id="ace-settings-btn">
                        <i class="ace-icon fa fa-cog bigger-130"></i>
                    </div>

                    <div class="ace-settings-box clearfix" id="ace-settings-box">
                        <div class="pull-left width-50">
                            <div class="ace-settings-item">
                                <div class="pull-left">
                                    <select id="skin-colorpicker" class="hide">
                                        <option data-skin="no-skin" value="#438EB9">#438EB9</option>
                                        <option data-skin="skin-1" value="#222A2D">#222A2D</option>
                                        <option data-skin="skin-2" value="#C6487E">#C6487E</option>
                                        <option data-skin="skin-3" value="#D0D0D0">#D0D0D0</option>
                                    </select>
                                </div>
                                <span>&nbsp; Choose Skin</span>
                            </div>

                            <div class="ace-settings-item">
                                <input type="checkbox" class="ace ace-checkbox-2 ace-save-state"
                                       id="ace-settings-navbar" autocomplete="off"/>
                                <label class="lbl" for="ace-settings-navbar"> Fixed Navbar</label>
                            </div>

                            <div class="ace-settings-item">
                                <input type="checkbox" class="ace ace-checkbox-2 ace-save-state"
                                       id="ace-settings-sidebar" autocomplete="off"/>
                                <label class="lbl" for="ace-settings-sidebar"> Fixed Sidebar</label>
                            </div>

                            <div class="ace-settings-item">
                                <input type="checkbox" class="ace ace-checkbox-2 ace-save-state"
                                       id="ace-settings-breadcrumbs" autocomplete="off"/>
                                <label class="lbl" for="ace-settings-breadcrumbs"> Fixed Breadcrumbs</label>
                            </div>

                            <div class="ace-settings-item">
                                <input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-rtl"
                                       autocomplete="off"/>
                                <label class="lbl" for="ace-settings-rtl"> Right To Left (rtl)</label>
                            </div>

                            <div class="ace-settings-item">
                                <input type="checkbox" class="ace ace-checkbox-2 ace-save-state"
                                       id="ace-settings-add-container" autocomplete="off"/>
                                <label class="lbl" for="ace-settings-add-container">
                                    Inside
                                    <b>.container</b>
                                </label>
                            </div>
                        </div><!-- /.pull-left -->

                        <div class="pull-left width-50">
                            <div class="ace-settings-item">
                                <input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-hover"
                                       autocomplete="off"/>
                                <label class="lbl" for="ace-settings-hover"> Submenu on Hover</label>
                            </div>

                            <div class="ace-settings-item">
                                <input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-compact"
                                       autocomplete="off"/>
                                <label class="lbl" for="ace-settings-compact"> Compact Sidebar</label>
                            </div>

                            <div class="ace-settings-item">
                                <input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-highlight"
                                       autocomplete="off"/>
                                <label class="lbl" for="ace-settings-highlight"> Alt. Active Item</label>
                            </div>
                        </div><!-- /.pull-left -->
                    </div><!-- /.ace-settings-box -->
                </div><!-- /.ace-settings-container -->

                <div class="page-header">
                    <h1>
                        <small>
                            <i class="ace-icon fa fa-angle-double-right"></i>
                            overview &amp; stats
                        </small>
                    </h1>
                </div><!-- /.page-header -->

                <div class="row">
                    <div class="col-xs-12">
                        <!-- PAGE CONTENT BEGINS -->
                        <div class="alert alert-block alert-success">
                            <button type="button" class="close" data-dismiss="alert">
                                <i class="ace-icon fa fa-times"></i>
                            </button>

                            <i class="ace-icon fa fa-check green"></i>

                            Welcome to
                            <strong class="green">
                                Asia Global Servis
                            </strong>
                        </div>

                        <div class="row">
                            <div class="col-sm-6">

                                <div class="widget-box transparent">
                                    <div class="widget-header widget-header-flat">
                                        <h4 class="widget-title lighter">
                                            <i class="ace-icon fa fa-star orange"></i>
                                            Summary
                                        </h4>

                                        <div class="widget-toolbar">
                                            <a href="#" data-action="collapse">
                                                <i class="ace-icon fa fa-chevron-up"></i>
                                            </a>
                                        </div>
                                    </div>

                                    <div class="widget-body">
                                        <div class="widget-main no-padding infobox-container">

                                            <div class="space-14"></div>

                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <div class="infobox infobox-green infobox-dark"
                                                         style="width: 100%; height: 100%; padding: 15px">
                                                        <div class="infobox-progress">
                                                            <div class="easy-pie-chart percentage" data-percent="100"
                                                                 data-size="75">
                                                                <span class="percent"><?= $internalAuditReportCount ?></span>
                                                            </div>
                                                        </div>

                                                        <div class="infobox-data"
                                                             style="margin-left: 30px; margin-top: 15px;">
                                                            <div class="infobox-content">Internal Audit Report</div>
                                                            <div class="infobox-content">Count</div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-sm-6">
                                                    <div class="infobox infobox-blue infobox-dark"
                                                         style="width: 100%; height: 100%; padding: 15px">
                                                        <div class="infobox-progress">
                                                            <div class="easy-pie-chart percentage" data-percent="100"
                                                                 data-size="75">
                                                                <span class="percent"><?= $nonConformanceCount ?></span>
                                                            </div>
                                                        </div>

                                                        <div class="infobox-data"
                                                             style="margin-left: 30px; margin-top: 15px;">
                                                            <div class="infobox-content">Non Conformance</div>
                                                            <div class="infobox-content">Count</div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row" style="margin-top: 30px;">
                                                <div class="col-sm-6">
                                                    <div class="infobox infobox-green infobox-dark"
                                                         style="width: 100%; height: 100%; padding: 15px">
                                                        <div class="infobox-progress">
                                                            <div class="easy-pie-chart percentage" data-percent="100"
                                                                 data-size="75">
                                                                <span class="percent"><?= $wiCount ?></span>
                                                            </div>
                                                        </div>

                                                        <div class="infobox-data"
                                                             style="margin-left: 30px; margin-top: 15px;">
                                                            <div class="infobox-content">Work Instruction</div>
                                                            <div class="infobox-content">Count</div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-sm-6">
                                                    <div class="infobox infobox-blue infobox-dark"
                                                         style="width: 100%; height: 100%; padding: 15px">
                                                        <div class="infobox-progress">
                                                            <div class="easy-pie-chart percentage" data-percent="100"
                                                                 data-size="75">
                                                                <span class="percent"><?= $sopCount ?></span>
                                                            </div>
                                                        </div>

                                                        <div class="infobox-data"
                                                             style="margin-left: 30px; margin-top: 15px;">
                                                            <div class="infobox-content">Procedure</div>
                                                            <div class="infobox-content">Count</div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </div><!-- /.widget-main -->
                                    </div><!-- /.widget-body -->
                                </div><!-- /.widget-box -->

                            </div>

                            <div class="vspace-12-sm"></div>

                            <div class="col-sm-6">
                                <div class="widget-box transparent">
                                    <div class="widget-header widget-header-flat">
                                        <h4 class="widget-title lighter">
                                            <i class="ace-icon fa fa-star orange"></i>
                                            Risk Matrix
                                        </h4>

                                        <div class="widget-toolbar">
                                            <a href="#" data-action="collapse">
                                                <i class="ace-icon fa fa-chevron-up"></i>
                                            </a>
                                        </div>
                                    </div>

                                    <div class="widget-body">
                                        <div class="widget-main no-padding">

                                            <div class="space-14"></div>

                                            <table class="bordered" style="padding: 30px;">
                                                <tr>
                                                    <td style="font-weight: bolder">Probability</td>
                                                    <td>Low</td>
                                                    <td>Medium</td>
                                                    <td>High</td>
                                                </tr>
                                                <tr>
                                                    <td>Low</td>
                                                    <td style="background-color: #34c788">1</td>
                                                    <td style="background-color: #34c788">1</td>
                                                    <td style="background-color: #ffff8e">1</td>
                                                </tr>
                                                <tr>
                                                    <td>Medium</td>
                                                    <td style="background-color: #34c788">1</td>
                                                    <td style="background-color: #ffff8e">1</td>
                                                    <td style="background-color: #ff6c69">1</td>
                                                </tr>
                                                <tr>
                                                    <td>High</td>
                                                    <td style="background-color: #ffff8e">1</td>
                                                    <td style="background-color: #ff6c69">1</td>
                                                    <td style="background-color: #ff6c69">1</td>
                                                </tr>
                                            </table>

                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div><!-- /.row -->

                        <div class="row">

                            <div class="col-sm-6">
                                <div class="widget-box transparent">
                                    <div class="widget-header widget-header-flat">
                                        <h4 class="widget-title lighter">
                                            <i class="ace-icon fa fa-star orange"></i>
                                            Calendar of Event
                                        </h4>

                                        <div class="widget-toolbar">
                                            <a href="#" data-action="collapse">
                                                <i class="ace-icon fa fa-chevron-up"></i>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="widget-body" style="height: 200px; position: relative;">
                                        <div class="widget-main no-padding middle">

                                            <div class="Marquee">
                                                <div class="Marquee-content">
                                                    <?php foreach ($calendarOfEvents as $row): ?>
                                                        <div class="Marquee-tag">
                                                            <p><?= $row->agenda ?></p>
                                                            <p><?= $row->waktu_pelaksanaan ?></p>
                                                            <a target="_blank"
                                                               href="<?= base_url("calendar_of_event/edit/" . $row->id) ?>"
                                                               class="btn btn-primary"
                                                               style="width: 90%; font-size: 12px">Buka</a>
                                                        </div>
                                                    <?php endforeach; ?>
                                                </div>
                                            </div>

                                        </div><!-- /.widget-main -->
                                    </div><!-- /.widget-body -->
                                </div><!-- /.widget-box -->
                            </div>


                            <div class="col-sm-6">
                                <div class="widget-box transparent">
                                    <div class="widget-header widget-header-flat">
                                        <h4 class="widget-title lighter">
                                            <i class="ace-icon fa fa-star orange"></i>
                                            Objective Evaluation
                                        </h4>
                                        <div class="widget-toolbar">
                                            <a href="#" data-action="collapse">
                                                <i class="ace-icon fa fa-chevron-up"></i>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="widget-body">
                                        <div class="widget-main no-padding">

                                            <div class="col-xs-12">
                                                <div id="bar-chart" height="300"></div>
                                            </div>

                                        </div><!-- /.widget-main -->
                                    </div><!-- /.widget-body -->
                                </div><!-- /.widget-box -->
                            </div>

                        </div><!-- /.row -->


                        <div class="row">
                            <div class="col-sm-6">
                                <div class="widget-box transparent">
                                    <div class="widget-header widget-header-flat">
                                        <h4 class="widget-title lighter">
                                            <i class="ace-icon fa fa-star orange"></i>
                                            Trainings (Planned vs Actual) (By Month)
                                        </h4>

                                        <div class="widget-toolbar">
                                            <a href="#" data-action="collapse">
                                                <i class="ace-icon fa fa-chevron-up"></i>
                                            </a>
                                        </div>
                                    </div>

                                    <div class="widget-body">
                                        <div class="widget-main no-padding">

                                            <div class="col-xs-12">
                                                <canvas id="radar-chart" height="350"></canvas>
                                            </div>

                                        </div><!-- /.widget-main -->
                                    </div><!-- /.widget-body -->
                                </div><!-- /.widget-box -->
                            </div><!-- /.col -->

                            <div class="col-sm-6">

                                <div class="widget-box transparent">
                                    <div class="widget-header widget-header-flat">
                                        <h4 class="widget-title lighter">
                                            <i class="ace-icon fa fa-star orange"></i>
                                            Documents (By Document Type)
                                        </h4>

                                        <div class="widget-toolbar">
                                            <a href="#" data-action="collapse">
                                                <i class="ace-icon fa fa-chevron-up"></i>
                                            </a>
                                        </div>
                                    </div>

                                    <div class="widget-body">
                                        <div class="widget-main no-padding">

                                            <div class="space-14"></div>

                                            <div class="col-xs-12">
                                                <div id="pie-chart"></div>
                                            </div>

                                        </div><!-- /.widget-main -->
                                    </div><!-- /.widget-body -->
                                </div><!-- /.widget-box -->
                            </div><!-- /.col -->
                        </div><!-- /.row -->



                        <div class="row">
                            <div class="col-sm-6">
                                <div class="widget-box transparent">
                                    <div class="widget-header widget-header-flat">
                                        <h4 class="widget-title lighter">
                                            <i class="ace-icon fa fa-star orange"></i>
                                            ISPO
                                        </h4>

                                        <div class="widget-toolbar">
                                            <a href="#" data-action="collapse">
                                                <i class="ace-icon fa fa-chevron-up"></i>
                                            </a>
                                        </div>
                                    </div>

                                    <div class="widget-body">
                                        <div class="widget-main no-padding">

                                            <div class="col-xs-12">
                                                <canvas id="radar-chart-ispo" height="350"></canvas>
                                            </div>

                                        </div><!-- /.widget-main -->
                                    </div><!-- /.widget-body -->
                                </div><!-- /.widget-box -->
                            </div><!-- /.col -->

                            <div class="col-sm-6">

                                <div class="widget-box transparent">
                                    <div class="widget-header widget-header-flat">
                                        <h4 class="widget-title lighter">
                                            <i class="ace-icon fa fa-star orange"></i>
                                            RSPO
                                        </h4>

                                        <div class="widget-toolbar">
                                            <a href="#" data-action="collapse">
                                                <i class="ace-icon fa fa-chevron-up"></i>
                                            </a>
                                        </div>
                                    </div>

                                    <div class="widget-body">
                                        <div class="widget-main no-padding">

                                            <div class="col-xs-12">
                                                <canvas id="radar-chart-rspo" height="350"></canvas>
                                            </div>

                                        </div><!-- /.widget-main -->
                                    </div><!-- /.widget-body -->
                                </div><!-- /.widget-box -->
                            </div><!-- /.col -->
                        </div><!-- /.row -->


                        <div class="row">
                            <div class="col-sm-6">
                                <div class="widget-box transparent">
                                    <div class="widget-header widget-header-flat">
                                        <h4 class="widget-title lighter">
                                            <i class="ace-icon fa fa-star orange"></i>
                                            SMK3
                                        </h4>

                                        <div class="widget-toolbar">
                                            <a href="#" data-action="collapse">
                                                <i class="ace-icon fa fa-chevron-up"></i>
                                            </a>
                                        </div>
                                    </div>

                                    <div class="widget-body">
                                        <div class="widget-main no-padding">

                                            <div class="col-xs-12">
                                                <canvas id="radar-chart-smk3" height="350"></canvas>
                                            </div>

                                        </div><!-- /.widget-main -->
                                    </div><!-- /.widget-body -->
                                </div><!-- /.widget-box -->
                            </div><!-- /.col -->

                        </div><!-- /.row -->


                        <!-- PAGE CONTENT ENDS -->
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.page-content -->
        </div>
    </div><!-- /.main-content -->


    <a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
        <i class="ace-icon fa fa-angle-double-up icon-only bigger-110"></i>
    </a>
</div><!-- /.main-container -->

<!-- basic scripts -->

<!--[if !IE]> -->
<script src="<?php echo base_url("assets"); ?>/js/jquery-2.1.4.min.js"></script>

<!-- <![endif]-->

<!--[if IE]>
<script src="assets/js/jquery-1.11.3.min.js"></script>
<![endif]-->
<script type="text/javascript">
    if ('ontouchstart' in document.documentElement) document.write("<script src='<?php echo base_url("assets"); ?>/js/jquery.mobile.custom.min.js'>" + "<" + "/script>");
</script>
<script src="<?php echo base_url("assets"); ?>/js/bootstrap.min.js"></script>

<!-- page specific plugin scripts -->

<!--[if lte IE 8]>
<script src="assets/js/excanvas.min.js"></script>
<![endif]-->
<script src="<?php echo base_url("assets"); ?>/js/jquery-ui.custom.min.js"></script>
<script src="<?php echo base_url("assets"); ?>/js/jquery.ui.touch-punch.min.js"></script>
<script src="<?php echo base_url("assets"); ?>/js/jquery.easypiechart.min.js"></script>
<script src="<?php echo base_url("assets"); ?>/js/jquery.sparkline.index.min.js"></script>
<script src="<?php echo base_url("assets"); ?>/js/jquery.flot.min.js"></script>
<script src="<?php echo base_url("assets"); ?>/js/jquery.flot.pie.min.js"></script>
<script src="<?php echo base_url("assets"); ?>/js/jquery.flot.resize.min.js"></script>

<!-- ace scripts -->
<script src="<?php echo base_url("assets"); ?>/js/ace-elements.min.js"></script>
<script src="<?php echo base_url("assets"); ?>/js/ace.min.js"></script>

<!-- inline scripts related to this page -->
<script type="text/javascript">
    jQuery(function ($) {
        $('.easy-pie-chart.percentage').each(function () {
            var $box = $(this).closest('.infobox');
            var barColor = $(this).data('color') || (!$box.hasClass('infobox-dark') ? $box.css('color') : 'rgba(255,255,255,0.95)');
            var trackColor = barColor == 'rgba(255,255,255,0.95)' ? 'rgba(255,255,255,0.25)' : '#E2E2E2';
            var size = parseInt($(this).data('size')) || 50;
            $(this).easyPieChart({
                barColor: barColor,
                trackColor: trackColor,
                scaleColor: false,
                lineCap: 'butt',
                lineWidth: parseInt(size / 10),
                animate: ace.vars['old_ie'] ? false : 1000,
                size: size
            });
        })

        $('.sparkline').each(function () {
            var $box = $(this).closest('.infobox');
            var barColor = !$box.hasClass('infobox-dark') ? $box.css('color') : '#FFF';
            $(this).sparkline('html',
                {
                    tagValuesAttribute: 'data-values',
                    type: 'bar',
                    barColor: barColor,
                    chartRangeMin: $(this).data('min') || 0
                });
        });


        //flot chart resize plugin, somehow manipulates default browser resize event to optimize it!
        //but sometimes it brings up errors with normal resize event handlers
        $.resize.throttleWindow = false;


    })
</script>


<!-- BEGIN: Page Vendor JS-->
<script src="https://pixinvent.com/modern-admin-clean-bootstrap-4-dashboard-html-template/app-assets/vendors/js/charts/chart.min.js"></script>
<script src="https://pixinvent.com/modern-admin-clean-bootstrap-4-dashboard-html-template/app-assets/vendors/js/charts/d3.min.js"></script>
<script src="https://pixinvent.com/modern-admin-clean-bootstrap-4-dashboard-html-template/app-assets/vendors/js/charts/d3.tip.js"></script>
<script src="https://pixinvent.com/modern-admin-clean-bootstrap-4-dashboard-html-template/app-assets/vendors/js/charts/c3.min.js"></script>
<!-- END: Page Vendor JS-->

<!-- BEGIN: Page JS-->
<!--<script src="https://pixinvent.com/modern-admin-clean-bootstrap-4-dashboard-html-template/app-assets/js/scripts/pages/material-app.js"></script>-->
<!--<script src="https://pixinvent.com/modern-admin-clean-bootstrap-4-dashboard-html-template/app-assets/js/scripts/charts/chartjs/polar-radar/polar.js"></script>-->
<!--<script src="https://pixinvent.com/modern-admin-clean-bootstrap-4-dashboard-html-template/app-assets/js/scripts/charts/chartjs/polar-radar/radar.js"></script>-->
<!--<script src="https://pixinvent.com/modern-admin-clean-bootstrap-4-dashboard-html-template/app-assets/js/scripts/charts/chartjs/polar-radar/polar-skip-points.js"></script>-->
<!--<script src="https://pixinvent.com/modern-admin-clean-bootstrap-4-dashboard-html-template/app-assets/js/scripts/charts/chartjs/polar-radar/radar-skip-points.js"></script>-->

<script>
    /*=========================================================================================
        File Name: column.js
        Description: c3 column chart
        ----------------------------------------------------------------------------------------
        Item Name: Modern Admin - Clean Bootstrap 4 Dashboard HTML Template
       Version: 3.0
        Author: PIXINVENT
        Author URL: http://www.themeforest.net/user/pixinvent
    ==========================================================================================*/

    // Column chart
    // ------------------------------
    $(window).on("load", function () {

        // Callback that creates and populates a data table, instantiates the column chart, passes in the data and draws it.
        var columnChart = c3.generate({
            bindto: '#bar-chart',
            size: {height: 300},
            color: {
                pattern: ['#99B898', '#FECEA8', '#FF847C']
            },
            // Create the data table.
            data: {
                x: 'x',
                columns: [
                    ['x'
                        <?php
                        foreach ($objectiveEvaluationList as $item):
                        ?>
                        , '<?= $item->dept_location ?>'
                        <?php endforeach; ?>
                    ],
                    ['Persentase berdasarkan Dept/Location'
                        <?php
                        foreach ($objectiveEvaluationList as $item):
                        ?>
                        , <?= $item->percentage ?>
                        <?php endforeach; ?>
                    ]

                ],
                type: 'bar'
            },
            bar: {
                width: {
                    ratio: 0.45 // this makes bar width 50% of length between ticks
                }
                // or
                //width: 100 // this makes bar width 100px
            },
            axis: {
                x: {
                    type: 'category',
                },
                y: {
                    max: 97.5,
                    tick: {
                        // format: d3.format("$,")
                        // https://c3js.org/samples/axes_y_tick_format.html
                        format: function (d) {
                            return d + " %";
                        }
                    }
                }
            },
            grid: {
                y: {
                    show: true
                }
            }
        });

        // // Instantiate and draw our chart, passing in some options.
        // setTimeout(function () {
        //     columnChart.load({
        //         columns: [
        //             ['data3', 130, -150, 200, 300, -200, 100]
        //         ]
        //     });
        // }, 1000);

        // Resize chart on sidebar width change
        $(".menu-toggle").on('click', function () {
            columnChart.resize();
        });
    });
</script>

<script>
    /*=========================================================================================
    File Name: radar.js
    Description: Chartjs radar chart
    ----------------------------------------------------------------------------------------
    Item Name: Modern Admin - Clean Bootstrap 4 Dashboard HTML Template
   Version: 3.0
    Author: PIXINVENT
    Author URL: http://www.themeforest.net/user/pixinvent
==========================================================================================*/

    // Radar chart
    // ------------------------------
    $(window).on("load", function () {

        // Chart Options
        // Chart Options
        var chartOptions = {
            responsive: true,
            maintainAspectRatio: false,
            responsiveAnimationDuration: 400,
            legend: {
                position: 'top',
            },
            title: {
                display: false,
                text: ''
            },
            scale: {
                reverse: false,
                ticks: {
                    callback: function (value, index, values) {
                        return (value.toFixed(2)) + "%";
                    },
                    beginAtZero: true
                }
            }
        };

        // Chart Data
        var chartData = {
            labels: ["2018-12", "2019-01", "2019-02", "2019-03", "2019-04", "2019-05", "2019-06"],
            datasets: [{
                label: "Actual",
                backgroundColor: "rgba(49,187,230, .3)",
                borderColor: "rgba(49,187,230, .3)",
                pointBorderColor: "#1d90b1",
                pointBackgroundColor: "#FFF",
                pointBorderWidth: 2,
                pointHoverBorderWidth: 2,
                pointRadius: 4,
                data: [0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5],
            }, {
                label: "Planned",
                backgroundColor: "rgba(115,103,186, .3)",
                borderColor: "rgba(115,103,186, .3)",
                pointBorderColor: "#5f559b",
                pointBackgroundColor: "#FFF",
                pointBorderWidth: 2,
                pointHoverBorderWidth: 2,
                pointRadius: 4,
                data: [0.8, 0.9, 0.85, 0.75, 0.85, 0.85, 0.85],
            },]
        };

        var chartData_ispo = {
            labels: ["P1", "P2", "P3", "P4", "P5", "P6", "P7"],
            datasets: [{
                label: "ISPO",
                backgroundColor: "rgba(49,187,230, .3)",
                borderColor: "rgba(49,187,230, .3)",
                pointBorderColor: "#1d90b1",
                pointBackgroundColor: "#FFF",
                pointBorderWidth: 2,
                pointHoverBorderWidth: 2,
                pointRadius: 2,
                data: [
                    <?= number_format(($percentages_ispo[1] ?? 0) * 100, 2, '.', ''); ?>,
                    <?= number_format(($percentages_ispo[2] ?? 0) * 100, 2, '.', ''); ?>,
                    <?= number_format(($percentages_ispo[3] ?? 0) * 100, 2, '.', ''); ?>,
                    <?= number_format(($percentages_ispo[4] ?? 0) * 100, 2, '.', ''); ?>,
                    <?= number_format(($percentages_ispo[5] ?? 0) * 100, 2, '.', ''); ?>,
                    <?= number_format(($percentages_ispo[6] ?? 0) * 100, 2, '.', ''); ?>,
                    <?= number_format(($percentages_ispo[7] ?? 0) * 100, 2, '.', ''); ?>,
                ],
            },]
        };

        var chartData_rspo = {
            labels: ["P1", "P2", "P3", "P4", "P5", "P6", "P7"],
            datasets: [{
                label: "RSPO",
                backgroundColor: "rgba(49,187,230, .3)",
                borderColor: "rgba(49,187,230, .3)",
                pointBorderColor: "#1d90b1",
                pointBackgroundColor: "#FFF",
                pointBorderWidth: 2,
                pointHoverBorderWidth: 2,
                pointRadius: 2,
                data: [
                    <?= number_format(($percentages_rspo[1] ?? 0) * 100, 2, '.', ''); ?>,
                    <?= number_format(($percentages_rspo[2] ?? 0) * 100, 2, '.', ''); ?>,
                    <?= number_format(($percentages_rspo[3] ?? 0) * 100, 2, '.', ''); ?>,
                    <?= number_format(($percentages_rspo[4] ?? 0) * 100, 2, '.', ''); ?>,
                    <?= number_format(($percentages_rspo[5] ?? 0) * 100, 2, '.', ''); ?>,
                    <?= number_format(($percentages_rspo[6] ?? 0) * 100, 2, '.', ''); ?>,
                    <?= number_format(($percentages_rspo[7] ?? 0) * 100, 2, '.', ''); ?>,
                ],
            },]
        };

        var chartData_smk3 = {
            labels: ["P1", "P2", "P3", "P4", "P5", "P6", "P7", "P8", "P9", "P10", "P11", "P12"],
            datasets: [{
                label: "SMK3",
                backgroundColor: "rgba(49,187,230, .3)",
                borderColor: "rgba(49,187,230, .3)",
                pointBorderColor: "#1d90b1",
                pointBackgroundColor: "#FFF",
                pointBorderWidth: 2,
                pointHoverBorderWidth: 2,
                pointRadius: 2,
                data: [
                    <?= number_format(($percentages_smk3[1] ?? 0) * 100, 2, '.', ''); ?>,
                    <?= number_format(($percentages_smk3[2] ?? 0) * 100, 2, '.', ''); ?>,
                    <?= number_format(($percentages_smk3[3] ?? 0) * 100, 2, '.', ''); ?>,
                    <?= number_format(($percentages_smk3[4] ?? 0) * 100, 2, '.', ''); ?>,
                    <?= number_format(($percentages_smk3[5] ?? 0) * 100, 2, '.', ''); ?>,
                    <?= number_format(($percentages_smk3[6] ?? 0) * 100, 2, '.', ''); ?>,
                    <?= number_format(($percentages_smk3[7] ?? 0) * 100, 2, '.', ''); ?>,
                    <?= number_format(($percentages_smk3[8] ?? 0) * 100, 2, '.', ''); ?>,
                    <?= number_format(($percentages_smk3[9] ?? 0) * 100, 2, '.', ''); ?>,
                    <?= number_format(($percentages_smk3[10] ?? 0) * 100, 2, '.', ''); ?>,
                    <?= number_format(($percentages_smk3[11] ?? 0) * 100, 2, '.', ''); ?>,
                    <?= number_format(($percentages_smk3[12] ?? 0) * 100, 2, '.', ''); ?>,
                ],
            },]
        };

        //Get the context of the Chart canvas element we want to select
        var ctx = $("#radar-chart");
        var ctx_ispo = $("#radar-chart-ispo");
        var ctx_rspo = $("#radar-chart-rspo");
        var ctx_smk3 = $("#radar-chart-smk3");

        // Create the chart
        var polarChart = new Chart(ctx, {
            type: 'radar',
            // Chart Options
            options: chartOptions,
            data: chartData
        });

        new Chart(ctx_ispo, {
            type: 'radar',
            // Chart Options
            options: chartOptions,
            data: chartData_ispo
        });

        new Chart(ctx_rspo, {
            type: 'radar',
            // Chart Options
            options: chartOptions,
            data: chartData_rspo
        });

        new Chart(ctx_smk3, {
            type: 'radar',
            // Chart Options
            options: chartOptions,
            data: chartData_smk3
        });
    });
</script>

<script>
    /*=========================================================================================
    File Name: pie.js
    Description: c3 pie chart
    ----------------------------------------------------------------------------------------
    Item Name: Modern Admin - Clean Bootstrap 4 Dashboard HTML Template
   Version: 3.0
    Author: PIXINVENT
    Author URL: http://www.themeforest.net/user/pixinvent
==========================================================================================*/

    // Pie chart
    // ------------------------------
    $(window).on("load", function () {

        // Callback that creates and populates a data table, instantiates the pie chart, passes in the data and draws it.
        var pieChart = c3.generate({
            bindto: '#pie-chart',
            color: {
                pattern: ['#25b7e5', '#FECEA8', '#FF847C']
            },

            // Create the data table.
            data: {
                // iris data from R
                columns: [
                    ["Manual", 30],
                ],
                type: 'pie',
                onclick: function (d, i) {
                    console.log("onclick", d, i);
                },
                onmouseover: function (d, i) {
                    console.log("onmouseover", d, i);
                },
                onmouseout: function (d, i) {
                    console.log("onmouseout", d, i);
                }
            }
        });

        // Instantiate and draw our chart, passing in some options.
        setTimeout(function () {
            pieChart.load({
                columns: [
                    ['data1', 30],
                    ['data2', 70],
                ]
            });
        }, 1500);

        setTimeout(function () {
            pieChart.unload({
                ids: 'data1'
            });
            pieChart.unload({
                ids: 'data2'
            });
        }, 2500);

        // Resize chart on sidebar width change
        $(".menu-toggle").on('click', function () {
            pieChart.resize();
        });
    });
</script>

</body>
</html>
