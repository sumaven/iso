<!DOCTYPE html>
<html lang="en">

<?php echo view('_partials/header'); ?>

<body class="no-skin">

<?php echo view('_partials/navbar'); ?>

<div class="main-container ace-save-state" id="main-container">
    <script type="text/javascript">
        try {
            ace.settings.loadState('main-container')
        } catch (e) {
        }
    </script>

    <?php echo view('_partials/sidebar'); ?>

    <div class="main-content">
        <div class="main-content-inner">
            <div class="breadcrumbs ace-save-state" id="breadcrumbs">
                <ul class="breadcrumb">
                    <li>
                        <i class="ace-icon fa fa-home home-icon"></i>
                        <a href="#">Home</a>
                    </li>
                    <li class="active"><?= $module_title ?></li>
                </ul><!-- /.breadcrumb -->
            </div>

            <div class="page-content">
                <div class="ace-settings-container" id="ace-settings-container">
                    <div class="btn btn-app btn-xs btn-warning ace-settings-btn" id="ace-settings-btn">
                        <i class="ace-icon fa fa-cog bigger-130"></i>
                    </div>

                    <div class="ace-settings-box clearfix" id="ace-settings-box">
                        <div class="pull-left width-50">
                            <div class="ace-settings-item">
                                <div class="pull-left">
                                    <select id="skin-colorpicker" class="hide">
                                        <option data-skin="no-skin" value="#438EB9">#438EB9</option>
                                        <option data-skin="skin-1" value="#222A2D">#222A2D</option>
                                        <option data-skin="skin-2" value="#C6487E">#C6487E</option>
                                        <option data-skin="skin-3" value="#D0D0D0">#D0D0D0</option>
                                    </select>
                                </div>
                                <span>&nbsp; Choose Skin</span>
                            </div>

                            <div class="ace-settings-item">
                                <input type="checkbox" class="ace ace-checkbox-2 ace-save-state"
                                       id="ace-settings-navbar" autocomplete="off"/>
                                <label class="lbl" for="ace-settings-navbar"> Fixed Navbar</label>
                            </div>

                            <div class="ace-settings-item">
                                <input type="checkbox" class="ace ace-checkbox-2 ace-save-state"
                                       id="ace-settings-sidebar" autocomplete="off"/>
                                <label class="lbl" for="ace-settings-sidebar"> Fixed Sidebar</label>
                            </div>

                            <div class="ace-settings-item">
                                <input type="checkbox" class="ace ace-checkbox-2 ace-save-state"
                                       id="ace-settings-breadcrumbs" autocomplete="off"/>
                                <label class="lbl" for="ace-settings-breadcrumbs"> Fixed Breadcrumbs</label>
                            </div>

                            <div class="ace-settings-item">
                                <input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-rtl"
                                       autocomplete="off"/>
                                <label class="lbl" for="ace-settings-rtl"> Right To Left (rtl)</label>
                            </div>

                            <div class="ace-settings-item">
                                <input type="checkbox" class="ace ace-checkbox-2 ace-save-state"
                                       id="ace-settings-add-container" autocomplete="off"/>
                                <label class="lbl" for="ace-settings-add-container">
                                    Inside
                                    <b>.container</b>
                                </label>
                            </div>
                        </div><!-- /.pull-left -->

                        <div class="pull-left width-50">
                            <div class="ace-settings-item">
                                <input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-hover"
                                       autocomplete="off"/>
                                <label class="lbl" for="ace-settings-hover"> Submenu on Hover</label>
                            </div>

                            <div class="ace-settings-item">
                                <input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-compact"
                                       autocomplete="off"/>
                                <label class="lbl" for="ace-settings-compact"> Compact Sidebar</label>
                            </div>

                            <div class="ace-settings-item">
                                <input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-highlight"
                                       autocomplete="off"/>
                                <label class="lbl" for="ace-settings-highlight"> Alt. Active Item</label>
                            </div>
                        </div><!-- /.pull-left -->
                    </div><!-- /.ace-settings-box -->
                </div><!-- /.ace-settings-container -->

                <div class="page-header">
                    <h1>
                        <small>
                            <i class="ace-icon fa fa-angle-double-right"></i>
                            Treatment Document
                        </small>
                        <small>
                            <i class="ace-icon fa fa-angle-double-right"></i>
                            <?= $submodule_title ?>
                        </small>
                        <small><i class="ace-icon fa fa-angle-double-right"></i> Add New </small>
                    </h1>
                </div><!-- /.page-header -->

                <!-- PAGE CONTENT BEGINS -->
                <?php
                if (session()->get('id') == $row->id_employee && $row->step != 0) {
                    $readonly = "disabled";
                } else {
                    $readonly = "";
                }
                ?>

                <form class="form-horizontal container-fluid" role="form"
                      action="<?php echo base_url("treatment_document/rekaman/process_edit") ?>" method="POST">

                    <input style="display:none" type="text" name="id" id="form-field-1"/>

                    <div class="row">
                        <div class="col-xs-6">
                            <div class="form-group">
                                <label class="col-sm-3 control-label no-padding-right" > <?= lang("reference") ?> </label>

                                <div class="col-sm-9">
                                    <select name="referensi" class="form-control" <?= $readonly ?> id="form-field-select-1">
                                        <option value="" selected="selected">Please select...</option>
                                        <?php
                                        $results = $this->db->query("SELECT * FROM referensi")->result();
                                        foreach ($results as $item) {
                                            $val = $item->id;
                                            $select = $item->referensi;
                                            echo "<option value='$val'>$select</option>";
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                        </div><!-- /.col -->
                        <div class="col-xs-6">
                            <div class="form-group">
                                <label class="col-sm-3 control-label no-padding-right" > <?= lang("header_title") ?> </label>

                                <div class="col-sm-9">
                                    <input type="text" name="header_title" id="form-field-1" placeholder="Judul Header"
                                           class="form-control" <?= $readonly ?>/>
                                </div>
                            </div>
                        </div><!-- /.col -->
                    </div><!-- /.row -->

                    <div class="row">
                        <div class="col-xs-12">
                            <div class="form-group">
                                <label> <?= lang("class_project") ?> </label>
                                <div class="wysiwyg-editor" name="class_project"></div>
                                <textarea style="display:none" name="class_project" <?= $readonly ?>></textarea>
                            </div>
                        </div>
                    </div><!-- /.row -->
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="form-group">
                                <label> <?= lang("date_time_location") ?> </label>
                                <div class="wysiwyg-editor" name="date_time_location"></div>
                                <textarea style="display:none" name="date_time_location" <?= $readonly ?>></textarea>
                            </div>
                        </div>
                    </div><!-- /.row -->
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="form-group">
                                <label> <?= lang("goal") ?> </label>
                                <div class="wysiwyg-editor" name="goal"></div>
                                <textarea style="display:none" name="goal" <?= $readonly ?>></textarea>
                            </div>
                        </div>
                    </div><!-- /.row -->
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="form-group">
                                <label> <?= lang("attendees") ?> </label>
                                <div class="wysiwyg-editor" name="attendees"></div>
                                <textarea style="display:none" name="attendees" <?= $readonly ?>></textarea>
                            </div>
                        </div>
                    </div><!-- /.row -->
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="form-group">
                                <label> <?= lang("description_activity") ?> </label>
                                <div class="wysiwyg-editor" name="description_activity"></div>
                                <textarea style="display:none" name="description_activity" <?= $readonly ?>></textarea>
                            </div>
                        </div>
                    </div><!-- /.row -->
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="form-group">
                                <label> <?= lang("result") ?> </label>
                                <div class="wysiwyg-editor" name="result"></div>
                                <textarea style="display:none" name="result" <?= $readonly ?>></textarea>
                            </div>
                        </div>
                    </div><!-- /.row -->
                    <div class="row">
                        <div class="col-xs-6">
                            <div class="form-group">
                                <label class="col-sm-3 control-label no-padding-right" > <?= lang("distribution_1") ?> </label>

                                <div class="col-sm-9">
                                    <select name="distribusi_1" class="form-control" <?= $readonly ?> id="form-field-select-1">
                                        <option value="" selected="selected">Please select...</option>
                                        <?php
                                        $results = $this->db->query("SELECT * FROM distribusi_user")->result();
                                        foreach ($results as $item) {
                                            $val = $item->id;
                                            $select = $item->distribusi;
                                            echo "<option value='$val'>$select</option>";
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                        </div><!-- /.col -->
                        <div class="col-xs-6">
                            <div class="form-group">
                                <label class="col-sm-3 control-label no-padding-right" > <?= lang("distribution_2") ?> </label><div class="col-sm-9">
                                    <select name="distribusi_2" class="form-control" <?= $readonly ?> id="form-field-select-1">
                                        <option value="" selected="selected">Please select...</option>
                                        <?php
                                        $results = $this->db->query("SELECT * FROM distribusi_user")->result();
                                        foreach ($results as $item) {
                                            $val = $item->id;
                                            $select = $item->distribusi;
                                            echo "<option value='$val'>$select</option>";
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                        </div><!-- /.col -->
                    </div><!-- /.row -->
                    <div class="row">
                        <div class="col-xs-6">
                            <div class="form-group">
                                <label class="col-sm-3 control-label no-padding-right" > <?= lang("approval_1") ?> </label>

                                <div class="col-sm-9">
                                    <select name="approval_1" class="form-control" <?= $readonly ?> id="form-field-select-1">
                                        <option value="" selected="selected">Please select...</option>
                                        <?php
                                        $results = $this->db->query("SELECT * FROM approval")->result();
                                        foreach ($results as $item) {
                                            $val = $item->id;
                                            $select = $item->approval;
                                            echo "<option value='$val'>$select</option>";
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                        </div><!-- /.col -->
                        <div class="col-xs-6">
                            <div class="form-group">
                                <label class="col-sm-3 control-label no-padding-right" > <?= lang("approval_2") ?> </label>

                                <div class="col-sm-9">
                                    <select name="approval_2" class="form-control" <?= $readonly ?> id="form-field-select-1">
                                        <option value="" selected="selected">Please select...</option>
                                        <?php
                                        $results = $this->db->query("SELECT * FROM approval")->result();
                                        foreach ($results as $item) {
                                            $val = $item->id;
                                            $select = $item->approval;
                                            echo "<option value='$val'>$select</option>";
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                        </div><!-- /.col -->
                    </div><!-- /.row -->

                    <?php
                    if (session()->get('id') == $row->id_employee && $row->step != 0) {
                        ?>
                        <div class="row">
                            <div class="col-xs-6">
                            <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" >
                                        Komentar </label>

                                    <div class="col-sm-9">
                                        <textarea name="komentar" placeholder="Komentar"
                                                  class="form-control" ></textarea>
                                    </div>
                                </div>
                            </div><!-- /.col -->
                        </div>
                        <?php
                    }
                    ?>

                    <?php
                    if (session()->get('id') == $row->id_employee) {
                        ?>
                        <div class="row clearfix form-actions">
                            <div class="col-md-offset-3 col-md-12">
                                <?php
                                if ($row->step != 0) {
                                    ?>
                                    <button class="btn btn-success" name="btn_submit" value="setuju" type="submit "/>
                                    <i class="ace-icon fa fa-check bigger-110"></i>
                                    Setuju

                                    <button class="btn btn-danger" name="btn_submit" value="kembalikan" type="submit"/>
                                    <i class="ace-icon fa fa-check bigger-110"></i>
                                    Kembalikan
                                    <?php
                                } else {
                                    ?>
                                    <button class="btn btn-info" name="btn_submit" value="edit" type="submit"/>
                                    <i class="ace-icon fa fa-check bigger-110"></i>
                                    Submit
                                    <?php
                                } ?>
                            </div>
                        </div>
                        <?php
                    }
                    ?>
                </form>
            </div><!-- /.page-content -->
        </div>
    </div><!-- /.main-content -->

    

    <a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
        <i class="ace-icon fa fa-angle-double-up icon-only bigger-110"></i>
    </a>
</div><!-- /.main-container -->

<!-- basic scripts -->

<!--[if !IE]> -->
<script src="<?php echo base_url("assets/assets"); ?>/js/jquery-2.1.4.min.js"></script>

<!-- <![endif]-->

<!--[if IE]>
<script src="assets/js/jquery-1.11.3.min.js"></script>
<![endif]-->
<script type="text/javascript">
    if ('ontouchstart' in document.documentElement) document.write("<script src='<?php echo base_url("assets/assets"); ?>/js/jquery.mobile.custom.min.js'>" + "<" + "/script>");
</script>
<script src="<?php echo base_url("assets/assets"); ?>/js/bootstrap.min.js"></script>

<!-- page specific plugin scripts -->

<!--[if lte IE 8]>
<script src="assets/js/excanvas.min.js"></script>
<![endif]-->
<script src="<?php echo base_url("assets/assets"); ?>/js/jquery-ui.custom.min.js"></script>
<script src="<?php echo base_url("assets/assets"); ?>/js/jquery.ui.touch-punch.min.js"></script>
<script src="<?php echo base_url("assets/assets"); ?>/js/markdown.min.js"></script>
<script src="<?php echo base_url("assets/assets"); ?>/js/bootstrap-markdown.min.js"></script>
<script src="<?php echo base_url("assets/assets"); ?>/js/jquery.hotkeys.index.min.js"></script>
<script src="<?php echo base_url("assets/assets"); ?>/js/bootstrap-wysiwyg.min.js"></script>
<script src="<?php echo base_url("assets/assets"); ?>/js/bootbox.js"></script>
<script src="<?php echo base_url("assets/assets"); ?>/js/bootstrap-wysiwyg.min.js"></script>
 <!-- ace scripts -->
<script src="<?php echo base_url("assets/assets"); ?>/js/ace-elements.min.js"></script>
<script src="<?php echo base_url("assets/assets"); ?>/js/ace.min.js"></script>

<script src="<?php echo base_url("assets/assets"); ?>/js/jquery.maskedinput.min.js"></script>
<script src="<?php echo base_url("assets/assets"); ?>/js/bootstrap-datepicker.min.js"></script>
<script>
    $("input[placeholder='yyyy-mm-dd']").datepicker({
        autoclose: true,
        format: 'yyyy-mm-dd',
        orientation: "bottom",
        todayHighlight: true,
    });
    $("input[placeholder='yyyy-mm-dd']").mask("9999-99-99");
    $("input[placeholder='yyyy-mm-dd']").css("width", "100%");
</script>

<!-- inline scripts related to this page -->
<script type="text/javascript">
    jQuery(function ($) {

        $('textarea[data-provide="markdown"]').each(function () {
            var $this = $(this);

            if ($this.data('markdown')) {
                $this.data('markdown').showEditor();
            } else $this.markdown()

            $this.parent().find('.btn').addClass('btn-white');
        })

        function parseMe(value) {
            return value.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;');
        }

        var convertToHtmlTag = function(convertToHtmlTag){
            var passValue = $("<span />", { html: convertToHtmlTag }).text();
            passValue = passValue.replace('&nbsp;', " ")
            return passValue
            //return document.createElement("span").innerText;
        };

        function showErrorAlert(reason, detail) {
            var msg = '';
            if (reason === 'unsupported-file-type') {
                msg = "Unsupported format " + detail;
            } else {
                //console.log("error uploading file", reason, detail);
            }
            $('<div class="alert"> <button type="button" class="close" data-dismiss="alert">&times;</button>' +
                '<strong>File upload error</strong> ' + msg + ' </div>').prependTo('#alerts');
        }

        $(".wysiwyg-editor").on('keyup DOMSubtreeModified', function () {
            $(this).siblings("textarea").val(parseMe($(this).html()))
        });

        $(".wysiwyg-editor").ace_wysiwyg({
            toolbar: [
                'font',
                null,
                'fontSize',
                null,
                {name: 'bold', className: 'btn-info'},
                {name: 'italic', className: 'btn-info'},
                {name: 'strikethrough', className: 'btn-info'},
                {name: 'underline', className: 'btn-info'},
                null,
                {name: 'insertunorderedlist', className: 'btn-success'},
                {name: 'insertorderedlist', className: 'btn-success'},
                {name: 'outdent', className: 'btn-purple'},
                {name: 'indent', className: 'btn-purple'},
                null,
                {name: 'justifyleft', className: 'btn-primary'},
                {name: 'justifycenter', className: 'btn-primary'},
                {name: 'justifyright', className: 'btn-primary'},
                {name: 'justifyfull', className: 'btn-inverse'},
                null,
                {name: 'createLink', className: 'btn-pink'},
                {name: 'unlink', className: 'btn-pink'},
                null,
                {name: 'insertImage', className: 'btn-success'},
                null,
                'foreColor',
                null,
                {name: 'undo', className: 'btn-grey'},
                {name: 'redo', className: 'btn-grey'}
            ],
            'wysiwyg': {
                fileUploadError: showErrorAlert
            }
        }).prev().addClass('wysiwyg-style2');

        //RESIZE IMAGE

        //Add Image Resize Functionality to Chrome and Safari
        //webkit browsers don't have image resize functionality when content is editable
        //so let's add something using jQuery UI resizable
        //another option would be opening a dialog for user to enter dimensions.
        if (typeof jQuery.ui !== 'undefined' && ace.vars['webkit']) {

            var lastResizableImg = null;

            function destroyResizable() {
                if (lastResizableImg == null) return;
                lastResizableImg.resizable("destroy");
                lastResizableImg.removeData('resizable');
                lastResizableImg = null;
            }

            var enableImageResize = function () {
                $('.wysiwyg-editor')
                    .on('mousedown', function (e) {
                        var target = $(e.target);
                        if (e.target instanceof HTMLImageElement) {
                            if (!target.data('resizable')) {
                                target.resizable({
                                    aspectRatio: e.target.width / e.target.height,
                                });
                                target.data('resizable', true);

                                if (lastResizableImg != null) {
                                    //disable previous resizable image
                                    lastResizableImg.resizable("destroy");
                                    lastResizableImg.removeData('resizable');
                                }
                                lastResizableImg = target;
                            }
                        }
                    })
                    .on('click', function (e) {
                        if (lastResizableImg != null && !(e.target instanceof HTMLImageElement)) {
                            destroyResizable();
                        }
                    })
                    .on('keydown', function () {
                        destroyResizable();
                    });
            };

            enableImageResize();

        }

        $('input[name=id]').val(convertToHtmlTag('<?= filter_output($row->id) ?>'));
        $('div[name=class_project]').html(convertToHtmlTag('<?= filter_output($row->class_project) ?>'));
        $('div[name=date_time_location]').html(convertToHtmlTag('<?= filter_output($row->date_time_location) ?>'));
        $('div[name=goal]').html(convertToHtmlTag('<?= filter_output($row->goal) ?>'));
        $('div[name=attendees]').html(convertToHtmlTag('<?= filter_output($row->attendees) ?>'));
        $('div[name=description_activity]').html(convertToHtmlTag('<?= filter_output($row->description_activity) ?>'));
        $('div[name=result]').html(convertToHtmlTag('<?= filter_output($row->result) ?>'));

        $('input[name=header_title]').val(convertToHtmlTag('<?= filter_output($row->header_title) ?>'));

        $('select[name=referensi]').val(convertToHtmlTag('<?= filter_output($row->referensi) ?>'));
        $('select[name=distribusi_1]').val(convertToHtmlTag('<?= filter_output($row->distribusi_1) ?>'));
        $('select[name=distribusi_2]').val(convertToHtmlTag('<?= filter_output($row->distribusi_2) ?>'));
        $('select[name=approval_1]').val(convertToHtmlTag('<?= filter_output($row->approval_1) ?>'));
        $('select[name=approval_2]').val(convertToHtmlTag('<?= filter_output($row->approval_2) ?>'));

        <?php
        if (session()->get('id') == $row->id_employee && $row->step != 0) {
            echo <<<EOT
        $('.wysiwyg-editor').attr("contenteditable", false)
        $('.wysiwyg-editor').css("background-color", "#f5f5f5")
        $('.wysiwyg-editor').css("color", "#a29f9d")
EOT;
        }
        ?>
    });
</script>
</body>
</html>
