<!DOCTYPE html>
<html lang="en" dir="ltr">
    <head>
        <meta charset="utf-8">
        <title>Treatment Document</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

        <style type="text/css">
            table { page-break-inside:avoid }
            tr    { page-break-inside:avoid; page-break-after:auto }
            thead { display:table-header-group }
            tfoot { display:table-footer-group }
        </style>

    </head>

    <body>

        <table border="1"  cellspacing="0" cellpadding="2" style="width: 100%; margin-bottom: 30px; font-size: 10px;">
            <tr>
                <td style="width: 20%; height: 60px; text-align: center;" rowspan="4">
                    <img src="<?php echo base_url('assets/images/logo.jpg'); ?>" width="100%">
                </td>
                <td style="width: 50%; text-align: center; font-weight: bold;" rowspan="4">
                    Asia Global Servis
                </td>
                <td style="width: 30%; height: 20px;">
                    Doc :
                </td>
            </tr>
            <tr>
                <td style="height: 20px;">
                    Rev :
                </td>
            </tr>
            <tr>
                <td>
                    Referensi :
                </td>
            </tr>
            <tr>
                <td>
                    Klausul :
                </td>
            </tr>
            <tr>
                <td colspan="3" style="height: 30px; vertical-align: center">&nbsp;&nbsp;&nbsp;&nbsp;Title : <?= clean_string($header_title) ?></td>
            </tr>
        </table>

        <table cellspacing="0" cellpadding="5" border="1" style="width: 100%; font-size: 10px;margin-top: 20px;">
            <tr>
                <td style="background-color: #ccc; height: 20px; width: 100%; padding-left: 20px;">
                    Department
                </td>
            </tr>
            <tr>
                <td>
                    <div style="min-height: 50px; padding-left: 20px;">
                        <?= clean_string($department) ?>
                    </div>
                </td>
            </tr>
        </table>

        <table cellspacing="0" cellpadding="5" border="1" style="width: 100%; font-size: 10px;margin-top: 20px;">
            <tr>
                <td style="background-color: #ccc; height: 20px; width: 100%; padding-left: 20px;">
                    Revision
                </td>
            </tr>
            <tr>
                <td>
                    <div style="min-height: 50px; padding-left: 20px;">
                        <?= clean_string($revision) ?>
                    </div>
                </td>
            </tr>
        </table>

        <table cellspacing="0" cellpadding="5" border="1" style="width: 100%; font-size: 10px;margin-top: 20px;">
            <tr>
                <td style="background-color: #ccc; height: 20px; width: 100%; padding-left: 20px;">
                    Date Issued
                </td>
            </tr>
            <tr>
                <td>
                    <div style="min-height: 50px; padding-left: 20px;">
                        <?= clean_string($date_issued) ?>
                    </div>
                </td>
            </tr>
        </table>

        <table cellspacing="0" cellpadding="5" border="1" style="width: 100%; font-size: 10px;margin-top: 20px;">
            <tr>
                <td style="background-color: #ccc; height: 20px; width: 100%; padding-left: 20px;">
                    Scope
                </td>
            </tr>
            <tr>
                <td>
                    <div style="min-height: 50px; padding-left: 20px;">
                        <?= clean_string($scope) ?>
                    </div>
                </td>
            </tr>
        </table>

        <table cellspacing="0" cellpadding="5" border="1" style="width: 100%; font-size: 10px;margin-top: 20px;">
            <tr>
                <td style="background-color: #ccc; height: 20px; width: 100%; padding-left: 20px;">
                    Definition
                </td>
            </tr>
            <tr>
                <td>
                    <div style="min-height: 50px; padding-left: 20px;">
                        <?= clean_string($definition) ?>
                    </div>
                </td>
            </tr>
        </table>

        <table cellspacing="0" cellpadding="5" border="1" style="width: 100%; font-size: 10px;margin-top: 20px;">
            <tr>
                <td style="background-color: #ccc; height: 20px; width: 100%; padding-left: 20px;">
                    General Information
                </td>
            </tr>
            <tr>
                <td>
                    <div style="min-height: 50px; padding-left: 20px;">
                        <?= clean_string($general_information) ?>
                    </div>
                </td>
            </tr>
        </table>

        <table cellspacing="0" cellpadding="5" border="1" style="width: 100%; font-size: 10px;margin-top: 20px;">
            <tr>
                <td style="background-color: #ccc; height: 20px; width: 100%; padding-left: 20px;">
                    Procedure
                </td>
            </tr>
            <tr>
                <td>
                    <div style="min-height: 50px; padding-left: 20px;">
                        <?= clean_string($prosedure) ?>
                    </div>
                </td>
            </tr>
        </table>

        <table cellspacing="0" cellpadding="5" border="1" style="width: 100%; font-size: 10px;margin-top: 20px;">
            <tr>
                <td style="background-color: #ccc; height: 20px; width: 100%; padding-left: 20px;">
                    Recording
                </td>
            </tr>
            <tr>
                <td>
                    <div style="min-height: 50px; padding-left: 20px;">
                        <?= clean_string($recording) ?>
                    </div>
                </td>
            </tr>
        </table>

        <table cellspacing="0" cellpadding="5" border="1" style="width: 100%; font-size: 10px;margin-top: 20px;">
            <tr>
                <td style="background-color: #ccc; height: 20px; width: 100%; padding-left: 20px;">
                    Reference
                </td>
            </tr>
            <tr>
                <td>
                    <div style="min-height: 50px; padding-left: 20px;">
                        <?= clean_string($reference) ?>
                    </div>
                </td>
            </tr>
        </table>

        <br /><br />

        <div style="width:50%; margin-right: 0px; margin-left: auto;">
            <table cellspacing="0" cellpadding="0" border="1" style="width: 100%; font-size: 10px;">
                <tr style="width: 100%">
                    <td align="center" style="background-color: #ccc; height: 20px; width: 15%; text-align: center;">
                        Persetujuan
                    </td>
                    <td align="center" style="background-color: #ccc; height: 20px; width: 30%; text-align: center;">

                    </td>
                    <td align="center" style="background-color: #ccc; height: 20px; width: 30%; text-align: center;">

                    </td>
                </tr>

                <tr>
                    <td style="height: 75px; text-align: center;">
                         Tanda tangan
                    </td>
                    <td>

                    </td>
                    <td>

                    </td>
                </tr>


            </table>
        </div>
    </body>
</html>
