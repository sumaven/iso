<!DOCTYPE html>
<html lang="en" dir="ltr">
    <head>
        <meta charset="utf-8">
        <title>Usulan Dokumen</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <style>
            body {
                font-size: 12px;
            }

            table { page-break-inside:avoid }
            tr    { page-break-inside:avoid; page-break-after:auto }
            thead { display:table-header-group }
            tfoot { display:table-footer-group }
        </style>
    </head>
    <body style="padding: 40px">

        <div>
            <img src="<?= $logo->url ?>" width="200px">
        </div>

        <div align="center" style="margin-bottom: -10px">
            <p style="font-weight: bold">FORMULIR PERUBAHAN DAN PEMBUATAN DOKUMEN</p>
        </div>

        <table border="1"  cellspacing="0" cellpadding="2" style="width: 100%; margin-bottom: 10px; font-size: 10px;">
            <tr>
                <td style="width: 40%; height: 35px; text-align: center;">
                    <?= $header_doc_no ?>
                </td>
                <td style="width: 30%; text-align: center; font-weight: bold;">
                    FORMULIR
                </td>
                <td style="width: 30%; text-align: center;">
                    <?= $header_rev_no ?>
                </td>
            </tr>
        </table>

        <table style="margin-bottom: 10px;" cellpadding="3">
            <tr>
                <td>NAMA PENGUSUL</td>
                <td>:</td>
                <td><?= clean_string($nama_pengusul) ?></td>
            </tr>
            <tr>
                <td>NAMA BAGIAN</td>
                <td>:</td>
                <td><?= clean_string($nama_bagian) ?></td>
            </tr>
            <tr>
                <td>JUDUL DOKUMEN</td>
                <td>:</td>
                <td><?= clean_string($judul_dokumen) ?></td>
            </tr>
            <tr>
                <td>NOMOR DOKUMEN</td>
                <td>:</td>
                <td><?= clean_string($doc_no) ?></td>
            </tr>
            <tr>
                <td>VERSI</td>
                <td>:</td>
                <td><?= clean_string($versi) ?></td>
        </table>


        <table cellspacing="0" cellpadding="5" border="1" style="width: 100%; font-size: 10px; text-align: center">
            <tr>
                <td style="background-color: #ccc; height: 20px; width: 100%; padding-left: 20px;" colspan="7">
                    USULAN PERUBAHAN
                </td>
            </tr>
            <tr>
                <td rowspan="2">
                    NO
                </td>
                <td colspan="2">
                    NOMOR
                </td>
                <td colspan="4">
                    DESKRIPSI PERUBAHAN
                </td>
            </tr>
            <tr>
                <td>HALAMAN</td>
                <td>BAGIAN</td>
                <td colspan="2">KLAUSUL AWAL</td>
                <td colspan="2">KLAUSUL PERUBAHAN</td>
            </tr>
            <?php
                echo "\n";
                foreach ($rows as $row) {
                    $print_out = <<<EOT
                    <tr>
                        <td>$row->no</td>
                        <td>$row->no_halaman</td>
                        <td>$row->no_bagian</td>
                        <td>$row->klausul_awal_1</td>
                        <td>$row->klausul_awal_2</td>
                        <td>$row->klausul_perubahan_1</td>
                        <td>$row->klausul_perubahan_2</td>
                    </tr>
EOT;
                    echo $print_out . "\n";
                }
            ?>

            <!-- <tr>
                <td>2</td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr> -->

        </table>

        <br /><br />

        <table cellspacing="0" cellpadding="0" border="1" style="width: 100%; font-size: 10px;">
            <tr>
                <td align="center" style="background-color: #ccc; height: 20px; text-align: center;">
                    Diusulkan oleh
                </td>
                <td align="center" style="background-color: #ccc; height: 20px; text-align: center;">
                    Diperiksa oleh
                </td>
                <td align="center" style="background-color: #ccc; height: 20px; text-align: center;">
                    Disetujui oleh
                </td>
            </tr>

            <tr>
                <td style="text-align: center;">
                    <br><br><br><br>
                     (......................................................)
                </td>
                <td style="text-align: center;">
                    <br><br><br><br>
                     (......................................................)
                </td>
                <td style="text-align: center;">
                    <br><br><br><br>
                     (......................................................)
                </td>
            </tr>


        </table>


    </body>
</html>
