<!DOCTYPE html>
<html lang="en" dir="ltr">

<head>
    <meta charset="utf-8">
    <title>Treatment Document</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <style type="text/css">
        table {
            page-break-inside: avoid
        }

        tr {
            page-break-inside: avoid;
            page-break-after: auto
        }

        thead {
            display: table-header-group
        }

        tfoot {
            display: table-footer-group
        }
    </style>

</head>

<body>

    <table border="1" cellspacing="0" cellpadding="2" style="width: 100%; margin-bottom: 30px; font-size: 10px;">
        <tr>
            <td style="width: 20%; height: 60px; text-align: center;" rowspan="4">
                <img src="<?= $logo->url ?>" width="100%">
            </td>
            <td style="width: 50%; text-align: center; font-weight: bold;" rowspan="4">
                Asia Global Servis
            </td>
            <td style="width: 30%; height: 20px;">
                Doc :
            </td>
        </tr>
        <tr>
            <td style="height: 20px;">
                Rev :
            </td>
        </tr>
        <tr>
            <td>
                Referensi :
            </td>
        </tr>
        <tr>
            <td>
                Klausul :
            </td>
        </tr>
        <tr>
            <td colspan="3" style="height: 30px; vertical-align: center">&nbsp;&nbsp;&nbsp;&nbsp;Title : </td>
        </tr>
    </table>

    <div class="row">
        <div class="col">
            <table cellspacing="0" cellpadding="5" border="1" style="width: 100%; font-size: 10px;margin-top: 20px;">
                <tr>
                    <td style="background-color: #ccc; height: 20px; width: 100%; padding-left: 20px;">
                        <?= lang("planning.process_name") ?>
                    </td>
                </tr>
                <tr>
                    <td style="height: 100px; padding-left: 20px;">
                        <?= clean_string($process_name) ?>
                    </td>
                </tr>
            </table>
        </div>
        <div class="col">
            <table cellspacing="0" cellpadding="5" border="1" style="width: 100%; font-size: 10px;margin-top: 20px;">
                <tr>
                    <td style="background-color: #ccc; height: 20px; width: 100%; padding-left: 20px;">
                        <?= lang("planning.activity") ?>
                    </td>
                </tr>
                <tr>
                    <td style="height: 100px; padding-left: 20px;">
                        <?= clean_string($activity) ?>
                    </td>
                </tr>
            </table>
        </div>
    </div>

    <div class="row">
        <div class="col">
            <table cellspacing="0" cellpadding="5" border="1" style="width: 100%; font-size: 10px;margin-top: 20px;">
                <tr>
                    <td style="background-color: #ccc; height: 20px; width: 100%; padding-left: 20px;">
                        <?= lang("planning.qhse_risk") ?>
                    </td>
                </tr>
                <tr>
                    <td style="height: 100px; padding-left: 20px;">
                        <?= clean_string($qhse_risk) ?>
                    </td>
                </tr>
            </table>
        </div>
        <div class="col">
            <table cellspacing="0" cellpadding="5" border="1" style="width: 100%; font-size: 10px;margin-top: 20px;">
                <tr>
                    <td style="background-color: #ccc; height: 20px; width: 100%; padding-left: 20px;">
                        <?= lang("planning.category") ?>
                    </td>
                </tr>
                <tr>
                    <td style="height: 100px; padding-left: 20px;">
                        <?= clean_string($category) ?>
                    </td>
                </tr>
            </table>
        </div>
    </div>

    <div class="row">
        <div class="col">
            <table cellspacing="0" cellpadding="5" border="1" style="width: 100%; font-size: 10px;margin-top: 20px;">
                <tr>
                    <td style="background-color: #ccc; height: 20px; width: 100%; padding-left: 20px;">
                        <?= lang("planning.qhse_issue_category") ?>
                    </td>
                </tr>
                <tr>
                    <td style="height: 100px; padding-left: 20px;">
                        <?= clean_string($qhse_issue_category) ?>
                    </td>
                </tr>
            </table>
        </div>
        <div class="col">
            <table cellspacing="0" cellpadding="5" border="1" style="width: 100%; font-size: 10px;margin-top: 20px;">
                <tr>
                    <td style="background-color: #ccc; height: 20px; width: 100%; padding-left: 20px;">
                        <?= lang("planning.qhse_potential_issue_description") ?>
                    </td>
                </tr>
                <tr>
                    <td style="height: 100px; padding-left: 20px;">
                        <?= clean_string($qhse_potential_issue_description) ?>
                    </td>
                </tr>
            </table>
        </div>
    </div>

    <div class="row">
        <div class="col">
            <table cellspacing="0" cellpadding="5" border="1" style="width: 100%; font-size: 10px;margin-top: 20px;">
                <tr>
                    <td style="background-color: #ccc; height: 20px; width: 100%; padding-left: 20px;">
                        <?= lang("planning.operational_condition") ?>
                    </td>
                </tr>
                <tr>
                    <td style="height: 100px; padding-left: 20px;">
                        <?= clean_string($operational_condition) ?>
                    </td>
                </tr>
            </table>
        </div>
        <div class="col">
            <table cellspacing="0" cellpadding="5" border="1" style="width: 100%; font-size: 10px;margin-top: 20px;">
                <tr>
                    <td style="background-color: #ccc; height: 20px; width: 100%; padding-left: 20px;">
                        <?= lang("planning.risk_opportunity") ?>
                    </td>
                </tr>
                <tr>
                    <td style="height: 100px; padding-left: 20px;">
                        <?= clean_string($risk_opportunity) ?>
                    </td>
                </tr>
            </table>
        </div>
    </div>

    <div class="row">
        <div class="col">
            <table cellspacing="0" cellpadding="5" border="1" style="width: 100%; font-size: 10px;margin-top: 20px;">
                <tr>
                    <td style="background-color: #ccc; height: 20px; width: 100%; padding-left: 20px;">
                        <?= lang("planning.qhse_potential_impact") ?>
                    </td>
                </tr>
                <tr>
                    <td style="height: 100px; padding-left: 20px;">
                        <?= clean_string($qhse_potential_impact) ?>
                    </td>
                </tr>
            </table>
        </div>
        <div class="col">
            <table cellspacing="0" cellpadding="5" border="1" style="width: 100%; font-size: 10px;margin-top: 20px;">
                <tr>
                    <td style="background-color: #ccc; height: 20px; width: 100%; padding-left: 20px;">
                        <?= lang("planning.ro_impact_company") ?>
                    </td>
                </tr>
                <tr>
                    <td style="height: 100px; padding-left: 20px;">
                        <?= clean_string($ro_impact_company) ?>
                    </td>
                </tr>
            </table>
        </div>
    </div>

    <div class="row">
        <div class="col">
            <table cellspacing="0" cellpadding="5" border="1" style="width: 100%; font-size: 10px;margin-top: 20px;">
                <tr>
                    <td style="background-color: #ccc; height: 20px; width: 100%; padding-left: 20px;">
                        <?= lang("planning.interested_parties") ?>
                    </td>
                </tr>
                <tr>
                    <td style="height: 100px; padding-left: 20px;">
                        <?= clean_string($interested_parties) ?>
                    </td>
                </tr>
            </table>
        </div>
        <div class="col">
            <table cellspacing="0" cellpadding="5" border="1" style="width: 100%; font-size: 10px;margin-top: 20px;">
                <tr>
                    <td style="background-color: #ccc; height: 20px; width: 100%; padding-left: 20px;">
                        <?= lang("planning.compliance_description") ?>
                    </td>
                </tr>
                <tr>
                    <td style="height: 100px; padding-left: 20px;">
                        <?= clean_string($compliance_description) ?>
                    </td>
                </tr>
            </table>
        </div>
    </div>

    <div class="row">
        <div class="col">
            <table cellspacing="0" cellpadding="5" border="1" style="width: 100%; font-size: 10px;margin-top: 20px;">
                <tr>
                    <td style="background-color: #ccc; height: 20px; width: 100%; padding-left: 20px;">
                        <?= lang("planning.pre_consequences_benefit_initial_rating") ?>
                    </td>
                </tr>
                <tr>
                    <td style="height: 100px; padding-left: 20px;">
                        <?= clean_string($pre_consequences_benefit_initial_rating) ?>
                    </td>
                </tr>
            </table>
        </div>
        <div class="col">
            <table cellspacing="0" cellpadding="5" border="1" style="width: 100%; font-size: 10px;margin-top: 20px;">
                <tr>
                    <td style="background-color: #ccc; height: 20px; width: 100%; padding-left: 20px;">
                        <?= lang("planning.post_consequences_benefit_initial_rating") ?>
                    </td>
                </tr>
                <tr>
                    <td style="height: 100px; padding-left: 20px;">
                        <?= clean_string($post_consequences_benefit_initial_rating) ?>
                    </td>
                </tr>
            </table>
        </div>
    </div>

    <div class="row">
        <div class="col">
            <table cellspacing="0" cellpadding="5" border="1" style="width: 100%; font-size: 10px;margin-top: 20px;">
                <tr>
                    <td style="background-color: #ccc; height: 20px; width: 100%; padding-left: 20px;">
                        <?= lang("planning.pre_likelihood_initial_rating") ?>
                    </td>
                </tr>
                <tr>
                    <td style="height: 100px; padding-left: 20px;">
                        <?= clean_string($pre_likelihood_initial_rating) ?>
                    </td>
                </tr>
            </table>
        </div>
        <div class="col">
            <table cellspacing="0" cellpadding="5" border="1" style="width: 100%; font-size: 10px;margin-top: 20px;">
                <tr>
                    <td style="background-color: #ccc; height: 20px; width: 100%; padding-left: 20px;">
                        <?= lang("planning.post_likelihood_initial_rating") ?>
                    </td>
                </tr>
                <tr>
                    <td style="height: 100px; padding-left: 20px;">
                        <?= clean_string($post_likelihood_initial_rating) ?>
                    </td>
                </tr>
            </table>
        </div>
    </div>

    <div class="row">
        <div class="col">
            <table cellspacing="0" cellpadding="5" border="1" style="width: 100%; font-size: 10px;margin-top: 20px;">
                <tr>
                    <td style="background-color: #ccc; height: 20px; width: 100%; padding-left: 20px;">
                        <?= lang("planning.initial_risk_rating") ?>
                    </td>
                </tr>
                <tr>
                    <td style="height: 100px; padding-left: 20px;">
                        <?= clean_string($initial_risk_rating) ?>
                    </td>
                </tr>
            </table>
        </div>
        <div class="col">
            <table cellspacing="0" cellpadding="5" border="1" style="width: 100%; font-size: 10px;margin-top: 20px;">
                <tr>
                    <td style="background-color: #ccc; height: 20px; width: 100%; padding-left: 20px;">
                        <?= lang("planning.final_risk_rating") ?>
                    </td>
                </tr>
                <tr>
                    <td style="height: 100px; padding-left: 20px;">
                        <?= clean_string($final_risk_rating) ?>
                    </td>
                </tr>
            </table>
        </div>
    </div>

    <div class="row">
        <div class="col">
            <table cellspacing="0" cellpadding="5" border="1" style="width: 100%; font-size: 10px;margin-top: 20px;">
                <tr>
                    <td style="background-color: #ccc; height: 20px; width: 100%; padding-left: 20px;">
                        <?= lang("planning.existing_control") ?>
                    </td>
                </tr>
                <tr>
                    <td style="height: 100px; padding-left: 20px;">
                        <?= clean_string($existing_control) ?>
                    </td>
                </tr>
            </table>
        </div>
        <div class="col">
            <table cellspacing="0" cellpadding="5" border="1" style="width: 100%; font-size: 10px;margin-top: 20px;">
                <tr>
                    <td style="background-color: #ccc; height: 20px; width: 100%; padding-left: 20px;">
                        <?= lang("planning.additional_control") ?>
                    </td>
                </tr>
                <tr>
                    <td style="height: 100px; padding-left: 20px;">
                        <?= clean_string($additional_control) ?>
                    </td>
                </tr>
            </table>
        </div>
    </div>

    <div class="row">
        <div class="col">
            <table cellspacing="0" cellpadding="5" border="1" style="width: 100%; font-size: 10px;margin-top: 20px;">
                <tr>
                    <td style="background-color: #ccc; height: 20px; width: 100%; padding-left: 20px;">
                        <?= lang("planning.approve_author") ?>
                    </td>
                </tr>
                <tr>
                    <td style="height: 100px; padding-left: 20px;">
                        <?= clean_string($approve_author) ?>
                    </td>
                </tr>
            </table>
        </div>
        <div class="col">

        </div>
    </div>







    <br /><br />

    <table border="1" cellspacing="0" cellpadding="2" style="width: 100%;">
        <tr>
            <td class="p-3" style="font-size: 12px">
                <div style="min-height: 100px;">
                    <div class="container-fluid">
                        <div class="row pl-3 pr-3">
                            <div class="col row">
                                <div class="col" style="font-weight: bold">
                                    User
                                </div>
                                <div class="col" style="font-weight: bold">
                                    Apprv 1
                                </div>
                            </div>
                            <div class="col row">
                                <div align="center" class="col" style="margin-left: -3%; font-weight: bold;">
                                    Apprv 2
                                </div>
                            </div>
                        </div>

                        <div style="height: 100px"></div>

                        <div class="row pl-3 pr-3">
                            <div class="col row">
                                <div class="col ml-3 mr-3" style="border-bottom: 1px solid black;"></div>
                                <div class="col ml-3 mr-3" style="border-bottom: 1px solid black;"></div>
                            </div>
                            <div class="col row">
                                <div class="col ml-3 mr-3" style="border-bottom: 1px solid black;"></div>
                                <div class="col ml-3 mr-3" style="border-bottom: 1px solid black;"></div>
                            </div>
                        </div>

                        <div class="row pl-3 pr-3">
                            <div class="col row">
                                <div class="col">
                                    Tgl :
                                </div>
                                <div class="col">
                                    Tgl :
                                </div>
                            </div>
                            <div class="col row">
                                <div class="col">
                                    Tgl :
                                </div>
                                <div class="col">
                                    Tgl :
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </td>
        </tr>
    </table>
</body>

</html>