<!DOCTYPE html>
<html lang="en">

<?php echo view('_partials/header'); ?>

<body class="no-skin">

<?php echo view('_partials/navbar'); ?>

<div class="main-container ace-save-state" id="main-container">
    <script type="text/javascript">
        try {
            ace.settings.loadState('main-container')
        } catch (e) {
        }
    </script>

    <?php echo view('_partials/sidebar'); ?>

    <div class="main-content">
        <div class="main-content-inner">
            <div class="breadcrumbs ace-save-state" id="breadcrumbs">
                <ul class="breadcrumb">
                    <li>
                        <i class="ace-icon fa fa-home home-icon"></i>
                        <a href="#">Home</a>
                    </li>
                    <li class="active"><?= $module_title ?></li>
                </ul><!-- /.breadcrumb -->


            </div>

            <div class="page-content">
                <div class="ace-settings-container" id="ace-settings-container">
                    <div class="btn btn-app btn-xs btn-warning ace-settings-btn" id="ace-settings-btn">
                        <i class="ace-icon fa fa-cog bigger-130"></i>
                    </div>

                    <div class="ace-settings-box clearfix" id="ace-settings-box">
                        <div class="pull-left width-50">
                            <div class="ace-settings-item">
                                <div class="pull-left">
                                    <select id="skin-colorpicker" class="hide">
                                        <option data-skin="no-skin" value="#438EB9">#438EB9</option>
                                        <option data-skin="skin-1" value="#222A2D">#222A2D</option>
                                        <option data-skin="skin-2" value="#C6487E">#C6487E</option>
                                        <option data-skin="skin-3" value="#D0D0D0">#D0D0D0</option>
                                    </select>
                                </div>
                                <span>&nbsp; Choose Skin</span>
                            </div>

                            <div class="ace-settings-item">
                                <input type="checkbox" class="ace ace-checkbox-2 ace-save-state"
                                       id="ace-settings-navbar" autocomplete="off"/>
                                <label class="lbl" for="ace-settings-navbar"> Fixed Navbar</label>
                            </div>

                            <div class="ace-settings-item">
                                <input type="checkbox" class="ace ace-checkbox-2 ace-save-state"
                                       id="ace-settings-sidebar" autocomplete="off"/>
                                <label class="lbl" for="ace-settings-sidebar"> Fixed Sidebar</label>
                            </div>

                            <div class="ace-settings-item">
                                <input type="checkbox" class="ace ace-checkbox-2 ace-save-state"
                                       id="ace-settings-breadcrumbs" autocomplete="off"/>
                                <label class="lbl" for="ace-settings-breadcrumbs"> Fixed Breadcrumbs</label>
                            </div>

                            <div class="ace-settings-item">
                                <input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-rtl"
                                       autocomplete="off"/>
                                <label class="lbl" for="ace-settings-rtl"> Right To Left (rtl)</label>
                            </div>

                            <div class="ace-settings-item">
                                <input type="checkbox" class="ace ace-checkbox-2 ace-save-state"
                                       id="ace-settings-add-container" autocomplete="off"/>
                                <label class="lbl" for="ace-settings-add-container">
                                    Inside
                                    <b>.container</b>
                                </label>
                            </div>
                        </div><!-- /.pull-left -->

                        <div class="pull-left width-50">
                            <div class="ace-settings-item">
                                <input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-hover"
                                       autocomplete="off"/>
                                <label class="lbl" for="ace-settings-hover"> Submenu on Hover</label>
                            </div>

                            <div class="ace-settings-item">
                                <input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-compact"
                                       autocomplete="off"/>
                                <label class="lbl" for="ace-settings-compact"> Compact Sidebar</label>
                            </div>

                            <div class="ace-settings-item">
                                <input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-highlight"
                                       autocomplete="off"/>
                                <label class="lbl" for="ace-settings-highlight"> Alt. Active Item</label>
                            </div>
                        </div><!-- /.pull-left -->
                    </div><!-- /.ace-settings-box -->
                </div><!-- /.ace-settings-container -->

                <div class="page-header">
                    <h1>
                        <?= $module_title ?>
                        <small>
                            <i class="ace-icon fa fa-angle-double-right"></i>
                            <?= $submodule_title ?>
                        </small>
                    </h1>
                </div><!-- /.page-header -->

                <div class="row">
                    <div class="col-xs-12">
                        <h3 class="header smaller lighter blue">Data Tabel</h3>

                        <div class="clearfix">
                            <div class="pull-right tableTools-container"></div>
                            <a href="<?php echo $controller_path_url . "/create" ?>"
                               class="btn btn-white btn-info btn-bold">
                                <i class="ace-icon fa fa-plus bigger-120 blue"></i>
                                Add New
                                </utton>
                            </a>
                        </div>

                        <div class="table-header">
                            Results for <?= $submodule_title ?>
                        </div>

                        <!-- div.table-responsive -->
                        <!-- div.dataTables_borderWrap -->
                        <div class="table-responsive">
                            <table id="dynamic-table" class="table table-striped table-bordered table-hover" style="width: 100%">
                                <thead>
                                <tr>
                                    <th rowspan="2" style="width: 20px;text-align:center">No</th>
                                    <th rowspan="2"><?= lang("internal_audit.dept_location") ?></th>
                                    <th rowspan="2"><?= lang("internal_audit.schedule_month") ?></th>
                                    <th rowspan="2"><?= lang("internal_audit.auditor_name") ?></th>
                                    <th rowspan="2"><?= lang("internal_audit.auditee_name") ?></th>
                                    <th rowspan="2"><?= lang("internal_audit.location_audit") ?></th>
                                    <th rowspan="2"><?= lang("internal_audit.approve_author") ?></th>
                                    <th rowspan="2">Approval Status</th>
                                    <th colspan="4">Action</th>
                                </tr>
                                <tr>
                                    <th style="text-align: center">Preview</th>
                                    <th style="text-align: center">Download</th>
                                    <th style="text-align: center">Attachment</th>
                                    <th style="text-align: center">Delete</th>
                                </tr>
                                </thead>
                                <tbody>

                                <?php
                                $count = 0;
                                foreach ($rows as $row) {
                                    ++$count; ?>
                                    <tr>
                                        <td class="center">
                                            <?php echo $count; ?>
                                        </td>
                                        <td><?php echo strip_tags(html_entity_decode($row->dept_location)); ?></td>
                                        <td><?php echo strip_tags(html_entity_decode($row->schedule_month)); ?></td>
                                        <td><?php echo strip_tags(html_entity_decode($row->auditor_name)); ?></td>
                                        <td><?php echo strip_tags(html_entity_decode($row->auditee_name)); ?></td>
                                        <td><?php echo strip_tags(html_entity_decode($row->location_audit)); ?></td>
                                        <td><?php echo get_approval_val(($row->approve_author)); ?></td>
                                        <td><?php echo $row->approval_status; ?></td>

                                        <!-- Action start -->
                                        <td type="action">
                                            <a class="green" href="<?= $controller_path_url . "/edit" . "/" . $row->id ?>">
                                                <i class="ace-icon fa fa-pencil bigger-130"></i>
                                            </a>
                                        </td>
                                        <td type="action">
                                            <?php if ($row->step == 3) { ?>
                                                <a class="blue" target="_blank" href="<?= $controller_path_url . "/distribution/" . $row->id ?>">
                                                    <i class="ace-icon fa fa-file bigger-130"></i>
                                                </a>
                                            <?php } ?>
                                        </td>
                                        <td type="action">
                                            <?php if (!empty($row->file_url)): ?>
                                                <a class="green" href="<?= $row->file_url ?>" target="_blank">
                                                    <i class="ace-icon fa fa-download bigger-130"></i>
                                                </a>
                                            <?php endif; ?>
                                        </td>
                                        <td type="action">
                                            <a class="red" href="<?= $controller_path_url . "/process_delete" . "/" . $row->id ?>">
                                                <i class="ace-icon fa fa-trash-o bigger-130"></i>
                                            </a>
                                        </td>
                                        <!-- Action end -->
                                    </tr>
                                <?php } ?>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div><!-- /.row -->
            </div><!-- /.page-content -->
        </div>
    </div><!-- /.main-content -->

    <div class="modal fade" id="modal-default">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Delete Data</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p>Are you sure to delete item ?</p>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <a class="btn btn-danger btn-ok" data-dismiss="modal">Delete</a>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

    

    <a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
        <i class="ace-icon fa fa-angle-double-up icon-only bigger-110"></i>
    </a>
</div><!-- /.main-container -->

<?php echo view("_partials/read_script"); ?>

</body>
</html>