<!DOCTYPE html>
<html lang="en">

<?php echo view('_partials/header'); ?>

<body class="no-skin">

<?php echo view('_partials/navbar'); ?>

<div class="main-container ace-save-state" id="main-container">
    <script type="text/javascript">
        try {
            ace.settings.loadState('main-container')
        } catch (e) {
        }
    </script>

    <?php echo view('_partials/sidebar'); ?>

    <div class="main-content">
        <div class="main-content-inner">
            <div class="breadcrumbs ace-save-state" id="breadcrumbs">
                <ul class="breadcrumb">
                    <li>
                        <i class="ace-icon fa fa-home home-icon"></i>
                        <a href="#">Home</a>
                    </li>
                    <li class="active"><?= $module_title ?></li>
                </ul><!-- /.breadcrumb -->
            </div>

            <div class="page-content">
                <div class="ace-settings-container" id="ace-settings-container">
                    <div class="btn btn-app btn-xs btn-warning ace-settings-btn" id="ace-settings-btn">
                        <i class="ace-icon fa fa-cog bigger-130"></i>
                    </div>

                    <div class="ace-settings-box clearfix" id="ace-settings-box">
                        <div class="pull-left width-50">
                            <div class="ace-settings-item">
                                <div class="pull-left">
                                    <select id="skin-colorpicker" class="hide">
                                        <option data-skin="no-skin" value="#438EB9">#438EB9</option>
                                        <option data-skin="skin-1" value="#222A2D">#222A2D</option>
                                        <option data-skin="skin-2" value="#C6487E">#C6487E</option>
                                        <option data-skin="skin-3" value="#D0D0D0">#D0D0D0</option>
                                    </select>
                                </div>
                                <span>&nbsp; Choose Skin</span>
                            </div>

                            <div class="ace-settings-item">
                                <input type="checkbox" class="ace ace-checkbox-2 ace-save-state"
                                       id="ace-settings-navbar" autocomplete="off"/>
                                <label class="lbl" for="ace-settings-navbar"> Fixed Navbar</label>
                            </div>

                            <div class="ace-settings-item">
                                <input type="checkbox" class="ace ace-checkbox-2 ace-save-state"
                                       id="ace-settings-sidebar" autocomplete="off"/>
                                <label class="lbl" for="ace-settings-sidebar"> Fixed Sidebar</label>
                            </div>

                            <div class="ace-settings-item">
                                <input type="checkbox" class="ace ace-checkbox-2 ace-save-state"
                                       id="ace-settings-breadcrumbs" autocomplete="off"/>
                                <label class="lbl" for="ace-settings-breadcrumbs"> Fixed Breadcrumbs</label>
                            </div>

                            <div class="ace-settings-item">
                                <input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-rtl"
                                       autocomplete="off"/>
                                <label class="lbl" for="ace-settings-rtl"> Right To Left (rtl)</label>
                            </div>

                            <div class="ace-settings-item">
                                <input type="checkbox" class="ace ace-checkbox-2 ace-save-state"
                                       id="ace-settings-add-container" autocomplete="off"/>
                                <label class="lbl" for="ace-settings-add-container">
                                    Inside
                                    <b>.container</b>
                                </label>
                            </div>
                        </div><!-- /.pull-left -->

                        <div class="pull-left width-50">
                            <div class="ace-settings-item">
                                <input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-hover"
                                       autocomplete="off"/>
                                <label class="lbl" for="ace-settings-hover"> Submenu on Hover</label>
                            </div>

                            <div class="ace-settings-item">
                                <input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-compact"
                                       autocomplete="off"/>
                                <label class="lbl" for="ace-settings-compact"> Compact Sidebar</label>
                            </div>

                            <div class="ace-settings-item">
                                <input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-highlight"
                                       autocomplete="off"/>
                                <label class="lbl" for="ace-settings-highlight"> Alt. Active Item</label>
                            </div>
                        </div><!-- /.pull-left -->
                    </div><!-- /.ace-settings-box -->
                </div><!-- /.ace-settings-container -->

                <div class="page-header">
                    <h1>
                        <?= $submodule_title ?>
                        <small><i class="ace-icon fa fa-angle-double-right"></i> Add New </small>
                    </h1>
                </div><!-- /.page-header -->

                <!-- PAGE CONTENT BEGINS -->
                <form class="form-horizontal container-fluid" role="form"
                      action="<?php echo $controller_path_url . "/process_add" ?>" method="POST"
                      enctype="multipart/form-data">

                    <div class="row">
                        <div class="col-xs-6">
                            <div class="form-group">
                                <label class="col-sm-3 control-label no-padding-right" >
                                    Equipment </label>

                                <div class="col-sm-9">
                                    <select name="equipment" class="form-control" id="form-field-select-1">
                                        <option value="" selected="selected">Please select...</option>
                                        <?php
                                        $results = $this->db->query("SELECT * FROM operation_equipment WHERE step=3")->result();
                                        foreach ($results as $item) {
                                            $val = $item->id;
                                            $select = $item->equipment_name;
                                            echo "<option value='$val'>$select</option>";
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                        </div><!-- /.col -->
                        <div class="col-xs-6">
                            <div class="form-group">
                                <label class="col-sm-3 control-label no-padding-right" >
                                    Frequency </label>

                                <div class="col-sm-9">
                                    <select name="frequency" class="form-control" id="form-field-select-1">
                                        <option value="" selected="selected">Please select...</option>
                                        <option value="Daily">Daily</option>
                                        <option value="Fortnightly">Fortnightly</option>
                                        <option value="Half-Yearly">Half-Yearly</option>
                                        <option value="Monthly">Monthly</option>
                                        <option value="Quarterly">Quarterly</option>
                                        <option value="Weekly">Weekly</option>
                                        <option value="Yearly">Yearly</option>
                                    </select>
                                </div>
                            </div>
                        </div><!-- /.col -->
                    </div><!-- /.row -->

                    <div class="row">
                        <div class="col-xs-6">
                            <div class="form-group">
                                <label class="col-sm-3 control-label no-padding-right" > Item </label>

                                <div class="col-sm-9">
                                    <input type="text" name="item" id="form-field-1" placeholder="Item"
                                           class="form-control"/>
                                </div>
                            </div>
                        </div><!-- /.col -->
                        <div class="col-xs-6">
                            <div class="form-group">
                                <label class="col-sm-3 control-label no-padding-right" >
                                    Activity </label>

                                <div class="col-sm-9">
                                    <input type="text" name="activity" id="form-field-1" placeholder="Activity"
                                           class="form-control"/>
                                </div>
                            </div>
                        </div><!-- /.col -->
                    </div><!-- /.row -->

                    <div class="row">
                        <div class="col-xs-6">
                            <div class="form-group">
                                <label class="col-sm-3 control-label no-padding-right" >
                                    Responsibility </label>

                                <div class="col-sm-9">
                                    <input type="text" name="responsibility" id="form-field-1"
                                           placeholder="Responsibility" class="form-control"/>
                                </div>
                            </div>
                        </div><!-- /.col -->

                    </div><!-- /.row -->

                    <div class="row">
                        <div class="col">
                            <label><?= lang("app.supporting_files") ?></label>
                            <input name="file_upload" type="file" id="file_choose" class="custom-file-input">
                        </div>
                    </div>

                    <div id="toolbar">
                        <div class="form-inline" role="form">
                            <div class="form-group">
                                <!-- <input name="search" class="form-control" type="text" placeholder="Search"> -->
                                <!-- <a href='http://localhost/iso/hr/designation/create'> -->
                                <a onClick="addRow()">
                                    <button type="button" class="btn btn-labeled btn-primary"><span class="btn-label"><i
                                                    class="fa fa-plus"></i></span> Add new
                                    </button>
                                </a>
                            </div>
                            <!-- <button id="ok" type="submit" class="btn btn-primary">OK</button> -->
                        </div>
                    </div>

                    <table
                            id="table"
                            data-pagination="true"
                            data-toggle="table"
                            data-toolbar="#toolbar"
                            data-show-refresh="true"
                            data-show-toggle="true">
                        <!-- data-url="https://examples.wenzhixin.net.cn/examples/bootstrap_table/data" -->
                        <thead>
                        <tr>
                            <th data-field="no">No</th>
                            <th data-field="action">Action</th>
                            <th data-field="col1">Customer Name</th>
                            <th data-field="col2">External Property Name</th>
                            <th data-field="col3">External Property Type</th>
                            <th data-field="col4">External Property Status</th>
                            <th data-field="col5">File</th>
                        </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>

                    <div class="row clearfix form-actions">
                        <div class="col-md-offset-3 col-md-9">
                            <button class="btn btn-info" type="submit ">
                                <i class="ace-icon fa fa-check bigger-110"></i>
                                Submit
                        </div>
                    </div>
                </form>
            </div><!-- /.page-content -->
        </div>
    </div><!-- /.main-content -->

    <div class="footer">
        <div class="footer-inner">
            <div class="footer-content">
                <span class="bigger-120">
                    <span class="blue bolder">Ace</span>
                    Application &copy; 2013-2014
                </span>
                &nbsp; &nbsp;
                <span class="action-buttons">
                    <a href="#">
                        <i class="ace-icon fa fa-twitter-square light-blue bigger-150"></i>
                    </a>

                    <a href="#">
                        <i class="ace-icon fa fa-facebook-square text-primary bigger-150"></i>
                    </a>

                    <a href="#">
                        <i class="ace-icon fa fa-rss-square orange bigger-150"></i>
                    </a>
                </span>
            </div>
        </div>
    </div>

    <a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
        <i class="ace-icon fa fa-angle-double-up icon-only bigger-110"></i>
    </a>
</div><!-- /.main-container -->

<!-- basic scripts -->

<!--[if !IE]> -->
<script src="<?php echo base_url("assets/assets"); ?>/js/jquery-2.1.4.min.js"></script>

<!-- <![endif]-->

<!--[if IE]>
<script src="assets/js/jquery-1.11.3.min.js"></script>
<![endif]-->
<script type="text/javascript">
    if ('ontouchstart' in document.documentElement) document.write("<script src='<?php echo base_url("assets/assets"); ?>/js/jquery.mobile.custom.min.js'>" + "<" + "/script>");
</script>
<script src="<?php echo base_url("assets/assets"); ?>/js/bootstrap.min.js"></script>

<!-- page specific plugin scripts -->

<!--[if lte IE 8]>
<script src="assets/js/excanvas.min.js"></script>
<![endif]-->
<script src="<?php echo base_url("assets/assets"); ?>/js/jquery-ui.custom.min.js"></script>
<script src="<?php echo base_url("assets/assets"); ?>/js/jquery.ui.touch-punch.min.js"></script>
<script src="<?php echo base_url("assets/assets"); ?>/js/markdown.min.js"></script>
<script src="<?php echo base_url("assets/assets"); ?>/js/bootstrap-markdown.min.js"></script>
<script src="<?php echo base_url("assets/assets"); ?>/js/jquery.hotkeys.index.min.js"></script>
<script src="<?php echo base_url("assets/assets"); ?>/js/bootstrap-wysiwyg.min.js"></script>
<script src="<?php echo base_url("assets/assets"); ?>/js/bootbox.js"></script>
<script src="<?php echo base_url("assets/assets"); ?>/js/bootstrap-wysiwyg.min.js"></script>
<!-- ace scripts -->
<script src="<?php echo base_url("assets/assets"); ?>/js/ace-elements.min.js"></script>
<script src="<?php echo base_url("assets/assets"); ?>/js/ace.min.js"></script>

<script src="<?php echo base_url("assets/assets"); ?>/js/jquery.maskedinput.min.js"></script>
<script src="<?php echo base_url("assets/assets"); ?>/js/bootstrap-datepicker.min.js"></script>
<script>
    $("input[placeholder='yyyy-mm-dd']").datepicker({
        autoclose: true,
        format: 'yyyy-mm-dd',
        orientation: "bottom",
        todayHighlight: true,
    });
    $("input[placeholder='yyyy-mm-dd']").mask("9999-99-99");
    $("input[placeholder='yyyy-mm-dd']").css("width", "100%");
</script>

<!-- Bootstrap DataTables -->
<script src="https://unpkg.com/bootstrap-table@1.15.3/dist/bootstrap-table.min.js"></script>
<script src="https://unpkg.com/tableexport.jquery.plugin/tableExport.min.js"></script>
<script src="https://unpkg.com/tableexport.jquery.plugin/libs/jsPDF/jspdf.min.js"></script>
<script src="https://unpkg.com/tableexport.jquery.plugin/libs/jsPDF-AutoTable/jspdf.plugin.autotable.js"></script>
<!-- <script src="extensions/print/bootstrap-table-print.js"></script> -->
<script src="<?php echo base_url(); ?>assets/bootstrap-table/extensions/export/bootstrap-table-export.js"></script>

<script type="text/javascript">
    var col1Values, col2Values, col3Values, col4Values, col5Values;
    col1Values = []
    col2Values = []
    col3Values = []
    col4Values = []
    col5Values = []

    function backup() {

        col1Values = $("input[name='col1[]']").map(function () {
            return $(this).val();
        }).get();

        col2Values = $("input[name='col2[]']").map(function () {
            return $(this).val();
        }).get();

        col3Values = $("input[name='col3[]']").map(function () {
            return $(this).val();
        }).get();

        col4Values = $("input[name='col4[]']").map(function () {
            return $(this).val();
        }).get();

        col5Values = $("input[name='col5[]']").map(function () {
            return $(this).prop("files")[0];
        }).get();
    }

    function restore() {
        var index = 0;
        $("input[name='col1[]']").map(function () {
            if (index < col1Values.length) {
                $(this).val(col1Values[index]);
            }
            index++;
        }).get();

        var index = 0;
        $("input[name='col2[]']").map(function () {
            if (index < col2Values.length) {
                $(this).val(col2Values[index]);
            }
            index++;
        }).get();

        var index = 0;
        $("input[name='col3[]']").map(function () {
            if (index < col3Values.length) {
                $(this).val(col3Values[index]);
            }
            index++;
        }).get();

        var index = 0;
        $("input[name='col4[]']").map(function () {
            if (index < col4Values.length) {
                $(this).val(col4Values[index]);
            }
            index++;
        }).get();

        var index = 0;
        $("input[name='col5_text[]']").map(function () {
            if (index < col5Values.length) {
                $(this).val(col5Values[index].name);
            }
            index++;
        }).get();
    }

    var id = 0
    var numbers = []

    function refreshNumberOrder() {
        var no = 1;
        $("[id='field_no']").map(function () {
            $(this).text(no);
            no++;
        }).get();

        var no = 1;
        $("[id='delete_button']").map(function () {
            $(this).attr("onClick", "deleteRow(" + (no - 1) + ", " + numbers[no - 1] + ")");
            no++;
        }).get();
    }

    function deleteRow(index, oldNumber) {
        backup();

        col1Values.splice(index, 1);
        col2Values.splice(index, 1);
        col3Values.splice(index, 1);
        col4Values.splice(index, 1);
        col5Values.splice(index, 1);
        numbers.splice(index, 1)

        $("#table").bootstrapTable('remove', {
            field: 'no',
            values: ['<p id="field_no">' + oldNumber + '</p>']
        });

        refreshNumberOrder();
        restore();
    }

    function addRow(init) {
        backup();

        numbers.push(id + 1)
        var numberIndex = numbers.indexOf(id + 1);

        $("#table").bootstrapTable('insertRow', {
            index: id,
            row: {
                no: '<p id="field_no">' + (id + 1) + '</p>',
                action: '\
            <a id="delete_button" onclick="" style="cursor: pointer; margin: 5px" title="Remove"><i class="fa fa-trash"></i></a>\
            ',
                col1: '\
            <div id="col1_group_' + (id + 1) + '" class="form-group" style="margin: 15px;">\
                <input name="col1[]" class="form-control" type="text" style="width: 230px;"/>\
            </div>\
            ',
                col2: '\
            <div id="col2_group_' + (id + 1) + '" class="form-group" style="margin: 15px;">\
                <input name="col2[]" class="form-control" type="text" style="width: 230px;"/>\
            </div>\
            ',
                col3: '\
            <div id="col3_group' + (id + 1) + '" class="form-group" style="margin: 15px;">\
                <input name="col3[]" class="form-control" style="width: 230px;"/>\
            </div>\
            ',
                col4: '\
            <div id="col4_group' + (id + 1) + '" class="form-group" style="margin: 15px;">\
                <input name="col4[]" class="form-control" style="width: 230px;"/>\
            </div>\
            ',
                col5: '\
            <div id="col5_group' + (id + 1) + '" class="form-group" style="margin: 15px;">\
                <input name="col5_text[]"  type="text" placeholder="add file" class="form-control" style="min-width: 400px; width: 100%;"/>\
                <input name="col5[]" type="file" class="form-control" style="display: none"/>\
            </div>\
            ',
            }
        });

        restore();
        id++;

        refreshNumberOrder();

        $("input[type='text']").click(function () {
            $(this).siblings("input[type='file']").trigger("click");
        });
        $("input[type='file']").change(function () {
            var filename = $(this).prop("files")[0].name
            $(this).siblings("input").val(filename)
        });
    }
</script>

<script type="text/javascript">

    $('form').submit(function (e) {
        e.preventDefault();

        $('.form-group').removeClass('has-error'); // remove the error class
        $('.help-block').remove(); // remove the error text
        $('.alert-success').remove();

        var datas = []
        backup();

        var formData = new FormData();
        for (var i = 0; i < $("table").bootstrapTable('getData').length; i++) {
            var no = i + 1;
            formData.append("datas[" + i + "][no]", no)
            formData.append("datas[" + i + "][numbers]", numbers[i])
            formData.append("datas[" + i + "][col1]", col1Values[i])
            formData.append("datas[" + i + "][col2]", col2Values[i])
            formData.append("datas[" + i + "][col3]", col3Values[i])
            formData.append("datas[" + i + "][col4]", col4Values[i])
            formData.append("datas-file-" + no, col5Values[i])
        }

        var dataArray = $('form').serializeArray();
        $.map(dataArray, function (val, i) {
            formData.append("data[" + val.name + "]", val.value)
        });

        formData.append("data[step]", 1)
        formData.append("data[approval_status]", 0)

        // jQuery.each(jQuery("input[type='file']")[0].files, function (i, file) {
        // jQuery.each(jQuery("input[type='file']").prop('files'), function (i, file) {
        //     formData.append("file_upload", file);
        // });
        formData.append("file_upload", jQuery("input[name='file_upload']").prop("files")[0]);

        // process the form
        $.ajax({
            url: '<?= "$controller_path_url/process_add" ?>',
            data: formData, // data object
            dataType: 'json', // what type of data do we expect back from the server
            cache: false,
            contentType: false,
            processData: false,
            type: 'POST',
            method: 'POST',
            encode: true,
            error: function (data) {
                alert("AJAX ERROR")
                alert(JSON.stringify(data));
            }
        }).done(function (data) {
            // using the done promise callback
            // alert(JSON.stringify(data));

            // log data to the console so we can see
            console.log(data);

            // here we will handle errors and validation messages
            if (!data.success) {

                alert(data.message);

            } else {

                // ALL GOOD! just show the success message!
                alert(data.message);
                window.location = '<?= $controller_path_url . "/list" ?>'; // redirect a user to another page

            }
        });
    });
</script>

<!-- inline scripts related to this page -->
<script type="text/javascript">

    jQuery(function ($) {

        $('textarea[data-provide="markdown"]').each(function () {
            var $this = $(this);

            if ($this.data('markdown')) {
                $this.data('markdown').showEditor();
            } else $this.markdown()

            $this.parent().find('.btn').addClass('btn-white');
        })

        function parseMe(value) {
            return value.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;');
        }

        var convertToHtmlTag = function(convertToHtmlTag){
            var passValue = $("<span />", { html: convertToHtmlTag }).text();
            passValue = passValue.replace('&nbsp;', " ")
            return passValue
            //return document.createElement("span").innerText;
        };

        function showErrorAlert(reason, detail) {
            var msg = '';
            if (reason === 'unsupported-file-type') {
                msg = "Unsupported format " + detail;
            } else {
                //console.log("error uploading file", reason, detail);
            }
            $('<div class="alert"> <button type="button" class="close" data-dismiss="alert">&times;</button>' +
                '<strong>File upload error</strong> ' + msg + ' </div>').prependTo('#alerts');
        }

        $(".wysiwyg-editor").on('keyup DOMSubtreeModified', function () {
            $(this).siblings("textarea").val(parseMe($(this).html()))
        });

        $(".wysiwyg-editor").ace_wysiwyg({
            toolbar: [
                'font',
                null,
                'fontSize',
                null,
                {name: 'bold', className: 'btn-info'},
                {name: 'italic', className: 'btn-info'},
                {name: 'strikethrough', className: 'btn-info'},
                {name: 'underline', className: 'btn-info'},
                null,
                {name: 'insertunorderedlist', className: 'btn-success'},
                {name: 'insertorderedlist', className: 'btn-success'},
                {name: 'outdent', className: 'btn-purple'},
                {name: 'indent', className: 'btn-purple'},
                null,
                {name: 'justifyleft', className: 'btn-primary'},
                {name: 'justifycenter', className: 'btn-primary'},
                {name: 'justifyright', className: 'btn-primary'},
                {name: 'justifyfull', className: 'btn-inverse'},
                null,
                {name: 'createLink', className: 'btn-pink'},
                {name: 'unlink', className: 'btn-pink'},
                null,
                {name: 'insertImage', className: 'btn-success'},
                null,
                'foreColor',
                null,
                {name: 'undo', className: 'btn-grey'},
                {name: 'redo', className: 'btn-grey'}
            ],
            'wysiwyg': {
                fileUploadError: showErrorAlert
            }
        }).prev().addClass('wysiwyg-style2');


        //RESIZE IMAGE
        //Add Image Resize Functionality to Chrome and Safari
        //webkit browsers don't have image resize functionality when content is editable
        //so let's add something using jQuery UI resizable
        //another option would be opening a dialog for user to enter dimensions.
        if (typeof jQuery.ui !== 'undefined' && ace.vars['webkit']) {

            var lastResizableImg = null;

            function destroyResizable() {
                if (lastResizableImg == null) return;
                lastResizableImg.resizable("destroy");
                lastResizableImg.removeData('resizable');
                lastResizableImg = null;
            }

            var enableImageResize = function () {
                $('.wysiwyg-editor')
                    .on('mousedown', function (e) {
                        var target = $(e.target);
                        if (e.target instanceof HTMLImageElement) {
                            if (!target.data('resizable')) {
                                target.resizable({
                                    aspectRatio: e.target.width / e.target.height,
                                });
                                target.data('resizable', true);

                                if (lastResizableImg != null) {
                                    //disable previous resizable image
                                    lastResizableImg.resizable("destroy");
                                    lastResizableImg.removeData('resizable');
                                }
                                lastResizableImg = target;
                            }
                        }
                    })
                    .on('click', function (e) {
                        if (lastResizableImg != null && !(e.target instanceof HTMLImageElement)) {
                            destroyResizable();
                        }
                    })
                    .on('keydown', function () {
                        destroyResizable();
                    });
            }

            enableImageResize();
        }
    });
</script>
</body>
</html>
