<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
    <meta charset="utf-8">
    <title> PDF </title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <style type="text/css">
        table { page-break-inside:avoid }
        tr    { page-break-inside:avoid; page-break-after:auto }
        thead { display:table-header-group }
        tfoot { display:table-footer-group }
        body {
            font-size: 12px;
        }
    </style>

</head>
<body>
<div class="container-fluid">
    <div class="row m-4">
        <div class="col-3">
            <img src="<?= $logo->url ?>" width="80%">
        </div>
        <div align="center" class="col-6 mt-auto mb-auto">
            <h5 style="font-weight: bold">Asia Global Servis</h5>
        </div>
        <div class="col-3 mt-auto mb-auto">
            <table>
                <tr>
                    <td class="pr-3" style="font-weight: bold">Doc.</td>
                    <td> : </td>
                    <td class="pl-2"> </td>
                </tr>
                <tr>
                    <td class="pr-3" style="font-weight: bold">Form No.</td>
                    <td> : </td>
                    <td class="pl-2"> </td>
                </tr>
                <tr>
                    <td class="pr-3" style="font-weight: bold">Rev.</td>
                    <td> : </td>
                    <td class="pl-2"> </td>
                </tr>
            </table>
        </div>
    </div>

    <div class="row mb-3 mt-2">
        <div class="col-12">
            <div class="d-flex justify-content-center align-items-center">
                <div><span style="font-size: 15px; font-weight: bold;">  </span></div>
            </div>
        </div>
    </div>

    <table border="1"  cellspacing="0" cellpadding="5" style="width: 100%;">
        <tr>
            <td>
                <div class="container-fluid">
                    <div class="row" style="min-height: 50px;">
                        <div class="col-12 pl-3 pr-3 pt-2 pb-2">
                            <table>
                                <tr>
                                    <td style="font-weight: bold"><?= lang("operation.enquiry_date") ?></td>
                                    <td>: </td>
                                </tr>
                                <tr>
                                    <td><?= clean_string($enquiry_date) ?></td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </td>
        </tr>
    </table>
    
    <table border="1"  cellspacing="0" cellpadding="5" style="width: 100%;">
        <tr>
            <td>
                <div class="container-fluid">
                    <div class="row" style="min-height: 50px;">
                        <div class="col-12 pl-3 pr-3 pt-2 pb-2">
                            <table>
                                <tr>
                                    <td style="font-weight: bold"><?= lang("operation.phone") ?></td>
                                    <td>: </td>
                                </tr>
                                <tr>
                                    <td><?= clean_string($phone) ?></td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </td>
        </tr>
    </table>
    
    <table border="1"  cellspacing="0" cellpadding="5" style="width: 100%;">
        <tr>
            <td>
                <div class="container-fluid">
                    <div class="row" style="min-height: 50px;">
                        <div class="col-12 pl-3 pr-3 pt-2 pb-2">
                            <table>
                                <tr>
                                    <td style="font-weight: bold"><?= lang("operation.prospect_company") ?></td>
                                    <td>: </td>
                                </tr>
                                <tr>
                                    <td><?= clean_string($prospect_company) ?></td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </td>
        </tr>
    </table>
    
    <table border="1"  cellspacing="0" cellpadding="5" style="width: 100%;">
        <tr>
            <td>
                <div class="container-fluid">
                    <div class="row" style="min-height: 50px;">
                        <div class="col-12 pl-3 pr-3 pt-2 pb-2">
                            <table>
                                <tr>
                                    <td style="font-weight: bold"><?= lang("operation.website") ?></td>
                                    <td>: </td>
                                </tr>
                                <tr>
                                    <td><?= clean_string($website) ?></td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </td>
        </tr>
    </table>
    
    <table border="1"  cellspacing="0" cellpadding="5" style="width: 100%;">
        <tr>
            <td>
                <div class="container-fluid">
                    <div class="row" style="min-height: 50px;">
                        <div class="col-12 pl-3 pr-3 pt-2 pb-2">
                            <table>
                                <tr>
                                    <td style="font-weight: bold"><?= lang("operation.address") ?></td>
                                    <td>: </td>
                                </tr>
                                <tr>
                                    <td><?= clean_string($address) ?></td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </td>
        </tr>
    </table>
    
    <table border="1"  cellspacing="0" cellpadding="5" style="width: 100%;">
        <tr>
            <td>
                <div class="container-fluid">
                    <div class="row" style="min-height: 50px;">
                        <div class="col-12 pl-3 pr-3 pt-2 pb-2">
                            <table>
                                <tr>
                                    <td style="font-weight: bold"><?= lang("operation.prospect_name") ?></td>
                                    <td>: </td>
                                </tr>
                                <tr>
                                    <td><?= clean_string($prospect_name) ?></td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </td>
        </tr>
    </table>
    
    <table border="1"  cellspacing="0" cellpadding="5" style="width: 100%;">
        <tr>
            <td>
                <div class="container-fluid">
                    <div class="row" style="min-height: 50px;">
                        <div class="col-12 pl-3 pr-3 pt-2 pb-2">
                            <table>
                                <tr>
                                    <td style="font-weight: bold"><?= lang("operation.city") ?></td>
                                    <td>: </td>
                                </tr>
                                <tr>
                                    <td><?= clean_string($city) ?></td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </td>
        </tr>
    </table>
    
    <table border="1"  cellspacing="0" cellpadding="5" style="width: 100%;">
        <tr>
            <td>
                <div class="container-fluid">
                    <div class="row" style="min-height: 50px;">
                        <div class="col-12 pl-3 pr-3 pt-2 pb-2">
                            <table>
                                <tr>
                                    <td style="font-weight: bold"><?= lang("operation.email") ?></td>
                                    <td>: </td>
                                </tr>
                                <tr>
                                    <td><?= clean_string($email) ?></td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </td>
        </tr>
    </table>

    <table border="1"  cellspacing="0" cellpadding="2" style="width: 100%;">
        <tr>
            <td class="p-3">
                <div style="min-height: 100px;">
                    <div class="container-fluid">
                        <div class="row pl-3 pr-3">
                            <div class="col row">
                                <div class="col" style="font-weight: bold">
                                    Approver 1
                                </div>
                                <div class="col" style="font-weight: bold">
                                    Approver 2
                                </div>
                            </div>
                            <div class="col row">
                                <div align="center" class="col" style="margin-left: -3%; font-weight: bold;">
                                    Approver 3
                                </div>
                            </div>
                        </div>

                        <div style="height: 100px"></div>

                        <div class="row pl-3 pr-3">
                            <div class="col row">
                                <div class="col">_______________</div>
                                <div class="col">_______________</div>
                            </div>
                            <div class="col row">
                                <div class="col ml-5">_______________</div>
                                <div class="col">_______________</div>
                            </div>
                        </div>

                        <div class="row pl-3 pr-3">
                            <div class="col row">
                                <div class="col">
                                    Tgl :
                                </div>
                                <div class="col">
                                    Tgl :
                                </div>
                            </div>
                            <div class="col row">
                                <div class="col ml-5">
                                    Tgl :
                                </div>
                                <div class="col">
                                    Tgl :
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </td>
        </tr>
    </table>
</div>
</body>
</html>
