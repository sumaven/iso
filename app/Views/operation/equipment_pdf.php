<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
    <meta charset="utf-8">
    <title>Non Conformance</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <style type="text/css">
        table { page-break-inside:avoid }
        tr    { page-break-inside:avoid; page-break-after:auto }
        thead { display:table-header-group }
        tfoot { display:table-footer-group }
        body {
            font-size: 12px;
        }
    </style>

</head>
<body>
<div class="container-fluid">
    <div class="row m-4">
        <div class="col-3">
            <img src="<?= $logo->url ?>" width="80%">
        </div>
        <div align="center" class="col-6 mt-auto mb-auto">
            <h5 style="font-weight: bold">Asia Global Servis</h5>
        </div>
        <div class="col-3 mt-auto mb-auto">
            <table>
                <tr>
                    <td class="pr-3" style="font-weight: bold">Doc.</td>
                    <td> : </td>
                    <td class="pl-2"> </td>
                </tr>
                <tr>
                    <td class="pr-3" style="font-weight: bold">Form No.</td>
                    <td> : </td>
                    <td class="pl-2"> </td>
                </tr>
                <tr>
                    <td class="pr-3" style="font-weight: bold">Rev.</td>
                    <td> : </td>
                    <td class="pl-2"> </td>
                </tr>
            </table>
        </div>
    </div>

    <div class="row mb-3 mt-2">
        <div class="col-12">
            <div class="d-flex justify-content-center align-items-center">
                <div><span style="font-size: 15px; font-weight: bold;">  </span></div>
            </div>
        </div>
    </div>

<!--    <table border="1"  cellspacing="0" cellpadding="2" style="width: 100%;">-->
<!--        <tr>-->
<!--            <td>-->
<!--                <div class="container-fluid">-->
<!--                    <div class="row" style="min-height: 50px;">-->
<!--                        <div class="col-5 pl-3 pr-3 pt-2 pb-2" style="background: #eee">-->
<!--                            <table>-->
<!--                                <tr>-->
<!--                                    <td style="font-weight: bold">Equipment</td>-->
<!--                                    <td>: </td>-->
<!--                                </tr>-->
<!--                                <tr>-->
<!--                                    <td style="font-weight: bold">Frequency</td>-->
<!--                                    <td>: </td>-->
<!--                                </tr>-->
<!--                            </table>-->
<!--                        </div>-->
<!--                        <div class="col-4 pl-3 pr-3 pt-2 pb-2" style="background: #eee">-->
<!--                            <table>-->
<!--                                <tr>-->
<!--                                    <td style="font-weight: bold">Item </td>-->
<!--                                    <td>: </td>-->
<!--                                </tr>-->
<!--                                <tr>-->
<!--                                    <td style="font-weight: bold">Activity </td>-->
<!--                                    <td>: </td>-->
<!--                                </tr>-->
<!--                            </table>-->
<!--                        </div>-->
<!--                        <div class="col-3 pl-3 pr-3 pt-2 pb-2" style="background: #eee">-->
<!--                            <table>-->
<!--                                <tr>-->
<!--                                    <td style="font-weight: bold">Responsibility</td>-->
<!--                                    <td>: </td>-->
<!--                                </tr>-->
<!--                            </table>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                </div>-->
<!--            </td>-->
<!--        </tr>-->
<!--    </table>-->

    <table border="1"  cellspacing="0" cellpadding="5" style="width: 100%;">
        <tr>
            <td>
                <div class="container-fluid">
                    <div class="row" style="min-height: 50px;">
                        <div class="col-12 pl-3 pr-3 pt-2 pb-2">
                            <table>
                                <tr>
                                    <td style="font-weight: bold"><?= lang("operation.identification_number") ?></td>
                                    <td>: </td>
                                </tr>
                                <tr>
                                    <td><?= clean_string($identification_number) ?></td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </td>
        </tr>
    </table>

    <table border="1"  cellspacing="0" cellpadding="5" style="width: 100%;">
        <tr>
            <td>
                <div class="container-fluid">
                    <div class="row" style="min-height: 50px;">
                        <div class="col-12 pl-3 pr-3 pt-2 pb-2">
                            <table>
                                <tr>
                                    <td style="font-weight: bold"><?= lang("operation.status") ?></td>
                                    <td>: </td>
                                </tr>
                                <tr>
                                    <td><?= clean_string($status) ?></td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </td>
        </tr>
    </table>

    <table border="1"  cellspacing="0" cellpadding="5" style="width: 100%;">
        <tr>
            <td>
                <div class="container-fluid">
                    <div class="row" style="min-height: 50px;">
                        <div class="col-12 pl-3 pr-3 pt-2 pb-2">
                            <table>
                                <tr>
                                    <td style="font-weight: bold"><?= lang("operation.equipment_name") ?></td>
                                    <td>: </td>
                                </tr>
                                <tr>
                                    <td><?= clean_string($equipment_name) ?></td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </td>
        </tr>
    </table>

    <table border="1"  cellspacing="0" cellpadding="5" style="width: 100%;">
        <tr>
            <td>
                <div class="container-fluid">
                    <div class="row" style="min-height: 50px;">
                        <div class="col-12 pl-3 pr-3 pt-2 pb-2">
                            <table>
                                <tr>
                                    <td style="font-weight: bold"><?= lang("operation.equipment_owner") ?></td>
                                    <td>: </td>
                                </tr>
                                <tr>
                                    <td><?= clean_string($equipment_owner) ?></td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </td>
        </tr>
    </table>

    <table border="1"  cellspacing="0" cellpadding="5" style="width: 100%;">
        <tr>
            <td>
                <div class="container-fluid">
                    <div class="row" style="min-height: 50px;">
                        <div class="col-12 pl-3 pr-3 pt-2 pb-2">
                            <table>
                                <tr>
                                    <td style="font-weight: bold"><?= lang("operation.equipment_location") ?></td>
                                    <td>: </td>
                                </tr>
                                <tr>
                                    <td><?= clean_string($equipment_location) ?></td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </td>
        </tr>
    </table>

    <table border="1"  cellspacing="0" cellpadding="5" style="width: 100%;">
        <tr>
            <td>
                <div class="container-fluid">
                    <div class="row" style="min-height: 50px;">
                        <div class="col-12 pl-3 pr-3 pt-2 pb-2">
                            <table>
                                <tr>
                                    <td style="font-weight: bold"><?= lang("operation.model_name") ?></td>
                                    <td>: </td>
                                </tr>
                                <tr>
                                    <td><?= clean_string($model_name) ?></td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </td>
        </tr>
    </table>

    <table border="1"  cellspacing="0" cellpadding="5" style="width: 100%;">
        <tr>
            <td>
                <div class="container-fluid">
                    <div class="row" style="min-height: 50px;">
                        <div class="col-12 pl-3 pr-3 pt-2 pb-2">
                            <table>
                                <tr>
                                    <td style="font-weight: bold"><?= lang("operation.asset_number") ?></td>
                                    <td>: </td>
                                </tr>
                                <tr>
                                    <td><?= clean_string($asset_number) ?></td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </td>
        </tr>
    </table>

    <table border="1"  cellspacing="0" cellpadding="5" style="width: 100%;">
        <tr>
            <td>
                <div class="container-fluid">
                    <div class="row" style="min-height: 50px;">
                        <div class="col-12 pl-3 pr-3 pt-2 pb-2">
                            <table>
                                <tr>
                                    <td style="font-weight: bold"><?= lang("operation.date_of_purchase") ?></td>
                                    <td>: </td>
                                </tr>
                                <tr>
                                    <td><?= clean_string($date_of_purchase) ?></td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </td>
        </tr>
    </table>

    <br /><br />

    <table border="1"  cellspacing="0" cellpadding="2" style="width: 100%;">
        <tr>
            <td class="p-3" style="font-size: 12px">
                <div style="min-height: 100px;">
                    <div class="container-fluid">
                        <div class="row pl-3 pr-3">
                            <div class="col row">
                                <div class="col" style="font-weight: bold">
                                    User
                                </div>
                                <div class="col" style="font-weight: bold">
                                    Apprv 1
                                </div>
                            </div>
                            <div class="col row">
                                <div align="center" class="col" style="margin-left: -3%; font-weight: bold;">
                                    Apprv 2
                                </div>
                            </div>
                        </div>

                        <div style="height: 100px"></div>

                        <div class="row pl-3 pr-3">
                            <div class="col row">
                                <div class="col ml-3 mr-3" style="border-bottom: 1px solid black;"></div>
                                <div class="col ml-3 mr-3" style="border-bottom: 1px solid black;"></div>
                            </div>
                            <div class="col row">
                                <div class="col ml-3 mr-3" style="border-bottom: 1px solid black;"></div>
                                <div class="col ml-3 mr-3" style="border-bottom: 1px solid black;"></div>
                            </div>
                        </div>

                        <div class="row pl-3 pr-3">
                            <div class="col row">
                                <div class="col">
                                    Tgl :
                                </div>
                                <div class="col">
                                    Tgl :
                                </div>
                            </div>
                            <div class="col row">
                                <div class="col">
                                    Tgl :
                                </div>
                                <div class="col">
                                    Tgl :
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </td>
        </tr>
    </table>
</div>
</body>
</html>
