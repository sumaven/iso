<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
    <meta charset="utf-8">
    <title> PDF </title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <style type="text/css">
        table {
            page-break-inside: avoid
        }

        tr {
            page-break-inside: avoid;
            page-break-after: auto
        }

        thead {
            display: table-header-group
        }

        tfoot {
            display: table-footer-group
        }

        body {
            font-size: 12px;
        }
    </style>

</head>
<body>
<div class="container-fluid">
    <div class="row m-4">
        <div class="col-3">
            <img src="<?= $logo->url ?>" width="80%">
        </div>
        <div align="center" class="col-6 mt-auto mb-auto">
            <h5 style="font-weight: bold">Asia Global Servis</h5>
        </div>
        <div class="col-3 mt-auto mb-auto">
            <table>
                <tr>
                    <td class="pr-3" style="font-weight: bold">Doc.</td>
                    <td> :</td>
                    <td class="pl-2"></td>
                </tr>
                <tr>
                    <td class="pr-3" style="font-weight: bold">Form No.</td>
                    <td> :</td>
                    <td class="pl-2"></td>
                </tr>
                <tr>
                    <td class="pr-3" style="font-weight: bold">Rev.</td>
                    <td> :</td>
                    <td class="pl-2"></td>
                </tr>
            </table>
        </div>
    </div>

    <div class="row mb-3 mt-2">
        <div class="col-12">
            <div class="d-flex justify-content-center align-items-center">
                <div><span style="font-size: 15px; font-weight: bold;">  </span></div>
            </div>
        </div>
    </div>

    <table border="1" cellspacing="0" cellpadding="5" style="width: 100%;">
        <tr>
            <td>
                <div class="container-fluid">
                    <div class="row" style="min-height: 50px;">
                        <div class="col-12 pl-3 pr-3 pt-2 pb-2">
                            <table>
                                <tr>
                                    <td style="font-weight: bold"><?= lang("management_review.date_of_management_review") ?></td>
                                    <td>:</td>
                                </tr>
                                <tr>
                                    <td><?= clean_string($date_of_management_review) ?></td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </td>
        </tr>
    </table>

    <table border="1" cellspacing="0" cellpadding="5" style="width: 100%;">
        <tr>
            <td>
                <div class="container-fluid">
                    <div class="row" style="min-height: 50px;">
                        <div class="col-12 pl-3 pr-3 pt-2 pb-2">
                            <table>
                                <tr>
                                    <td style="font-weight: bold"><?= lang("management_review.approve_author") ?></td>
                                    <td>:</td>
                                </tr>
                                <tr>
                                    <td><?= get_approval_val($approve_author) ?></td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </td>
        </tr>
    </table>

    <table border="1" cellspacing="0" cellpadding="5" style="width: 100%;">
        <tr>
            <td>
                <div class="container-fluid">
                    <div class="row" style="min-height: 50px;">
                        <div class="col-12 pl-3 pr-3 pt-2 pb-2">
                            <table>
                                <tr>
                                    <td style="font-weight: bold"><?= lang("management_review.attendee") ?></td>
                                    <td>:</td>
                                </tr>
                                <tr>
                                    <td><?= clean_string($attendee) ?></td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </td>
        </tr>
    </table>

    <table border="1" cellspacing="0" cellpadding="5" style="width: 100%; margin-bottom: 20px; margin-top: 20px">
        <thead>
        <tr>
            <th>No</th>
            <th>Agenda</th>
            <th>Resposibility</th>
            <th>Document Required</th>
        </tr>
        </thead>
        <tbody>
        <?php $no = 0; ?>
        <?php foreach ($internal_audit_management_review_items as $item): ?>
        <?php ++$no; ?>
        <tr>
            <td><?= $no ?></td>
            <td><?= $item["agenda"] ?></td>
            <td><?= $item["responsibility"] ?></td>
            <td><?= $item["document_required"] ?></td>
        </tr>
        <?php endforeach; ?>
        </tbody>
    </table>

    <br /><br />

    <table border="1"  cellspacing="0" cellpadding="2" style="width: 100%;">
        <tr>
            <td class="p-3" style="font-size: 12px">
                <div style="min-height: 100px;">
                    <div class="container-fluid">
                        <div class="row pl-3 pr-3">
                            <div class="col row">
                                <div class="col" style="font-weight: bold">
                                    User
                                </div>
                                <div class="col" style="font-weight: bold">
                                    Apprv 1
                                </div>
                            </div>
                            <div class="col row">
                                <div align="center" class="col" style="margin-left: -3%; font-weight: bold;">
                                    Apprv 2
                                </div>
                            </div>
                        </div>

                        <div style="height: 100px"></div>

                        <div class="row pl-3 pr-3">
                            <div class="col row">
                                <div class="col ml-3 mr-3" style="border-bottom: 1px solid black;"></div>
                                <div class="col ml-3 mr-3" style="border-bottom: 1px solid black;"></div>
                            </div>
                            <div class="col row">
                                <div class="col ml-3 mr-3" style="border-bottom: 1px solid black;"></div>
                                <div class="col ml-3 mr-3" style="border-bottom: 1px solid black;"></div>
                            </div>
                        </div>

                        <div class="row pl-3 pr-3">
                            <div class="col row">
                                <div class="col">
                                    Tgl :
                                </div>
                                <div class="col">
                                    Tgl :
                                </div>
                            </div>
                            <div class="col row">
                                <div class="col">
                                    Tgl :
                                </div>
                                <div class="col">
                                    Tgl :
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </td>
        </tr>
    </table>
</div>
</body>
</html>
