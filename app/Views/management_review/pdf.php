<!DOCTYPE html>
<html lang="en" dir="ltr">
    <head>
        <meta charset="utf-8">
        <title>Treatment Document</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

        <style type="text/css">
            table { page-break-inside:avoid }
            tr    { page-break-inside:avoid; page-break-after:auto }
            thead { display:table-header-group }
            tfoot { display:table-footer-group }
        </style>

    </head>
    <body>

        <table border="1"  cellspacing="0" cellpadding="2" style="width: 100%; margin-bottom: 30px; font-size: 10px;">
            <tr>
                <td style="width: 20%; height: 60px; text-align: center;" rowspan="4">
                    <img src="<?= $logo->url ?>" width="100%">
                </td>
                <td style="width: 50%; text-align: center; font-weight: bold;" rowspan="4">
                    Asia Global Servis
                </td>
                <td style="width: 30%; height: 20px;">
                    Doc : <?= $doc ?>
                </td>
            </tr>
            <tr>
                <td style="height: 20px;">
                    Rev : <?= $rev ?>
                </td>
            </tr>
            <tr>
                <td>
                    Referensi : <?= $referensi ?>
                </td>
            </tr>
            <tr>
                <td>
                    Klausul : <?= $klausul ?>
                </td>
            </tr>
            <tr>
                <td colspan="3" style="height: 30px; vertical-align: center">&nbsp;&nbsp;&nbsp;&nbsp;Title : <?= $header_title ?></td>
            </tr>
        </table>

        <table cellspacing="0" cellpadding="5" border="1" style="width: 100%; font-size: 10px;margin-top: 20px;">
            <tr>
                <td style="background-color: #ccc; height: 20px; width: 100%; padding-left: 20px;">
                    Deskripsi Perubahan
                </td>
            </tr>
            <tr>
                <td style="height: 100px; padding-left: 20px;">
                    <?= $deskripsi_perubahan ?>
                </td>
            </tr>
        </table>

        <table cellspacing="0" cellpadding="5" border="1" style="width: 100%; font-size: 10px;margin-top: 20px;">
            <tr>
                <td style="background-color: #ccc; height: 20px; width: 100%; padding-left: 20px;">
                    Ruang Lingkup
                </td>
            </tr>
            <tr>
                <td style="height: 100px; padding-left: 20px;">
                    <?= $ruang_lingkup ?>
                </td>
            </tr>
        </table>

        <table cellspacing="0" cellpadding="5" border="1" style="width: 100%; font-size: 10px;margin-top: 20px;">
            <tr>
                <td style="background-color: #ccc; height: 20px; width: 100%; padding-left: 20px;">
                    Tujuan
                </td>
            </tr>
            <tr>
                <td style="height: 100px; padding-left: 20px;">
                    <?= $tujuan ?>
                </td>
            </tr>
        </table>

        <table cellspacing="0" cellpadding="5" border="1" style="width: 100%; font-size: 10px;margin-top: 20px;">
            <tr>
                <td style="background-color: #ccc; height: 20px; width: 100%; padding-left: 20px;">
                    Definisi
                </td>
            </tr>
            <tr>
                <td style="height: 100px; padding-left: 20px;">
                    <?= $definisi ?>
                </td>
            </tr>
        </table>

        <table cellspacing="0" cellpadding="5" border="1" style="width: 100%; font-size: 10px;margin-top: 20px;">
            <tr>
                <td style="background-color: #ccc; height: 20px; width: 100%; padding-left: 20px;">
                    Informasi Umum
                </td>
            </tr>
            <tr>
                <td style="height: 100px; padding-left: 20px;">
                    <?= $informasi_umum ?>
                </td>
            </tr>
        </table>

        <table border="1" cellspacing="0" cellpadding="2" style="width: 100%; font-size: 10px;margin-top: 20px;">
            <tr>
                <td style="background-color: #ccc; height: 20px; width: 100%; padding-left: 20px;">
                    Prosedur
                </td>
            </tr>
            <tr>
                <td style="height: 100px; padding-left: 20px;">
                    <?= $prosedur ?>
                </td>
            </tr>
        </table>

        <table cellspacing="0" cellpadding="5" border="1" style="width: 100%; font-size: 10px;margin-top: 20px;">
            <tr>
                <td style="background-color: #ccc; height: 20px; width: 100%; padding-left: 20px;">
                    Keadaan Khusus
                </td>
            </tr>
            <tr>
                <td style="height: 100px; padding-left: 20px;">
                    <?= $keadaan_khusus ?>
                </td>
            </tr>
        </table>

        <table cellspacing="0" cellpadding="5" border="1" style="width: 100%; font-size: 10px;margin-top: 20px;">
            <tr>
                <td style="background-color: #ccc; height: 20px; width: 100%; padding-left: 20px;">
                    Rekaman
                </td>
            </tr>
            <tr>
                <td style="height: 100px; padding-left: 20px;">
                    <?= $rekaman ?>
                </td>
            </tr>
        </table>

        <table cellspacing="0" cellpadding="5" border="1" style="width: 100%; font-size: 10px;margin-top: 20px;">
            <tr>
                <td style="background-color: #ccc; height: 20px; width: 100%; padding-left: 20px;">
                    Lampiran
                </td>
            </tr>
            <tr>
                <td style="height: 100px; padding-left: 20px;">
                    <?= $lampiran ?>
                </td>
            </tr>
        </table>
        <br /><br />

        <div style="width:50%; margin-right: 0px; margin-left: auto;">
            <table cellspacing="0" cellpadding="0" border="1" style="width: 100%; font-size: 10px;">
                <tr style="width: 100%">
                    <td align="center" style="background-color: #ccc; height: 20px; width: 15%; text-align: center;">
                        Persetujuan
                    </td>
                    <td align="center" style="background-color: #ccc; height: 20px; width: 30%; text-align: center;">
                        <?= $approval_1 ?>
                    </td>
                    <td align="center" style="background-color: #ccc; height: 20px; width: 30%; text-align: center;">
                        <?= $approval_2 ?>
                    </td>
                </tr>

                <tr>
                    <td style="height: 75px; text-align: center;">
                         Tanda tangan
                    </td>
                    <td>
                        <img src="<?= $ttd1 ?>">
                    </td>
                    <td>
                        <img src="<?= $ttd2 ?>">
                    </td>
                </tr>


            </table>
        </div>
    </body>
</html>
