<?php

?><!DOCTYPE html>
<html lang="en">

<?php echo view('_partials/header'); ?>

<body class="no-skin">

<?php echo view('_partials/navbar'); ?>

<div class="main-container ace-save-state" id="main-container">
    <script type="text/javascript">
        try {
            ace.settings.loadState('main-container')
        } catch (e) {
        }
    </script>

    <?php echo view('_partials/sidebar'); ?>

    <div class="main-content">
        <div class="main-content-inner">
            <div class="breadcrumbs ace-save-state" id="breadcrumbs">
                <ul class="breadcrumb">
                    <li>
                        <i class="ace-icon fa fa-home home-icon"></i>
                        <a href="#">Home</a>
                    </li>
                    <li class="active"><?= $module_title ?></li>
                </ul><!-- /.breadcrumb -->
            </div>

            <div class="page-content">
                <div class="ace-settings-container" id="ace-settings-container">
                    <div class="btn btn-app btn-xs btn-warning ace-settings-btn" id="ace-settings-btn">
                        <i class="ace-icon fa fa-cog bigger-130"></i>
                    </div>

                    <div class="ace-settings-box clearfix" id="ace-settings-box">
                        <div class="pull-left width-50">
                            <div class="ace-settings-item">
                                <div class="pull-left">
                                    <select id="skin-colorpicker" class="hide">
                                        <option data-skin="no-skin" value="#438EB9">#438EB9</option>
                                        <option data-skin="skin-1" value="#222A2D">#222A2D</option>
                                        <option data-skin="skin-2" value="#C6487E">#C6487E</option>
                                        <option data-skin="skin-3" value="#D0D0D0">#D0D0D0</option>
                                    </select>
                                </div>
                                <span>&nbsp; Choose Skin</span>
                            </div>

                            <div class="ace-settings-item">
                                <input type="checkbox" class="ace ace-checkbox-2 ace-save-state"
                                       id="ace-settings-navbar" autocomplete="off"/>
                                <label class="lbl" for="ace-settings-navbar"> Fixed Navbar</label>
                            </div>

                            <div class="ace-settings-item">
                                <input type="checkbox" class="ace ace-checkbox-2 ace-save-state"
                                       id="ace-settings-sidebar" autocomplete="off"/>
                                <label class="lbl" for="ace-settings-sidebar"> Fixed Sidebar</label>
                            </div>

                            <div class="ace-settings-item">
                                <input type="checkbox" class="ace ace-checkbox-2 ace-save-state"
                                       id="ace-settings-breadcrumbs" autocomplete="off"/>
                                <label class="lbl" for="ace-settings-breadcrumbs"> Fixed Breadcrumbs</label>
                            </div>

                            <div class="ace-settings-item">
                                <input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-rtl"
                                       autocomplete="off"/>
                                <label class="lbl" for="ace-settings-rtl"> Right To Left (rtl)</label>
                            </div>

                            <div class="ace-settings-item">
                                <input type="checkbox" class="ace ace-checkbox-2 ace-save-state"
                                       id="ace-settings-add-container" autocomplete="off"/>
                                <label class="lbl" for="ace-settings-add-container">
                                    Inside
                                    <b>.container</b>
                                </label>
                            </div>
                        </div><!-- /.pull-left -->

                        <div class="pull-left width-50">
                            <div class="ace-settings-item">
                                <input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-hover"
                                       autocomplete="off"/>
                                <label class="lbl" for="ace-settings-hover"> Submenu on Hover</label>
                            </div>

                            <div class="ace-settings-item">
                                <input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-compact"
                                       autocomplete="off"/>
                                <label class="lbl" for="ace-settings-compact"> Compact Sidebar</label>
                            </div>

                            <div class="ace-settings-item">
                                <input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-highlight"
                                       autocomplete="off"/>
                                <label class="lbl" for="ace-settings-highlight"> Alt. Active Item</label>
                            </div>
                        </div><!-- /.pull-left -->
                    </div><!-- /.ace-settings-box -->
                </div><!-- /.ace-settings-container -->

                <div class="page-header">
                    <h1>
                        <a href="<?php echo base_url("smk3/checklist_audit_eksternal/main/list") ?>"><i
                                    class="fa fa-arrow-circle-left"></i></a>
                        <?= $module_title ?>
                        <small>
                            <i class="ace-icon fa fa-angle-double-right"></i>
                            <?= $submodule_title ?>
                        </small>
                    </h1>
                </div><!-- /.page-header -->

                <div class="row">
                    <div class="col-xs-12">

                        <div class="table-header" style="margin-bottom: 15px">
                            Kriteria
                        </div>

                        <div>
                            <div class="col-lg-8">

                                <?php
                                //                                if (array_key_exists("description", $submain_list)) :
                                //                                if (empty($_GET['sub_main'])) {
                                //                                    $url = base_url("smk3/checklist_audit_eksternal/sub_main/list?main={$_GET["main"]}&sub_main={$key}");
                                //                                } else if (empty($_GET['sub_main_2'])) {
                                //                                    $url = base_url("smk3/checklist_audit_eksternal/sub_main/list?main={$_GET["main"]}&sub_main={$_GET["sub_main"]}&sub_main2={$key}");
                                //                                } else {
                                //                                    $url = base_url("smk3/checklist_audit_eksternal/indicator/list?main={$_GET["main"]}&sub_main={$_GET["sub_main"]}&sub_main2={{$_GET["sub_main2"]}&sub_main3={$key}");
                                //                                }
                                ?>

                                <?php
                                foreach ($submain_list as $key => $value):
                                    if (is_array($value) && array_key_exists("sub", $value)) {
                                        if (isset($_GET['sub_main'])) {
                                            $url = base_url("smk3/checklist_audit_eksternal/sub_main/list?main={$_GET["main"]}&sub_main={$_GET['sub_main']}-{$key}");
                                        } else {
                                            $url = base_url("smk3/checklist_audit_eksternal/sub_main/list?main={$_GET["main"]}&sub_main={$key}");
                                        }
                                    } else {
                                        if (isset($_GET['sub_main'])) {
                                            $url = base_url("smk3/checklist_audit_eksternal/indicator/list?main={$_GET["main"]}&sub_main={$_GET['sub_main']}-{$key}");
                                        } else {
                                            $url = base_url("smk3/checklist_audit_eksternal/indicator/list?main={$_GET["main"]}&sub_main={$key}");
                                        }
                                    }
                                    ?>

                                    <div class="col-xs-12">
                                        <div class="x_panel">

                                            <div class="x_content bs-example-popovers">
                                                <h5>
                                                    <label class="badge badge-primary"
                                                           style="height:30px; text-align:center; padding:8px; background:#95C6F8;">
                                                        K <?= $_GET['main'] . "." . $key ?></label>
                                                    <?php
                                                    if (is_array($value) && array_key_exists("description", $value)) {
                                                        echo $value["description"];
                                                    } else {
                                                        echo $value;
                                                    }
                                                    ?>

                                                    <span style="float:right;">
                                                <span class="label label-info">0%</span>

                                                <a href="<?= $url ?>"
                                                   title="Nilai indikator">
                                                    <img src="<?php echo base_url('assets/images/indikator.png'); ?>"
                                                         style="width:30px;"/>
                                                </a>
                                                </span>
                                                </h5>
                                            </div>
                                        </div>
                                    </div>

                                <?php endforeach; ?>

                            </div>
                        </div>

                    </div>
                </div><!-- /.row -->
            </div><!-- /.page-content -->
        </div>
    </div><!-- /.main-content -->

    

    <a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
        <i class="ace-icon fa fa-angle-double-up icon-only bigger-110"></i>
    </a>
</div><!-- /.main-container -->

<?php echo view("_partials/read_script"); ?>

</body>
</html>
