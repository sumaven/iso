<?php

?><!DOCTYPE html>
<html lang="en">

<?php echo view('_partials/header'); ?>

<body class="no-skin">

<?php echo view('_partials/navbar'); ?>

<div class="main-container ace-save-state" id="main-container">
    <script type="text/javascript">
        try {
            ace.settings.loadState('main-container')
        } catch (e) {
        }
    </script>

    <?php echo view('_partials/sidebar'); ?>

    <div class="main-content">
        <div class="main-content-inner">
            <div class="breadcrumbs ace-save-state" id="breadcrumbs">
                <ul class="breadcrumb">
                    <li>
                        <i class="ace-icon fa fa-home home-icon"></i>
                        <a href="#">Home</a>
                    </li>
                    <li class="active"><?= $module_title ?></li>
                </ul><!-- /.breadcrumb -->
            </div>

            <div class="page-content">
                <div class="ace-settings-container" id="ace-settings-container">
                    <div class="btn btn-app btn-xs btn-warning ace-settings-btn" id="ace-settings-btn">
                        <i class="ace-icon fa fa-cog bigger-130"></i>
                    </div>

                    <div class="ace-settings-box clearfix" id="ace-settings-box">
                        <div class="pull-left width-50">
                            <div class="ace-settings-item">
                                <div class="pull-left">
                                    <select id="skin-colorpicker" class="hide">
                                        <option data-skin="no-skin" value="#438EB9">#438EB9</option>
                                        <option data-skin="skin-1" value="#222A2D">#222A2D</option>
                                        <option data-skin="skin-2" value="#C6487E">#C6487E</option>
                                        <option data-skin="skin-3" value="#D0D0D0">#D0D0D0</option>
                                    </select>
                                </div>
                                <span>&nbsp; Choose Skin</span>
                            </div>

                            <div class="ace-settings-item">
                                <input type="checkbox" class="ace ace-checkbox-2 ace-save-state"
                                       id="ace-settings-navbar" autocomplete="off"/>
                                <label class="lbl" for="ace-settings-navbar"> Fixed Navbar</label>
                            </div>

                            <div class="ace-settings-item">
                                <input type="checkbox" class="ace ace-checkbox-2 ace-save-state"
                                       id="ace-settings-sidebar" autocomplete="off"/>
                                <label class="lbl" for="ace-settings-sidebar"> Fixed Sidebar</label>
                            </div>

                            <div class="ace-settings-item">
                                <input type="checkbox" class="ace ace-checkbox-2 ace-save-state"
                                       id="ace-settings-breadcrumbs" autocomplete="off"/>
                                <label class="lbl" for="ace-settings-breadcrumbs"> Fixed Breadcrumbs</label>
                            </div>

                            <div class="ace-settings-item">
                                <input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-rtl"
                                       autocomplete="off"/>
                                <label class="lbl" for="ace-settings-rtl"> Right To Left (rtl)</label>
                            </div>

                            <div class="ace-settings-item">
                                <input type="checkbox" class="ace ace-checkbox-2 ace-save-state"
                                       id="ace-settings-add-container" autocomplete="off"/>
                                <label class="lbl" for="ace-settings-add-container">
                                    Inside
                                    <b>.container</b>
                                </label>
                            </div>
                        </div><!-- /.pull-left -->

                        <div class="pull-left width-50">
                            <div class="ace-settings-item">
                                <input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-hover"
                                       autocomplete="off"/>
                                <label class="lbl" for="ace-settings-hover"> Submenu on Hover</label>
                            </div>

                            <div class="ace-settings-item">
                                <input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-compact"
                                       autocomplete="off"/>
                                <label class="lbl" for="ace-settings-compact"> Compact Sidebar</label>
                            </div>

                            <div class="ace-settings-item">
                                <input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-highlight"
                                       autocomplete="off"/>
                                <label class="lbl" for="ace-settings-highlight"> Alt. Active Item</label>
                            </div>
                        </div><!-- /.pull-left -->
                    </div><!-- /.ace-settings-box -->
                </div><!-- /.ace-settings-container -->

                <div class="page-header">
                    <h1>
                        <?= $module_title ?>
                        <small>
                            <i class="ace-icon fa fa-angle-double-right"></i>
                            <?= $submodule_title ?>
                        </small>
                    </h1>
                </div><!-- /.page-header -->

                <div class="row">
                    <div class="col-xs-12">

                        <div class="table-header">
                            Criteria
                            <i class="ace-icon fa fa-angle-double-right"></i>
                            <a href="#" style="padding-left: 0px;" class="table-header"> main</a>
                        </div>

                        <div style="margin-top:15px">
                            <div class="col-lg-6">
                                <div class="col-xs-12">
                                    <div class="x_panel">
                                        <div class="x_content bs-example-popovers">
                                            <h4>
                                                <a href="<?php echo base_url("smk3/checklist_audit_eksternal/sub_main/list?main=1") ?>">
                                                    <label class="badge badge-primary"
                                                           style="width:30px; height:30px; text-align:center; padding:8px; background:#95C6F8;">
                                                        P1</label> Pembangunan dan Pemeliharaan Komitmen </a>
                                            </h4>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-xs-12">
                                    <div class="x_panel">

                                        <div class="x_content bs-example-popovers">
                                            <h4>
                                                <a href="<?php echo base_url("smk3/checklist_audit_eksternal/sub_main/list?main=2") ?>">
                                                    <label class="badge badge-primary"
                                                           style="width:30px; height:30px; text-align:center; padding:8px; background:#95C6F8;">
                                                        P2</label> Pembuatan dan Pendokumentasian Rencana K3 </a></h4>
                                            </a>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-xs-12">
                                    <div class="x_panel">

                                        <div class="x_content bs-example-popovers">
                                            <h4>
                                                <a href="<?php echo base_url("smk3/checklist_audit_eksternal/sub_main/list?main=3") ?>">
                                                    <label class="badge badge-primary"
                                                           style="width:30px; height:30px; text-align:center; padding:8px; background:#95C6F8;">
                                                        P3</label> Pengendalian Perancangan dan Peninjauan Kontrak </a>
                                            </h4>
                                            </a>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-xs-12">
                                    <div class="x_panel">

                                        <div class="x_content bs-example-popovers">
                                            <h4>
                                                <a href="<?php echo base_url("smk3/checklist_audit_eksternal/sub_main/list?main=4") ?>">
                                                    <label class="badge badge-primary"
                                                           style="width:30px; height:30px; text-align:center; padding:8px; background:#95C6F8;">
                                                        P4</label> Pengendalian Dokumen </a></h4>
                                            </a>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-xs-12">
                                    <div class="x_panel">

                                        <div class="x_content bs-example-popovers">
                                            <h4>
                                                <a href="<?php echo base_url("smk3/checklist_audit_eksternal/sub_main/list?main=5") ?>">
                                                    <label class="badge badge-primary"
                                                           style="width:30px; height:30px; text-align:center; padding:8px; background:#95C6F8;">
                                                        P5</label> Pembelian dan Pengendalian Produk </a></h4>
                                            </a>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-xs-12">
                                    <div class="x_panel">

                                        <div class="x_content bs-example-popovers">
                                            <h4>
                                                <a href="<?php echo base_url("smk3/checklist_audit_eksternal/sub_main/list?main=6") ?>">
                                                    <label class="badge badge-primary"
                                                           style="width:30px; height:30px; text-align:center; padding:8px; background:#95C6F8;">
                                                        P6</label> Tanggung Jawab Sosial dan Pemberdayaan Ekonomi
                                                    Masyarakat </a></h4>
                                            </a>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-xs-12">
                                    <div class="x_panel">

                                        <div class="x_content bs-example-popovers">
                                            <h4>
                                                <a href="<?php echo base_url("smk3/checklist_audit_eksternal/sub_main/list?main=7") ?>">
                                                    <label class="badge badge-primary"
                                                           style="width:30px; height:30px; text-align:center; padding:8px; background:#95C6F8;">
                                                        P7</label> Standard Pemantauan </a></h4>
                                            </a>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-xs-12">
                                    <div class="x_panel">

                                        <div class="x_content bs-example-popovers">
                                            <h4>
                                                <a href="<?php echo base_url("smk3/checklist_audit_eksternal/sub_main/list?main=8") ?>">
                                                    <label class="badge badge-primary"
                                                           style="width:30px; height:30px; text-align:center; padding:8px; background:#95C6F8;">
                                                        P8</label> Pelaporan dan Perbaikan Kekurangan </a></h4>
                                            </a>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-xs-12">
                                    <div class="x_panel">

                                        <div class="x_content bs-example-popovers">
                                            <h4>
                                                <a href="<?php echo base_url("smk3/checklist_audit_eksternal/sub_main/list?main=9") ?>">
                                                    <label class="badge badge-primary"
                                                           style="width:30px; height:30px; text-align:center; padding:8px; background:#95C6F8;">
                                                        P9</label> Pengelolaan Material dan Perpindahannya </a></h4>
                                            </a>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-xs-12">
                                    <div class="x_panel">

                                        <div class="x_content bs-example-popovers">
                                            <h4>
                                                <a href="<?php echo base_url("smk3/checklist_audit_eksternal/sub_main/list?main=10") ?>">
                                                    <label class="badge badge-primary"
                                                           style="width:30px; height:30px; text-align:center; padding:8px; background:#95C6F8;">
                                                        P10</label> Pengumpulan dan Penggunaan Data </a></h4>
                                            </a>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-xs-12">
                                    <div class="x_panel">

                                        <div class="x_content bs-example-popovers">
                                            <h4>
                                                <a href="<?php echo base_url("smk3/checklist_audit_eksternal/sub_main/list?main=11") ?>">
                                                    <label class="badge badge-primary"
                                                           style="width:30px; height:30px; text-align:center; padding:8px; background:#95C6F8;">
                                                        P11</label> Pemeriksaan SMK3 </a></h4>
                                            </a>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-xs-12">
                                    <div class="x_panel">

                                        <div class="x_content bs-example-popovers">
                                            <h4>
                                                <a href="<?php echo base_url("smk3/checklist_audit_eksternal/sub_main/list?main=12") ?>">
                                                    <label class="badge badge-primary"
                                                           style="width:30px; height:30px; text-align:center; padding:8px; background:#95C6F8;">
                                                        P12</label> Pengembangan Ketrampilan dan Kemampuan </a></h4>
                                            </a>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>

                        <div class="col-lg-6 col-md-12">

                            <div class="x_panel">
                                <div class="x_content">
                                    <div class="col-xs-12">
                                        <canvas id="radar-chart1" height="250"></canvas>
                                    </div>
                                </div>

                                <div class="x_content">
                                    <table class="table table-responsive table-bordered">
                                        <thead>
                                        <tr>
                                            <th>Kode</th>
                                            <th>Prinsip</th>
                                            <th>Score</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td>P1</td>
                                            <td>Pembangunan dan Pemeliharaan Komitmen</td>
                                            <td><?= number_format(($percentages[1] ?? 0) * 100, 2, '.', ''); ?>%</td>
                                        </tr>
                                        <tr>
                                            <td>P2</td>
                                            <td>Pembuatan dan Pendokumentasian Rencana K3</td>
                                            <td><?= number_format(($percentages[2] ?? 0) * 100, 2, '.', ''); ?>%</td>
                                        </tr>
                                        <tr>
                                            <td>P3</td>
                                            <td>Pengendalian Perancangan dan Peninjauan Kontrak</td>
                                            <td><?= number_format(($percentages[3] ?? 0) * 100, 2, '.', ''); ?>%</td>
                                        </tr>
                                        <tr>
                                            <td>P4</td>
                                            <td>Pengendalian Dokumen</td>
                                            <td><?= number_format(($percentages[4] ?? 0) * 100, 2, '.', ''); ?>%</td>
                                        </tr>
                                        <tr>
                                            <td>P5</td>
                                            <td>Pembelian dan Pengendalian Produk</td>
                                            <td><?= number_format(($percentages[5] ?? 0) * 100, 2, '.', ''); ?>%</td>
                                        </tr>
                                        <tr>
                                            <td>P6</td>
                                            <td>Keamanan Bekerja Berdasarkan SMK3</td>
                                            <td><?= number_format(($percentages[6] ?? 0) * 100, 2, '.', ''); ?>%</td>
                                        </tr>
                                        <tr>
                                            <td>P7</td>
                                            <td>Standard Pemantauan</td>
                                            <td><?= number_format(($percentages[7] ?? 0) * 100, 2, '.', ''); ?>%</td>
                                        </tr>
                                        <tr>
                                            <td>P8</td>
                                            <td>Pelaporan dan Perbaikan Kekurangan</td>
                                            <td><?= number_format(($percentages[8] ?? 0) * 100, 2, '.', ''); ?>%</td>
                                        </tr>
                                        <tr>
                                            <td>P9</td>
                                            <td>Pengelolaan Material dan Perpindahannya</td>
                                            <td><?= number_format(($percentages[9] ?? 0) * 100, 2, '.', ''); ?>%</td>
                                        </tr>
                                        <tr>
                                            <td>P10</td>
                                            <td>Pengumpulan dan Penggunaan Data</td>
                                            <td><?= number_format(($percentages[10] ?? 0) * 100, 2, '.', ''); ?>%</td>
                                        </tr>
                                        <tr>
                                            <td>P11</td>
                                            <td>Pemeriksaan SMK3</td>
                                            <td><?= number_format(($percentages[11] ?? 0) * 100, 2, '.', ''); ?>%</td>
                                        </tr>
                                        <tr>
                                            <td>P12</td>
                                            <td>Pengembangan Ketrampilan dan Kemampuan</td>
                                            <td><?= number_format(($percentages[12] ?? 0) * 100, 2, '.', ''); ?>%</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>

                            <div class="x_panel">
                                <div class="x_content">
                                    <div class="col-xs-12">
                                        <canvas id="radar-chart2" height="250"></canvas>
                                    </div>
                                </div>

                                <div class="x_content">
                                    <table class="table table-responsive table-bordered">
                                        <thead>
                                        <tr>
                                            <th>Kode</th>
                                            <th>Prinsip</th>
                                            <th>Score</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td>P1</td>
                                            <td>Pembangunan dan Pemeliharaan Komitmen</td>
                                            <td>67.42%</td>
                                            <!--                                            <td>-->
                                            <? //= number_format(($percentages[1] * 100), 2, '.', '') . "%" ?><!--</td>-->
                                        </tr>
                                        <tr>
                                            <td>P2</td>
                                            <td>Pembuatan dan Pendokumentasian Rencana K3</td>
                                            <td>87.12%</td>
                                            <!--                                            <td>-->
                                            <? //= number_format(($percentages[2] * 100), 2, '.', '') . "%" ?><!--</td>-->
                                        </tr>
                                        <tr>
                                            <td>P3</td>
                                            <td>Pengendalian Perancangan dan Peninjauan Kontrak</td>
                                            <td>95.12%</td>
                                            <!--                                            <td>-->
                                            <? //= number_format(($percentages[3] * 100), 2, '.', '') . "%" ?><!--</td>-->
                                        </tr>
                                        <tr>
                                            <td>P4</td>
                                            <td>Pengendalian Dokumen</td>
                                            <td>77.65%</td>
                                            <!--                                            <td>-->
                                            <? //= number_format(($percentages[4] * 100), 2, '.', '') . "%" ?><!--</td>-->
                                        </tr>
                                        <tr>
                                            <td>P5</td>
                                            <td>Pembelian dan Pengendalian Produk</td>
                                            <td>81.73%</td>
                                            <!--                                            <td>-->
                                            <? //= number_format(($percentages[5] * 100), 2, '.', '') . "%" ?><!--</td>-->
                                        </tr>
                                        <tr>
                                            <td>P6</td>
                                            <td>Keamanan Bekerja Berdasarkan SMK3</td>
                                            <td>67.35%</td>
                                            <!--                                            <td>-->
                                            <? //= number_format(($percentages[6] * 100), 2, '.', '') . "%" ?><!--</td>-->
                                        </tr>
                                        <tr>
                                            <td>P7</td>
                                            <td>Standard Pemantauan</td>
                                            <td>75.37%</td>
                                            <!--                                            <td>-->
                                            <? //= number_format(($percentages[7] * 100), 2, '.', '') . "%" ?><!--</td>-->
                                        </tr>
                                        <tr>
                                            <td>P8</td>
                                            <td>Pelaporan dan Perbaikan Kekurangan</td>
                                            <td>90.37%</td>
                                            <!--                                            <td>-->
                                            <? //= number_format(($percentages[7] * 100), 2, '.', '') . "%" ?><!--</td>-->
                                        </tr>
                                        <tr>
                                            <td>P9</td>
                                            <td>Pengelolaan Material dan Perpindahannya</td>
                                            <td>87.37%</td>
                                            <!--                                            <td>-->
                                            <? //= number_format(($percentages[7] * 100), 2, '.', '') . "%" ?><!--</td>-->
                                        </tr>
                                        <tr>
                                            <td>P10</td>
                                            <td>Pengumpulan dan Penggunaan Data</td>
                                            <td>77.37%</td>
                                            <!--                                            <td>-->
                                            <? //= number_format(($percentages[7] * 100), 2, '.', '') . "%" ?><!--</td>-->
                                        </tr>
                                        <tr>
                                            <td>P11</td>
                                            <td>Pemeriksaan SMK3</td>
                                            <td>85.37%</td>
                                            <!--                                            <td>-->
                                            <? //= number_format(($percentages[7] * 100), 2, '.', '') . "%" ?><!--</td>-->
                                        </tr>
                                        <tr>
                                            <td>P12</td>
                                            <td>Pengembangan Ketrampilan dan Kemampuan</td>
                                            <td>68.37%</td>
                                            <!--                                            <td>-->
                                            <? //= number_format(($percentages[7] * 100), 2, '.', '') . "%" ?><!--</td>-->
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>

                            <div class="x_panel">
                                <div class="x_content">
                                    <div class="col-xs-12">
                                        <canvas id="radar-chart3" height="250"></canvas>
                                    </div>
                                </div>

                                <div class="x_content">
                                    <table class="table table-responsive table-bordered">
                                        <thead>
                                        <tr>
                                            <th>Kode</th>
                                            <th>Prinsip</th>
                                            <th>Score</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td>P1</td>
                                            <td>Pembangunan dan Pemeliharaan Komitmen</td>
                                            <td>67.53%</td>
                                            <!--                                            <td>-->
                                            <? //= number_format(($percentages[1] * 100), 2, '.', '') . "%" ?><!--</td>-->
                                        </tr>
                                        <tr>
                                            <td>P2</td>
                                            <td>Pembuatan dan Pendokumentasian Rencana K3</td>
                                            <td>87.35%</td>
                                            <!--                                            <td>-->
                                            <? //= number_format(($percentages[2] * 100), 2, '.', '') . "%" ?><!--</td>-->
                                        </tr>
                                        <tr>
                                            <td>P3</td>
                                            <td>Pengendalian Perancangan dan Peninjauan Kontrak</td>
                                            <td>95.35%</td>
                                            <!--                                            <td>-->
                                            <? //= number_format(($percentages[3] * 100), 2, '.', '') . "%" ?><!--</td>-->
                                        </tr>
                                        <tr>
                                            <td>P4</td>
                                            <td>Pengendalian Dokumen</td>
                                            <td>87.75%</td>
                                            <!--                                            <td>-->
                                            <? //= number_format(($percentages[4] * 100), 2, '.', '') . "%" ?><!--</td>-->
                                        </tr>
                                        <tr>
                                            <td>P5</td>
                                            <td>Pembelian dan Pengendalian Produk</td>
                                            <td>81.73%</td>
                                            <!--                                            <td>-->
                                            <? //= number_format(($percentages[5] * 100), 2, '.', '') . "%" ?><!--</td>-->
                                        </tr>
                                        <tr>
                                            <td>P6</td>
                                            <td>Keamanan Bekerja Berdasarkan SMK3</td>
                                            <td>67.35%</td>
                                            <!--                                            <td>-->
                                            <? //= number_format(($percentages[6] * 100), 2, '.', '') . "%" ?><!--</td>-->
                                        </tr>
                                        <tr>
                                            <td>P7</td>
                                            <td>Standard Pemantauan</td>
                                            <td>25.35%</td>
                                            <!--                                            <td>-->
                                            <? //= number_format(($percentages[7] * 100), 2, '.', '') . "%" ?><!--</td>-->
                                        </tr>
                                        <tr>
                                            <td>P8</td>
                                            <td>Pelaporan dan Perbaikan Kekurangan</td>
                                            <td>90.35%</td>
                                            <!--                                            <td>-->
                                            <? //= number_format(($percentages[7] * 100), 2, '.', '') . "%" ?><!--</td>-->
                                        </tr>
                                        <tr>
                                            <td>P9</td>
                                            <td>Pengelolaan Material dan Perpindahannya</td>
                                            <td>87.35%</td>
                                            <!--                                            <td>-->
                                            <? //= number_format(($percentages[7] * 100), 2, '.', '') . "%" ?><!--</td>-->
                                        </tr>
                                        <tr>
                                            <td>P10</td>
                                            <td>Pengumpulan dan Penggunaan Data</td>
                                            <td>77.35%</td>
                                            <!--                                            <td>-->
                                            <? //= number_format(($percentages[7] * 100), 2, '.', '') . "%" ?><!--</td>-->
                                        </tr>
                                        <tr>
                                            <td>P11</td>
                                            <td>Pemeriksaan SMK3</td>
                                            <td>85.35%</td>
                                            <!--                                            <td>-->
                                            <? //= number_format(($percentages[7] * 100), 2, '.', '') . "%" ?><!--</td>-->
                                        </tr>
                                        <tr>
                                            <td>P12</td>
                                            <td>Pengembangan Ketrampilan dan Kemampuan</td>
                                            <td>68.35%</td>
                                            <!--                                            <td>-->
                                            <? //= number_format(($percentages[7] * 100), 2, '.', '') . "%" ?><!--</td>-->
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>

                    </div>
                </div><!-- /.row -->
            </div><!-- /.page-content -->
        </div>
    </div><!-- /.main-content -->


    <a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
        <i class="ace-icon fa fa-angle-double-up icon-only bigger-110"></i>
    </a>
</div><!-- /.main-container -->

<!-- basic scripts -->

<!--[if !IE]> -->
<script src="<?php echo base_url("assets"); ?>/js/jquery-2.1.4.min.js"></script>

<!-- <![endif]-->

<!--[if IE]>
<script src="assets/js/jquery-1.11.3.min.js"></script>
<![endif]-->
<script type="text/javascript">
    if ('ontouchstart' in document.documentElement) document.write("<script src='<?php echo base_url("assets"); ?>/js/jquery.mobile.custom.min.js'>" + "<" + "/script>");
</script>
<script src="<?php echo base_url("assets"); ?>/js/bootstrap.min.js"></script>

<!-- page specific plugin scripts -->

<!--[if lte IE 8]>
<script src="assets/js/excanvas.min.js"></script>
<![endif]-->

<script src="<?php echo base_url("assets"); ?>/js/jquery-ui.custom.min.js"></script>
<script src="<?php echo base_url("assets"); ?>/js/jquery.ui.touch-punch.min.js"></script>
<script src="<?php echo base_url("assets"); ?>/js/jquery.easypiechart.min.js"></script>
<script src="<?php echo base_url("assets"); ?>/js/jquery.sparkline.index.min.js"></script>
<script src="<?php echo base_url("assets"); ?>/js/jquery.flot.min.js"></script>
<script src="<?php echo base_url("assets"); ?>/js/jquery.flot.pie.min.js"></script>
<script src="<?php echo base_url("assets"); ?>/js/jquery.flot.resize.min.js"></script>

<!-- ace scripts -->
<script src="<?php echo base_url("assets"); ?>/js/ace-elements.min.js"></script>
<script src="<?php echo base_url("assets"); ?>/js/ace.min.js"></script>


<!-- BEGIN: Page Vendor JS-->
<script src="https://pixinvent.com/modern-admin-clean-bootstrap-4-dashboard-html-template/app-assets/vendors/js/charts/chart.min.js"></script>
<script src="https://pixinvent.com/modern-admin-clean-bootstrap-4-dashboard-html-template/app-assets/vendors/js/charts/d3.min.js"></script>
<script src="https://pixinvent.com/modern-admin-clean-bootstrap-4-dashboard-html-template/app-assets/vendors/js/charts/c3.min.js"></script>
<!-- END: Page Vendor JS-->

<script>
    /*=========================================================================================
    File Name: radar.js
    Description: Chartjs radar chart
    ----------------------------------------------------------------------------------------
    Item Name: Modern Admin - Clean Bootstrap 4 Dashboard HTML Template
   Version: 3.0
    Author: PIXINVENT
    Author URL: http://www.themeforest.net/user/pixinvent
==========================================================================================*/

    // Radar chart
    // ------------------------------
    $(window).on("load", function () {

        //Get the context of the Chart canvas element we want to select
        var ctx = $("#radar-chart1");
        var ctx2 = $("#radar-chart2");
        var ctx3 = $("#radar-chart3");

        // Chart Options
        var chartOptions = {
            responsive: true,
            maintainAspectRatio: false,
            responsiveAnimationDuration: 400,
            legend: {
                position: 'top',
            },
            title: {
                display: false,
                text: ''
            },
            scale: {
                reverse: false,
                ticks: {
                    callback: function (value, index, values) {
                        return (value.toFixed(2)) + "%";
                    },
                    beginAtZero: true
                }
            }
        };

        // Chart Data
        var chartData = {
            labels: ["P1", "P2", "P3", "P4", "P5", "P6", "P7", "P8", "P9", "P10", "P11", "P12"],
            datasets: [{
                label: "PT. A",
                backgroundColor: "rgba(49,187,230, .3)",
                borderColor: "transparent",
                pointBorderColor: "#1d90b1",
                pointBackgroundColor: "#FFF",
                pointBorderWidth: 2,
                pointHoverBorderWidth: 2,
                pointRadius: 2,
                data: [
                    <?= number_format(($percentages[1] ?? 0) * 100, 2, '.', ''); ?>,
                    <?= number_format(($percentages[2] ?? 0) * 100, 2, '.', ''); ?>,
                    <?= number_format(($percentages[3] ?? 0) * 100, 2, '.', ''); ?>,
                    <?= number_format(($percentages[4] ?? 0) * 100, 2, '.', ''); ?>,
                    <?= number_format(($percentages[5] ?? 0) * 100, 2, '.', ''); ?>,
                    <?= number_format(($percentages[6] ?? 0) * 100, 2, '.', ''); ?>,
                    <?= number_format(($percentages[7] ?? 0) * 100, 2, '.', ''); ?>,
                    <?= number_format(($percentages[8] ?? 0) * 100, 2, '.', ''); ?>,
                    <?= number_format(($percentages[9] ?? 0) * 100, 2, '.', ''); ?>,
                    <?= number_format(($percentages[10] ?? 0) * 100, 2, '.', ''); ?>,
                    <?= number_format(($percentages[11] ?? 0) * 100, 2, '.', ''); ?>,
                    <?= number_format(($percentages[12] ?? 0) * 100, 2, '.', ''); ?>,
                ],
            },]
        };

        // Chart Data
        var chartData2 = {
            labels: ["P1", "P2", "P3", "P4", "P5", "P6", "P7", "P8", "P9", "P10", "P11", "P12"],
            datasets: [{
                label: "PT. B",
                backgroundColor: "rgba(230,179,49,0.3)",
                borderColor: "transparent",
                pointBorderColor: "#b19d1d",
                pointBackgroundColor: "#FFF",
                pointBorderWidth: 2,
                pointHoverBorderWidth: 2,
                pointRadius: 2,
                data: [
                    67.42,
                    87.12,
                    95.12,
                    78.65,
                    53.73,
                    65.35,
                    71.37,
                    85.37,
                    71.37,
                    87.37,
                    77.37,
                    68.37,
                ],
            },]
        };

        // Chart Data
        var chartData3 = {
            labels: ["P1", "P2", "P3", "P4", "P5", "P6", "P7", "P8", "P9", "P10", "P11", "P12"],
            datasets: [{
                label: "PT. C",
                backgroundColor: "rgba(103,230,49,0.3)",
                borderColor: "transparent",
                pointBorderColor: "#33b11d",
                pointBackgroundColor: "#FFF",
                pointBorderWidth: 2,
                pointHoverBorderWidth: 2,
                pointRadius: 2,
                data: [
                    67.53,
                    87.35,
                    95.35,
                    89.75,
                    93.73,
                    40.35,
                    25.35,
                    85.35,
                    25.35,
                    87.35,
                    77.35,
                    68.35,
                ],
            },]
        };

        var config = {
            type: 'radar',
            // Chart Options
            options: chartOptions,
            data: chartData
        };

        // Create the chart
        var polarChart = new Chart(ctx, config);
        var polarChart2 = new Chart(ctx2,
            {
                type: 'radar',
                // Chart Options
                options: chartOptions,
                data: chartData2
            }
        );
        var polarChart3 = new Chart(ctx3,
            {
                type: 'radar',
                // Chart Options
                options: chartOptions,
                data: chartData3
            }
        );
    });
</script>

<!--
<?= number_format(((isset($percentages[1]) ? $percentages[1] : 0) * 100), 2, '.', '') ?>,
<?= number_format(((isset($percentages[2]) ? $percentages[2] : 0) * 100), 2, '.', '') ?>,
<?= number_format(((isset($percentages[3]) ? $percentages[3] : 0) * 100), 2, '.', '') ?>,
<?= number_format(((isset($percentages[4]) ? $percentages[4] : 0) * 100), 2, '.', '') ?>,
<?= number_format(((isset($percentages[5]) ? $percentages[5] : 0) * 100), 2, '.', '') ?>,
<?= number_format(((isset($percentages[6]) ? $percentages[6] : 0) * 100), 2, '.', '') ?>,
<?= number_format(((isset($percentages[7]) ? $percentages[7] : 0) * 100), 2, '.', '') ?>
-->

</body>
</html>
