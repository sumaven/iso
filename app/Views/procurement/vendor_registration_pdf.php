<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
    <meta charset="utf-8">
    <title>Treatment Document</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <style type="text/css">
        table {
            page-break-inside: avoid
        }

        tr {
            page-break-inside: avoid;
            page-break-after: auto
        }

        thead {
            display: table-header-group
        }

        tfoot {
            display: table-footer-group
        }
    </style>

</head>
<body>

<table border="1" cellspacing="0" cellpadding="2" style="width: 100%; margin-bottom: 30px; font-size: 10px;">
    <tr>
        <td style="width: 20%; height: 60px; text-align: center;" rowspan="4">
            <img src="<?= $logo->url ?>" width="100%">
        </td>
        <td style="width: 50%; text-align: center; font-weight: bold;" rowspan="4">
            Asia Global Servis
        </td>
        <td style="width: 30%; height: 20px;">
            Doc :
        </td>
    </tr>
    <tr>
        <td style="height: 20px;">
            Rev :
        </td>
    </tr>
    <tr>
        <td>
            Referensi :
        </td>
    </tr>
    <tr>
        <td>
            Klausul :
        </td>
    </tr>
    <tr>
        <td colspan="3" style="height: 30px; vertical-align: center">&nbsp;&nbsp;&nbsp;&nbsp;Title :
    </tr>
</table>

<table cellspacing="0" cellpadding="5" border="1" style="width: 100%; font-size: 10px;margin-top: 20px;">
    <tr>
        <td style="background-color: #ccc; height: 20px; width: 100%; padding-left: 20px;">
            Deskripsi <?= lang("procurement.company_type") ?>
        </td>
    </tr>
    <tr>
        <td style="height: 100px; padding-left: 20px;">
            <?= $company_type ?>
        </td>
    </tr>
</table>

<table cellspacing="0" cellpadding="5" border="1" style="width: 100%; font-size: 10px;margin-top: 20px;">
    <tr>
        <td style="background-color: #ccc; height: 20px; width: 100%; padding-left: 20px;">
            Ruang <?= lang("procurement.vendor_name") ?>
        </td>
    </tr>
    <tr>
        <td style="height: 100px; padding-left: 20px;">
            <?= $vendor_name ?>
        </td>
    </tr>
</table>

<table cellspacing="0" cellpadding="5" border="1" style="width: 100%; font-size: 10px;margin-top: 20px;">
    <tr>
        <td style="background-color: #ccc; height: 20px; width: 100%; padding-left: 20px;">
            <?= lang("procurement.directore_name") ?>
        </td>
    </tr>
    <tr>
        <td style="height: 100px; padding-left: 20px;">
            <?= $directore_name ?>
        </td>
    </tr>
</table>

<table cellspacing="0" cellpadding="5" border="1" style="width: 100%; font-size: 10px;margin-top: 20px;">
    <tr>
        <td style="background-color: #ccc; height: 20px; width: 100%; padding-left: 20px;">
            <?= lang("procurement.phone_number") ?>
        </td>
    </tr>
    <tr>
        <td style="height: 100px; padding-left: 20px;">
            <?= $phone_number ?>
        </td>
    </tr>
</table>

<table cellspacing="0" cellpadding="5" border="1" style="width: 100%; font-size: 10px;margin-top: 20px;">
    <tr>
        <td style="background-color: #ccc; height: 20px; width: 100%; padding-left: 20px;">
            Informasi <?= lang("procurement.email") ?>
        </td>
    </tr>
    <tr>
        <td style="height: 100px; padding-left: 20px;">
            <?= $email ?>
        </td>
    </tr>
</table>

<table border="1" cellspacing="0" cellpadding="2" style="width: 100%; font-size: 10px;margin-top: 20px;">
    <tr>
        <td style="background-color: #ccc; height: 20px; width: 100%; padding-left: 20px;">
            <?= lang("procurement.npwp_number") ?>
        </td>
    </tr>
    <tr>
        <td style="height: 100px; padding-left: 20px;">
            <?= $npwp_number ?>
        </td>
    </tr>
</table>

<table cellspacing="0" cellpadding="5" border="1" style="width: 100%; font-size: 10px;margin-top: 20px;">
    <tr>
        <td style="background-color: #ccc; height: 20px; width: 100%; padding-left: 20px;">
            <?= lang("procurement.nib_number") ?>
        </td>
    </tr>
    <tr>
        <td style="height: 100px; padding-left: 20px;">
            <?= $nib_number ?>
        </td>
    </tr>
</table>

<div style="width:50%; margin-right: 0px; margin-left: auto;">
    <table cellspacing="0" cellpadding="0" border="1" style="width: 100%; font-size: 10px;">
        <tr style="width: 100%">
            <td align="center" style="background-color: #ccc; height: 20px; width: 15%; text-align: center;">
                Persetujuan
            </td>
            <td align="center" style="background-color: #ccc; height: 20px; width: 30%; text-align: center;">

            </td>
            <td align="center" style="background-color: #ccc; height: 20px; width: 30%; text-align: center;">

            </td>
        </tr>

        <tr>
            <td style="height: 75px; text-align: center;">
                Tanda tangan
            </td>
            <td>
                <img src="">
            </td>
            <td>
                <img src="">
            </td>
        </tr>


    </table>
</div>
</body>
</html>
