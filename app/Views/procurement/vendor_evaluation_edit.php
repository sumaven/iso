<!DOCTYPE html>
<html lang="en">

<?php echo view('_partials/header'); ?>

<body class="no-skin">

<?php echo view('_partials/navbar'); ?>

<div class="main-container ace-save-state" id="main-container">
    <script type="text/javascript">
        try {
            ace.settings.loadState('main-container')
        } catch (e) {
        }
    </script>

    <?php echo view('_partials/sidebar'); ?>

    <div class="main-content">
        <div class="main-content-inner">
            <div class="breadcrumbs ace-save-state" id="breadcrumbs">
                <ul class="breadcrumb">
                    <li>
                        <i class="ace-icon fa fa-home home-icon"></i>
                        <a href="#">Home</a>
                    </li>
                    <li class="active"><?= $module_title ?></li>
                </ul><!-- /.breadcrumb -->
            </div>

            <div class="page-content">
                <div class="ace-settings-container" id="ace-settings-container">
                    <div class="btn btn-app btn-xs btn-warning ace-settings-btn" id="ace-settings-btn">
                        <i class="ace-icon fa fa-cog bigger-130"></i>
                    </div>

                    <div class="ace-settings-box clearfix" id="ace-settings-box">
                        <div class="pull-left width-50">
                            <div class="ace-settings-item">
                                <div class="pull-left">
                                    <select id="skin-colorpicker" class="hide">
                                        <option data-skin="no-skin" value="#438EB9">#438EB9</option>
                                        <option data-skin="skin-1" value="#222A2D">#222A2D</option>
                                        <option data-skin="skin-2" value="#C6487E">#C6487E</option>
                                        <option data-skin="skin-3" value="#D0D0D0">#D0D0D0</option>
                                    </select>
                                </div>
                                <span>&nbsp; Choose Skin</span>
                            </div>

                            <div class="ace-settings-item">
                                <input type="checkbox" class="ace ace-checkbox-2 ace-save-state"
                                       id="ace-settings-navbar" autocomplete="off"/>
                                <label class="lbl" for="ace-settings-navbar"> Fixed Navbar</label>
                            </div>

                            <div class="ace-settings-item">
                                <input type="checkbox" class="ace ace-checkbox-2 ace-save-state"
                                       id="ace-settings-sidebar" autocomplete="off"/>
                                <label class="lbl" for="ace-settings-sidebar"> Fixed Sidebar</label>
                            </div>

                            <div class="ace-settings-item">
                                <input type="checkbox" class="ace ace-checkbox-2 ace-save-state"
                                       id="ace-settings-breadcrumbs" autocomplete="off"/>
                                <label class="lbl" for="ace-settings-breadcrumbs"> Fixed Breadcrumbs</label>
                            </div>

                            <div class="ace-settings-item">
                                <input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-rtl"
                                       autocomplete="off"/>
                                <label class="lbl" for="ace-settings-rtl"> Right To Left (rtl)</label>
                            </div>

                            <div class="ace-settings-item">
                                <input type="checkbox" class="ace ace-checkbox-2 ace-save-state"
                                       id="ace-settings-add-container" autocomplete="off"/>
                                <label class="lbl" for="ace-settings-add-container">
                                    Inside
                                    <b>.container</b>
                                </label>
                            </div>
                        </div><!-- /.pull-left -->

                        <div class="pull-left width-50">
                            <div class="ace-settings-item">
                                <input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-hover"
                                       autocomplete="off"/>
                                <label class="lbl" for="ace-settings-hover"> Submenu on Hover</label>
                            </div>

                            <div class="ace-settings-item">
                                <input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-compact"
                                       autocomplete="off"/>
                                <label class="lbl" for="ace-settings-compact"> Compact Sidebar</label>
                            </div>

                            <div class="ace-settings-item">
                                <input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-highlight"
                                       autocomplete="off"/>
                                <label class="lbl" for="ace-settings-highlight"> Alt. Active Item</label>
                            </div>
                        </div><!-- /.pull-left -->
                    </div><!-- /.ace-settings-box -->
                </div><!-- /.ace-settings-container -->

                <div class="page-header">
                    <h1>
                        <?= $submodule_title ?>
                        <small><i class="ace-icon fa fa-angle-double-right"></i> Add New </small>
                    </h1>
                </div><!-- /.page-header -->


                <!-- PAGE CONTENT BEGINS -->
                <?php
                if (session()->get('id') != $row->id_employee || $row->step != 0) {
                    $readonly = "disabled";
                } else {
                    $readonly = "";
                }
                ?>
                <form class="form-horizontal container-fluid" role="form"
                      action="<?php echo $controller_path_url . "/process_edit" ?>" method="POST">

                    <input style="display:none" type="text" name="id" id="form-field-1"/>

                    <div class="row">
                        <img src="<?= base_url("assets/img/vendor_evaluation.png") ?>"/>
                    </div>

                    <div class="row">
                        <div class="col-xs-6">
                            <div class="form-group">
                                <label class="col-sm-3 control-label no-padding-right" > Kode
                                    Dok. </label>

                                <div class="col-sm-9">
                                    <input type="text" name="doc" id="form-field-1" placeholder="Kode Dokumen"
                                           class="form-control" <?= $readonly ?> />
                                </div>
                            </div>
                        </div><!-- /.col -->
                        <div class="col-xs-6">
                            <div class="form-group">
                                <label class="col-sm-3 control-label no-padding-right" > Revision Code </label>

                                <div class="col-sm-9">
                                    <input type="text" name="rev" id="form-field-1" placeholder="Kode Rev"
                                           class="form-control" <?= $readonly ?> />
                                </div>
                            </div>
                        </div><!-- /.col -->
                    </div><!-- /.row -->

                    <div class="row">
                        <div class="col-xs-6">
                            <div class="form-group">
                                <label class="col-sm-3 control-label no-padding-right" > Nomor
                                    Form </label>

                                <div class="col-sm-9">
                                    <input type="text" name="form_no" id="form-field-1" placeholder="Nomor Form"
                                           class="form-control" <?= $readonly ?> />
                                </div>
                            </div>
                        </div><!-- /.col -->
                    </div><!-- /.row -->

                    <br/>

                    <div class="row">
                        <div class="col-xs-6">
                            <div class="form-group">
                                <label class="col-sm-3 control-label no-padding-right" >
                                    Company </label>

                                <div class="col-sm-9">
                                    <input type="text" name="company" id="form-field-1" placeholder="Company"
                                           class="form-control" <?= $readonly ?> />
                                </div>
                            </div>
                        </div><!-- /.col -->
                        <div class="col-xs-6">
                            <div class="form-group">
                                <label class="col-sm-3 control-label no-padding-right" >
                                    Address </label>

                                <div class="col-sm-9">
                                    <input type="text" name="address" id="form-field-1" placeholder="Address"
                                           class="form-control" <?= $readonly ?> />
                                </div>
                            </div>
                        </div><!-- /.col -->
                    </div><!-- /.row -->

                    <div class="row">
                        <div class="col-xs-6">
                            <div class="form-group">
                                <label class="col-sm-3 control-label no-padding-right" >
                                    Phone </label>

                                <div class="col-sm-9">
                                    <input type="text" name="phone" id="form-field-1" placeholder="Phone"
                                           class="form-control" <?= $readonly ?> />
                                </div>
                            </div>
                        </div><!-- /.col -->

                        <div class="col-xs-6">
                            <div class="form-group">
                                <label class="col-sm-3 control-label no-padding-right" > Fax </label>

                                <div class="col-sm-9">
                                    <input type="text" name="fax" id="form-field-1" placeholder="Fax"
                                           class="form-control" <?= $readonly ?> />
                                </div>
                            </div>
                        </div><!-- /.col -->
                    </div><!-- /.row -->

                    <div class="row">
                        <div class="col-xs-6">
                            <div class="form-group">
                                <label class="col-sm-3 control-label no-padding-right" >
                                    Product </label>

                                <div class="col-sm-9">
                                    <input type="text" name="product" id="form-field-1" placeholder="Product"
                                           class="form-control" <?= $readonly ?> />
                                </div>
                            </div>
                        </div><!-- /.col -->

                        <div class="col-xs-6">
                            <div class="form-group">
                                <label class="col-sm-3 control-label no-padding-right" > NPWP
                                    No. </label>

                                <div class="col-sm-9">
                                    <input type="text" name="npwp" id="form-field-1" placeholder="NPWP No."
                                           class="form-control" <?= $readonly ?> />
                                </div>
                            </div>
                        </div><!-- /.col -->
                    </div>

                    <div class="row">
                        <div class="col-xs-6">
                            <div class="form-group">
                                <label class="col-sm-3 control-label no-padding-right" > TDP
                                    No. </label>

                                <div class="col-sm-9">
                                    <input type="text" name="tdp" id="form-field-1" placeholder="TDP No."
                                           class="form-control" <?= $readonly ?> />
                                </div>
                            </div>
                        </div><!-- /.col -->

                        <div class="col-xs-6">
                            <div class="form-group">
                                <label class="col-sm-3 control-label no-padding-right" > PKP
                                    No. </label>

                                <div class="col-sm-9">
                                    <input type="text" name="pkp" id="form-field-1" placeholder="PKP No."
                                           class="form-control" <?= $readonly ?> />
                                </div>
                            </div>
                        </div><!-- /.col -->
                    </div>

                    <div class="row">
                        <div class="col-xs-6">
                            <div class="form-group">
                                <label class="col-sm-3 control-label no-padding-right" > TDR
                                    No. </label>

                                <div class="col-sm-9">
                                    <input type="text" name="tdr" id="form-field-1" placeholder="TDR No."
                                           class="form-control" <?= $readonly ?> />
                                </div>
                            </div>
                        </div><!-- /.col -->

                        <div class="col-xs-6">
                            <div class="form-group">
                                <label class="col-sm-3 control-label no-padding-right" > SIUP
                                    No. </label>

                                <div class="col-sm-9">
                                    <input type="text" name="siup" id="form-field-1" placeholder="SIUP No."
                                           class="form-control" <?= $readonly ?> />
                                </div>
                            </div>
                        </div><!-- /.col -->
                    </div>

                    <br/>

                    <div class="row" style="margin: 30px 10px 30px -40px;">
                        <div class="col-sm-2">
                            <div class="form-group" style="text-align: right; font-weight: bold">
                                Evaluation Criteria
                            </div>
                        </div><!-- /.col -->

                        <div class="col-sm-10">
                            <div class="col-sm-4">
                                <div class="form-group" style="margin: 0 auto; text-align: center; font-weight: bold">
                                    <div class="col-sm-12">
                                        Standard Score
                                    </div>
                                </div>
                            </div><!-- /.col -->

                            <div class="col-sm-4">
                                <div class="form-group" style="margin: 0 auto; text-align: center; font-weight: bold">
                                    <div class="col-sm-12">
                                        Evaluation (W X S)
                                    </div>
                                </div>
                            </div><!-- /.col -->

                            <div class="col-sm-4">
                                <div class="form-group" style="margin: 0 auto; text-align: center; font-weight: bold">
                                    <div class="col-sm-12">
                                        Remarks
                                    </div>
                                </div>
                            </div><!-- /.col -->
                        </div>
                    </div>

                    <div class="row" style="margin: 30px 10px 30px -40px;">
                        <div class="col-sm-2">
                            <div class="form-group" style="text-align: right;">
                                Quality
                            </div>
                        </div><!-- /.col -->

                        <div class="col-sm-10">
                            <div class="col-sm-4">
                                <div class="form-group" style="margin: 0 auto">
                                    <div class="col-sm-12">
                                        <input type="number" name="quality_score" id="form-field-1" placeholder="score"
                                               class="form-control" <?= $readonly ?> />
                                    </div>
                                </div>
                            </div><!-- /.col -->

                            <div class="col-sm-4">
                                <div class="form-group" style="margin: 0 auto">
                                    <div class="col-sm-12">
                                        <input type="number" name="quality_evaluation" id="form-field-1"
                                               placeholder="evaluation" class="form-control" <?= $readonly ?> />
                                    </div>
                                </div>
                            </div><!-- /.col -->

                            <div class="col-sm-4">
                                <div class="form-group" style="margin: 0 auto">
                                    <div class="col-sm-12">
                                        <input type="number" name="quality_remarks" id="form-field-1"
                                               placeholder="remarks" class="form-control" <?= $readonly ?> />
                                    </div>
                                </div>
                            </div><!-- /.col -->
                        </div>
                    </div>

                    <div class="row" style="margin: 30px 10px 30px -40px;">
                        <div class="col-sm-2">
                            <div class="form-group" style="text-align: right;">
                                K3/APD
                            </div>
                        </div><!-- /.col -->

                        <div class="col-sm-10">
                            <div class="col-sm-4">
                                <div class="form-group" style="margin: 0 auto">
                                    <div class="col-sm-12">
                                        <input type="number" name="k3_apd_score" id="form-field-1" placeholder="score"
                                               class="form-control" <?= $readonly ?> />
                                    </div>
                                </div>
                            </div><!-- /.col -->

                            <div class="col-sm-4">
                                <div class="form-group" style="margin: 0 auto">
                                    <div class="col-sm-12">
                                        <input type="number" name="k3_apd_evaluation" id="form-field-1"
                                               placeholder="evaluation" class="form-control" <?= $readonly ?> />
                                    </div>
                                </div>
                            </div><!-- /.col -->

                            <div class="col-sm-4">
                                <div class="form-group" style="margin: 0 auto">
                                    <div class="col-sm-12">
                                        <input type="number" name="k3_apd_remarks" id="form-field-1"
                                               placeholder="remarks" class="form-control" <?= $readonly ?> />
                                    </div>
                                </div>
                            </div><!-- /.col -->
                        </div>
                    </div>

                    <div class="row" style="margin: 30px 10px 30px -40px;">
                        <div class="col-sm-2">
                            <div class="form-group" style="text-align: right;">
                                Quantity
                            </div>
                        </div><!-- /.col -->

                        <div class="col-sm-10">
                            <div class="col-sm-4">
                                <div class="form-group" style="margin: 0 auto">
                                    <div class="col-sm-12">
                                        <input type="number" name="quantity_score" id="form-field-1" placeholder="score"
                                               class="form-control" <?= $readonly ?> />
                                    </div>
                                </div>
                            </div><!-- /.col -->

                            <div class="col-sm-4">
                                <div class="form-group" style="margin: 0 auto">
                                    <div class="col-sm-12">
                                        <input type="number" name="quantity_evaluation" id="form-field-1"
                                               placeholder="evaluation" class="form-control" <?= $readonly ?> />
                                    </div>
                                </div>
                            </div><!-- /.col -->

                            <div class="col-sm-4">
                                <div class="form-group" style="margin: 0 auto">
                                    <div class="col-sm-12">
                                        <input type="number" name="quantity_remarks" id="form-field-1"
                                               placeholder="remarks" class="form-control" <?= $readonly ?> />
                                    </div>
                                </div>
                            </div><!-- /.col -->
                        </div>
                    </div>

                    <div class="row" style="margin: 30px 10px 30px -40px;">
                        <div class="col-sm-2">
                            <div class="form-group" style="text-align: right;">
                                Delivery
                            </div>
                        </div><!-- /.col -->

                        <div class="col-sm-10">
                            <div class="col-sm-4">
                                <div class="form-group" style="margin: 0 auto">
                                    <div class="col-sm-12">
                                        <input type="number" name="delivery_score" id="form-field-1" placeholder="score"
                                               class="form-control" <?= $readonly ?> />
                                    </div>
                                </div>
                            </div><!-- /.col -->

                            <div class="col-sm-4">
                                <div class="form-group" style="margin: 0 auto">
                                    <div class="col-sm-12">
                                        <input type="number" name="delivery_evaluation" id="form-field-1"
                                               placeholder="evaluation" class="form-control" <?= $readonly ?> />
                                    </div>
                                </div>
                            </div><!-- /.col -->

                            <div class="col-sm-4">
                                <div class="form-group" style="margin: 0 auto">
                                    <div class="col-sm-12">
                                        <input type="number" name="delivery_remarks" id="form-field-1"
                                               placeholder="remarks" class="form-control" <?= $readonly ?> />
                                    </div>
                                </div>
                            </div><!-- /.col -->
                        </div>
                    </div>

                    <div class="row" style="margin: 30px 10px 30px -40px;">
                        <div class="col-sm-2">
                            <div class="form-group" style="text-align: right;">
                                Respon to complains
                            </div>
                        </div><!-- /.col -->

                        <div class="col-sm-10">
                            <div class="col-sm-4">
                                <div class="form-group" style="margin: 0 auto">
                                    <div class="col-sm-12">
                                        <input type="number" name="respon_to_complains_score" id="form-field-1"
                                               placeholder="score" class="form-control" <?= $readonly ?> />
                                    </div>
                                </div>
                            </div><!-- /.col -->

                            <div class="col-sm-4">
                                <div class="form-group" style="margin: 0 auto">
                                    <div class="col-sm-12">
                                        <input type="number" name="respon_to_complains_evaluation" id="form-field-1"
                                               placeholder="evaluation" class="form-control" <?= $readonly ?> />
                                    </div>
                                </div>
                            </div><!-- /.col -->

                            <div class="col-sm-4">
                                <div class="form-group" style="margin: 0 auto">
                                    <div class="col-sm-12">
                                        <input type="number" name="respon_to_complains_remarks" id="form-field-1"
                                               placeholder="remarks" class="form-control" <?= $readonly ?> />
                                    </div>
                                </div>
                            </div><!-- /.col -->
                        </div>
                    </div>

                    <div class="row" style="margin: 30px 10px 30px -40px;">
                        <div class="col-sm-2">
                            <div class="form-group" style="text-align: right;">
                                Accuracy in Invoicing
                            </div>
                        </div><!-- /.col -->

                        <div class="col-sm-10">
                            <div class="col-sm-4">
                                <div class="form-group" style="margin: 0 auto">
                                    <div class="col-sm-12">
                                        <input type="number" name="accuracy_in_invoicing_score" id="form-field-1"
                                               placeholder="score" class="form-control" <?= $readonly ?> />
                                    </div>
                                </div>
                            </div><!-- /.col -->

                            <div class="col-sm-4">
                                <div class="form-group" style="margin: 0 auto">
                                    <div class="col-sm-12">
                                        <input type="number" name="accuracy_in_invoicing_evaluation" id="form-field-1"
                                               placeholder="evaluation" class="form-control" <?= $readonly ?> />
                                    </div>
                                </div>
                            </div><!-- /.col -->

                            <div class="col-sm-4">
                                <div class="form-group" style="margin: 0 auto">
                                    <div class="col-sm-12">
                                        <input type="number" name="accuracy_in_invoicing_remarks" id="form-field-1"
                                               placeholder="remarks" class="form-control" <?= $readonly ?> />
                                    </div>
                                </div>
                            </div><!-- /.col -->
                        </div>
                    </div>

                    <div class="row" style="margin: 30px 10px 30px -40px;">
                        <div class="col-sm-2">
                            <div class="form-group" style="text-align: right;">
                                Respon to Order
                            </div>
                        </div><!-- /.col -->

                        <div class="col-sm-10">
                            <div class="col-sm-4">
                                <div class="form-group" style="margin: 0 auto">
                                    <div class="col-sm-12">
                                        <input type="number" name="respon_to_order_score" id="form-field-1"
                                               placeholder="score" class="form-control" <?= $readonly ?> />
                                    </div>
                                </div>
                            </div><!-- /.col -->

                            <div class="col-sm-4">
                                <div class="form-group" style="margin: 0 auto">
                                    <div class="col-sm-12">
                                        <input type="number" name="respon_to_order_evaluation" id="form-field-1"
                                               placeholder="evaluation" class="form-control" <?= $readonly ?> />
                                    </div>
                                </div>
                            </div><!-- /.col -->

                            <div class="col-sm-4">
                                <div class="form-group" style="margin: 0 auto">
                                    <div class="col-sm-12">
                                        <input type="number" name="respon_to_order_remarks" id="form-field-1"
                                               placeholder="remarks" class="form-control" <?= $readonly ?> />
                                    </div>
                                </div>
                            </div><!-- /.col -->
                        </div>
                    </div>

                    <div class="row" style="margin: 20px;">
                        <div class="col-sm-12">
                            <div class="form-group" style="margin: 0px 20px 0px 20px;">
                                <label> Recommendation </label>
                                <div class="wysiwyg-editor" name="recommendation"></div>
                                <textarea style="display:none" name="recommendation"></textarea>
                            </div>
                        </div>
                    </div><!-- /.row -->

                    <?php
                    if (session()->get('id') == $row->id_employee && $row->step != 0) {
                        ?>
                        <div class="row">
                            <div class="col-xs-6">
                            <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" >
                                        Komentar </label>

                                    <div class="col-sm-9">
                                        <textarea name="komentar" placeholder="Komentar"
                                                  class="form-control"> </textarea>
                                    </div>
                                </div>
                            </div><!-- /.col -->
                        </div>
                        <?php
                    }
                    ?>

                    <?php
                    if (session()->get('id') == $row->id_employee) {
                        ?>
                        <div class="row clearfix form-actions">
                            <div class="col-md-offset-3 col-md-12">
                                <?php
                                if ($row->step != 0) {
                                    ?>
                                    <button class="btn btn-success" name="btn_submit" value="setuju" type="submit "/>
                                    <i class="ace-icon fa fa-check bigger-110"></i>
                                    Setuju

                                    <button class="btn btn-danger" name="btn_submit" value="kembalikan" type="submit"/>
                                    <i class="ace-icon fa fa-check bigger-110"></i>
                                    Kembalikan
                                    <?php
                                } else {
                                    ?>
                                    <button class="btn btn-info" name="btn_submit" value="edit" type="submit"/>
                                    <i class="ace-icon fa fa-check bigger-110"></i>
                                    Submit
                                    <?php
                                } ?>
                            </div>
                        </div>
                        <?php
                    }
                    ?>

                </form>
            </div><!-- /.page-content -->
        </div>
    </div><!-- /.main-content -->

    

    <a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
        <i class="ace-icon fa fa-angle-double-up icon-only bigger-110"></i>
    </a>
</div><!-- /.main-container -->

<!-- basic scripts -->

<!--[if !IE]> -->
<script src="<?php echo base_url("assets/assets"); ?>/js/jquery-2.1.4.min.js"></script>
<!-- <![endif]-->
<!--[if IE]>
<script src="assets/js/jquery-1.11.3.min.js"></script>
<![endif]-->

<script type="text/javascript">
    if ('ontouchstart' in document.documentElement) document.write("<script src='<?php echo base_url("assets/assets"); ?>/js/jquery.mobile.custom.min.js'>" + "<" + "/script>");
</script>
<script src="<?php echo base_url("assets/assets"); ?>/js/bootstrap.min.js"></script>

<!-- page specific plugin scripts -->

<!--[if lte IE 8]>
<script src="assets/js/excanvas.min.js"></script>
<![endif]-->
<script src="<?php echo base_url("assets/assets"); ?>/js/jquery-ui.custom.min.js"></script>
<script src="<?php echo base_url("assets/assets"); ?>/js/jquery.ui.touch-punch.min.js"></script>
<script src="<?php echo base_url("assets/assets"); ?>/js/markdown.min.js"></script>
<script src="<?php echo base_url("assets/assets"); ?>/js/bootstrap-markdown.min.js"></script>
<script src="<?php echo base_url("assets/assets"); ?>/js/jquery.hotkeys.index.min.js"></script>
<script src="<?php echo base_url("assets/assets"); ?>/js/bootstrap-wysiwyg.min.js"></script>
<script src="<?php echo base_url("assets/assets"); ?>/js/bootbox.js"></script>
<script src="<?php echo base_url("assets/assets"); ?>/js/bootstrap-wysiwyg.min.js"></script>
 <!-- ace scripts -->
<script src="<?php echo base_url("assets/assets"); ?>/js/ace-elements.min.js"></script>
<script src="<?php echo base_url("assets/assets"); ?>/js/ace.min.js"></script>

<script src="<?php echo base_url("assets/assets"); ?>/js/jquery.maskedinput.min.js"></script>
<script src="<?php echo base_url("assets/assets"); ?>/js/bootstrap-datepicker.min.js"></script>
<script>
    $("input[placeholder='yyyy-mm-dd']").datepicker({
        autoclose: true,
        format: 'yyyy-mm-dd',
        orientation: "bottom",
        todayHighlight: true,
    });
    $("input[placeholder='yyyy-mm-dd']").mask("9999-99-99");
    $("input[placeholder='yyyy-mm-dd']").css("width", "100%");
</script>

<!-- inline scripts related to this page -->
<script type="text/javascript">
    jQuery(function ($) {

        $('textarea[data-provide="markdown"]').each(function () {
            var $this = $(this);

            if ($this.data('markdown')) {
                $this.data('markdown').showEditor();
            } else $this.markdown()

            $this.parent().find('.btn').addClass('btn-white');
        })

        function parseMe(value) {
            return value.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;');
        }

        var convertToHtmlTag = function(convertToHtmlTag){
            var passValue = $("<span />", { html: convertToHtmlTag }).text();
            passValue = passValue.replace('&nbsp;', " ")
            return passValue
            //return document.createElement("span").innerText;
        };

        function showErrorAlert(reason, detail) {
            var msg = '';
            if (reason === 'unsupported-file-type') {
                msg = "Unsupported format " + detail;
            } else {
                //console.log("error uploading file", reason, detail);
            }
            $('<div class="alert"> <button type="button" class="close" data-dismiss="alert">&times;</button>' +
                '<strong>File upload error</strong> ' + msg + ' </div>').prependTo('#alerts');
        }

        $(".wysiwyg-editor").on('keyup DOMSubtreeModified', function () {
            $(this).siblings("textarea").val(parseMe($(this).html()))
        });

        $(".wysiwyg-editor").ace_wysiwyg({
            toolbar: [
                'font',
                null,
                'fontSize',
                null,
                {name: 'bold', className: 'btn-info'},
                {name: 'italic', className: 'btn-info'},
                {name: 'strikethrough', className: 'btn-info'},
                {name: 'underline', className: 'btn-info'},
                null,
                {name: 'insertunorderedlist', className: 'btn-success'},
                {name: 'insertorderedlist', className: 'btn-success'},
                {name: 'outdent', className: 'btn-purple'},
                {name: 'indent', className: 'btn-purple'},
                null,
                {name: 'justifyleft', className: 'btn-primary'},
                {name: 'justifycenter', className: 'btn-primary'},
                {name: 'justifyright', className: 'btn-primary'},
                {name: 'justifyfull', className: 'btn-inverse'},
                null,
                {name: 'createLink', className: 'btn-pink'},
                {name: 'unlink', className: 'btn-pink'},
                null,
                {name: 'insertImage', className: 'btn-success'},
                null,
                'foreColor',
                null,
                {name: 'undo', className: 'btn-grey'},
                {name: 'redo', className: 'btn-grey'}
            ],
            'wysiwyg': {
                fileUploadError: showErrorAlert
            }
        }).prev().addClass('wysiwyg-style2');

        //RESIZE IMAGE

        //Add Image Resize Functionality to Chrome and Safari
        //webkit browsers don't have image resize functionality when content is editable
        //so let's add something using jQuery UI resizable
        //another option would be opening a dialog for user to enter dimensions.
        if (typeof jQuery.ui !== 'undefined' && ace.vars['webkit']) {

            var lastResizableImg = null;

            function destroyResizable() {
                if (lastResizableImg == null) return;
                lastResizableImg.resizable("destroy");
                lastResizableImg.removeData('resizable');
                lastResizableImg = null;
            }

            var enableImageResize = function () {
                $('.wysiwyg-editor')
                    .on('mousedown', function (e) {
                        var target = $(e.target);
                        if (e.target instanceof HTMLImageElement) {
                            if (!target.data('resizable')) {
                                target.resizable({
                                    aspectRatio: e.target.width / e.target.height,
                                });
                                target.data('resizable', true);

                                if (lastResizableImg != null) {
                                    //disable previous resizable image
                                    lastResizableImg.resizable("destroy");
                                    lastResizableImg.removeData('resizable');
                                }
                                lastResizableImg = target;
                            }
                        }
                    })
                    .on('click', function (e) {
                        if (lastResizableImg != null && !(e.target instanceof HTMLImageElement)) {
                            destroyResizable();
                        }
                    })
                    .on('keydown', function () {
                        destroyResizable();
                    });
            }

            enableImageResize();

            /**
             //or we can load the jQuery UI dynamically only if needed
             if (typeof jQuery.ui !== 'undefined') enableImageResize();
             else {//load jQuery UI if not loaded
						//in Ace demo ./components will be replaced by correct components path
						$.getScript("assets/js/jquery-ui.custom.min.js", function(data, textStatus, jqxhr) {
							enableImageResize()
						});
					}
             */
        }

        $('input[name=id]').val(convertToHtmlTag('<?= filter_output($row->id) ?>'));

        $('input[name=doc]').val(convertToHtmlTag('<?= filter_output($row->doc) ?>'));
        $('input[name=form_no]').val(convertToHtmlTag('<?= filter_output($row->form_no) ?>'));
        $('input[name=rev]').val(convertToHtmlTag('<?= filter_output($row->rev) ?>'));
        $('input[name=company]').val(convertToHtmlTag('<?= filter_output($row->company) ?>'));
        $('input[name=address]').val(convertToHtmlTag('<?= filter_output($row->address) ?>'));
        $('input[name=phone]').val(convertToHtmlTag('<?= filter_output($row->phone) ?>'));
        $('input[name=fax]').val(convertToHtmlTag('<?= filter_output($row->fax) ?>'));
        $('input[name=product]').val(convertToHtmlTag('<?= filter_output($row->product) ?>'));
        $('input[name=npwp]').val(convertToHtmlTag('<?= filter_output($row->npwp) ?>'));
        $('input[name=tdp]').val(convertToHtmlTag('<?= filter_output($row->tdp) ?>'));
        $('input[name=pkp]').val(convertToHtmlTag('<?= filter_output($row->pkp) ?>'));
        $('input[name=tdr]').val(convertToHtmlTag('<?= filter_output($row->tdr) ?>'));
        $('input[name=siup]').val(convertToHtmlTag('<?= filter_output($row->siup) ?>'));

        $('input[name=quality_score]').val(convertToHtmlTag('<?= filter_output($row->quality_score) ?>'));
        $('input[name=quality_evaluation]').val(convertToHtmlTag('<?= filter_output($row->quality_evaluation) ?>'));
        $('input[name=quality_remarks]').val(convertToHtmlTag('<?= filter_output($row->quality_remarks) ?>'));

        $('input[name=k3_apd_score]').val(convertToHtmlTag('<?= filter_output($row->k3_apd_score) ?>'));
        $('input[name=k3_apd_evaluation]').val(convertToHtmlTag('<?= filter_output($row->k3_apd_evaluation) ?>'));
        $('input[name=k3_apd_remarks]').val(convertToHtmlTag('<?= filter_output($row->k3_apd_remarks) ?>'));

        $('input[name=quantity_score]').val(convertToHtmlTag('<?= filter_output($row->quantity_score) ?>'));
        $('input[name=quantity_evaluation]').val(convertToHtmlTag('<?= filter_output($row->quantity_evaluation) ?>'));
        $('input[name=quantity_remarks]').val(convertToHtmlTag('<?= filter_output($row->quantity_remarks) ?>'));

        $('input[name=delivery_score]').val(convertToHtmlTag('<?= filter_output($row->delivery_score) ?>'));
        $('input[name=delivery_evaluation]').val(convertToHtmlTag('<?= filter_output($row->delivery_evaluation) ?>'));
        $('input[name=delivery_remarks]').val(convertToHtmlTag('<?= filter_output($row->delivery_remarks) ?>'));

        $('input[name=respon_to_complains_score]').val(convertToHtmlTag('<?= filter_output($row->respon_to_complains_score) ?>'));
        $('input[name=respon_to_complains_evaluation]').val(convertToHtmlTag('<?= filter_output($row->respon_to_complains_evaluation) ?>'));
        $('input[name=respon_to_complains_remarks]').val(convertToHtmlTag('<?= filter_output($row->respon_to_complains_remarks) ?>'));

        $('input[name=accuracy_in_invoicing_score]').val(convertToHtmlTag('<?= filter_output($row->accuracy_in_invoicing_score) ?>'));
        $('input[name=accuracy_in_invoicing_evaluation]').val(convertToHtmlTag('<?= filter_output($row->accuracy_in_invoicing_evaluation) ?>'));
        $('input[name=accuracy_in_invoicing_remarks]').val(convertToHtmlTag('<?= filter_output($row->accuracy_in_invoicing_remarks) ?>'));

        $('input[name=respon_to_order_score]').val(convertToHtmlTag('<?= filter_output($row->respon_to_order_score) ?>'));
        $('input[name=respon_to_order_evaluation]').val(convertToHtmlTag('<?= filter_output($row->respon_to_order_evaluation) ?>'));
        $('input[name=respon_to_order_remarks]').val(convertToHtmlTag('<?= filter_output($row->respon_to_order_remarks) ?>'));

        $('div[name=recommendation]').html(convertToHtmlTag('<?= filter_output($row->recommendation) ?>'));

        <?php
        if (session()->get('id') == $row->id_employee && $row->step != 0) {
            echo <<<EOT
        $('.wysiwyg-editor').attr("contenteditable", false)
        $('.wysiwyg-editor').css("background-color", "#f5f5f5")
        $('.wysiwyg-editor').css("color", "#a29f9d")
EOT;
        }
        ?>
    });
</script>
</body>
</html>
