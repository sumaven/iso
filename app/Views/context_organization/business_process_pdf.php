<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
    <meta charset="utf-8">
    <title></title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <style type="text/css">
        table {
            page-break-inside: avoid
        }

        tr {
            page-break-inside: avoid;
            page-break-after: auto
        }

        thead {
            display: table-header-group
        }

        tfoot {
            display: table-footer-group
        }

        body {
            font-size: 12px;
        }
    </style>

</head>
<body>
<div class="container-fluid">
    <div class="row m-4">
        <div class="col-3">
            <img src="<?= $logo->url ?>" width="80%">
        </div>
        <div align="center" class="col-6 mt-auto mb-auto">
            <h5 style="font-weight: bold">Asia Global Servis</h5>
        </div>
        <div class="col-3 mt-auto mb-auto">
            <table>
                <tr>
                    <td class="pr-3" style="font-weight: bold">Doc.</td>
                    <td> :</td>
                    <td class="pl-2"><?= "" ?></td>
                </tr>
                <tr>
                    <td class="pr-3" style="font-weight: bold">Form No.</td>
                    <td> :</td>
                    <td class="pl-2"><?= "" ?></td>
                </tr>
                <tr>
                    <td class="pr-3" style="font-weight: bold">Rev.</td>
                    <td> :</td>
                    <td class="pl-2"><?= "" ?></td>
                </tr>
            </table>
        </div>
    </div>

    <div class="row mb-3 mt-2">
        <div class="col-12">
            <div class="d-flex justify-content-center align-items-center">
                <div><span style="font-size: 15px; font-weight: bold;"><?= "" ?></span></div>
            </div>
        </div>
    </div>

    <table border="1"  cellspacing="0" cellpadding="2" style="width: 100%;">
        <tr>
            <td>
                <div class="container-fluid">
                    <div class="row" style="min-height: 50px;">
                        <div class="col pl-sm-3 pr-sm-3 pt-sm-2 pb-sm-2" style="background: #eee">
                            <table>
                                <tr>
                                    <td style="font-weight: bold">Process Name</td>
                                    <td>: <?= $process_name ?></td>
                                </tr>
                                <tr>
                                    <td style="font-weight: bold">Process Type </td>
                                    <td>: <?= $process_type ?></td>
                                </tr>
                            </table>
                        </div>
                        <div class="col pl-sm-3 pr-sm-3 pt-sm-2 pb-sm-2" style="background: #eee">
                            <table>
                                <tr>
                                    <td style="font-weight: bold">Dept / Location</td>
                                    <td>: <?= $dept_location ?></td>
                                </tr>
                                <tr>
                                    <td style="font-weight: bold">Monitoring Methodology</td>
                                    <td>: <?= $monitoring_methodology ?></td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </td>
        </tr>
    </table>
    <table border="1" cellspacing="0" cellpadding="2" style="width: 100%;">
        <tr>
            <td class="p-3">
                <div style="min-height: 100px;">
                    <div class="d-flex flex-column">
                        <div>Input Category :</div>
                        <div><?= $input_category ?></div>
                    </div>
                </div>
            </td>
        </tr>
    </table>
    <table border="1" cellspacing="0" cellpadding="2" style="width: 100%;">
        <tr>
            <td class="p-3">
                <div style="min-height: 100px;">
                    <div class="d-flex flex-column">
                        <div>Input :</div>
                        <div><?= $input ?></div>
                    </div>
                </div>
            </td>
        </tr>
    </table>
    <table border="1" cellspacing="0" cellpadding="2" style="width: 100%;">
        <tr>
            <td class="p-3">
                <div style="min-height: 100px;">
                    <div class="d-flex flex-column">
                        <div>Output Category :</div>
                        <div><?= $output_category ?></div>
                    </div>
                </div>
            </td>
        </tr>
    </table>
    <table border="1" cellspacing="0" cellpadding="2" style="width: 100%;">
        <tr>
            <td class="p-3">
                <div style="min-height: 100px;">
                    <div class="d-flex flex-column">
                        <div>Output :</div>
                        <div><?= $output ?></div>
                    </div>
                </div>
            </td>
        </tr>
    </table>
    <table border="1" cellspacing="0" cellpadding="2" style="width: 100%;">
        <tr>
            <td class="p-3">
                <div style="min-height: 100px;">
                    <div class="d-flex flex-column">
                        <div>Performance Indicator :</div>
                        <div><?= $performance_indicator ?></div>
                    </div>
                </div>
            </td>
        </tr>
    </table>
    <table border="1" cellspacing="0" cellpadding="2" style="width: 100%;">
        <tr>
            <td class="p-3">
                <div style="min-height: 100px;">
                    <div class="d-flex flex-column">
                        <div>Monitoring Methodology :</div>
                        <div><?= $monitoring_methodology ?></div>
                    </div>
                </div>
            </td>
        </tr>
    </table>
    <table border="1" cellspacing="0" cellpadding="2" style="width: 100%;">
        <tr>
            <td class="p-3">
                <div style="min-height: 100px;">
                    <div class="d-flex flex-column">
                        <div>Measurement :</div>
                        <div><?= $measurement ?></div>
                    </div>
                </div>
            </td>
        </tr>
    </table>
    <table border="1" cellspacing="0" cellpadding="2" style="width: 100%;">
        <tr>
            <td class="p-3">
                <div style="min-height: 100px;">
                    <div class="d-flex flex-column">
                        <div>Resources :</div>
                        <div><?= $resources ?></div>
                    </div>
                </div>
            </td>
        </tr>
    </table>
    <table border="1" cellspacing="0" cellpadding="2" style="width: 100%;">
        <tr>
            <td class="p-3">
                <div style="min-height: 100px;">
                    <div class="d-flex flex-column">
                        <div>Process Documented Infomation :</div>
                        <div><?= $process_documented_information ?></div>
                    </div>
                </div>
            </td>
        </tr>
    </table>
    <table border="1" cellspacing="0" cellpadding="2" style="width: 100%;">
        <tr>
            <td class="p-3">
                <div style="min-height: 100px;">
                    <div class="d-flex flex-column">
                        <div>Person in Charge :</div>
                        <div><?= $person_in_charge ?></div>
                    </div>
                </div>
            </td>
        </tr>
    </table>

    <br /><br />

    <table border="1"  cellspacing="0" cellpadding="2" style="width: 100%;">
        <tr>
            <td class="p-3" style="font-size: 12px">
                <div style="min-height: 100px;">
                    <div class="container-fluid">
                        <div class="row pl-3 pr-3">
                            <div class="col row">
                                <div class="col" style="font-weight: bold">
                                    User
                                </div>
                                <div class="col" style="font-weight: bold">
                                    Apprv 1
                                </div>
                            </div>
                            <div class="col row">
                                <div align="center" class="col" style="margin-left: -3%; font-weight: bold;">
                                    Apprv 2
                                </div>
                            </div>
                        </div>

                        <div style="height: 100px"></div>

                        <div class="row pl-3 pr-3">
                            <div class="col row">
                                <div class="col ml-3 mr-3" style="border-bottom: 1px solid black;"></div>
                                <div class="col ml-3 mr-3" style="border-bottom: 1px solid black;"></div>
                            </div>
                            <div class="col row">
                                <div class="col ml-3 mr-3" style="border-bottom: 1px solid black;"></div>
                                <div class="col ml-3 mr-3" style="border-bottom: 1px solid black;"></div>
                            </div>
                        </div>

                        <div class="row pl-3 pr-3">
                            <div class="col row">
                                <div class="col">
                                    Tgl :
                                </div>
                                <div class="col">
                                    Tgl :
                                </div>
                            </div>
                            <div class="col row">
                                <div class="col">
                                    Tgl :
                                </div>
                                <div class="col">
                                    Tgl :
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </td>
        </tr>
    </table>
</div>
</body>
</html>
