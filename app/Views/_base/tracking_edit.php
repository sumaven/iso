<?php

?><!DOCTYPE html>
<html lang="en">

<?php echo view('_partials/header'); ?>

<body class="no-skin">

<?php echo view('_partials/navbar'); ?>

<div class="main-container ace-save-state" id="main-container">
    <script type="text/javascript">
        try {
            ace.settings.loadState('main-container')
        } catch (e) {
        }
    </script>

    <?php echo view('_partials/sidebar'); ?>

    <div class="main-content">
        <div class="main-content-inner">
            <div class="breadcrumbs ace-save-state" id="breadcrumbs">
                <ul class="breadcrumb">
                    <li>
                        <i class="ace-icon fa fa-home home-icon"></i>
                        <a href="#">Home</a>
                    </li>
                    <li class="active"><?= $module_title ?></li>
                </ul><!-- /.breadcrumb -->
            </div>

            <div class="page-content">
                <div class="ace-settings-container" id="ace-settings-container">
                    <div class="btn btn-app btn-xs btn-warning ace-settings-btn" id="ace-settings-btn">
                        <i class="ace-icon fa fa-cog bigger-130"></i>
                    </div>

                    <div class="ace-settings-box clearfix" id="ace-settings-box">
                        <div class="pull-left width-50">
                            <div class="ace-settings-item">
                                <div class="pull-left">
                                    <select id="skin-colorpicker" class="hide">
                                        <option data-skin="no-skin" value="#438EB9">#438EB9</option>
                                        <option data-skin="skin-1" value="#222A2D">#222A2D</option>
                                        <option data-skin="skin-2" value="#C6487E">#C6487E</option>
                                        <option data-skin="skin-3" value="#D0D0D0">#D0D0D0</option>
                                    </select>
                                </div>
                                <span>&nbsp; Choose Skin</span>
                            </div>

                            <div class="ace-settings-item">
                                <input type="checkbox" class="ace ace-checkbox-2 ace-save-state"
                                       id="ace-settings-navbar" autocomplete="off"/>
                                <label class="lbl" for="ace-settings-navbar"> Fixed Navbar</label>
                            </div>

                            <div class="ace-settings-item">
                                <input type="checkbox" class="ace ace-checkbox-2 ace-save-state"
                                       id="ace-settings-sidebar" autocomplete="off"/>
                                <label class="lbl" for="ace-settings-sidebar"> Fixed Sidebar</label>
                            </div>

                            <div class="ace-settings-item">
                                <input type="checkbox" class="ace ace-checkbox-2 ace-save-state"
                                       id="ace-settings-breadcrumbs" autocomplete="off"/>
                                <label class="lbl" for="ace-settings-breadcrumbs"> Fixed Breadcrumbs</label>
                            </div>

                            <div class="ace-settings-item">
                                <input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-rtl"
                                       autocomplete="off"/>
                                <label class="lbl" for="ace-settings-rtl"> Right To Left (rtl)</label>
                            </div>

                            <div class="ace-settings-item">
                                <input type="checkbox" class="ace ace-checkbox-2 ace-save-state"
                                       id="ace-settings-add-container" autocomplete="off"/>
                                <label class="lbl" for="ace-settings-add-container">
                                    Inside
                                    <b>.container</b>
                                </label>
                            </div>
                        </div><!-- /.pull-left -->

                        <div class="pull-left width-50">
                            <div class="ace-settings-item">
                                <input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-hover"
                                       autocomplete="off"/>
                                <label class="lbl" for="ace-settings-hover"> Submenu on Hover</label>
                            </div>

                            <div class="ace-settings-item">
                                <input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-compact"
                                       autocomplete="off"/>
                                <label class="lbl" for="ace-settings-compact"> Compact Sidebar</label>
                            </div>

                            <div class="ace-settings-item">
                                <input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-highlight"
                                       autocomplete="off"/>
                                <label class="lbl" for="ace-settings-highlight"> Alt. Active Item</label>
                            </div>
                        </div><!-- /.pull-left -->
                    </div><!-- /.ace-settings-box -->
                </div><!-- /.ace-settings-container -->

                <div class="page-header">
                    <h1>
                        <?= $submodule_title ?>
                    </h1>
                </div><!-- /.page-header -->

                <!-- PAGE CONTENT BEGINS -->
                <form class="form-horizontal container-fluid" role="form" action="<?php echo $controller_path_url . "/process_edit" ?>" method="POST"
                      enctype="multipart/form-data">

                    <input style="display:none" type="text" name="id" id="form-field-1"/>

                    <?= $form ?>

                    <?php if ($hasSupportingFiles): ?>
                        <div class="row">
                            <div class="col-xs-2" align="right">
                                <label class="pt-1" style=""><?= lang("app.supporting_files") ?></label>
                            </div>
                            <div class="col-xs-10">
                                <input name="file_upload" type="file" id="file_choose" class="custom-file-input">
                                <?php if (!empty($row->file_url)): ?>
                                    <div class="row" style="padding-top: 10px; padding-left: 15px;">
                                        <a href="<?= $row->file_url ?>" target="_blank">
                                            <label style="cursor: pointer;">
                                                <?= $row->file_url ?>
                                            </label>
                                        </a>
                                    </div>
                                <?php endif; ?>
                            </div>
                        </div>

                    <?php endif ?>

                    <?php
                    if (session()->get('id') == $row->id_employee && $row->step != 0) {
                        ?>
                        <div class="row">
                            <div class="col-xs-6">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right">
                                        Komentar </label>

                                    <div class="col-sm-9">
                                        <textarea name="komentar" placeholder="Komentar"
                                                  class="form-control"> </textarea>
                                    </div>
                                </div>
                            </div><!-- /.col -->
                        </div>
                        <?php
                    }
                    ?>

                    <?php
                    if (session()->get('id') == $row->id_employee) {
                        ?>
                        <div class="row clearfix form-actions">
                            <div class="col-md-offset-3 col-md-12">
                                <?php
                                if ($row->step != 0) {
                                    ?>
                                    <button class="btn btn-success" name="btn_submit" value="setuju" type="submit "/>
                                    <i class="ace-icon fa fa-check bigger-110"></i>
                                    Setuju

                                    <button class="btn btn-danger" name="btn_submit" value="kembalikan" type="submit"/>
                                    <i class="ace-icon fa fa-check bigger-110"></i>
                                    Kembalikan
                                    <?php
                                } else {
                                    ?>
                                    <button class="btn btn-info" name="btn_submit" value="edit" type="submit"/>
                                    <i class="ace-icon fa fa-check bigger-110"></i>
                                    Submit
                                    <?php
                                } ?>
                            </div>
                        </div>
                        <?php
                    }
                    ?>

                </form>
            </div><!-- /.page-content -->
        </div>
    </div><!-- /.main-content -->

<!--    <div class="footer">-->
<!--        <div class="footer-inner">-->
<!--            <div class="footer-content">-->
<!--                <span class="bigger-120">-->
<!--                    <span class="blue bolder">Ace</span>-->
<!--                    Application &copy; 2013-2014-->
<!--                </span>-->
<!---->
<!--                <span class="action-buttons">-->
<!--                    <a href="#">-->
<!--                        <i class="ace-icon fa fa-twitter-square light-blue bigger-150"></i>-->
<!--                    </a>-->
<!---->
<!--                    <a href="#">-->
<!--                        <i class="ace-icon fa fa-facebook-square text-primary bigger-150"></i>-->
<!--                    </a>-->
<!---->
<!--                    <a href="#">-->
<!--                        <i class="ace-icon fa fa-rss-square orange bigger-150"></i>-->
<!--                    </a>-->
<!--                </span>-->
<!--            </div>-->
<!--        </div>-->
<!--    </div>-->

    <a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
        <i class="ace-icon fa fa-angle-double-up icon-only bigger-110"></i>
    </a>
</div><!-- /.main-container -->

<?php echo view("_partials/edit_script"); ?>
<script>
    <?php
    if (session()->get('id') == $row->id_employee && $row->step != 0) {
        echo <<<EOT
        
    $('.wysiwyg-editor').attr("contenteditable", false)
    $('.wysiwyg-editor').css("background-color", "#f5f5f5")
    $('.wysiwyg-editor').css("color", "#a29f9d")
EOT;
    }
    ?>

    <?php
    if (session()->get('id') == $row->id_employee && $row->step != 0) {
        echo <<<EOT
        
    $('form input').attr("disabled", true)
    $('form select').attr("disabled", true)
EOT;

    }
    ?>

    $("form input[name='id']").attr("disabled", false);
</script>

</body>
</html>
