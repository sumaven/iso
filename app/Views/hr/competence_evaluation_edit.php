<!DOCTYPE html>
<html lang="en">

<?php echo view('_partials/header'); ?>

<body class="no-skin">

<?php echo view('_partials/navbar'); ?>

<div class="main-container ace-save-state" id="main-container">
    <script type="text/javascript">
        try {
            ace.settings.loadState('main-container')
        } catch (e) {
        }
    </script>

    <?php echo view('_partials/sidebar'); ?>

    <div class="main-content">
        <div class="main-content-inner">
            <div class="breadcrumbs ace-save-state" id="breadcrumbs">
                <ul class="breadcrumb">
                    <li>
                        <i class="ace-icon fa fa-home home-icon"></i>
                        <a href="#">Home</a>
                    </li>
                    <li class="active"><?= $module_title ?></li>
                </ul><!-- /.breadcrumb -->
            </div>

            <div class="page-content">
                <div class="ace-settings-container" id="ace-settings-container">
                    <div class="btn btn-app btn-xs btn-warning ace-settings-btn" id="ace-settings-btn">
                        <i class="ace-icon fa fa-cog bigger-130"></i>
                    </div>

                    <div class="ace-settings-box clearfix" id="ace-settings-box">
                        <div class="pull-left width-50">
                            <div class="ace-settings-item">
                                <div class="pull-left">
                                    <select id="skin-colorpicker" class="hide">
                                        <option data-skin="no-skin" value="#438EB9">#438EB9</option>
                                        <option data-skin="skin-1" value="#222A2D">#222A2D</option>
                                        <option data-skin="skin-2" value="#C6487E">#C6487E</option>
                                        <option data-skin="skin-3" value="#D0D0D0">#D0D0D0</option>
                                    </select>
                                </div>
                                <span>&nbsp; Choose Skin</span>
                            </div>

                            <div class="ace-settings-item">
                                <input type="checkbox" class="ace ace-checkbox-2 ace-save-state"
                                       id="ace-settings-navbar" autocomplete="off"/>
                                <label class="lbl" for="ace-settings-navbar"> Fixed Navbar</label>
                            </div>

                            <div class="ace-settings-item">
                                <input type="checkbox" class="ace ace-checkbox-2 ace-save-state"
                                       id="ace-settings-sidebar" autocomplete="off"/>
                                <label class="lbl" for="ace-settings-sidebar"> Fixed Sidebar</label>
                            </div>

                            <div class="ace-settings-item">
                                <input type="checkbox" class="ace ace-checkbox-2 ace-save-state"
                                       id="ace-settings-breadcrumbs" autocomplete="off"/>
                                <label class="lbl" for="ace-settings-breadcrumbs"> Fixed Breadcrumbs</label>
                            </div>

                            <div class="ace-settings-item">
                                <input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-rtl"
                                       autocomplete="off"/>
                                <label class="lbl" for="ace-settings-rtl"> Right To Left (rtl)</label>
                            </div>

                            <div class="ace-settings-item">
                                <input type="checkbox" class="ace ace-checkbox-2 ace-save-state"
                                       id="ace-settings-add-container" autocomplete="off"/>
                                <label class="lbl" for="ace-settings-add-container">
                                    Inside
                                    <b>.container</b>
                                </label>
                            </div>
                        </div><!-- /.pull-left -->

                        <div class="pull-left width-50">
                            <div class="ace-settings-item">
                                <input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-hover"
                                       autocomplete="off"/>
                                <label class="lbl" for="ace-settings-hover"> Submenu on Hover</label>
                            </div>

                            <div class="ace-settings-item">
                                <input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-compact"
                                       autocomplete="off"/>
                                <label class="lbl" for="ace-settings-compact"> Compact Sidebar</label>
                            </div>

                            <div class="ace-settings-item">
                                <input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-highlight"
                                       autocomplete="off"/>
                                <label class="lbl" for="ace-settings-highlight"> Alt. Active Item</label>
                            </div>
                        </div><!-- /.pull-left -->
                    </div><!-- /.ace-settings-box -->
                </div><!-- /.ace-settings-container -->

                <div class="page-header">
                    <h1>
                        <?= $submodule_title ?>
                        <small><i class="ace-icon fa fa-angle-double-right"></i> Add New </small>
                    </h1>
                </div><!-- /.page-header -->

                <!-- PAGE CONTENT BEGINS -->
                <form class="form-horizontal container-fluid" role="form" action="<?php echo $controller_path_url . "/process_add" ?>"
                      method="POST" enctype="multipart/form-data">

                    <div class="row">
                        <div class="col-xs-6">
                            <div class="form-group">
                                <label class="col-sm-3 control-label no-padding-right"
                                       > <?= lang("designation") ?> </label>

                                <div class="col-sm-9">
                                    <select name="designation" class="form-control select2" style="width: 100%;">
                                        <option value="" selected="selected">Please select...</option>
                                        <?php
                                        foreach ($designations as $designation) {
                                            $val = $designation->id;
                                            $select = $designation->designation_name;
                                            echo "<option value='$val'>$select</option>";
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                        </div><!-- /.col -->
                        <div class="col-xs-6">
                        </div><!-- /.col -->
                    </div><!-- /.row -->

                    <div class="row">
                        <div class="col-xs-12">
                            <div class="form-group">
                                <h4 style="font-weight: bold; margin-top: 30px;"> Create Ideal Competence </h4 style="">

                                <div id="toolbar" style="margin-left: 15px">
                                    <div class="form-inline" role="form">
                                        <div class="form-group">
                                            <a onClick="addRow()">
                                                <button type="button" class="btn btn-labeled btn-primary">Add new
                                                </button>
                                            </a>
                                        </div>
                                        <!-- <button id="ok" type="submit" class="btn btn-primary">OK</button> -->
                                    </div>
                                </div>

                                <table
                                        id="table"
                                        data-pagination="true"
                                        data-toggle="table"
                                        data-toolbar="#toolbar"
                                        data-show-refresh="true"
                                        data-show-toggle="true">
                                    <!-- data-url="https://examples.wenzhixin.net.cn/examples/bootstrap_table/data" -->
                                    <thead>
                                    <tr>
                                        <th data-field="no" data-width="5" data-width-unit="%">No</th>
                                        <th data-field="action" data-width="5" data-width-unit="%">Action</th>
                                        <th data-field="competence_type" data-width="25" data-width-unit="%">Competence
                                            Type
                                        </th>
                                        <th data-field="description" data-width="40" data-width-unit="%">Description
                                        </th>
                                        <th data-field="ideal_value">Ideal Value</th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div><!-- /.row -->

                    <div class="row clearfix form-actions">
                        <div class="col-md-offset-3 col-md-9">
                            <button class="btn btn-info" type="submit ">
                                <i class="ace-icon fa fa-check bigger-110"></i>
                                Submit
                        </div>
                    </div>
                </form>
            </div><!-- /.page-content -->
        </div>
    </div><!-- /.main-content -->

    

    <a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
        <i class="ace-icon fa fa-angle-double-up icon-only bigger-110"></i>
    </a>
</div><!-- /.main-container -->

<!-- basic scripts -->

<!--[if !IE]> -->
<script src="<?php echo base_url("assets/assets"); ?>/js/jquery-2.1.4.min.js"></script>

<!-- <![endif]-->

<!--[if IE]>
<script src="assets/js/jquery-1.11.3.min.js"></script>
<![endif]-->
<script type="text/javascript">
    if ('ontouchstart' in document.documentElement) document.write("<script src='<?php echo base_url("assets/assets"); ?>/js/jquery.mobile.custom.min.js'>" + "<" + "/script>");
</script>
<script src="<?php echo base_url("assets/assets"); ?>/js/bootstrap.min.js"></script>

<!-- page specific plugin scripts -->

<!--[if lte IE 8]>
<script src="assets/js/excanvas.min.js"></script>
<![endif]-->
<script src="<?php echo base_url("assets/assets"); ?>/js/jquery-ui.custom.min.js"></script>
<script src="<?php echo base_url("assets/assets"); ?>/js/jquery.ui.touch-punch.min.js"></script>
<script src="<?php echo base_url("assets/assets"); ?>/js/markdown.min.js"></script>
<script src="<?php echo base_url("assets/assets"); ?>/js/bootstrap-markdown.min.js"></script>
<script src="<?php echo base_url("assets/assets"); ?>/js/jquery.hotkeys.index.min.js"></script>
<script src="<?php echo base_url("assets/assets"); ?>/js/bootstrap-wysiwyg.min.js"></script>
<script src="<?php echo base_url("assets/assets"); ?>/js/bootbox.js"></script>
<script src="<?php echo base_url("assets/assets"); ?>/js/bootstrap-wysiwyg.min.js"></script>
 <!-- ace scripts -->
<script src="<?php echo base_url("assets/assets"); ?>/js/ace-elements.min.js"></script>
<script src="<?php echo base_url("assets/assets"); ?>/js/ace.min.js"></script>

<script src="<?php echo base_url("assets/assets"); ?>/js/jquery.maskedinput.min.js"></script>
<script src="<?php echo base_url("assets/assets"); ?>/js/bootstrap-datepicker.min.js"></script>
<script>
    $("input[placeholder='yyyy-mm-dd']").datepicker({
        autoclose: true,
        format: 'yyyy-mm-dd',
        orientation: "bottom",
        todayHighlight: true,
    });
    $("input[placeholder='yyyy-mm-dd']").mask("9999-99-99");
    $("input[placeholder='yyyy-mm-dd']").css("width", "100%");
</script>

<!-- Bootstrap DataTables -->
<script src="https://unpkg.com/bootstrap-table@1.15.3/dist/bootstrap-table.min.js"></script>
<script src="https://unpkg.com/tableexport.jquery.plugin/tableExport.min.js"></script>
<script src="https://unpkg.com/tableexport.jquery.plugin/libs/jsPDF/jspdf.min.js"></script>
<script src="https://unpkg.com/tableexport.jquery.plugin/libs/jsPDF-AutoTable/jspdf.plugin.autotable.js"></script>
<!-- <script src="extensions/print/bootstrap-table-print.js"></script> -->
<script src="<?php echo base_url(); ?>assets/bootstrap-table/extensions/export/bootstrap-table-export.js"></script>
<script>
    var convertToHtmlTag = function(convertToHtmlTag){
        var passValue = $("<span />", { html: convertToHtmlTag }).text();
        passValue = passValue.replace('&nbsp;', " ")
        return passValue
        //return document.createElement("span").innerText;
    };
</script>
<script>
    var descriptionValues, competenceTypeValues, idealValues;
    descriptionValues = [];
    competenceTypeValues = [];
    idealValues = [];

    var remarksValues, currentDescriptionValues, currentValues;
    remarksValues = [];
    currentDescriptionValues = [];
    currentValues = [];

    var id = 0;
    var numbers = [];
    var evaluationNumbers = [];

    function refreshNumberOrder() {
        var no = 1;
        $("[id='field_no']").map(function () {
            $(this).text(no);
            no++;
        }).get();

        var no = 1;
        $("[id='delete_button']").map(function () {
            $(this).attr("onClick", "deleteRow(" + (no - 1) + ", " + numbers[no - 1] + ")");
            no++;
        }).get();
    }

    function restore() {
        var index = 0;
        $("select[name='competence_type[]']").map(function () {
            if (index < competenceTypeValues.length) {
                $(this).val(competenceTypeValues[index]);
                $(this).prop('disabled', true);
            }
            index++;
        }).get();

        var index = 0;
        $("textarea[name='description[]']").map(function () {
            if (index < descriptionValues.length) {
                $(this).text(descriptionValues[index]);
                $(this).prop('disabled', true);
            }
            index++;
        }).get();

        var index = 0;
        $("input[name='ideal_value[]']").map(function () {
            if (index < idealValues.length) {
                $(this).val(idealValues[index]);
                $(this).prop('disabled', true);
            }
            index++;
        }).get();
    }

    function restoreEvaluation() {
        var index = 0;
        $("textarea[name='remarks[]']").map(function () {
            if (index < remarksValues.length) {
                $(this).text(remarksValues[index]);
            }
            index++;
        }).get();

        var index = 0;
        $("textarea[name='current_description[]']").map(function () {
            if (index < currentDescriptionValues.length) {
                $(this).text(currentDescriptionValues[index]);
            }
            index++;
        }).get();

        var index = 0;
        $("input[name='current_value[]']").map(function () {
            if (index < currentValues.length) {
                $(this).val(currentValues[index]);
            }
            index++;
        }).get();
    }

    function addRow() {
        // backup();

        numbers.push(id + 1);
        var numberIndex = numbers.indexOf(id + 1);

        $("table").bootstrapTable('insertRow', {
            index: id,
            row: {
                no: '<p id="field_no">' + (id + 1) + '</p>',
                competence_type: '\
            <div id="competence_type_group_' + (id + 1) + '" class="form-group" style="margin: 15px;">\
                <select name="competence_type[]" class="form-control select2" style="width: 100%;">\
                    <option value="" selected="selected">Please select...</option>\
                    <option value="0">Value 1</option>\
                    <option value="1">Value 2</option>\
                </select>\
            </div>\
            ',
                description: '\
            <div id="description_group_' + (id + 1) + '" class="form-group" style="margin: 15px;">\
                <textarea name="description[]" form="form_objective" class="form-control" rows="3" placeholder=""></textarea>\
            </div>\
            ',
                ideal_value: '\
            <div id="ideal_value_group_' + (id + 1) + '" class="form-group" style="margin: 15px;">\
                <input name="ideal_value[]" type="number" class="form-control" min="1" max="1000" step="1"/>\
            </div>\
            '
            }
        });

        // restore();
        id++

        // refreshNumberOrder();
    }

    function addEvaluationRow() {
        // backup();

        evaluationNumbers.push(id + 1);
        var numberIndex = evaluationNumbers.indexOf(id + 1);

        $("table").bootstrapTable('insertRow', {
            index: id,
            row: {
                no: '<p></p>',
                competence_type: '\
            <div id="remarks_group_' + (id + 1) + '" class="form-group" style="margin: 15px;">\
                <label><?= lang("remarks") ?></label>\
                <textarea name="remarks[]" form="form_objective" class="form-control" rows="3" placeholder=""></textarea>\
            </div>\
            ',
                description: '\
            <div id="current_description_group_' + (id + 1) + '" class="form-group" style="margin: 15px;">\
                <label>Current Description</label>\
                <textarea name="current_description[]" form="form_objective" class="form-control" rows="3" placeholder=""></textarea>\
            </div>\
            ',
                ideal_value: '\
            <div id="current_value_group_' + (id + 1) + '" class="form-group" style="margin: 15px;">\
                <label>Current Value</label>\
                <input name="current_value[]" type="number" class="form-control" min="1" max="1000" step="1"/>\
            </div>\
            '
            }
        });

        // restore();
        id++

        // refreshNumberOrder();
    }


    $('form').submit(function (e) {
        e.preventDefault();

        $('.form-group').removeClass('has-error'); // remove the error class
        $('.help-block').remove(); // remove the error text
        $('.alert-success').remove();


        var datas = [];
        // backup();

        remarksValues = $("textarea[name='remarks[]']").map(function () {
            return $(this).val();
        }).get();

        currentDescriptionValues = $("textarea[name='current_description[]']").map(function () {
            return $(this).val();
        }).get();

        currentValues = $("input[name='current_value[]']").map(function () {
            return $(this).val();
        }).get();

        // designation_id, competenceTypeValues, descriptionValues, idealValues
        for (var i = 0; i < remarksValues.length; i++) {
            datas.push({
                no: i + 1,
                number: evaluationNumbers[i],
                designation_id: $("select[name='designation'] option:selected").val(),
                remarks: remarksValues[i],
                current_description: currentDescriptionValues[i],
                current_value: currentValues[i]
            })
        }

        var formData = {
            'id': <?= $id ?>,
            'datas': datas,
        };

        // alert("Submitted");
        // alert(JSON.stringify(formData));

        // process the form
        $.ajax({
            type: 'POST',
            url: '<?=base_url("hr/competence_evaluation/process_edit")?>',
            data: formData, // data object
            dataType: 'json', // what type of data do we expect back from the server
            encode: true,
            error: function (data) {
                alert("AJAX ERROR");
                alert(JSON.stringify(data));
            }
        })

            // using the done promise callback
            .done(function (data) {

                // alert(JSON.stringify(data));

                // log data to the console so we can see
                console.log(data);

                // here we will handle errors and validation messages
                if (!data.success) {

                    alert('Please complete the form');

                    for (var key in data.errors) {
                        eval(data.errors[key]);
                    }


                } else {
                    alert(data.message);
                }
            });
    });
</script>
<script>
    $(document).ready(function () {
        <?php
        foreach ($rows as $row) {
            echo "descriptionValues.push('" . filter_output($row->description) . "');\n";
            echo "competenceTypeValues.push(" . filter_output($row->competence_type) . ");\n";
            echo "idealValues.push(" . filter_output($row->ideal_value) . ");\n";
            echo "addRow();\n";
            echo "addEvaluationRow();\n";
        }
        ?>

        <?php
        foreach ($evaluationRows as $row2) {
            echo "remarksValues.push('" . filter_output($row2->remarks) . "');\n";
            echo "currentDescriptionValues.push('" . filter_output($row2->current_description) . "');\n";
            echo "currentValues.push(" . filter_output($row2->current_value) . ");\n";
        }
        ?>

        refreshNumberOrder();

        restore();
        restoreEvaluation();

        $("select[name='designation']").val(convertToHtmlTag('<?= $rows[0]->designation_id ?>'));
    });
</script>
</body>
</html>
