<?php namespace Config;

// Create a new instance of our RouteCollection class.
$routes = Services::routes();

// Load the system's routing file first, so that the app and ENVIRONMENT
// can override as needed.
if (file_exists(SYSTEMPATH . 'Config/Routes.php'))
{
	require SYSTEMPATH . 'Config/Routes.php';
}

/**
 * --------------------------------------------------------------------
 * Router Setup
 * --------------------------------------------------------------------
 */
$routes->setDefaultNamespace('App\Controllers');
$routes->setDefaultController('Home');
$routes->setDefaultMethod('index');
$routes->setTranslateURIDashes(false);
$routes->set404Override();
$routes->setAutoRoute(true);

/**
 * --------------------------------------------------------------------
 * Route Definitions
 * --------------------------------------------------------------------
 */

// We get a performance increase by specifying the default
// route since we don't have to scan directories.
$routes->get('/', 'Home::index');
$routes->get('/login', 'Home::login');
$routes->post('/auth', 'Home::process_auth');
$routes->get('/logout', 'Home::logout');

// Context of Organization
$routes->get('context_organization/list', 'Context_organization\Context_organization::list');
$routes->get('context_organization/create', 'Context_organization\Context_organization::create');
$routes->get('context_organization/edit/(:any)', 'Context_organization\Context_organization::edit/$1');
$routes->get('context_organization/process_delete/(:any)', 'Context_organization\Context_organization::process_delete/$1');
$routes->post('context_organization/process_add', 'Context_organization\Context_organization::process_add');
$routes->post('context_organization/process_edit', 'Context_organization\Context_organization::process_edit');

// Risk Opportunity
$routes->get("risk_opportunity/list", "Risk_opportunity\Risk_opportunity::list");
$routes->get("risk_opportunity/create", "Risk_opportunity\Risk_opportunity::create");
$routes->get("risk_opportunity/edit/(:any)", "Risk_opportunity\Risk_opportunity::edit/$1");
$routes->get("risk_opportunity/process_delete/(:any)", "Risk_opportunity\Risk_opportunity::process_delete/$1");
$routes->post("risk_opportunity/process_add", "Risk_opportunity\Risk_opportunity::process_add");
$routes->post("risk_opportunity/process_edit", "Risk_opportunity\Risk_opportunity::process_edit");
/**
 * --------------------------------------------------------------------
 * Additional Routing
 * --------------------------------------------------------------------
 *
 * There will often be times that you need additional routing and you
 * need to it be able to override any defaults in this file. Environment
 * based routes is one such time. require() additional route files here
 * to make that happen.
 *
 * You will have access to the $routes object within that file without
 * needing to reload it.
 */
if (file_exists(APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php'))
{
	require APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php';
}
