<?php namespace App\Database\Migrations;

class AddHiaro extends \CodeIgniter\Database\Migration {

    public function up()
    {
        $this->forge->addField([
            'id'          => [
                'type'           => 'INT',
                'auto_increment' => TRUE
            ],
            'blog_title'       => [
                'type'           => 'VARCHAR',
                'constraint'     => '100',
            ],
            'blog_description' => [
                'type'           => 'TEXT',
                'null'           => TRUE,
            ],
        ]);
        $this->forge->addKey('id', TRUE);
        $this->forge->createTable('planning_hiaro_environment_aspect');
    }

    public function down()
    {
        $this->forge->dropTable('planning_hiaro_environment_aspect');
    }
}