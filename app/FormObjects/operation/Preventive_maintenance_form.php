<?php namespace App\FormObjects\operation;

use App\FormEntities\FormCol;
use App\FormEntities\FormColType;
use App\FormEntities\FormRow;
use App\FormEntities\FormRowType;
use App\FormEntities\SelectChoice;

trait Preventive_maintenance_form
{
    private $equipmentChoices;
    private $frequencyChoices;

    public function getFormObject()
    {
        $this->initCreateEditDependencies();
        $formArrayObject = array();

        array_push($formArrayObject, new FormRow(FormRowType::TWO_SPLIT, null,
                (new FormCol())
                    ->setFormColType(FormColType::FORM_SELECT)
                    ->setFormName("equipment")
                    ->setFormLabel(lang("operation.equipment"))
                    ->setFormData($this->equipmentChoices),
                (new FormCol())
                    ->setFormColType(FormColType::FORM_SELECT)
                    ->setFormName("frequency")
                    ->setFormLabel(lang("operation.frequency"))
                    ->setFormData($this->frequencyChoices)
            )
        );

        array_push($formArrayObject, new FormRow(FormRowType::TWO_SPLIT, null,
                (new FormCol())
                    ->setFormColType(FormColType::FORM_INPUT)
                    ->setFormName("item")
                    ->setFormLabel(lang("operation.item")),
                (new FormCol())
                    ->setFormColType(FormColType::FORM_INPUT)
                    ->setFormName("activity")
                    ->setFormLabel(lang("operation.activity"))
            )
        );

        array_push($formArrayObject, new FormRow(FormRowType::TWO_SPLIT, null,
                (new FormCol())
                    ->setFormColType(FormColType::FORM_INPUT)
                    ->setFormName("responsibility")
                    ->setFormLabel(lang("operation.responsibility")),
                (new FormCol())
                    ->setFormColType(FormColType::FORM_TEXTAREA)
                    ->setFormName("remark")
                    ->setFormLabel(lang("operation.remark"))
            )
        );

//        $formRow = new FormRow(FormRowType::ADDABLE, "",
//            FormCol::createInstance(
//                FormColType::FORM_INPUT,
//                lang("operation.customer_name"),
//                "customer_name"),
//            FormCol::createInstance(
//                FormColType::FORM_INPUT,
//                lang("operation.external_property_name"),
//                "external_property_name"),
//            FormCol::createInstance(
//                FormColType::FORM_INPUT,
//                lang("operation.external_property_type"),
//                "external_property_type"),
//            FormCol::createInstance(
//                FormColType::FORM_INPUT,
//                lang("operation.external_property_status"),
//                "external_property_status")
//        );
//        $formRow->form_data = new FormData("operation_preventive_maintenance_items");
//        array_push($formArrayObject, $formRow);

        return $formArrayObject;
    }

    public function initCreateEditDependencies()
    {
        helper("select_option/frequency_list");

        $this->frequencyChoices = array();
        $choices = get_frequency_list();
        foreach ($choices as $value => $option) {
            array_push($this->frequencyChoices, new SelectChoice($value, $option));
        }

        $this->equipmentChoices = array();
        $rows = $this->EquipmentModel->findAll();
        foreach ($rows as $item) {
            $value = $item->id;
            $option = $item->equipment_name;
            array_push($this->equipmentChoices, new SelectChoice($value, $option));
        }
    }

}
