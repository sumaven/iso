<?php namespace App\FormObjects\operation;

use App\FormEntities\FormCol;
use App\FormEntities\FormColType;
use App\FormEntities\FormRow;
use App\FormEntities\FormRowType;
use App\FormEntities\SelectChoice;

trait Calibration_log_form
{
    private $approvalChoices;

    public function getFormObject()
    {
        $this->initCreateEditDependencies();
        $formArrayObject = array();

        array_push($formArrayObject, new FormRow(FormRowType::TWO_SPLIT, null,
                (new FormCol())
                    ->setFormColType(FormColType::FORM_INPUT)
                    ->setFormName("equipment_name")
                    ->setFormLabel(lang("operation.equipment_name")),
                (new FormCol())
                    ->setFormColType(FormColType::FORM_INPUT)
                    ->setFormName("model_name")
                    ->setFormLabel(lang("operation.model_name"))
            )
        );

        array_push($formArrayObject, new FormRow(FormRowType::TWO_SPLIT, null,
                (new FormCol())
                    ->setFormColType(FormColType::FORM_INPUT)
                    ->setFormName("equipment_owner")
                    ->setFormLabel(lang("operation.equipment_owner")),
                (new FormCol())
                    ->setFormColType(FormColType::FORM_INPUT_DATE)
                    ->setFormName("date_of_last_calibration")
                    ->setFormLabel(lang("operation.date_of_last_calibration"))

            )
        );

        array_push($formArrayObject, new FormRow(FormRowType::TWO_SPLIT, null,
                (new FormCol())
                    ->setFormColType(FormColType::FORM_INPUT)
                    ->setFormName("equipment_location")
                    ->setFormLabel(lang("operation.equipment_location")),
                (new FormCol())
                    ->setFormColType(FormColType::FORM_INPUT_DATE)
                    ->setFormName("next_calibration_date")
                    ->setFormLabel(lang("operation.next_calibration_date"))
            )
        );

        array_push($formArrayObject, new FormRow(FormRowType::TWO_SPLIT, null,
                (new FormCol())
                    ->setFormColType(FormColType::FORM_INPUT)
                    ->setFormName("identification_number")
                    ->setFormLabel(lang("operation.identification_number")),
                (new FormCol())
                    ->setFormColType(FormColType::FORM_INPUT)
                    ->setFormName("asset_number")
                    ->setFormLabel(lang("operation.asset_number"))
            )
        );

        return $formArrayObject;
    }

    public function initCreateEditDependencies()
    {
        helper("select_option/approval_list");

        $this->approvalChoices = array();
        $choices = get_approval_list();
        foreach ($choices as $value => $option) {
            array_push($this->approvalChoices, new SelectChoice($value, $option));
        }
    }

}
