<?php namespace App\FormObjects\operation;

use App\FormEntities\FormCol;
use App\FormEntities\FormColType;
use App\FormEntities\FormRow;
use App\FormEntities\FormRowType;
use App\FormEntities\SelectChoice;

trait Management_of_change_form
{
    private $approvalChoices;

    public function getFormObject()
    {
        $this->initCreateEditDependencies();
        $formArrayObject = array();

        array_push($formArrayObject, new FormRow(FormRowType::TWO_SPLIT, null,
                array(
                    (new FormCol())
                        ->setFormColType(FormColType::FORM_INPUT)
                        ->setFormName("propose_change")
                        ->setFormLabel(lang("operation.propose_change")),
                    (new FormCol())
                        ->setFormColType(FormColType::FORM_INPUT)
                        ->setFormName("reason_for_change")
                        ->setFormLabel(lang("operation.reason_for_change")),
                    (new FormCol())
                        ->setFormColType(FormColType::FORM_INPUT)
                        ->setFormName("type_of_proposed_change")
                        ->setFormLabel(lang("operation.type_of_proposed_change")),
                ),
                array(
                    (new FormCol())
                        ->setFormColType(FormColType::FORM_INPUT)
                        ->setFormName("possible_impact_of_change")
                        ->setFormLabel(lang("operation.possible_impact_of_change")),
                    (new FormCol())
                        ->setFormColType(FormColType::FORM_TEXTAREA)
                        ->setFormName("remark")
                        ->setFormLabel(lang("operation.remark")),
                )
            )
        );

        return $formArrayObject;
    }

    public function initCreateEditDependencies()
    {
        helper("select_option/approval_list");

        $this->approvalChoices = array();
        $choices = get_approval_list();
        foreach ($choices as $value => $option) {
            array_push($this->approvalChoices, new SelectChoice($value, $option));
        }
    }

}
