<?php namespace App\FormObjects\operation;

use App\FormEntities\FormCol;
use App\FormEntities\FormColType;
use App\FormEntities\FormRow;
use App\FormEntities\FormRowType;
use App\FormEntities\SelectChoice;

trait Enquiry_form
{
    private $statusChoices;

    public function getFormObject()
    {
        $this->initCreateEditDependencies();
        $formArrayObject = array();

        array_push($formArrayObject, new FormRow(FormRowType::TWO_SPLIT, null,
                (new FormCol())
                    ->setFormColType(FormColType::FORM_INPUT_DATE)
                    ->setFormName("enquiry_date")
                    ->setFormLabel(lang("operation.enquiry_date")),
                (new FormCol())
                    ->setFormColType(FormColType::FORM_INPUT)
                    ->setFormName("phone")
                    ->setFormLabel(lang("operation.phone"))
            )
        );

        array_push($formArrayObject, new FormRow(FormRowType::TWO_SPLIT, null,
                (new FormCol())
                    ->setFormColType(FormColType::FORM_INPUT)
                    ->setFormName("prospect_company")
                    ->setFormLabel(lang("operation.prospect_company")),
                (new FormCol())
                    ->setFormColType(FormColType::FORM_INPUT)
                    ->setFormName("website")
                    ->setFormLabel(lang("operation.website"))
            )
        );

        array_push($formArrayObject, new FormRow(FormRowType::TWO_SPLIT, null,
                (new FormCol())
                    ->setFormColType(FormColType::FORM_INPUT)
                    ->setFormName("address")
                    ->setFormLabel(lang("operation.address")),
                (new FormCol())
                    ->setFormColType(FormColType::FORM_INPUT)
                    ->setFormName("prospect_name")
                    ->setFormLabel(lang("operation.prospect_name"))
            )
        );

        array_push($formArrayObject, new FormRow(FormRowType::TWO_SPLIT, null,
                (new FormCol())
                    ->setFormColType(FormColType::FORM_INPUT)
                    ->setFormName("city")
                    ->setFormLabel(lang("operation.city")),
                (new FormCol())
                    ->setFormColType(FormColType::FORM_INPUT)
                    ->setFormName("email")
                    ->setFormLabel(lang("operation.email"))
            )
        );

        array_push($formArrayObject, new FormRow(FormRowType::TWO_SPLIT, null,
                (new FormCol()),
                (new FormCol())
                    ->setFormColType(FormColType::FORM_INPUT)
                    ->setFormName("remark")
                    ->setFormLabel(lang("operation.remark"))
            )
        );

        return $formArrayObject;
    }

    public function initCreateEditDependencies()
    {
        helper("select_option/status_list");

        $this->statusChoices = array();
        $choices = get_status_list();
        foreach ($choices as $value => $option) {
            array_push($this->statusChoices, new SelectChoice($value, $option));
        }
    }

}
