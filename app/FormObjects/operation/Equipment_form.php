<?php namespace App\FormObjects\operation;

use App\FormEntities\FormCol;
use App\FormEntities\FormColType;
use App\FormEntities\FormRow;
use App\FormEntities\FormRowType;
use App\FormEntities\SelectChoice;

trait Equipment_form
{
    private $statusChoices;
    private $calibrationChecklist;

    public function getFormObject()
    {
        $this->initCreateEditDependencies();
        $formArrayObject = array();

        array_push($formArrayObject, new FormRow(FormRowType::TWO_SPLIT, null,
                (new FormCol())
                    ->setFormColType(FormColType::FORM_INPUT)
                    ->setFormName("identification_number")
                    ->setFormLabel(lang("operation.identification_number")),
                (new FormCol())
                    ->setFormColType(FormColType::FORM_SELECT)
                    ->setFormName("status")
                    ->setFormLabel(lang("operation.status"))
                    ->setFormData($this->statusChoices)
            )
        );

        array_push($formArrayObject, new FormRow(FormRowType::TWO_SPLIT, null,
                (new FormCol())
                    ->setFormColType(FormColType::FORM_INPUT)
                    ->setFormName("equipment_name")
                    ->setFormLabel(lang("operation.equipment_name")),
                (new FormCol())
                    ->setFormColType(FormColType::FORM_INPUT)
                    ->setFormName("equipment_owner")
                    ->setFormLabel(lang("operation.equipment_owner"))
            )
        );

        array_push($formArrayObject, new FormRow(FormRowType::TWO_SPLIT, null,
                (new FormCol())
                    ->setFormColType(FormColType::FORM_INPUT)
                    ->setFormName("equipment_location")
                    ->setFormLabel(lang("operation.equipment_location")),
                (new FormCol())
                    ->setFormColType(FormColType::FORM_INPUT)
                    ->setFormName("model_name")
                    ->setFormLabel(lang("operation.model_name"))
            )
        );

        array_push($formArrayObject, new FormRow(FormRowType::TWO_SPLIT, null,
                (new FormCol())
                    ->setFormColType(FormColType::FORM_INPUT)
                    ->setFormName("asset_number")
                    ->setFormLabel(lang("operation.asset_number")),
                (new FormCol())
                    ->setFormColType(FormColType::FORM_INPUT_DATE)
                    ->setFormName("date_of_purchase")
                    ->setFormLabel(lang("operation.date_of_purchase"))
            )
        );

        array_push($formArrayObject, new FormRow(FormRowType::TWO_SPLIT, null,
                (new FormCol()),
                (new FormCol())
                    ->setFormColType(FormColType::FORM_CHECKBOX)
                    ->setFormName("calibration_check")
                    ->setFormLabel("If the equipment requires calibration, please checklist the circle")
                    ->setFormData($this->calibrationChecklist)
            )
        );


        return $formArrayObject;
    }

    public function initCreateEditDependencies()
    {
        helper("select_option/status_list");

        $this->statusChoices = array();
        $choices = get_status_list();
        foreach ($choices as $value => $option) {
            array_push($this->statusChoices, new SelectChoice($value, $option));
        }

        $this->calibrationChecklist = array();
        $choices = array(
            "checked" => "",
        );
        foreach ($choices as $value => $option) {
            array_push($this->calibrationChecklist, new SelectChoice($value, $option));
        }
    }

}
