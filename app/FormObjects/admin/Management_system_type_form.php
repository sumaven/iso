<?php namespace App\FormObjects\admin;

use App\FormEntities\FormCol;
use App\FormEntities\FormColType;
use App\FormEntities\FormRow;
use App\FormEntities\FormRowType;

trait Management_system_type_form
{

    public function getFormObject()
    {
        $this->initCreateEditDependencies();
        $formArrayObject = array();

        array_push($formArrayObject, new FormRow(FormRowType::TWO_SPLIT, null,
            (new FormCol())
                ->setFormColType(FormColType::FORM_INPUT)
                ->setFormLabel(lang("admin.management_system_short_name"))
                ->setFormName("management_system_short_name"),
            (new FormCol())
                ->setFormColType(FormColType::FORM_INPUT)
                ->setFormLabel(lang("admin.management_system_full_name"))
                ->setFormName("management_system_full_name")
        ));

        return $formArrayObject;
    }

    public function initCreateEditDependencies()
    {

    }

}
