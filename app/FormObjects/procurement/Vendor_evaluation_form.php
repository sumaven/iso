<?php namespace App\FormObjects\procurement;

use App\FormEntities\FormCol;
use App\FormEntities\FormColType;
use App\FormEntities\FormRow;
use App\FormEntities\FormRowType;

trait Vendor_evaluation_form
{

    private $departmentChoices;
    private $documentTypeChoices;

    public function getFormObject()
    {
        $this->initCreateEditDependencies();
        $formArrayObject = array();

        array_push($formArrayObject, new FormRow(FormRowType::TWO_SPLIT, null,
            (new FormCol())
                ->setFormColType(FormColType::FORM_INPUT)
                ->setFormLabel(lang("procurement.doc"))
                ->setFormName("doc"),
            (new FormCol())
                ->setFormColType(FormColType::FORM_INPUT)
                ->setFormLabel(lang("procurement.rev"))
                ->setFormName("rev")
        ));

        array_push($formArrayObject, new FormRow(FormRowType::TWO_SPLIT, null,
            (new FormCol())
                ->setFormColType(FormColType::FORM_INPUT)
                ->setFormLabel(lang("procurement.form_no"))
                ->setFormName("form_no")
        ));

        array_push($formArrayObject, new FormRow(FormRowType::TWO_SPLIT, null,
            (new FormCol())
                ->setFormColType(FormColType::FORM_INPUT)
                ->setFormLabel(lang("procurement.company"))
                ->setFormName("company"),
            (new FormCol())
                ->setFormColType(FormColType::FORM_INPUT)
                ->setFormLabel(lang("procurement.address"))
                ->setFormName("address")
        ));

        array_push($formArrayObject, new FormRow(FormRowType::TWO_SPLIT, null,
            (new FormCol())
                ->setFormColType(FormColType::FORM_INPUT)
                ->setFormLabel(lang("procurement.phone"))
                ->setFormName("phone"),
            (new FormCol())
                ->setFormColType(FormColType::FORM_INPUT)
                ->setFormLabel(lang("procurement.fax"))
                ->setFormName("fax")
        ));

        array_push($formArrayObject, new FormRow(FormRowType::TWO_SPLIT, null,
            (new FormCol())
                ->setFormColType(FormColType::FORM_INPUT)
                ->setFormLabel(lang("procurement.product"))
                ->setFormName("product"),
            (new FormCol())
                ->setFormColType(FormColType::FORM_INPUT)
                ->setFormLabel(lang("procurement.npwp"))
                ->setFormName("npwp")
        ));

        array_push($formArrayObject, new FormRow(FormRowType::TWO_SPLIT, null,
            (new FormCol())
                ->setFormColType(FormColType::FORM_INPUT)
                ->setFormLabel(lang("procurement.tdp"))
                ->setFormName("tdp"),
            (new FormCol())
                ->setFormColType(FormColType::FORM_INPUT)
                ->setFormLabel(lang("procurement.pkp"))
                ->setFormName("pkp")
        ));

        array_push($formArrayObject, new FormRow(FormRowType::TWO_SPLIT, null,
            (new FormCol())
                ->setFormColType(FormColType::FORM_INPUT)
                ->setFormLabel(lang("procurement.tdr"))
                ->setFormName("tdr"),
            (new FormCol())
                ->setFormColType(FormColType::FORM_INPUT)
                ->setFormLabel(lang("procurement.siup"))
                ->setFormName("siup")
        ));

        // Kurang di sini

        array_push($formArrayObject, new FormRow(FormRowType::ONE_SOLO, null,
            (new FormCol())
                ->setFormColType(FormColType::FORM_TEXTAREA)
                ->setFormLabel(lang("procurement.recommendation"))
                ->setFormName("recommendation")
        ));

        // Kurang di sini

        return $formArrayObject;
    }

    public function initCreateEditDependencies()
    {
//        helper("select_option/document_type_list");
//
//        $this->documentTypeChoices = array();
//        $choices = get_document_type_list();
//        foreach ($choices as $value => $option) {
//            array_push($this->documentTypeChoices, new SelectChoice($value, $option));
//        }
//
//        // Department Choice
//        $this->departmentChoices = array();
//        $rows = $this->DepartmentModel->findAll();
//        foreach ($rows as $item) {
//            $value = $item->id;
//            $option = $item->department_name;
//            array_push($this->departmentChoices, new SelectChoice($value, $option));
//        }
    }

}
