<?php namespace App\FormObjects\procurement;

use App\FormEntities\FormCol;
use App\FormEntities\FormColType;
use App\FormEntities\FormRow;
use App\FormEntities\FormRowType;
use App\FormEntities\SelectChoice;

trait Purchase_order_form
{

    private $departmentChoices;
    private $documentTypeChoices;

    public function getFormObject()
    {
        $this->initCreateEditDependencies();
        $formArrayObject = array();

        array_push($formArrayObject, new FormRow(FormRowType::TWO_SPLIT, null,
            (new FormCol())
                ->setFormColType(FormColType::FORM_INPUT)
                ->setFormLabel(lang("procurement.po_number"))
                ->setFormName("po_number"),
            (new FormCol())
                ->setFormColType(FormColType::FORM_INPUT)
                ->setFormLabel(lang("procurement.npwp"))
                ->setFormName("npwp")
        ));

        array_push($formArrayObject, new FormRow(FormRowType::TWO_SPLIT, null,
            (new FormCol())
                ->setFormColType(FormColType::FORM_INPUT)
                ->setFormLabel(lang("procurement.alamat"))
                ->setFormName("alamat"),
            (new FormCol())
                ->setFormColType(FormColType::FORM_INPUT_DATE)
                ->setFormLabel(lang("procurement.date"))
                ->setFormName("date")
        ));

        array_push($formArrayObject, new FormRow(FormRowType::ONE_SOLO, null,
            (new FormCol())
                ->setFormColType(FormColType::FORM_INPUT)
                ->setFormLabel(lang("procurement.quantity"))
                ->setFormName("quantity")
        ));

        array_push($formArrayObject, new FormRow(FormRowType::ONE_SOLO, null,
            (new FormCol())
                ->setFormColType(FormColType::FORM_INPUT)
                ->setFormLabel(lang("procurement.price"))
                ->setFormName("price")
        ));

        array_push($formArrayObject, new FormRow(FormRowType::ONE_SOLO, null,
            (new FormCol())
                ->setFormColType(FormColType::FORM_INPUT)
                ->setFormLabel(lang("procurement.unit"))
                ->setFormName("unit")
        ));

        array_push($formArrayObject, new FormRow(FormRowType::ONE_SOLO, null,
            (new FormCol())
                ->setFormColType(FormColType::FORM_INPUT_DATE)
                ->setFormLabel(lang("procurement.delivery_date"))
                ->setFormName("delivery_date")
        ));

        array_push($formArrayObject, new FormRow(FormRowType::ONE_SOLO, null,
            (new FormCol())
                ->setFormColType(FormColType::FORM_INPUT)
                ->setFormLabel(lang("procurement.delivery_location"))
                ->setFormName("delivery_location")
        ));

        array_push($formArrayObject, new FormRow(FormRowType::ONE_SOLO, null,
            (new FormCol())
                ->setFormColType(FormColType::FORM_TEXTAREA)
                ->setFormLabel(lang("procurement.description"))
                ->setFormName("description")
        ));

        array_push($formArrayObject, new FormRow(FormRowType::ONE_SOLO, null,
            (new FormCol())
                ->setFormColType(FormColType::FORM_TEXTAREA)
                ->setFormLabel(lang("procurement.term_of_payment"))
                ->setFormName("term_of_payment")
        ));

        return $formArrayObject;
    }

    public function initCreateEditDependencies()
    {
//        helper("select_option/document_type_list");
//
//        $this->documentTypeChoices = array();
//        $choices = get_document_type_list();
//        foreach ($choices as $value => $option) {
//            array_push($this->documentTypeChoices, new SelectChoice($value, $option));
//        }
//
//        // Department Choice
//        $this->departmentChoices = array();
//        $rows = $this->DepartmentModel->findAll();
//        foreach ($rows as $item) {
//            $value = $item->id;
//            $option = $item->department_name;
//            array_push($this->departmentChoices, new SelectChoice($value, $option));
//        }
    }

}
