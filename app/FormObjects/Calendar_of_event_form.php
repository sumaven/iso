<?php namespace App\FormObjects;

use App\FormEntities\FormCol;
use App\FormEntities\FormColType;
use App\FormEntities\FormRow;
use App\FormEntities\FormRowType;

trait Calendar_of_event_form
{

    public function getFormObject()
    {
        $this->initCreateEditDependencies();
        $formArrayObject = array();

        array_push($formArrayObject, new FormRow(FormRowType::TWO_SPLIT, null,
            array(
                FormCol::createInstance(
                    FormColType::FORM_INPUT,
                    "Agenda",
                    "agenda"),
            ),
            array(
                FormCol::createInstance(
                    FormColType::FORM_INPUT,
                    "Waktu Pelaksanaan",
                    "waktu_pelaksanaan"),
            )
        ));

        return $formArrayObject;
    }

    public function initCreateEditDependencies()
    {

    }

}
