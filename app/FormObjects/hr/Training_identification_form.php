<?php namespace App\FormObjects\hr;

use App\FormEntities\FormCol;
use App\FormEntities\FormColType;
use App\FormEntities\FormData;
use App\FormEntities\FormRow;
use App\FormEntities\FormRowType;
use App\FormEntities\SelectChoice;

trait Training_identification_form
{
    private $designationChoices;

    public function getFormObject()
    {
        $this->initCreateEditDependencies();
        $formArrayObject = array();

        array_push($formArrayObject, new FormRow(FormRowType::TWO_SPLIT, null,
            FormCol::createInstance(
                FormColType::FORM_INPUT,
                lang("hr.training_topic"),
                "training_topic"),
            FormCol::createInstance(
                FormColType::FORM_INPUT,
                lang("hr.proposed_trainer"),
                "proposed_trainer")
        ));

        array_push($formArrayObject, new FormRow(FormRowType::TWO_SPLIT, null,
            FormCol::createInstance(
                FormColType::FORM_SELECT,
                lang("hr.department"),
                "department",
                $this->departmentChoices),
            FormCol::createInstance(
                FormColType::FORM_SELECT,
                lang("hr.training_type"),
                "training_type",
                $this->trainingTypeChoices)
        ));

        array_push($formArrayObject, new FormRow(FormRowType::TWO_SPLIT, null,
            FormCol::createInstance(
                FormColType::FORM_INPUT_DATE,
                lang("hr.planned_date"),
                "planned_date"),
            FormCol::createInstance(
                FormColType::FORM_TEXTAREA,
                lang("hr.remarks"),
                "remarks")
        ));

        $formRow = new FormRow(FormRowType::ADDABLE, lang("hr.trainees"),
            FormCol::createInstance(
                FormColType::FORM_INPUT,
                lang("hr.attendee_name"),
                "attendee_name"),
            FormCol::createInstance(
                FormColType::FORM_INPUT,
                lang("hr.pre_trainning_competence"),
                "pre_trainning_competence")
            );
        $formRow->form_data = new FormData("hr_trainee_items");
        array_push($formArrayObject, $formRow);

        return $formArrayObject;
    }

    public function initCreateEditDependencies()
    {
        helper("select_option/training_type_list");

        $this->trainingTypeChoices = array();
        $choices = get_training_type_list();
        foreach ($choices as $value => $option) {
            array_push($this->trainingTypeChoices, new SelectChoice($value, $option));
        }

        $this->departmentChoices = array();
        $rows = $this->DepartmentModel->findAll();
        foreach ($rows as $item) {
            $value = $item->id;
            $option = $item->department_name;
            array_push($this->departmentChoices, new SelectChoice($value, $option));
        }
    }

}
