<?php namespace App\FormObjects\hr;

use App\FormEntities\FormCol;
use App\FormEntities\FormColType;
use App\FormEntities\FormRow;
use App\FormEntities\FormRowType;
use App\FormEntities\SelectChoice;

trait Designation_form
{
    private $approveAuthorChoices;

    public function getFormObject()
    {
        $this->initCreateEditDependencies();
        $formArrayObject = array();

        array_push($formArrayObject, new FormRow(FormRowType::TWO_SPLIT, null,
            FormCol::createInstance(
                FormColType::FORM_INPUT,
                lang("hr.designation_name"),
                "designation_name"),
            FormCol::createInstance(
                FormColType::FORM_SELECT,
                lang("hr.approve_author"),
                "approve_author",
                $this->approveAuthorChoices)
        ));

        return $formArrayObject;
    }

    public function initCreateEditDependencies()
    {
        helper("select_option/monitoring_frequency_list");

        $this->monitoringFrequencyChoices = array();
        $choices = get_monitoring_frequency_list();
        foreach ($choices as $value => $option) {
            array_push($this->monitoringFrequencyChoices, new SelectChoice($value, $option));
        }

        $this->approveAuthorChoices = array();
        $rows = $this->EmployeeModel->findAll();
        foreach ($rows as $item) {
            $value = $item->id;
            $option = $item->first_name . " " . $item->last_name;
            array_push($this->approveAuthorChoices, new SelectChoice($value, $option));
        }
    }

}
