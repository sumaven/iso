<?php namespace App\FormObjects\risk_opportunity;

use App\FormEntities\FormCol;
use App\FormEntities\FormColType;
use App\FormEntities\FormRow;
use App\FormEntities\FormRowType;
use App\FormEntities\SelectChoice;

trait Risk_opportunity_form
{
    private $departmentChoices;
    private $approveAuthorChoices;
    private $managementSystemTypeChoices;
    private $riskTypeChoices;
    private $riskCategoryChoices;
    private $treatmentTypeChoices;
    private $riskChoices;
    private $isItOpportunityChoices;

    public function getFormObject()
    {
        $this->initCreateEditDependencies();
        $formArrayObject = array();

        array_push($formArrayObject, new FormRow(FormRowType::TWO_SPLIT, null,
            (new FormCol())
                ->setFormColType(FormColType::FORM_SELECT)
                ->setFormLabel(lang("risk_opportunity.management_system_type"))
                ->setFormName("management_system_type")
                ->setFormData($this->managementSystemTypeChoices),
            (new FormCol())
                ->setFormColType(FormColType::FORM_SELECT)
                ->setFormLabel(lang("risk_opportunity.department"))
                ->setFormName("department")
                ->setFormData($this->departmentChoices)
        ));

        array_push($formArrayObject, new FormRow(FormRowType::TWO_SPLIT, null,
            (new FormCol())
                ->setFormColType(FormColType::FORM_SELECT)
                ->setFormLabel(lang("risk_opportunity.risk_opportunity_type"))
                ->setFormName("risk_opportunity_type")
                ->setFormData($this->riskTypeChoices),
            (new FormCol())
                ->setFormColType(FormColType::FORM_SELECT)
                ->setFormLabel(lang("risk_opportunity.risk_category"))
                ->setFormName("risk_category")
                ->setFormData($this->riskCategoryChoices)
        ));

        array_push($formArrayObject, new FormRow(FormRowType::TWO_SPLIT, null,
            (new FormCol())
                ->setFormColType(FormColType::FORM_SELECT)
                ->setFormLabel(lang("risk_opportunity.risk_impact"))
                ->setFormName("risk_impact")
                ->setFormData($this->riskChoices),
            (new FormCol())
                ->setFormColType(FormColType::FORM_SELECT)
                ->setFormLabel(lang("risk_opportunity.risk_probability"))
                ->setFormName("risk_probability")
                ->setFormData($this->riskChoices)
        ));

        array_push($formArrayObject, new FormRow(FormRowType::TWO_SPLIT, null,
            (new FormCol())
                ->setFormColType(FormColType::FORM_SELECT)
                ->setFormLabel(lang("risk_opportunity.assigned_to"))
                ->setFormName("assigned_to")
                ->setFormData($this->approveAuthorChoices),
            (new FormCol())
                ->setFormColType(FormColType::FORM_SELECT)
                ->setFormLabel(lang("risk_opportunity.treatment_type"))
                ->setFormName("treatment_type")
                ->setFormData($this->treatmentTypeChoices)
        ));

        array_push($formArrayObject, new FormRow(FormRowType::TWO_SPLIT, null,
            (new FormCol())
                ->setFormColType(FormColType::FORM_SELECT)
                ->setFormLabel(lang("risk_opportunity.is_opportunity"))
                ->setFormName("is_opportunity")
                ->setFormData($this->isItOpportunityChoices),
            (new FormCol())
                ->setFormColType(FormColType::FORM_SELECT)
                ->setFormLabel(lang("risk_opportunity.approve_author"))
                ->setFormName("approve_author")
                ->setFormData($this->approveAuthorChoices)
        ));

        array_push($formArrayObject, new FormRow(FormRowType::ONE_SOLO, null,
            (new FormCol())
                ->setFormColType(FormColType::FORM_INPUT_DATE)
                ->setFormLabel(lang("risk_opportunity.proposed_review_date"))
                ->setFormName("proposed_review_date")
        ));

        array_push($formArrayObject, new FormRow(FormRowType::ONE_SOLO, null,
            (new FormCol())
                ->setFormColType(FormColType::FORM_INPUT_DATE)
                ->setFormLabel(lang("risk_opportunity.proposed_completion_date"))
                ->setFormName("proposed_completion_date")
        ));

        array_push($formArrayObject, new FormRow(FormRowType::ONE_SOLO, null,
            (new FormCol())
                ->setFormColType(FormColType::FORM_TEXTAREA)
                ->setFormLabel(lang("risk_opportunity.risk_opportunity_source"))
                ->setFormName("risk_opportunity_source")
        ));

        array_push($formArrayObject, new FormRow(FormRowType::ONE_SOLO, null,
            (new FormCol())
                ->setFormColType(FormColType::FORM_TEXTAREA)
                ->setFormLabel(lang("risk_opportunity.risk_description"))
                ->setFormName("risk_description")
        ));

        array_push($formArrayObject, new FormRow(FormRowType::ONE_SOLO, null,
            (new FormCol())
                ->setFormColType(FormColType::FORM_TEXTAREA)
                ->setFormLabel(lang("risk_opportunity.actions_proposed"))
                ->setFormName("actions_proposed")
        ));

        return $formArrayObject;
    }

    public function initCreateEditDependencies()
    {
        helper("select_option/risk_list");
        helper("select_option/treatment_type_list");

        // Risk Impact / Probability Choices
        $this->treatmentTypeChoices = array();
        $choices = get_treatment_type_list();
        foreach ($choices as $value => $option) {
            array_push($this->treatmentTypeChoices, new SelectChoice($value, $option));
        }

        // Risk Impact / Probability Choices
        $this->riskChoices = array();
        $choices = get_risk_list();
        foreach ($choices as $value => $option) {
            array_push($this->riskChoices, new SelectChoice($value, $option));
        }

        // Is it Opportunity Choices
        $this->isItOpportunityChoices = array();
        $choices = array(
            "Half-Hourly" => "Half-Hourly",
            "Hourly" => "Hourly",
        );
        foreach ($choices as $value => $option) {
            array_push($this->isItOpportunityChoices, new SelectChoice($value, $option));
        }

        // Management System Type Choice
        $this->managementSystemTypeChoices = array();
        $managementSystemTypes = $this->ManagementSystemTypeModel->findAll();
        foreach ($managementSystemTypes as $item){
            $value = $item->id;
            $option = $item->management_system_short_name;
            array_push($this->managementSystemTypeChoices, new SelectChoice($value, $option));
        }

        // Approve Author Choices
        $this->approveAuthorChoices = array();
        $rows = $this->EmployeeModel->findAll();
        foreach ($rows as $item) {
            $value = $item->id;
            $option = $item->first_name . " " . $item->last_name;
            array_push($this->approveAuthorChoices, new SelectChoice($value, $option));
        }

        // Department Choice
        $this->departmentChoices = array();
        $rows = $this->DepartmentModel->findAll();
        foreach ($rows as $item) {
            $value = $item->id;
            $option = $item->department_name;
            array_push($this->departmentChoices, new SelectChoice($value, $option));
        }

        // Risk Type Choice
        $this->riskTypeChoices = array();
        $rows = $this->RiskOpportunityTypeModel->findAll();
        foreach ($rows as $item) {
            $value = $item->id;
            $option = $item->risk_type;
            array_push($this->riskTypeChoices, new SelectChoice($value, $option));
        }

        // Risk Type Choice
        $this->riskCategoryChoices = array();
        $rows = $this->RiskOpportunityCategoryModel->findAll();
        foreach ($rows as $item) {
            $value = $item->id;
            $option = $item->risk_category;
            array_push($this->riskCategoryChoices, new SelectChoice($value, $option));
        }
    }

}
