<?php namespace App\FormObjects\internal_audit;

use App\FormEntities\FormCol;
use App\FormEntities\FormColType;
use App\FormEntities\FormRow;
use App\FormEntities\FormRowType;
use App\FormEntities\SelectChoice;

trait Non_conformance_form
{
    private $departmentChoices;
    private $referenceChoices;

    public function getFormObject()
    {
        $this->initCreateEditDependencies();
        $formArrayObject = array();

        array_push($formArrayObject, new FormRow(FormRowType::ONE_SOLO, null,
            (new FormCol())
                ->setFormColType(FormColType::FORM_INPUT)
                ->setFormLabel(lang("internal_audit.header_title"))
                ->setFormName("header_title")
        ));

        array_push($formArrayObject, new FormRow(FormRowType::TWO_SPLIT, null,
            (new FormCol())
                ->setFormColType(FormColType::FORM_INPUT)
                ->setFormLabel(lang("internal_audit.no"))
                ->setFormName("no"),
            (new FormCol())
                ->setFormColType(FormColType::FORM_SELECT)
                ->setFormLabel(lang("internal_audit.departemen"))
                ->setFormName("departemen")
                ->setFormData($this->departmentChoices)
        ));

        array_push($formArrayObject, new FormRow(FormRowType::TWO_SPLIT, null,
            (new FormCol())
                ->setFormColType(FormColType::FORM_INPUT)
                ->setFormLabel(lang("internal_audit.lokasi"))
                ->setFormName("lokasi"),
            (new FormCol())
                ->setFormColType(FormColType::FORM_SELECT)
                ->setFormLabel(lang("internal_audit.audit"))
                ->setFormName("audit")
                ->setFormData($this->referenceChoices)
        ));

        array_push($formArrayObject, new FormRow(FormRowType::TWO_SPLIT, null,
            (new FormCol())
                ->setFormColType(FormColType::FORM_INPUT_DATE)
                ->setFormLabel(lang("internal_audit.tanggal"))
                ->setFormName("tanggal")
        ));

        array_push($formArrayObject, new FormRow(FormRowType::ONE_SOLO, null,
            (new FormCol())
                ->setFormColType(FormColType::FORM_TEXTAREA)
                ->setFormLabel(lang("internal_audit.penjelasan_ketidaksesuaian"))
                ->setFormName("penjelasan_ketidaksesuaian")
        ));

        array_push($formArrayObject, new FormRow(FormRowType::ONE_SOLO, null,
            (new FormCol())
                ->setFormColType(FormColType::FORM_TEXTAREA)
                ->setFormLabel(lang("internal_audit.tindakan_perbaikan"))
                ->setFormName("tindakan_perbaikan")
        ));

        array_push($formArrayObject, new FormRow(FormRowType::ONE_SOLO, null,
            (new FormCol())
                ->setFormColType(FormColType::FORM_TEXTAREA)
                ->setFormLabel(lang("internal_audit.tanggal_penyelesaian"))
                ->setFormName("tanggal_penyelesaian")
        ));

        array_push($formArrayObject, new FormRow(FormRowType::ONE_SOLO, null,
            (new FormCol())
                ->setFormColType(FormColType::FORM_TEXTAREA)
                ->setFormLabel(lang("internal_audit.penjelasan_perbaikan"))
                ->setFormName("penjelasan_perbaikan")
        ));

        array_push($formArrayObject, new FormRow(FormRowType::ONE_SOLO, null,
            (new FormCol())
                ->setFormColType(FormColType::FORM_TEXTAREA)
                ->setFormLabel(lang("internal_audit.tindakan_perbaikan_selesai"))
                ->setFormName("tindakan_perbaikan_selesai")
        ));

        return $formArrayObject;
    }

    public function initCreateEditDependencies()
    {
        helper("select_option/reference_list");

        $this->referenceChoices = array();
        $choices = get_reference_list();
        foreach ($choices as $value => $option) {
            array_push($this->referenceChoices, new SelectChoice($value, $option));
        }

        // Department Choice
        $this->departmentChoices = array();
        $rows = $this->DepartmentModel->findAll();
        foreach ($rows as $item) {
            $value = $item->id;
            $option = $item->department_name;
            array_push($this->departmentChoices, new SelectChoice($value, $option));
        }
    }

}
