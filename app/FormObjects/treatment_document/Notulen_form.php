<?php namespace App\FormObjects\treatment_document;

use App\FormEntities\FormCol;
use App\FormEntities\FormColType;
use App\FormEntities\FormRow;
use App\FormEntities\FormRowType;
use App\FormEntities\SelectChoice;

trait Notulen_form
{
    private $approveAuthorChoices;
    private $konseptorChoices;

    public function getFormObject()
    {
        $this->initCreateEditDependencies();
        $formArrayObject = array();

        array_push($formArrayObject, new FormRow(FormRowType::TWO_SPLIT, null,
            (new FormCol())
                ->setFormColType(FormColType::FORM_INPUT)
                ->setFormLabel(lang("treatment_document.hari"))
                ->setFormName("hari"),
            (new FormCol())
                ->setFormColType(FormColType::FORM_INPUT_DATE)
                ->setFormLabel(lang("treatment_document.tanggal"))
                ->setFormName("tanggal")
        ));

        array_push($formArrayObject, new FormRow(FormRowType::TWO_SPLIT, null,
            (new FormCol())
                ->setFormColType(FormColType::FORM_INPUT)
                ->setFormLabel(lang("treatment_document.waktu"))
                ->setFormName("waktu"),
            (new FormCol())
                ->setFormColType(FormColType::FORM_INPUT)
                ->setFormLabel(lang("treatment_document.tempat"))
                ->setFormName("tempat")
        ));

        array_push($formArrayObject, new FormRow(FormRowType::TWO_SPLIT, null,
            (new FormCol())
                ->setFormColType(FormColType::FORM_INPUT)
                ->setFormLabel(lang("treatment_document.status"))
                ->setFormName("status"),
            (new FormCol())
                ->setFormColType(FormColType::FORM_SELECT)
                ->setFormLabel(lang("treatment_document.pic"))
                ->setFormName("pic")
                ->setFormData($this->approveAuthorChoices)
        ));

        array_push($formArrayObject, new FormRow(FormRowType::TWO_SPLIT, null,
            (new FormCol())
                ->setFormColType(FormColType::FORM_SELECT)
                ->setFormLabel(lang("treatment_document.konseptor"))
                ->setFormName("konseptor")
                ->setFormData($this->konseptorChoices)
        ));

        array_push($formArrayObject, new FormRow(FormRowType::ONE_SOLO, null,
            (new FormCol())
                ->setFormColType(FormColType::FORM_TEXTAREA)
                ->setFormLabel(lang("treatment_document.topik"))
                ->setFormName("topik")
        ));

        array_push($formArrayObject, new FormRow(FormRowType::ONE_SOLO, null,
            (new FormCol())
                ->setFormColType(FormColType::FORM_TEXTAREA)
                ->setFormLabel(lang("treatment_document.uraian"))
                ->setFormName("uraian")
        ));

        array_push($formArrayObject, new FormRow(FormRowType::ONE_SOLO, null,
            (new FormCol())
                ->setFormColType(FormColType::FORM_TEXTAREA)
                ->setFormLabel(lang("treatment_document.status_panjang"))
                ->setFormName("status_panjang")
        ));

        return $formArrayObject;
    }

    public function initCreateEditDependencies()
    {
//        helper("select_option/document_type_list");
//
//        $this->documentTypeChoices = array();
//        $choices = get_document_type_list();
//        foreach ($choices as $value => $option) {
//            array_push($this->documentTypeChoices, new SelectChoice($value, $option));
//        }

        // Approve Author Choice
        $this->approveAuthorChoices = array();
        $rows = $this->EmployeeModel->findAll();
        foreach ($rows as $item) {
            $value = $item->id;
            $option = $item->first_name . " " . $item->last_name;
            array_push($this->approveAuthorChoices, new SelectChoice($value, $option));
        }

        // Konseptor Choices
        $this->konseptorChoices = array();
        $value = session()->get("id");
        $option = get_approve_author_val($value);
        array_push($this->konseptorChoices, new SelectChoice($value, $option));
    }

}
