<?php namespace App\FormObjects\treatment_document;

use App\FormEntities\FormCol;
use App\FormEntities\FormColType;
use App\FormEntities\FormRow;
use App\FormEntities\FormRowType;
use App\FormEntities\SelectChoice;

trait Rekaman_form
{
    private $referenceChoices;
    private $approvalChoices;
    private $distributionChoices;

    public function getFormObject()
    {
        $this->initCreateEditDependencies();
        $formArrayObject = array();

        array_push($formArrayObject, new FormRow(FormRowType::TWO_SPLIT, null,
            (new FormCol())
                ->setFormColType(FormColType::FORM_SELECT)
                ->setFormLabel(lang("treatment_document.referensi"))
                ->setFormName("referensi")
                ->setFormData($this->referenceChoices),
            (new FormCol())
                ->setFormColType(FormColType::FORM_INPUT)
                ->setFormLabel(lang("treatment_document.header_title"))
                ->setFormName("header_title")
        ));

        array_push($formArrayObject, new FormRow(FormRowType::ONE_SOLO, null,
            (new FormCol())
                ->setFormColType(FormColType::FORM_TEXTAREA)
                ->setFormLabel(lang("treatment_document.class_project"))
                ->setFormName("class_project")
        ));

        array_push($formArrayObject, new FormRow(FormRowType::ONE_SOLO, null,
            (new FormCol())
                ->setFormColType(FormColType::FORM_TEXTAREA)
                ->setFormLabel(lang("treatment_document.date_time_location"))
                ->setFormName("date_time_location")
        ));

        array_push($formArrayObject, new FormRow(FormRowType::ONE_SOLO, null,
            (new FormCol())
                ->setFormColType(FormColType::FORM_TEXTAREA)
                ->setFormLabel(lang("treatment_document.goal"))
                ->setFormName("goal")
        ));

        array_push($formArrayObject, new FormRow(FormRowType::ONE_SOLO, null,
            (new FormCol())
                ->setFormColType(FormColType::FORM_TEXTAREA)
                ->setFormLabel(lang("treatment_document.attendees"))
                ->setFormName("attendees")
        ));

        array_push($formArrayObject, new FormRow(FormRowType::ONE_SOLO, null,
            (new FormCol())
                ->setFormColType(FormColType::FORM_TEXTAREA)
                ->setFormLabel(lang("treatment_document.description_activity"))
                ->setFormName("description_activity")
        ));

        array_push($formArrayObject, new FormRow(FormRowType::ONE_SOLO, null,
            (new FormCol())
                ->setFormColType(FormColType::FORM_TEXTAREA)
                ->setFormLabel(lang("treatment_document.result"))
                ->setFormName("result")
        ));

        array_push($formArrayObject, new FormRow(FormRowType::TWO_SPLIT, null,
            (new FormCol())
                ->setFormColType(FormColType::FORM_SELECT)
                ->setFormLabel(lang("treatment_document.distribusi_1"))
                ->setFormName("distribusi_1")
                ->setFormData($this->distributionChoices),
            (new FormCol())
                ->setFormColType(FormColType::FORM_SELECT)
                ->setFormLabel(lang("treatment_document.distribusi_2"))
                ->setFormName("distribusi_2")
                ->setFormData($this->distributionChoices)
        ));

        array_push($formArrayObject, new FormRow(FormRowType::TWO_SPLIT, null,
            (new FormCol())
                ->setFormColType(FormColType::FORM_SELECT)
                ->setFormLabel(lang("treatment_document.approval_1"))
                ->setFormName("approval_1")
                ->setFormData($this->approvalChoices),
            (new FormCol())
                ->setFormColType(FormColType::FORM_SELECT)
                ->setFormLabel(lang("treatment_document.approval_2"))
                ->setFormName("approval_2")
                ->setFormData($this->approvalChoices)
        ));

        return $formArrayObject;
    }

    public function initCreateEditDependencies()
    {
        helper("select_option/reference_list");
        helper("select_option/approval_list");
        helper("select_option/distribution_list");

        $this->referenceChoices = array();
        $choices = get_reference_list();
        foreach ($choices as $value => $option) {
            array_push($this->referenceChoices, new SelectChoice($value, $option));
        }

        $this->approvalChoices = array();
        $choices = get_approval_list();
        foreach ($choices as $value => $option) {
            array_push($this->approvalChoices, new SelectChoice($value, $option));
        }

        $this->distributionChoices = array();
        $choices = get_distribution_list();
        foreach ($choices as $value => $option) {
            array_push($this->distributionChoices, new SelectChoice($value, $option));
        }

//        // Approve Author Choice
//        $this->approveAuthorChoices = array();
//        $rows = $this->EmployeeModel->findAll();
//        foreach ($rows as $item) {
//            $value = $item->id;
//            $option = $item->first_name . " " . $item->last_name;
//            array_push($this->approveAuthorChoices, new SelectChoice($value, $option));
//        }
    }

}
