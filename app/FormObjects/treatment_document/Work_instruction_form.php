<?php namespace App\FormObjects\treatment_document;

use App\FormEntities\FormCol;
use App\FormEntities\FormColType;
use App\FormEntities\FormRow;
use App\FormEntities\FormRowType;
use App\FormEntities\SelectChoice;

trait Work_instruction_form
{
    private $referenceChoices;
    private $approvalChoices;
    private $distributionChoices;
    private $departmentChoices;

    public function getFormObject()
    {
        $this->initCreateEditDependencies();
        $formArrayObject = array();

        array_push($formArrayObject, new FormRow(FormRowType::TWO_SPLIT, null,
                (new FormCol())
                    ->setFormColType(FormColType::FORM_INPUT)
                    ->setFormName("revision")
                    ->setFormLabel(lang("treatment_document.work_instruction.revision")),
                (new FormCol())
                    ->setFormColType(FormColType::FORM_INPUT_DATE)
                    ->setFormName("date_issued")
                    ->setFormLabel(lang("treatment_document.work_instruction.date_issued"))
            )
        );

        array_push($formArrayObject, new FormRow(FormRowType::TWO_SPLIT, null,
                (new FormCol()),
                (new FormCol())
                    ->setFormColType(FormColType::FORM_SELECT)
                    ->setFormName("department")
                    ->setFormLabel(lang("treatment_document.procedure.department"))
                    ->setFormData($this->departmentChoices)
            )
        );

        array_push($formArrayObject, new FormRow(FormRowType::ONE_SOLO, null,
                (new FormCol())
                    ->setFormColType(FormColType::FORM_INPUT)
                    ->setFormName("header_title")
                    ->setFormLabel(lang("treatment_document.work_instruction.header_title"))
                    ->setFormDescription(lang("treatment_document.work_instruction.header_title_description"))
            )
        );

        array_push($formArrayObject, new FormRow(FormRowType::ONE_SOLO, null,
                (new FormCol())
                    ->setFormColType(FormColType::FORM_TEXTAREA)
                    ->setFormName("scope")
                    ->setFormLabel(lang("treatment_document.work_instruction.scope"))
                    ->setFormDescription(lang("treatment_document.work_instruction.scope_description"))
            )
        );

        array_push($formArrayObject, new FormRow(FormRowType::ONE_SOLO, null,
                (new FormCol())
                    ->setFormColType(FormColType::FORM_TEXTAREA)
                    ->setFormName("goal")
                    ->setFormLabel(lang("treatment_document.work_instruction.goal"))
                    ->setFormDescription(lang("treatment_document.work_instruction.goal_description"))
            )
        );

        array_push($formArrayObject, new FormRow(FormRowType::ONE_SOLO, null,
                (new FormCol())
                    ->setFormColType(FormColType::FORM_TEXTAREA)
                    ->setFormName("work_instruction")
                    ->setFormLabel(lang("treatment_document.work_instruction.work_instruction"))
                    ->setFormDescription(lang("treatment_document.work_instruction.work_instruction_description"))
            )
        );

        array_push($formArrayObject, new FormRow(FormRowType::TWO_SPLIT, null,
                (new FormCol())
                    ->setFormColType(FormColType::FORM_CHECKBOX)
                    ->setFormName("reference")
                    ->setFormLabel(lang("treatment_document.work_instruction.reference"))
                    ->setFormData($this->referenceChoices),
                (new FormCol())
                    ->setFormColType(FormColType::FORM_CHECKBOX)
                    ->setFormName("approval")
                    ->setFormLabel(lang("treatment_document.work_instruction.approval"))
                    ->setFormData($this->approvalChoices)
            )
        );

        array_push($formArrayObject, new FormRow(FormRowType::TWO_SPLIT, null,
                (new FormCol())
                    ->setFormColType(FormColType::FORM_CHECKBOX)
                    ->setFormName("distribution")
                    ->setFormLabel(lang("treatment_document.work_instruction.distribution"))
                    ->setFormData($this->distributionChoices),
                FormCol::createInstance()
            )
        );

        return $formArrayObject;
    }

    public function initCreateEditDependencies()
    {
        helper("select_option/reference_list");
        helper("select_option/approval_list");
        helper("select_option/distribution_list");

        $this->referenceChoices = array();
        $choices = get_reference_list();
        foreach ($choices as $value => $option) {
            array_push($this->referenceChoices, new SelectChoice($value, $option));
        }

        $this->approvalChoices = array();
        $choices = get_approval_list();
        foreach ($choices as $value => $option) {
            array_push($this->approvalChoices, new SelectChoice($value, $option));
        }

        $this->distributionChoices = array();
        $choices = get_distribution_list();
        foreach ($choices as $value => $option) {
            array_push($this->distributionChoices, new SelectChoice($value, $option));
        }

        $this->departmentChoices = array();
        $rows = $this->DepartmentModel->findAll();
        foreach ($rows as $item) {
            $value = $item->id;
            $option = $item->department_name;
            array_push($this->departmentChoices, new SelectChoice($value, $option));
        }
    }

}
