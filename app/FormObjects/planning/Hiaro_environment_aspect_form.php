<?php namespace App\FormObjects\planning;

use App\FormEntities\DependentSelectChoice;
use App\FormEntities\FormCol;
use App\FormEntities\FormColType;
use App\FormEntities\FormRow;
use App\FormEntities\FormRowType;
use App\FormEntities\SelectChoice;

trait Hiaro_environment_aspect_form
{

    private $qhseRiskChoices;
    private $approveAuthorChoices;
    private $categoryChoices;
    private $operationalConditionChoices;
    private $processNameChoices;
    private $deptLocationChoices;
    private $consequencesBenefitInitialRatingChoices;
    private $likelihoodInitialRating;
    private $qhseIssueCategoryChoices;
    private $existingControlChoices;
    private $riskOpportunityChoices;

    public function getFormObject()
    {
        $this->initCreateEditDependencies();
        $formArrayObject = array();

        array_push($formArrayObject, new FormRow(FormRowType::TWO_SPLIT, null,
            (new FormCol())
                ->setFormColType(FormColType::FORM_SELECT)
                ->setFormLabel(lang("planning.dept_location"))
                ->setFormDescription(lang("planning.dept_location_desc"))
                ->setFormName("dept_location")
                ->setFormData($this->deptLocationChoices),
            FormCol::createInstance()
        ));

        array_push($formArrayObject, new FormRow(FormRowType::TWO_SPLIT, null,
            (new FormCol())
                ->setFormColType(FormColType::FORM_SELECT)
                ->setFormLabel(lang("planning.process_name"))
                ->setFormDescription(lang("planning.process_name_desc"))
                ->setFormName("process_name")
                ->setFormData($this->processNameChoices)
                ->setFormLabelStyle(array(
                    "font-weight" => "bold"
                )),
            (new FormCol())
                ->setFormColType(FormColType::FORM_INPUT)
                ->setFormLabel(lang("planning.activity"))
                ->setFormDescription(lang("planning.activity_desc"))
                ->setFormName("activity")
                ->setFormLabelStyle(array(
                    "font-weight" => "bold"
                ))
        ));

        array_push($formArrayObject, new FormRow(FormRowType::TWO_SPLIT, " ",
            array(
                (new FormCol())
                    ->setFormColType(FormColType::FORM_SELECT)
                    ->setFormLabel(lang("planning.qhse_risk"))
                    ->setFormDescription(lang("planning.qhse_risk_desc"))
                    ->setFormName("qhse_risk")
                    ->setFormData($this->qhseRiskChoices),
                (new FormCol())
                    ->setFormColType(FormColType::FORM_SELECT)
                    ->setFormLabel(lang("planning.category"))
                    ->setFormDescription(lang("planning.category_desc"))
                    ->setFormName("category")
                    ->setFormData($this->categoryChoices),
                (new FormCol())
                    ->setFormColType(FormColType::FORM_SELECT)
                    ->setFormLabel(lang("planning.qhse_issue_category"))
                    ->setFormDescription(lang("planning.qhse_issue_category_desc"))
                    ->setFormName("qhse_issue_category")
                    ->setFormData($this->qhseIssueCategoryChoices),
                (new FormCol())
                    ->setFormColType(FormColType::FORM_TEXTAREA)
                    ->setFormLabel(lang("planning.qhse_potential_issue_description"))
                    ->setFormDescription(lang("planning.qhse_potential_issue_description_desc"))
                    ->setFormName("qhse_potential_issue_description"),
                (new FormCol())
                    ->setFormColType(FormColType::FORM_SELECT)
                    ->setFormLabel(lang("planning.operational_condition"))
                    ->setFormDescription(lang("planning.operational_condition_desc"))
                    ->setFormName("operational_condition")
                    ->setFormData($this->operationalConditionChoices)
            ),
            array(
                (new FormCol())
                    ->setFormColType(FormColType::FORM_SELECT)
                    ->setFormLabel(lang("planning.risk_opportunity"))
                    ->setFormDescription(lang("planning.risk_opportunity_desc"))
                    ->setFormName("risk_opportunity")
                    ->setFormData($this->riskOpportunityChoices),
                (new FormCol())
                    ->setFormColType(FormColType::FORM_TEXTAREA)
                    ->setFormLabel(lang("planning.qhse_potential_impact"))
                    ->setFormDescription(lang("planning.qhse_potential_impact_desc"))
                    ->setFormName("qhse_potential_impact"),
                (new FormCol())
                    ->setFormColType(FormColType::FORM_INPUT)
                    ->setFormLabel(lang("planning.ro_impact_company"))
                    ->setFormDescription(lang("planning.ro_impact_company_desc"))
                    ->setFormName("ro_impact_company"),
                (new FormCol())
                    ->setFormColType(FormColType::FORM_INPUT)
                    ->setFormLabel(lang("planning.interested_parties"))
                    ->setFormDescription(lang("planning.interested_parties_desc"))
                    ->setFormName("interested_parties"),
                (new FormCol())
                    ->setFormColType(FormColType::FORM_INPUT)
                    ->setFormLabel(lang("planning.compliance_description"))
                    ->setFormDescription(lang("planning.compliance_description_desc"))
                    ->setFormName("compliance_description")
            )
        ));

        array_push($formArrayObject, new FormRow(FormRowType::TWO_SPLIT, null,
            (new FormCol())
                ->setFormColType(FormColType::FORM_SELECT)
                ->setFormLabel(lang("planning.pre_consequences_benefit_initial_rating"))
                ->setFormDescription(lang("planning.pre_consequences_benefit_initial_rating_desc"))
                ->setFormName("pre_consequences_benefit_initial_rating")
                ->setFormTitle("Pre-Control")
                ->setFormData($this->consequencesBenefitInitialRatingChoices),
            (new FormCol())
                ->setFormColType(FormColType::FORM_SELECT)
                ->setFormLabel(lang("planning.post_consequences_benefit_initial_rating"))
                ->setFormDescription(lang("planning.post_consequences_benefit_initial_rating_desc"))
                ->setFormName("post_consequences_benefit_initial_rating")
                ->setFormTitle("Post-Control")
                ->setFormData($this->consequencesBenefitInitialRatingChoices)
        ));

        array_push($formArrayObject, new FormRow(FormRowType::TWO_SPLIT, NULL,
            (new FormCol())
                ->setFormColType(FormColType::FORM_SELECT)
                ->setFormLabel(lang("planning.pre_likelihood_initial_rating"))
                ->setFormDescription(lang("planning.pre_likelihood_initial_rating_desc"))
                ->setFormName("pre_likelihood_initial_rating")
                ->setFormData($this->likelihoodInitialRating),
            (new FormCol())
                ->setFormColType(FormColType::FORM_SELECT)
                ->setFormLabel(lang("planning.post_likelihood_initial_rating"))
                ->setFormDescription(lang("planning.post_likelihood_initial_rating_desc"))
                ->setFormName("post_likelihood_initial_rating")
                ->setFormData($this->likelihoodInitialRating)
        ));

        array_push($formArrayObject, new FormRow(FormRowType::TWO_SPLIT, NULL,
            (new FormCol())
                ->setFormColType(FormColType::FORM_INPUT)
                ->setFormLabel(lang("planning.initial_risk_rating"))
                ->setFormDescription(lang("planning.initial_risk_rating_desc"))
                ->setFormName("initial_risk_rating"),
            (new FormCol())
                ->setFormColType(FormColType::FORM_INPUT)
                ->setFormLabel(lang("planning.final_risk_rating"))
                ->setFormDescription(lang("planning.final_risk_rating_desc"))
                ->setFormName("final_risk_rating")
        ));

        array_push($formArrayObject, new FormRow(FormRowType::TWO_SPLIT, NULL,
            (new FormCol())
                ->setFormColType(FormColType::FORM_SELECT)
                ->setFormLabel("Select")
                ->setFormDescription(lang("planning.existing_control_desc"))
                ->setFormName("existing_control")
                ->setFormTitle(lang("planning.existing_control"))
                ->setFormData($this->existingControlChoices),
            (new FormCol())
                ->setFormColType(FormColType::FORM_SELECT)
                ->setFormLabel("Select")
                ->setFormDescription(lang("planning.additional_control_desc"))
                ->setFormName("additional_control")
                ->setFormTitle(lang("planning.additional_control"))
                ->setFormData($this->existingControlChoices)
        ));

        array_push($formArrayObject, new FormRow(FormRowType::TWO_SPLIT, NULL,
            (new FormCol())
                ->setFormColType(FormColType::FORM_TEXTAREA)
                ->setFormLabel("")
                ->setFormDescription("")
                ->setFormDescription(lang("planning.remarks_desc"))
                ->setFormName("remarks"),
            (new FormCol())
                ->setFormColType(FormColType::FORM_TEXTAREA)
                ->setFormLabel("")
                ->setFormDescription("")
                ->setFormDescription(lang("planning.remarks_2_desc"))
                ->setFormName("remarks_2")
        ));

        array_push($formArrayObject, new FormRow(FormRowType::TWO_SPLIT, NULL,
            FormCol::createInstance(),
            (new FormCol())
                ->setFormColType(FormColType::FORM_SELECT)
                ->setFormLabel(lang("planning.approve_author"))
                ->setFormDescription(lang("planning.approve_author_desc"))
                ->setFormName("approve_author")
                ->setFormData($this->approveAuthorChoices)
        ));


        return $formArrayObject;
    }

    public function initCreateEditDependencies()
    {
        helper("select_option/qhse_risk_list");
        helper("select_option/dependent_qhse_issue_category_list");
        helper("select_option/dependent_operational_condition_list");
        helper("select_option/dependent_category_list");
        helper("select_option/existing_control_list");

        $this->qhseRiskChoices = array();
        $choices = get_qhse_risk_list();
        foreach ($choices as $value => $option) {
            array_push($this->qhseRiskChoices, new SelectChoice($value, $option));
        }

        $selectChoice = new DependentSelectChoice();
        $selectChoice->setDependentToValueOf("qhse_risk");
        $selectChoice->dependantChoices = get_dependent_operational_condition_list();
        $this->operationalConditionChoices = $selectChoice;

        $selectChoice = new DependentSelectChoice();
        $selectChoice->setDependentToValueOf("qhse_risk");
        $selectChoice->dependantChoices = get_dependent_category_list();
        $this->categoryChoices = $selectChoice;

        $selectChoice = new DependentSelectChoice();
        $selectChoice->setDependentToValueOf("qhse_risk");
        $selectChoice->dependantChoices = get_dependent_qhse_issue_category_list();
        $this->qhseIssueCategoryChoices = $selectChoice;

        $this->existingControlChoices = array();
        $choices = get_existing_control_list();
        foreach ($choices as $value => $option) {
            array_push($this->existingControlChoices, new SelectChoice($value, $option));
        }

        $this->consequencesBenefitInitialRatingChoices = array();
        $this->likelihoodInitialRating = array();
        $choices = array(1 => 1, 2 => 2, 3 => 3, 4 => 4, 5 => 5);

        foreach ($choices as $value => $option) {
            array_push($this->consequencesBenefitInitialRatingChoices, new SelectChoice($value, $option));
        }

        foreach ($choices as $value => $option) {
            array_push($this->likelihoodInitialRating, new SelectChoice($value, $option));
        }

        // ########################################################


        // Business Proces relate - START
        // ########################################################
        $businessProcessList = $this->BusinessProcessModel->findAll();

        $this->processNameChoices = array();
        foreach ($businessProcessList as $item) {
            array_push($this->processNameChoices, new SelectChoice($item->process_name, $item->process_name));
        }

        // Dept Location
        $selectChoice = new DependentSelectChoice();
        $values = array();
        foreach ($businessProcessList as $item) {
            $values[$item->process_name] = array($item->dept_location => $item->dept_location);
        }
        $selectChoice->setDependentToValueOf("process_name");
        $selectChoice->dependantChoices = $values;
        $this->deptLocationChoices = $selectChoice;
        // ########################################################

        // Approve Author Choice
        $this->approveAuthorChoices = array();
        $rows = $this->EmployeeModel->findAll();
        foreach ($rows as $item) {
            $value = $item->id;
            $option = $item->first_name . " " . $item->last_name;
            array_push($this->approveAuthorChoices, new SelectChoice($value, $option));
        }

        // Approve Author Choice
        $this->riskOpportunityChoices = array();
        array_push($this->riskOpportunityChoices, new SelectChoice("Risk", "Risk"));
        array_push($this->riskOpportunityChoices, new SelectChoice("Opportunity", "Opportunity"));
    }

}
