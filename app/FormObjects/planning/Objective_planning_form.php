<?php namespace App\FormObjects\planning;

use App\FormEntities\DependentSelectChoice;
use App\FormEntities\FormCol;
use App\FormEntities\FormColType;
use App\FormEntities\FormData;
use App\FormEntities\FormRow;
use App\FormEntities\FormRowType;
use App\FormEntities\SelectChoice;

trait Objective_planning_form
{
    private $processNameChoices;
    private $deptLocationChoices;
    private $performanceIndicatorChoices;
    private $resourcesChoices;
    private $monitoringFrequencyChoices;
    private $objectiveChoices;
    private $monitoringMethodologyChoices;

    public function getFormObject()
    {
        $this->initCreateEditDependencies();
        $formArrayObject = array();

        array_push($formArrayObject, new FormRow(FormRowType::TWO_SPLIT, null,
            FormCol::createInstance(
                FormColType::FORM_SELECT,
                lang("planning.dept_location"),
                "dept_location",
                $this->deptLocationChoices),
            FormCol::createInstance()
        ));
//        FormCol::createInstance(
//            FormColType::FORM_SELECT,
//            lang("planning.performance_indicator"),
//            "performance_indicator",
//            $this->performanceIndicatorChoices)

        array_push($formArrayObject, new FormRow(FormRowType::TWO_SPLIT, null,
            FormCol::createInstance(
                FormColType::FORM_SELECT,
                lang("planning.process_name"),
                "process_name",
                $this->processNameChoices),
            FormCol::createInstance(
                FormColType::FORM_TEXTAREA,
                lang("planning.objective_plan"),
                "objective_plan")
        ));

        array_push($formArrayObject, new FormRow(FormRowType::TWO_SPLIT, null,
            FormCol::createInstance(
                FormColType::FORM_SELECT,
                lang("planning.objective"),
                "objective",
                $this->objectiveChoices
            ),
            FormCol::createInstance(
                FormColType::FORM_INPUT,
                lang("planning.remark"),
                "remark")
        ));

        array_push($formArrayObject, new FormRow(FormRowType::TWO_SPLIT, null,
            FormCol::createInstance(
                FormColType::FORM_INPUT,
                lang("planning.approve_author"),
                "approve_author"),
            FormCol::createInstance(
                FormColType::FORM_SELECT,
                lang("planning.monitoring_frequency"),
                "monitoring_frequency",
                $this->monitoringFrequencyChoices)
        ));
        
        array_push($formArrayObject, new FormRow(FormRowType::TWO_SPLIT, null,
            FormCol::createInstance(
                FormColType::FORM_SELECT,
                lang("planning.monitoring_methodology"),
                "monitoring_methodology",
                $this->monitoringMethodologyChoices),
            FormCol::createInstance(
                FormColType::FORM_SELECT,
                lang("planning.resources"),
                "resources",
                $this->resourcesChoices)
        ));

        $formRow = new FormRow(FormRowType::ADDABLE, lang("planning.documented_information"),
            FormCol::createInstance(
                FormColType::FORM_INPUT,
                lang("planning.document_type"),
                "document_type"),
            FormCol::createInstance(
                FormColType::FORM_INPUT,
                lang("planning.document_name"),
                "document_name")
        );
        $formRow->form_data = new FormData("planning_objective_planning_documented_information_items");
        array_push($formArrayObject, $formRow);

        array_push($formArrayObject, new FormRow(FormRowType::TWO_SPLIT, lang("planning.risk_opportunity"),
            FormCol::createInstance(
                FormColType::FORM_INPUT,
                lang("planning.risk_description"),
                "risk_description"),
            FormCol::createInstance(
                FormColType::FORM_INPUT,
                lang("planning.risk_impact"),
                "risk_impact")
        ));

        array_push($formArrayObject, new FormRow(FormRowType::TWO_SPLIT, NULL,
            FormCol::createInstance(
                FormColType::FORM_INPUT,
                lang("planning.risk_category"),
                "risk_category"),
            FormCol::createInstance(
                FormColType::FORM_INPUT,
                lang("planning.risk_possibility"),
                "risk_possibility")
        ));

        return $formArrayObject;
    }

    public function initCreateEditDependencies()
    {
        helper("select_option/monitoring_frequency_list");

        $this->monitoringFrequencyChoices = array();
        $choices = get_monitoring_frequency_list();

        foreach ($choices as $value => $option) {
            array_push($this->monitoringFrequencyChoices, new SelectChoice($value, $option));
        }

        $businessProcessList = $this->BusinessProcessModel->findAll();

        $this->processNameChoices = array();
        foreach ($businessProcessList as $item) {
            array_push($this->processNameChoices, new SelectChoice($item->process_name, $item->process_name));
        }

        $this->objectiveChoices = array();
        foreach ($businessProcessList as $item) {
            array_push($this->objectiveChoices, new SelectChoice($item->measurement, $item->measurement));
        }

        $this->monitoringMethodologyChoices = array();
        foreach ($businessProcessList as $item) {
            array_push($this->monitoringMethodologyChoices, new SelectChoice($item->monitoring_methodology, $item->monitoring_methodology));
        }

        // Dept Location
        $selectChoice = new DependentSelectChoice();
        $values = array();
        foreach ($businessProcessList as $item) {
            $values[$item->process_name] = array($item->dept_location => $item->dept_location);
        }
        $selectChoice->setDependentToValueOf("process_name");
        $selectChoice->dependantChoices = $values;
        $this->deptLocationChoices = $selectChoice;

        // Performance Indicator
        $selectChoice = new DependentSelectChoice();
        $values = array();
        foreach ($businessProcessList as $item) {
            $values[$item->process_name] = array($item->performance_indicator => $item->performance_indicator);
        }
        $selectChoice->setDependentToValueOf("process_name");
        $selectChoice->dependantChoices = $values;
        $this->performanceIndicatorChoices = $selectChoice;

        // Resources
        $selectChoice = new DependentSelectChoice();
        $values = array();
        foreach ($businessProcessList as $item) {
            $values[$item->process_name] = array($item->resources => $item->resources);
        }
        $selectChoice->setDependentToValueOf("process_name");
        $selectChoice->dependantChoices = $values;
        $this->resourcesChoices = $selectChoice;
    }

}
