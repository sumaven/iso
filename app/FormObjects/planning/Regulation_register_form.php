<?php namespace App\FormObjects\planning;

use App\FormEntities\FormCol;
use App\FormEntities\FormColType;
use App\FormEntities\FormRow;
use App\FormEntities\FormRowType;
use App\FormEntities\SelectChoice;

trait Regulation_register_form
{
    private $qhseRiskChoices;
    private $typeListChoices;
    private $regulationTypeListChoices;

    public function getFormObject()
    {
        $this->initCreateEditDependencies();
        $formArrayObject = array();

        array_push($formArrayObject, new FormRow(FormRowType::TWO_SPLIT, null,
            FormCol::createInstance(
                FormColType::FORM_SELECT,
                lang("planning.type_list"),
                "type_list",
                $this->typeListChoices),
            FormCol::createInstance(
                FormColType::FORM_INPUT_DATE,
                lang("planning.release_date"),
                "release_date")
        ));

        array_push($formArrayObject, new FormRow(FormRowType::TWO_SPLIT, null,
            FormCol::createInstance(
                FormColType::FORM_INPUT,
                lang("planning.regulation_title"),
                "regulation_title"),
            FormCol::createInstance(
                FormColType::FORM_INPUT,
                lang("planning.institution_owner"),
                "institution_owner")
        ));

        array_push($formArrayObject, new FormRow(FormRowType::TWO_SPLIT, null,
            FormCol::createInstance(
                FormColType::FORM_SELECT,
                lang("planning.regulation_type"),
                "regulation_type",
                $this->regulationTypeListChoices),
            FormCol::createInstance()
        ));

        return $formArrayObject;
    }

    public function initCreateEditDependencies()
    {
        helper("select_option/type_list");
        helper("select_option/regulation_type_list");

        $this->typeListChoices = array();
        $choices = get_type_list();

        foreach ($choices as $value => $option) {
            array_push($this->typeListChoices, new SelectChoice($value, $option));
        }

        $this->regulationTypeListChoices = array();
        $choices = get_regulation_type_list();

        foreach ($choices as $value => $option) {
            array_push($this->regulationTypeListChoices, new SelectChoice($value, $option));
        }
    }

}
