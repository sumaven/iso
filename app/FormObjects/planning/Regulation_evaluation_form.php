<?php namespace App\FormObjects\planning;

use App\FormEntities\FormCol;
use App\FormEntities\FormColType;
use App\FormEntities\FormRow;
use App\FormEntities\FormRowType;
use App\FormEntities\SelectChoice;

trait Regulation_evaluation_form
{
    private $regulationTitleChoices;
    private $fulfillmentChoices;

    public function getFormObject()
    {
        $this->initCreateEditDependencies();
        $formArrayObject = array();

        array_push($formArrayObject, new FormRow(FormRowType::TWO_SPLIT, null,
            FormCol::createInstance(
                FormColType::FORM_SELECT,
                lang("planning.regulation_title"),
                "regulation_title",
                $this->regulationTitleChoices)
        ));

        array_push($formArrayObject, new FormRow(FormRowType::TWO_SPLIT, null,
            FormCol::createInstance(
                FormColType::FORM_INPUT,
                lang("planning.clause"),
                "clause")
        ));

        array_push($formArrayObject, new FormRow(FormRowType::TWO_SPLIT, null,
            FormCol::createInstance(
                FormColType::FORM_TEXTAREA,
                lang("planning.aspect"),
                "aspect")
        ));

        array_push($formArrayObject, new FormRow(FormRowType::TWO_SPLIT, null,
            FormCol::createInstance(
                FormColType::FORM_SELECT,
                lang("planning.fulfillment"),
                "fulfillment",
                $this->fulfillmentChoices)
        ));

        array_push($formArrayObject, new FormRow(FormRowType::TWO_SPLIT, null,
            FormCol::createInstance(
                FormColType::FORM_INPUT,
                lang("planning.evidance"),
                "evidance")
        ));

        array_push($formArrayObject, new FormRow(FormRowType::TWO_SPLIT, null,
            FormCol::createInstance(
                FormColType::FORM_TEXTAREA,
                lang("planning.remark"),
                "remark")
        ));

        return $formArrayObject;
    }

    public function initCreateEditDependencies()
    {
        $this->fulfillmentChoices = array();
        $choices = array(
            "Yes",
            "No",
            "N/A",
        );
        foreach ($choices as $value) {
            array_push($this->fulfillmentChoices, new SelectChoice($value, $value));
        }

        $this->regulationTitleChoices = array();
        $regulationRegisters = $this->RegulationRegisterModel->findAll();
        foreach ($regulationRegisters as $item) {
            $value = $item->id;
            $option = $item->regulation_title;
            array_push($this->regulationTitleChoices, new SelectChoice($value, $option));
        }
    }

}
