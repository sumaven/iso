<?php namespace App\FormObjects\planning;

use App\FormEntities\DependentSelectChoice;
use App\FormEntities\FormCol;
use App\FormEntities\FormColType;
use App\FormEntities\FormRow;
use App\FormEntities\FormRowType;
use App\FormEntities\SelectChoice;

trait Objective_evaluation_form
{
    private $processNameChoices;
    private $deptLocationChoices;
    private $objectiveChoices;
    private $monitoringFrequencyChoices;
    private $obstacleFactorChoices;
    private $levelOfAchievementChoices;
    private $percentageChoices;

    public function getFormObject()
    {
        $this->initCreateEditDependencies();
        $formArrayObject = array();

        array_push($formArrayObject, new FormRow(FormRowType::TWO_SPLIT, null,
            FormCol::createInstance(
                FormColType::FORM_SELECT,
                lang("planning.dept_location"),
                "dept_location",
                $this->deptLocationChoices),
            FormCol::createInstance()
        ));

        array_push($formArrayObject, new FormRow(FormRowType::TWO_SPLIT, null,
            FormCol::createInstance(
                FormColType::FORM_SELECT,
                lang("planning.process_name"),
                "process_name",
                $this->processNameChoices),
            FormCol::createInstance(
                FormColType::FORM_SELECT,
                lang("planning.objective"),
                "objective",
                $this->objectiveChoices)
        ));

        array_push($formArrayObject, new FormRow(FormRowType::TWO_SPLIT, null,
            FormCol::createInstance(
                FormColType::FORM_INPUT,
                lang("planning.step_action"),
                "step_action"),
            FormCol::createInstance(
                FormColType::FORM_INPUT,
                lang("planning.parameter"),
                "parameter")
        ));

        array_push($formArrayObject, new FormRow(FormRowType::TWO_SPLIT, null,
            FormCol::createInstance(
                FormColType::FORM_FILE,
                lang("planning.evidance"),
                "evidance"),
            FormCol::createInstance(
                FormColType::FORM_SELECT,
                lang("planning.level_of_achievement"),
                "level_of_achievement",
                $this->levelOfAchievementChoices)
        ));

        array_push($formArrayObject, new FormRow(FormRowType::TWO_SPLIT, null,
            FormCol::createInstance(
                FormColType::FORM_SELECT,
                lang("planning.percentage"),
                "percentage",
                $this->percentageChoices),
            FormCol::createInstance(
                FormColType::FORM_SELECT,
                lang("planning.obstacle_factor"),
                "obstacle_factor",
                $this->obstacleFactorChoices)
        ));

        array_push($formArrayObject, new FormRow(FormRowType::TWO_SPLIT, null,
            FormCol::createInstance(
                FormColType::FORM_SELECT,
                lang("planning.monitoring_frequency"),
                "monitoring_frequency",
                $this->monitoringFrequencyChoices),
            FormCol::createInstance(
                FormColType::FORM_INPUT,
                lang("planning.approve_author"),
                "approve_author")
        ));

        array_push($formArrayObject, new FormRow(FormRowType::TWO_SPLIT, null,
            FormCol::createInstance(
                FormColType::FORM_TEXTAREA,
                lang("planning.improvement_suggestion"),
                "improvement_suggestion"),
            FormCol::createInstance(
                FormColType::FORM_TEXTAREA,
                lang("planning.remark"),
                "remark")
        ));

        return $formArrayObject;
    }

    public function initCreateEditDependencies()
    {
        helper("select_option/monitoring_frequency_list");
        helper("select_option/obstacle_factor_list");

        $this->monitoringFrequencyChoices = array();
        $choices = get_monitoring_frequency_list();
        foreach ($choices as $value => $option) {
            array_push($this->monitoringFrequencyChoices, new SelectChoice($value, $option));
        }

        $this->obstacleFactorChoices = array();
        $choices = get_obstacle_factor_list();
        foreach ($choices as $value => $option) {
            array_push($this->obstacleFactorChoices, new SelectChoice($value, $option));
        }

        $this->percentageChoices = array();
        $choices = array();
        for ($i = 5; $i <= 100; $i += 5) {
            $choices["$i%"] = "$i%";
        }
        foreach ($choices as $value => $option) {
            array_push($this->percentageChoices, new SelectChoice($value, $option));
        }

        $this->levelOfAchievementChoices = array();
        $choices = array(
            "Robust" => "Robust",
            "Commitment" => "Commitment",
            "Strength" => "Strength",
        );
        foreach ($choices as $value => $option) {
            array_push($this->levelOfAchievementChoices, new SelectChoice($value, $option));
        }

        $businessProcessList = $this->BusinessProcessModel->findAll();

        $this->processNameChoices = array();
        foreach ($businessProcessList as $item) {
            array_push($this->processNameChoices, new SelectChoice($item->process_name, $item->process_name));
        }

        $this->objectiveChoices = array();
        foreach ($businessProcessList as $item) {
            array_push($this->objectiveChoices, new SelectChoice($item->measurement, $item->measurement));
        }

        // Dept Location
        $selectChoice = new DependentSelectChoice();
        $values = array();
        foreach ($businessProcessList as $item) {
            $values[$item->process_name] = array($item->dept_location => $item->dept_location);
        }
        $selectChoice->setDependentToValueOf("process_name");
        $selectChoice->dependantChoices = $values;
        $this->deptLocationChoices = $selectChoice;
    }

}
