<?php namespace App\FormObjects\management_review;

use App\FormEntities\FormCol;
use App\FormEntities\FormColType;
use App\FormEntities\FormData;
use App\FormEntities\FormRow;
use App\FormEntities\FormRowType;
use App\FormEntities\SelectChoice;

trait Management_review_result_form
{
    private $approveAuthorChoices;

    public function getFormObject()
    {
        $this->initCreateEditDependencies();
        $formArrayObject = array();

        array_push($formArrayObject, new FormRow(FormRowType::TWO_SPLIT, null,
            (new FormCol())
                ->setFormColType(FormColType::FORM_INPUT_DATE)
                ->setFormName("actual_management_review")
                ->setFormLabel(lang("management_review.actual_management_review")),
            (new FormCol())
                ->setFormColType(FormColType::FORM_SELECT)
                ->setFormName("approve_author")
                ->setFormLabel(lang("management_review.approve_author"))
                ->setFormData($this->approveAuthorChoices)
        ));

        array_push($formArrayObject, new FormRow(FormRowType::TWO_SPLIT, null,
            (new FormCol())
                ->setFormColType(FormColType::FORM_INPUT)
                ->setFormName("attendee")
                ->setFormLabel(lang("management_review.attendee")),
            new FormCol()
        ));

        $formRow = new FormRow(FormRowType::ADDABLE, lang("management_review.create_edit_agenda"),
            (new FormCol())
                ->setFormColType(FormColType::FORM_TEXTAREA)
                ->setFormName("action_required")
                ->setFormLabel(lang("management_review.action_required")),
            (new FormCol())
                ->setFormColType(FormColType::FORM_INPUT)
                ->setFormName("responsibility")
                ->setFormLabel(lang("management_review.responsibility")),
            (new FormCol())
                ->setFormColType(FormColType::FORM_INPUT)
                ->setFormName("target_date")
                ->setFormLabel(lang("management_review.target_date")),
            (new FormCol())
                ->setFormColType(FormColType::FORM_INPUT)
                ->setFormName("actual_date")
                ->setFormLabel(lang("management_review.actual_date"))
        );
        $formRow->form_data = new FormData("internal_audit_management_review_result_items");
        array_push($formArrayObject, $formRow);

        return $formArrayObject;
    }

    public function initCreateEditDependencies()
    {
//        helper("select_option/monitoring_frequency_list");
//
//        $this->monitoringFrequencyChoices = array();
//        $choices = get_monitoring_frequency_list();
//
//        foreach ($choices as $value => $option) {
//            array_push($this->monitoringFrequencyChoices, new SelectChoice($value, $option));
//        }

        $this->approveAuthorChoices = array();
        $rows = $this->EmployeeModel->findAll();
        foreach ($rows as $item) {
            $value = $item->id;
            $option = $item->first_name . " " . $item->last_name;
            array_push($this->approveAuthorChoices, new SelectChoice($value, $option));
        }
    }

}
