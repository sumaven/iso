<?php namespace App\FormObjects\context_organization;

use App\FormEntities\FormCol;
use App\FormEntities\FormColType;
use App\FormEntities\FormRow;
use App\FormEntities\FormRowType;
use App\FormEntities\SelectChoice;

trait Internal_issue_form
{
    private $issueTypeChoices;
    private $approveAuthorChoices;
    private $riskOpportunityChoices;

    private $riskImpactChoices;
    private $riskPossibilityChoices;
    private $riskCategoryChoices;

    public function getFormObject()
    {
        $this->initCreateEditDependencies();
        $formArrayObject = array();

        array_push($formArrayObject, new FormRow(FormRowType::TWO_SPLIT, null,
            array(
                FormCol::createInstance(
                    FormColType::FORM_SELECT,
                    lang("context_organization.issue_type"),
                    "issue_type",
                    $this->issueTypeChoices
                ),
                FormCol::createInstance(
                    FormColType::FORM_SELECT,
                    lang("context_organization.risk_opportunity"),
                    "risk_opportunity",
                    $this->riskOpportunityChoices
                ),
                FormCol::createInstance(
                    FormColType::FORM_TEXTAREA,
                    lang("context_organization.how_affect"),
                    "how_affect"),
            ),
            array(

                FormCol::createInstance(
                    FormColType::FORM_TEXTAREA,
                    lang("context_organization.action_plan"),
                    "action_plan"),
                FormCol::createInstance(
                    FormColType::FORM_SELECT,
                    lang("context_organization.approve_author"),
                    "approve_author",
                    $this->approveAuthorChoices
                ),
            )
        ));

//        array_push($formArrayObject, new FormRow(FormRowType::TWO_SPLIT, "Create Risk",
//            FormCol::createInstance(
//                FormColType::FORM_TEXTAREA,
//                lang("context_organization.risk_description"),
//                "risk_description"),
//            array(
//                FormCol::createInstance(
//                    FormColType::FORM_SELECT,
//                    lang("context_organization.risk_possibility"),
//                    "risk_possibility",
//                    $this->riskPossibilityChoices),
//                FormCol::createInstance(
//                    FormColType::FORM_SELECT,
//                    lang("context_organization.risk_impact"),
//                    "risk_impact",
//                    $this->riskImpactChoices),
//                FormCol::createInstance(
//                    FormColType::FORM_SELECT,
//                    lang("context_organization.risk_category"),
//                    "risk_category",
//                    $this->riskCategoryChoices)
//            )
//        ));

        return $formArrayObject;
    }

    public function initCreateEditDependencies()
    {
        $this->issueTypeChoices = array();
        $issueTypes = $this->IssueTypeModel->where("scope", "internal")->findAll();
        foreach ($issueTypes as $item) {
            $value = $item->id;
            $option = $item->issue_type;
            array_push($this->issueTypeChoices, new SelectChoice($value, $option));
        }

        $this->riskOpportunityChoices = array();
        $values = ["Risk", "Opportunity"];
        foreach ($values as $item) {
            array_push($this->riskOpportunityChoices, new SelectChoice($item, $item));
        }

        $this->approveAuthorChoices = array();
        $rows = $this->EmployeeModel->findAll();
        foreach ($rows as $item) {
            $value = $item->id;
            $option = $item->first_name . " " . $item->last_name;
            array_push($this->approveAuthorChoices, new SelectChoice($value, $option));
        }

        $this->riskImpactChoices = array();
        $values = ["Low", "Medium", "High"];
        foreach ($values as $item) {
            array_push($this->riskImpactChoices, new SelectChoice($item, $item));
        }

        $this->riskPossibilityChoices = $this->riskImpactChoices;

        $this->riskCategoryChoices = array();
        $riskCategories = $this->RiskOpportunityCategoryModel->findAll();
        foreach ($riskCategories as $item) {
            $value = $item->id;
            $option = $item->risk_category;
            array_push($this->riskCategoryChoices, new SelectChoice($value, $option));
        }
    }

}