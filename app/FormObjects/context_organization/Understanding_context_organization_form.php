<?php namespace App\FormObjects\context_organization;

use App\FormEntities\FormCol;
use App\FormEntities\FormColType;
use App\FormEntities\FormRow;
use App\FormEntities\FormRowType;
use App\FormEntities\SelectChoice;

trait Understanding_context_organization_form
{
    private $approveAuthorChoices;

    public function getFormObject()
    {
        $this->initCreateEditDependencies();
        $formArrayObject = array();

        array_push($formArrayObject, new FormRow(FormRowType::TWO_SPLIT, null,
            FormCol::createInstance(
                FormColType::FORM_TEXTAREA,
                lang("context_organization.goals_vision_mission"),
                "goals_vision_mission"),
            FormCol::createInstance(
                FormColType::FORM_SELECT,
                lang("context_organization.approve_author"),
                "approve_author",
                $this->approveAuthorChoices
            )
        ));
        array_push($formArrayObject, new FormRow(FormRowType::TWO_SPLIT, null,
            FormCol::createInstance(
                FormColType::FORM_TEXTAREA,
                lang("context_organization.product_service"),
                "product_service"),
            FormCol::createInstance()
        ));

        array_push($formArrayObject, new FormRow(FormRowType::TWO_SPLIT, "Swot Analysis",
            FormCol::createInstance(
                FormColType::FORM_TEXTAREA,
                lang("context_organization.swot_strength"),
                "swot_strength"),
            FormCol::createInstance(
                FormColType::FORM_TEXTAREA,
                lang("context_organization.swot_opportunity"),
                "swot_opportunity")
        ));
        array_push($formArrayObject, new FormRow(FormRowType::TWO_SPLIT, null,
            FormCol::createInstance(
                FormColType::FORM_TEXTAREA,
                lang("context_organization.swot_weakness"),
                "swot_weakness"),
            FormCol::createInstance(
                FormColType::FORM_TEXTAREA,
                lang("context_organization.swot_threat"),
                "swot_threat")
        ));

        return $formArrayObject;
    }

    public function initCreateEditDependencies()
    {
        $this->approveAuthorChoices = array();
        $rows = $this->EmployeeModel->findAll();
        foreach ($rows as $item) {
            $value = $item->id;
            $option = $item->first_name . " " . $item->last_name;
            array_push($this->approveAuthorChoices, new SelectChoice($value, $option));
        }
    }

}
