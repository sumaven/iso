<?php namespace App\FormObjects\documented_information;

use App\FormEntities\FormCol;
use App\FormEntities\FormColType;
use App\FormEntities\FormRow;
use App\FormEntities\FormRowType;
use App\FormEntities\SelectChoice;

trait Documents_form
{

    private $managementSystemTypeChoices;
    private $departmentChoices;
    private $approveAuthorChoices;
    private $documentTypeChoices;
    private $documentCategoryChoices;

    public function getFormObject()
    {
        $this->initCreateEditDependencies();
        $formArrayObject = array();

        array_push($formArrayObject, new FormRow(FormRowType::TWO_SPLIT, null,
            (new FormCol())
                ->setFormColType(FormColType::FORM_SELECT)
                ->setFormLabel(lang("documented_information.management_system_type"))
                ->setFormName("management_system_type")
                ->setFormData($this->managementSystemTypeChoices),
            (new FormCol())
                ->setFormColType(FormColType::FORM_SELECT)
                ->setFormLabel(lang("documented_information.department"))
                ->setFormName("department")
                ->setFormData($this->departmentChoices)
        ));

        array_push($formArrayObject, new FormRow(FormRowType::TWO_SPLIT, null,
            (new FormCol())
                ->setFormColType(FormColType::FORM_SELECT)
                ->setFormLabel(lang("documented_information.approve_author"))
                ->setFormName("approve_author")
                ->setFormData($this->approveAuthorChoices),
            (new FormCol())
                ->setFormColType(FormColType::FORM_SELECT)
                ->setFormLabel(lang("documented_information.document_type"))
                ->setFormName("document_type")
                ->setFormData($this->documentTypeChoices)
        ));

        array_push($formArrayObject, new FormRow(FormRowType::TWO_SPLIT, null,
            (new FormCol())
                ->setFormColType(FormColType::FORM_SELECT)
                ->setFormLabel(lang("documented_information.document_category"))
                ->setFormName("document_category")
                ->setFormData($this->documentCategoryChoices)
        ));

        array_push($formArrayObject, new FormRow(FormRowType::ONE_SOLO, null,
            (new FormCol())
                ->setFormColType(FormColType::FORM_INPUT)
                ->setFormLabel(lang("documented_information.document_name"))
                ->setFormName("document_name")
        ));

        array_push($formArrayObject, new FormRow(FormRowType::ONE_SOLO, null,
            (new FormCol())
                ->setFormColType(FormColType::FORM_INPUT)
                ->setFormLabel(lang("documented_information.revision_number"))
                ->setFormName("revision_number")
        ));

        array_push($formArrayObject, new FormRow(FormRowType::ONE_SOLO, null,
            (new FormCol())
                ->setFormColType(FormColType::FORM_INPUT_DATE)
                ->setFormLabel(lang("documented_information.next_revision_date"))
                ->setFormName("next_revision_date")
        ));

        array_push($formArrayObject, new FormRow(FormRowType::ONE_SOLO, null,
            (new FormCol())
                ->setFormColType(FormColType::FORM_TEXTAREA)
                ->setFormLabel(lang("documented_information.document"))
                ->setFormName("document")
        ));

        return $formArrayObject;
    }

    public function initCreateEditDependencies()
    {
        helper("select_option/document_type_list");
        helper("select_option/document_category_list");

        $this->documentTypeChoices = array();
        $choices = get_document_type_list();
        foreach ($choices as $value => $option) {
            array_push($this->documentTypeChoices, new SelectChoice($value, $option));
        }

        $this->documentCategoryChoices = array();
        $choices = get_document_category_list();
        foreach ($choices as $value => $option) {
            array_push($this->documentCategoryChoices, new SelectChoice($value, $option));
        }

        // Management System Type Choice
        $this->managementSystemTypeChoices = array();
        $rows = $this->ManagementSystemTypeModel->findAll();
        foreach ($rows as $item) {
            $value = $item->id;
            $option = $item->management_system_short_name;
            array_push($this->managementSystemTypeChoices, new SelectChoice($value, $option));
        }

        // Department Choice
        $this->departmentChoices = array();
        $rows = $this->DepartmentModel->findAll();
        foreach ($rows as $item) {
            $value = $item->id;
            $option = $item->department_name;
            array_push($this->departmentChoices, new SelectChoice($value, $option));
        }

        // Approve Author Choice
        $this->approveAuthorChoices = array();
        $rows = $this->EmployeeModel->findAll();
        foreach ($rows as $item) {
            $value = $item->id;
            $option = $item->first_name . " " . $item->last_name;
            array_push($this->approveAuthorChoices, new SelectChoice($value, $option));
        }
    }

}
