<?php namespace App\FormObjects\policy_objective;

use App\FormEntities\FormCol;
use App\FormEntities\FormColType;
use App\FormEntities\FormRow;
use App\FormEntities\FormRowType;
use App\FormEntities\SelectChoice;

trait Objective_form
{
    private $managementSystemTypeChoices;
    private $departmentChoices;
    private $approveAuthorChoices;
    private $monitoringFrequencyChoices;

    public function getFormObject()
    {
        $this->initCreateEditDependencies();
        $formArrayObject = array();

        array_push($formArrayObject, new FormRow(FormRowType::TWO_SPLIT, null,
            (new FormCol())
                ->setFormColType(FormColType::FORM_SELECT)
                ->setFormLabel(lang("policy_objective.management_system_type"))
                ->setFormName("management_system_type")
                ->setFormData($this->managementSystemTypeChoices),
            (new FormCol())
                ->setFormColType(FormColType::FORM_SELECT)
                ->setFormLabel(lang("policy_objective.department"))
                ->setFormName("department")
                ->setFormData($this->departmentChoices)
        ));

        array_push($formArrayObject, new FormRow(FormRowType::ONE_SOLO, null,
            (new FormCol())
                ->setFormColType(FormColType::FORM_INPUT_DATE)
                ->setFormLabel(lang("policy_objective.objective_start_date"))
                ->setFormName("objective_start_date"),
            (new FormCol())
                ->setFormColType(FormColType::FORM_INPUT_DATE)
                ->setFormLabel(lang("policy_objective.objective_end_date"))
                ->setFormName("objective_end_date")
        ));

        array_push($formArrayObject, new FormRow(FormRowType::ONE_SOLO, null,
            (new FormCol())
                ->setFormColType(FormColType::FORM_SELECT)
                ->setFormLabel(lang("policy_objective.approve_author"))
                ->setFormName("approve_author")
                ->setFormData($this->approveAuthorChoices),
            (new FormCol())
                ->setFormColType(FormColType::FORM_INPUT_DATE)
                ->setFormLabel(lang("policy_objective.monitoring_frequency"))
                ->setFormName("monitoring_frequency")
                ->setFormData($this->monitoringFrequencyChoices)
        ));

        array_push($formArrayObject, new FormRow(FormRowType::ONE_SOLO, null,
            (new FormCol())
                ->setFormColType(FormColType::FORM_TEXTAREA)
                ->setFormLabel(lang("policy_objective.monitoring_methodology"))
                ->setFormName("monitoring_methodology")
        ));

        array_push($formArrayObject, new FormRow(FormRowType::ONE_SOLO, null,
            (new FormCol())
                ->setFormColType(FormColType::FORM_TEXTAREA)
                ->setFormLabel(lang("policy_objective.objective"))
                ->setFormName("objective")
        ));

        array_push($formArrayObject, new FormRow(FormRowType::ONE_SOLO, null,
            (new FormCol())
                ->setFormColType(FormColType::FORM_TEXTAREA)
                ->setFormLabel(lang("policy_objective.remarks"))
                ->setFormName("remarks")
        ));

        return $formArrayObject;
    }

    public function initCreateEditDependencies()
    {
        helper("select_option/monitoring_frequency_list");

        $this->monitoringFrequencyChoices = array();
        $choices = get_monitoring_frequency_list();
        foreach ($choices as $value => $option) {
            array_push($this->monitoringFrequencyChoices, new SelectChoice($value, $option));
        }

        // Management System Type Choice
        $this->managementSystemTypeChoices = array();
        $rows = $this->ManagementSystemTypeModel->findAll();
        foreach ($rows as $item) {
            $value = $item->id;
            $option = $item->management_system_short_name;
            array_push($this->managementSystemTypeChoices, new SelectChoice($value, $option));
        }

        // Department Choice
        $this->departmentChoices = array();
        $rows = $this->DepartmentModel->findAll();
        foreach ($rows as $item) {
            $value = $item->id;
            $option = $item->department_name;
            array_push($this->departmentChoices, new SelectChoice($value, $option));
        }

        // Approve Author Choice
        $this->approveAuthorChoices = array();
        $rows = $this->EmployeeModel->findAll();
        foreach ($rows as $item) {
            $value = $item->id;
            $option = $item->first_name . " " . $item->last_name;
            array_push($this->approveAuthorChoices, new SelectChoice($value, $option));
        }
    }

}
