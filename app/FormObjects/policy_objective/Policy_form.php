<?php namespace App\FormObjects\policy_objective;

use App\FormEntities\FormCol;
use App\FormEntities\FormColType;
use App\FormEntities\FormRow;
use App\FormEntities\FormRowType;
use App\FormEntities\SelectChoice;

trait Policy_form
{
    private $managementSystemTypeChoices;
    private $approveAuthorChoices;

    public function getFormObject()
    {
        $this->initCreateEditDependencies();
        $formArrayObject = array();

        array_push($formArrayObject, new FormRow(FormRowType::TWO_SPLIT, null,
            (new FormCol())
                ->setFormColType(FormColType::FORM_SELECT)
                ->setFormLabel(lang("policy_objective.management_system_type"))
                ->setFormName("management_system_type")
                ->setFormData($this->managementSystemTypeChoices),
            (new FormCol())
                ->setFormColType(FormColType::FORM_SELECT)
                ->setFormLabel(lang("policy_objective.approve_author"))
                ->setFormName("approve_author")
                ->setFormData($this->approveAuthorChoices)
        ));

        array_push($formArrayObject, new FormRow(FormRowType::ONE_SOLO, null,
            (new FormCol())
                ->setFormColType(FormColType::FORM_TEXTAREA)
                ->setFormLabel(lang("policy_objective.policy_statement"))
                ->setFormName("policy_statement")
        ));

        array_push($formArrayObject, new FormRow(FormRowType::ONE_SOLO, null,
            (new FormCol())
                ->setFormColType(FormColType::FORM_TEXTAREA)
                ->setFormLabel(lang("policy_objective.remarks"))
                ->setFormName("remarks")
        ));

        return $formArrayObject;
    }

    public function initCreateEditDependencies()
    {

        // Management System Type Choice
        $this->managementSystemTypeChoices = array();
        $rows = $this->ManagementSystemTypeModel->findAll();
        foreach ($rows as $item) {
            $value = $item->id;
            $option = $item->management_system_short_name;
            array_push($this->managementSystemTypeChoices, new SelectChoice($value, $option));
        }

        // Approve Author Choice
        $this->approveAuthorChoices = array();
        $rows = $this->EmployeeModel->findAll();
        foreach ($rows as $item) {
            $value = $item->id;
            $option = $item->first_name . " " . $item->last_name;
            array_push($this->approveAuthorChoices, new SelectChoice($value, $option));
        }
    }

}
