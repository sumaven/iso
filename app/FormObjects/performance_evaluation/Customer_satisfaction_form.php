<?php namespace App\FormObjects\performance_evaluation;

use App\FormEntities\FormCol;
use App\FormEntities\FormColType;
use App\FormEntities\FormRow;
use App\FormEntities\FormRowType;
use App\FormEntities\SelectChoice;

trait Customer_satisfaction_form
{
    private $tenRangeChoices;

    public function getFormObject()
    {
        $this->initCreateEditDependencies();
        $formArrayObject = array();

        array_push($formArrayObject, new FormRow(FormRowType::TWO_SPLIT, NULL,
            array(
                (new FormCol())
                    ->setFormColType(FormColType::FORM_INPUT)
                    ->setFormLabel(lang("performance_evaluation.nama"))
                    ->setFormName("nama"),
                (new FormCol())
                    ->setFormColType(FormColType::FORM_INPUT)
                    ->setFormLabel(lang("performance_evaluation.perusahaan"))
                    ->setFormName("perusahaan")
            ),
            array(
                (new FormCol())
                    ->setFormColType(FormColType::FORM_INPUT)
                    ->setFormLabel(lang("performance_evaluation.email"))
                    ->setFormName("email"),
                (new FormCol())
                    ->setFormColType(FormColType::FORM_INPUT)
                    ->setFormLabel(lang("performance_evaluation.no_telp"))
                    ->setFormName("no_telp"),
            )
        ));

        array_push($formArrayObject, new FormRow(FormRowType::TWO_SPLIT, "Satisfaction Rating",
            array(
                (new FormCol())
                    ->setFormColType(FormColType::FORM_SELECT)
                    ->setFormLabel(lang("performance_evaluation.komunikasi"))
                    ->setFormName("komunikasi")
                    ->setFormData($this->tenRangeChoices),
                (new FormCol())
                    ->setFormColType(FormColType::FORM_SELECT)
                    ->setFormLabel(lang("performance_evaluation.ketepatan_waktu"))
                    ->setFormName("ketepatan_waktu")
                    ->setFormData($this->tenRangeChoices),
                (new FormCol())
                    ->setFormColType(FormColType::FORM_SELECT)
                    ->setFormLabel(lang("performance_evaluation.kesesuaian_produk"))
                    ->setFormName("kesesuaian_produk")
                    ->setFormData($this->tenRangeChoices),
            ),
            array(
                (new FormCol())
                    ->setFormColType(FormColType::FORM_SELECT)
                    ->setFormLabel(lang("performance_evaluation.prosedur_pelayanan"))
                    ->setFormName("prosedur_pelayanan")
                    ->setFormData($this->tenRangeChoices),
                (new FormCol())
                    ->setFormColType(FormColType::FORM_SELECT)
                    ->setFormLabel(lang("performance_evaluation.kompetensi"))
                    ->setFormName("kompetensi")
                    ->setFormData($this->tenRangeChoices),
            )
        ));

        array_push($formArrayObject, new FormRow(FormRowType::ONE_SOLO, " ",
            (new FormCol())
                ->setFormColType(FormColType::FORM_TEXTAREA)
                ->setFormLabel(lang("performance_evaluation.respon_tambahan"))
                ->setFormName("respon_tambahan")
        ));

        return $formArrayObject;
    }

    public function initCreateEditDependencies()
    {
        helper("select_option/ten_range_list");

        $this->tenRangeChoices = array();
        $choices = get_ten_range_list();
        foreach ($choices as $value => $option) {
            array_push($this->tenRangeChoices, new SelectChoice($value, $option));
        }
    }

}
