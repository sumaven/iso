<?php

if (! function_exists('admin_check')) {
    function admin_check($argVal = '')
    {
        $session = session();

        if (($session->get('logged_in') != true) || ($session->get('email') != "admin")) {
            return false;
        } else {
            return true;
        }
    }
}

if (! function_exists('admin_access_check')) {
    function admin_access_check($argVal = '')
    {
        $session = session();

        if (($session->get('logged_in') != true) || ($session->get('email') != "admin")) {
            header('HTTP/1.1 403 Your access is denied');
            header('Status: 403 Your access is denied');
            header('Retry-After: 300');
            echo "Your access is denied, this is for admin only";

            header("refresh:2; url=".base_url());

//            if (!empty($_SERVER['HTTP_REFERER'])) {
//                header("refresh:2; url=".$_SERVER['HTTP_REFERER']);
//            } else {
//
//            }
            return false;
        } else {
            return true;
        }
    }
}


if (! function_exists('user_access_check')) {
    function user_access_check($argVal = '')
    {
        $session = session();

        if (($session->get('logged_in') != true)) {
            header('HTTP/1.1 403 Your access is denied');
            header('Status: 403 Your access is denied');
            header('Retry-After: 300');
            echo "Your access is denied, this is for logged-in user only";

            header("refresh:2; url=".base_url());

//            if (!empty($_SERVER['HTTP_REFERER'])) {
//                header("refresh:2; url=".$_SERVER['HTTP_REFERER']);
//            } else {
//                header("refresh:2; url=".base_url());
//            }
            return false;
        } else {
            return true;
        }
    }
}
