<?php

if (! function_exists('get_management_system_type_val')) {
    function get_management_system_type_val($id = '')
    {
        if (!is_numeric($id) || empty($id)) {
            return "";
        }
        $db = db_connect();
        $row = $db->query("select * from admin_management_system_type where id=" . $id)->getRow();

        return !$row? "null" : $row->management_system_short_name;
    }
}

if (! function_exists('get_issue_type_val')) {
    function get_issue_type_val($id = '')
    {
        if (!is_numeric($id) || empty($id)) {
            return "";
        }
        $db = db_connect();
        $row = $db->query("select * from context_issue_type where id=" . $id)->getRow();

        return !$row? "null" : $row->issue_type;
    }
}

if (! function_exists('get_department_val')) {
    function get_department_val($id = '')
    {
        if (!is_numeric($id) || empty($id)) {
            return "";
        }
        $db = db_connect();
        $row = $db->query("select * from hr_department where id=" . $id)->getRow();

        return !$row? "null" : $row->department_name;
    }
}

if (! function_exists('get_jab_val')) {
    function get_jab_val($id = '')
    {
        if (!is_numeric($id) || empty($id)) {
            return "";
        }
        $db = db_connect();
        $row = $db->query("select * from approval where id=" . $id)->getRow();

        return !$row? "null" : $row->approval;
    }
}

if (! function_exists('get_site_val')) {
    function get_site_val($id = '')
    {
        if (!is_numeric($id) || empty($id)) {
            return "";
        }
        $db = db_connect();
        $row = $db->query("select * from admin_site where id=" . $id)->getRow();

        return !$row? "null" : $row->site_name;
    }
}

if (! function_exists('get_designation_val')) {
    function get_designation_val($id = '')
    {
        if (!is_numeric($id) || empty($id)) {
            return "";
        }
        $db = db_connect();
        $row = $db->query("select * from hr_designation where id=" . $id)->getRow();

        return !$row? "null" : $row->designation_name;
    }
}

if (! function_exists('get_approve_author_val')) {
	function get_approve_author_val($id = '')
    {
        if (!is_numeric($id) || empty($id)) {
            return "";
        }
        $db = db_connect();
        $row = $db->query("select * from hr_employee where id=" . $id)->getRow();

        return !$row? "null" : $row->first_name . " " . $row->last_name;
    }
}

if (! function_exists('get_mgr')) {
    function get_mgr($id = '')
    {
        if (!is_numeric($id) || empty($id)) {
            return "";
        }
        $db = db_connect();
        $row = $db->query("select * from hr_employee where id=" . $id)->getRow();

        return !$row? "null" : $row->jabatan;
    }
}

if (! function_exists('getAppVp')) {
    function getAppVp($id = '')
    {
        if (!is_numeric($id) || empty($id)) {
            return "";
        }
        $db = db_connect();
        $row = $db->query("select * from hr_employee where id=" . $id)->getRow();

        $mgr = $db->query("SELECT id from hr_employee where jabatan = 3 and department = ".$row->jabatan)->row();
        return !$mgr? "" : $mgr->id;
    }
}

if (! function_exists('getAppMgr')) {
    function getAppMgr($id = '')
    {
        if (!is_numeric($id) || empty($id)) {
            return "";
        }
        $db = db_connect();
        $row = $db->query("select * from hr_employee where id=" . $id)->getRow();

        $mgr = $db->query("SELECT id from hr_employee where jabatan = 2 and department = ".$row->jabatan)->row();
        return !$mgr? "" : $mgr->id;
    }
}

if (! function_exists('getAppMr')) {
    function getAppMr($id = '')
    {
        if (!is_numeric($id) || empty($id)) {
            return "";
        }
        $db = db_connect();
        $row = $db->query("select * from hr_employee where id=" . $id)->getRow();

        $mgr = $db->query("SELECT id from hr_employee where jabatan = 1 and department = ".$row->jabatan)->row();
        return !$mgr? "" : $mgr->id;
    }
}


if (! function_exists('get_risk_opportunity_type_val')) {
    function get_risk_opportunity_type_val($id = '')
    {
        if (!is_numeric($id) || empty($id)) {
            return "";
        }
        $db = db_connect();
        $row = $db->query("select * from risk_opportunity_type where id=" . $id)->getRow();

        return !$row? "null" : $row->risk_type;
    }
}

if (! function_exists('get_risk_category_val')) {
    function get_risk_category_val($id = '')
    {
        if (!is_numeric($id) || empty($id)) {
            return "";
        }
        $db = db_connect();
        $row = $db->query("select * from risk_opportunity_category where id=" . $id)->getRow();

        return !$row? "null" : $row->risk_category;
    }
}

if (! function_exists('get_distribusi_val')) {
    function get_distribusi_val($id = '')
    {
        if (!is_numeric($id) || empty($id)) {
            return "";
        }
        $db = db_connect();
        $row = $db->query("select * from distribusi where id=" . $id)->getRow();

        return !$row? "null" : $row->distribusi;
    }
}

if (! function_exists('get_approval_val')) {
    function get_approval_val($id = '')
    {
        if (!is_numeric($id) || empty($id)) {
            return "";
        }
        $db = db_connect();
        $row = $db->query("select * from approval where id=" . $id)->getRow();

        return !$row? "null" : $row->approval;
    }
}

if (! function_exists('get_regulation_title_val')) {
    function get_regulation_title_val($id = '')
    {
        if (!is_numeric($id) || empty($id)) {
            return "";
        }
        $db = db_connect();
        $row = $db->query("select regulation_title from planning_regulation_register where id=" . $id)->getRow();

        return !$row? "null" : $row->regulation_title;
    }
}

if (! function_exists('get_referensi_val')) {
    function get_referensi_val($id = '')
    {
        if (!is_numeric($id) || empty($id)) {
            return "";
        }
        $db = db_connect();
        $row = $db->query("select * from referensi where id=" . $id)->getRow();

        return !$row? "null" : $row->referensi;
    }
}

if (! function_exists('get_doc_val')) {
    function get_doc_val($id = '')
    {
        if (!is_numeric($id) || empty($id)) {
            return "";
        }
        $db = db_connect();
        $row = $db->query("select * from documented_external where id=" . $id)->getRow();

        return !$row? "null" : $row->no_dokumen;
    }
}

if (! function_exists('get_ttd_val')) {
    function get_ttd_val($id = '')
    {
        if (!is_numeric($id) || empty($id)) {
            return "";
        }
        $db = db_connect();
        $row = $db->query("SELECT * FROM hr_department hd JOIN hr_employee he ON hd.id = he.department WHERE hd.id ='$id'")->row();
        return !$row? "null" : $row->url;
    }
}

if (! function_exists('get_rule_type_val')) {
    function get_rule_type_val($id = '')
    {
        if (!is_numeric($id) || empty($id)) {
            return "";
        }
        $db = db_connect();
        $row = $db->query("SELECT * FROM rule_type WHERE id ='$id'")->getRow();
        return !$row? "null" : $row->name;
    }
}

if (! function_exists('get_equipment_val')) {
    function get_equipment_val($id = '')
    {
        if (!is_numeric($id) || empty($id)) {
            return "";
        }
        $db = db_connect();
        $row = $db->query("SELECT * FROM operation_equipment WHERE id ='$id'")->getRow();
        return !$row? "null" : $row->equipment_name;
    }
}