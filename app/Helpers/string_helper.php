<?php

if (!function_exists('clean_string')) {
    function clean_string($string = "")
    {
        $str = utf8_decode($string);
        $str = strip_tags(html_entity_decode($str));
        $str = str_replace("&nbsp;", "", $str);
        $str = preg_replace('/\s+/', ' ', $str);
        $str = trim($str);
        return $str;
    }
}
