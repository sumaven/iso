<?php

if ( ! function_exists('get_dependent_qhse_issue_category_list'))
{
    function get_dependent_qhse_issue_category_list()
    {
        // "value" => "option"
        return array(
            "Q" => array(
                "Human Resources" => "Human Resources",
                "Financial" => "Financial",
                "Operational Performance" => "Operational Performance",
                "Contractor / Supplier Performance" => "Contractor / Supplier Performance",
                "Infrastructure" => "Infrastructure",
                "Psychosocial Hazard" => "Psychosocial Hazard",
                "Internal Process" => "Internal Process",
                "Political" => "Political",
                "Economic Condition" => "Economic Condition",
                "Social Condition" => "Social Condition",
                "Technology Aspect" => "Technology Aspect",
                "Regulation Change" => "Regulation Change",
            ),
            "E" => array(
                "Noise Pollution" => "Noise Pollution",
                "Increased Production Capacity" => "Increased Production Capacity",
                "Natural Resources" => "Natural Resources",
                "Flora & Fauna" => "Flora & Fauna",
                "Humans and Their Relationships" => "Humans and Their Relationships",
                "Hazardous & Toxic Material" => "Hazardous & Toxic Material",
                "Regulation Change" => "Regulation Change",
                "Technology Change" => "Technology Change",
            ),
            "HS" => array(
                "Working Environment Hazard (Physical & Chemical)" => "Working Environment Hazard (Physical & Chemical)",
                "Ergonomics Hazard" => "Ergonomics Hazard",
                "Somatic Hazard" => "Somatic Hazard",
                "Mechanical Hazard" => "Mechanical Hazard",
                "Biological Hazards (Micro & Macro)" => "Biological Hazards (Micro & Macro)",
                "Psychosocial Hazard" => "Psychosocial Hazard",
                "Hazardous & Toxic Material Hazard" => "Hazardous & Toxic Material Hazard",
                "Human Factor" => "Human Factor",
                "Infrastructure & Working Equipment" => "Infrastructure & Working Equipment",
                "Regulations Change" => "Regulations Change",
                "Social and Cultural" => "Social and Cultural",
                "Weather" => "Weather",
            )
        );
    }
}