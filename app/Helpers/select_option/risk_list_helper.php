<?php

if ( ! function_exists('get_risk_list'))
{
    function get_risk_list()
    {
        // "value" => "option"
        return array(
            "Low" => "Low",
            "Medium" => "Medium",
            "High" => "High",
        );
    }
}