<?php

if ( ! function_exists('get_type_list'))
{
    function get_type_list()
    {
        // "value" => "option"
        return array(
            "NS" => "National Safety Regulation",
            "NH" => "National Health Regulation",
            "NE" => "National Environment Regulation",
            "NG" => "National Government Regulation",
            "LS" => "Local Safety Regulation",
            "LH" => "Local Health Regulation",
            "LE" => "Local Environment Regulation",
            "LG" => "Local Government Regulation",
            "CR" => "Costumer Regulation",
            "OTH" => "Other: ISO, ANSI, ANZ, OSHA, ISPO, RSPO, PROPER, SNI, etc",
        );
    }
}