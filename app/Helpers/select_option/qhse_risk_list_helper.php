<?php

if ( ! function_exists('get_qhse_risk_list'))
{
    function get_qhse_risk_list()
    {
        // "value" => "option"
        return array(
            "Q" => "Q",
            "HS" => "HS",
            "E" => "E"
        );
    }
}