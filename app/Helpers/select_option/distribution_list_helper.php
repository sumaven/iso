<?php

if ( ! function_exists('get_distribution_list'))
{
    function get_distribution_list()
    {
        // "value" => "option"
        return array(
            "Marketing" => "Marketing",
            "Operation" => "Operation",
            "HR" => "HR",
            "Workshop" => "Workshop",
            "Warehouse" => "Warehouse",
            "Finance" => "Finance",
            "QC" => "QC",
            "Laboratorium" => "Laboratorium",
            "QHSE" => "QHSE",
            "Documented Information" => "Documented Information",
            "Other" => "Other",
        );
    }
}