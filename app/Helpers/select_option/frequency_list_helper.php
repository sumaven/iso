<?php

if ( ! function_exists('get_frequency_list'))
{
    function get_frequency_list()
    {
        // "value" => "option"
        return array(
            "Daily" => "Daily",
            "Fortnightly" => "Fortnightly",
            "Half-Yearly" => "Half-Yearly",
            "Monthly" => "Monthly",
            "Quarterly" => "Quarterly",
            "Weekly" => "Weekly",
            "Yearly" => "Yearly"
        );
    }
}