<?php

if ( ! function_exists('get_document_category_list'))
{
    function get_document_category_list()
    {
        // "value" => "option"
        return array(
            "Design & Development" => "Design & Development",
            "Engineering" => "Engineering",
            "Finance" => "Finance",
            "HR" => "HR",
            "IT" => "IT",
        );
    }
}