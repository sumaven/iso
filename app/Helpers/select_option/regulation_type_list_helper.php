<?php

if ( ! function_exists('get_regulation_type_list'))
{
    function get_regulation_type_list()
    {
        // "value" => "option"
        return array(
            "Undang-undang" => "Undang-undang",
            "Peraturan Pemerintah" => "Peraturan Pemerintah",
            "Keputusan Presiden" => "Keputusan Presiden",
            "Keputusan Menteri" => "Keputusan Menteri",
            "Peraturan Provinsi" => "Peraturan Provinsi",
            "Peraturan Kab/kota" => "Peraturan Kab/kota",
        );
    }
}