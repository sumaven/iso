<?php

if ( ! function_exists('get_monitoring_methodology_list'))
{
    function get_monitoring_methodology_list()
    {
        // "value" => "option"
        return array(
            "Pareto Chart" => "Pareto Chart",
            "Trend Analysis Chart" => "Trend Analysis Chart",
            "Coordination Meeting" => "Coordination Meeting",
            "Power BI Report" => "Power BI Report",
            "Performance Report" => "Performance Report",
            "etc."
        );
    }
}