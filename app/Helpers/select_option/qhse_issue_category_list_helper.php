<?php

if ( ! function_exists('get_qhse_issue_category_list'))
{
    function get_qhse_issue_category_list()
    {
        // "value" => "option"
        return array(
            "Working Environment Hazard (Physical & Chemical)" => "Working Environment Hazard (Physical & Chemical)",
            "Ergonomics Hazard" => "Ergonomics Hazard",
            "Somatic Hazard" => "Somatic Hazard",
            "Mechanical Hazard" => "Mechanical Hazard",
            "Biological Hazards (Micro & Macro)" => "Biological Hazards (Micro & Macro)",
            "Psychosocial Hazard" => "Psychosocial Hazard",
            "Hazardous & Toxic Material Hazard" => "Hazardous & Toxic Material Hazard",
            "Human Factor" => "Human Factor",
            "Infrastructure & Working Equipment" => "Infrastructure & Working Equipment",
            "Regulations Change" => "Regulations Change",
            "Social and Cultural" => "Social and Cultural",
            "Weather" => "Weather",
        );
    }
}