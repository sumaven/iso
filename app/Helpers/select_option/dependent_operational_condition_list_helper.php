<?php

if ( ! function_exists('get_dependent_operational_condition_list'))
{
    function get_dependent_operational_condition_list()
    {
        // "dependence value" => array("value" => "option", ..., ...)
        return array(
            "Q" => array("Routine" => "Routine", "Non-Routine" => "Non-Routine", "Emergency" => "Emergency"),
            "HS" => array("Routine" => "Routine", "Non-Routine" => "Non-Routine", "Emergency" => "Emergency"),
            "E" => array("Normal" => "Normal", "Abnormal" => "Abnormal", "Emergency" => "Emergency")
        );
    }
}