<?php

if ( ! function_exists('get_dependent_category_list'))
{
    function get_dependent_category_list()
    {
        // "dependence value" => array("value" => "option", ..., ...)
        return array(
            "Q" => array("Quality" => "Quality"),
            "HS" => array("Safety" => "Safety"),
            "E" => array("Environment" => "Environment")
        );
    }
}