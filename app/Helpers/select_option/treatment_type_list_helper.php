<?php

if ( ! function_exists('get_treatment_type_list'))
{
    function get_treatment_type_list()
    {
        // "value" => "option"
        return array(
            "Elimination" => "Elimination",
            "Reduction" => "Reduction",
            "Retention" => "Retention",
            "Transfer" => "Transfer",
        );
    }
}