<?php

if ( ! function_exists('get_type_of_change_list'))
{
    function get_type_of_change_list()
    {
        // "value" => "option"
        return array(
            "Document" => "Document",
            "Process" => "Process",
            "Technology" => "Technology",
            "Materials" => "Materials",
            "Tools" => "Tools",
            "Method" => "Method",
            "etc" => "etc"
        );
    }
}