<?php

if ( ! function_exists('get_reference_list'))
{
    function get_reference_list()
    {
        // "value" => "option"
        return array(
            "ISO 9001" => "ISO 9001",
            "ISO 14001" => "ISO 14001",
            "ISO 45001" => "ISO 45001",
            "ISPO" => "ISPO",
            "RSPO" => "RSPO",
            "Regulation" => "Regulation"
        );
    }
}