<?php

if ( ! function_exists('get_training_type_list'))
{
    function get_training_type_list()
    {
        // "value" => "option"
        return array(
            "Clasroom" => "Clasroom",
            "Online" => "Online",
            "On the Job" => "On the Job",
        );
    }
}