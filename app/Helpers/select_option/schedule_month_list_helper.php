<?php

if ( ! function_exists('get_schedule_month_list'))
{
    function get_schedule_month_list()
    {
        // "value" => "option"
        return array(
            "January" => "January",
            "February" => "February",
            "March" => "March",
            "April" => "April",
            "May" => "May",
            "June" => "June",
            "July" => "July",
            "August" => "August",
            "September" => "September",
            "October" => "October",
            "November" => "November",
            "December" => "December",
        );
    }
}