<?php

if (!function_exists('get_submain')) {
    function get_submain()
    {
        // "value" => "option"
        return array(
            1 => array(
                1 => "Unit sertifikasi memberikan informasi memadai kepada pemangku kepentingan terkait lingkungan, sosial dan hukum, yang relevan dengan Kriteria RSPO, dalam bahasa dan bentuk yang sesuai, sehingga partisipasi dapat dilaksanakan secara efektif dalam pengambilan keputusan.",
                2 => "Unit sertifikasi berkomitmen untuk bertindak etis pada semua operasi dan transaksi bisnis.",
            ),
            2 => array(
                1 => "Adanya kepatuhan terhadap semua hukum dan peraturan yang berlaku, baik  lokal, nasional, maupun internasional yang telah diratifikasi.",
                2 => "Semua kontraktor yang memberikan jasa operasional dan penyedia tenaga kerja, serta pemasok Tandan Buah Segar (“TBS”), mematuhi kewajiban-kewajiban hukum yang relevan.",
                3 => "Semua pasokan TBS dari luar unit sertifikasi diperoleh dari sumber yang legal.",
            ),
            3 => array(
                1 => "Adanya rencana manajemen (management plan) yang dilaksanakan untuk unit sertifikasi, yang bertujuan untuk mencapai kelayakan ekonomi dan finansial jangka panjang.",
                2 => "Unit sertifikasi memantau dan meninjau secara berkala kinerja ekonomi, sosial dan lingkungannya, serta mengembangkan dan melaksanakan rencana aksi yang disusun untuk mencapai perbaikan berkelanjutan dalam kegiatan utama yang dapat dibuktikan.",
                3 => "Prosedur operasi didokumentasikan dengan tepat, dilaksanakan dan dipantau secara konsisten.",
                4 => "Sebelum memulai penanaman atau operasi baru, dilakukan penilaian dampak sosial dan lingkungan yang menyeluruh. Rencana kelola dan pemantauan sosial dan lingkungan dilaksanakan dan dimutakhirkan secara berkala selama operasi berjalan.",
                5 => "Tersedia sistem manajemen sumber daya manusia",
                6 => "Rencana Keselamatan dan Kesehatan Kerja (“K3”) didokumentasikan, dikomunikasikan secara efektif, dan dilaksanakan.",
                7 => "Seluruh staf, pekerja, petani plasma, outgrowers, dan pekerja kontrak mendapatkan pelatihan yang memadai.",
                8 => "Persyaratan Rantai Pasok untuk PKS",
            ),
            4 => array(
                1 => "Unit sertifikasi menghormati hak asasi manusia (HAM), yang mencakup penghormatan terhadap hak-hak Pembela HAM.",
                2 => "Tersedia sistem yang disepakati bersama dan terdokumentasi untuk menangani pengaduan dan keluhan, yang dilaksanakan dan diterima oleh semua pihak terdampak.",
                3 => "Unit sertifikasi berkontribusi pada pembangunan berkelanjutan di tingkat lokal sebagaimana disepakati bersama masyarakat setempat.",
                4 => "Penggunaan lahan untuk kelapa sawit tidak mengurangi hak legal, hak adat atau hak pakai yang dimiliki pengguna-pengguna lainnya, tanpa melakukan proses Keputusan Bebas, Didahulukan, dan Diinformasikan (KBDD).",
                5 => "Tidak ada penanaman baru yang dilakukan di atas lahan masyarakat setempat tanpa - KBDD jika di atas lahan tersebut dapat dibuktikan adanya hak legal, hak adat, atau hak pakai. Hal ini dilakukan melalui sistem terdokumentasi yang dapat digunakan untuk mencapai KBDD dan dapat digunakan oleh pemangku kepentingan lain untuk menyampaikan pendapatnya melalui lembaga perwakilannya sendiri.",
                6 => "Semua negosiasi untuk kompensasi hilangnya hak legal, adat atau pemanfaatan dilakukan melalui sistem terdokumentasi yang dapat digunakan masyarakat adat, penduduk setempat, dan pemangku kepentingan lainnya untuk menyampaikan pendapatnya melalui lembaga perwakilannya sendiri.",
                7 => "Jika dapat dibuktikan bahwa masyarakat setempat memiliki hak legal, hak adat, atau hak pakai, maka mereka diberikan kompensasi untuk semua perolehan lahan dan pelepasan hak yang disepakati, dengan tunduk pada KBDD dan kesepakatan hasil negosiasi",
                8 => "Hak penggunaan lahan dapat dibuktikan dan tidak digugat secara sah oleh masyarakat setempat yang mampu membuktikan bahwa mereka memiliki hak legal, hak adat, atau hak pakai.",
            ),
            5 => array(
                1 => "Unit sertifikasi berhubungan dengan semua petani (Petani Mandiri dan Petani Plasma) dan semua pelaku usaha setempat lainnya secara adil dan transparan.",
                2 => "Unit sertifikasi mendukung perbaikan taraf penghidupan petani dan keikutsertaannya dalam rantai nilai minyak kelapa sawit berkelanjutan.",
            ),
            6 => array(
                1 => "Segala bentuk diskriminasi dilarang. ",
                2 => "Upah dan persyaratan-persyaratan kerja bagi staf, pekerja, dan pekerja kontrak selalu memenuhi sekurangnya standar minimum legal atau industri yang berlaku, dan cukup untuk memenuhi Upah Hidup Layak (“UHL”).",
                3 => "Unit sertifikasi menghormati hak seluruh pekerja untuk membentuk dan bergabung dengan serikat pekerja yang diinginkan, serta untuk berunding secara kolektif. Apabila hak dan kebebasan untuk berserikat dan berunding secara kolektif dibatasi oleh hukum, maka pemberi kerja memfasilitasi cara-cara serupa untuk berunding dan berasosiasi secara bebas dan independen untuk seluruh pekerja.",
                4 => "Anak-anak tidak dipekerjakan atau dieksploitasi.",
                5 => "Tidak ada pelecehan atau kekerasan di tempat kerja, dan hak reproduksi dilindungi.",
                6 => "Tidak ada bentuk penggunaan pekerja paksa dan pekerja dari perdagangan manusia.",
                7 => "Unit sertifikasi memastikan bahwa lingkungan kerja yang berada di bawah kendalinya tetap aman dan tidak memiliki risiko bagi kesehatan",
            ),
            7 => array(
                1 => "Hama, penyakit, gulma, dan spesies invasif yang diintroduksi dikendalikan secara efektif dengan menerapkan Teknik Pengendalian Hama Terpadu (PHT) yang tepat.",
                2 => "Pestisida digunakan dengan cara yang tidak membahayakan kesehatan pekerja, keluarganya, masyarakat sekitar atau lingkungan",
                3 => "Limbah dikurangi, didaur ulang, digunakan kembali, dan dibuang dengan cara-cara yang bertanggung jawab terhadap lingkungan dan sosial.",
                4 => "Praktik-praktik dilakukan untuk mempertahankan kesuburan tanah, atau apabila memungkinkan meningkatkan kesuburan tanah, sampai pada suatu tingkatan yang memberikan hasil optimal dan berkelanjutan.",
                5 => "Praktik-praktik yang meminimalkan dan mengendalikan erosi dan degradasi tanah.",
                6 => "Survei tanah dan informasi topografi digunakan untuk merencanakan lokasi pengembangan perkebunan baru dan hasilnya digabungkan ke dalam perencanaan dan operasi.",
                7 => "Tidak ada penanaman baru di lahan gambut, berapapun kedalamannya, setelah tanggal 15 November 2018 dan semua lahan gambut dikelola secara bertanggung jawab.",
                8 => "Praktik-praktik untuk menjaga kualitas dan ketersediaan air permukaan dan air tanah.",
                9 => "Efisiensi penggunaan bahan bakar fosil dan penggunaan energi terbarukan dioptimalkan.",
                10 => "Rencana untuk mengurangi pencemaran dan emisi, termasuk Gas Rumah Kaca (GRK), dikembangkan, diimplementasikan dan dipantau, dan pengembangan baru dirancang untuk meminimalkan emisi GRK.",
                11 => "Api tidak digunakan untuk pembukaan lahan dan dicegah penggunaannya pada lahan yang dikelola.",
                12 => "Pembukaan lahan tidak menyebabkan terjadinya deforestasi atau kerusakan pada area mana pun yang dipersyaratkan untuk melindungi atau meningkatkan Nilai Konservasi Tinggi (NKT) atau hutan Stok Karbon Tinggi (SKT). NKT dan hutan SKT yang ada di area yang dikelola, diidentifikasi dan dilindungi atau ditingkatkan.",
            ),
        );
    }
}