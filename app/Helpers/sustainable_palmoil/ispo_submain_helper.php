<?php

if (!function_exists('get_submain')) {
    function get_submain()
    {
        // "value" => "option"
        return array(
            1 => array(
                1 => "Izin Lokasi",
                2 => "Perusahaan Perkebunan harus memiliki izin usaha perkebunan",
                3 => "Perolehan lahan usaha perkebunan",
                4 => "Hak Atas Tanah",
                5 => "Fasilitasi pembangunan kebun masyarakat sekitar",
                6 => "Lokasi Perkebunan",
                7 => "Tanah Terlantar",
                8 => "Sengketa Lahan",
                9 => "Bentuk Badan Hukum",
            ),
            2 => array(
                1 => "Perencanaan Perkebunan",
                2 => array(
                    "description" => "Penerapan Teknis Budidaya dan Pengolahan Hasil",
                    "sub" => array(
                        1 => array(
                            "description" => "Penerapan Pedoman Teknis Budidaya",
                            "sub" => array(
                                1 => "Pembukaan Lahan",
                                2 => "Perbenihan",
                                3 => "Penanaman pada lahan Mineral",
                                4 => "Penanaman pada lahan Gambut",
                                5 => "Pemeliharaan Tanaman",
                                6 => "Pengendalian Organisme Pengganggu Tumbuhan (OPT)",
                                7 => "Pemanenan",
                            )
                        ),
                        2 => array(
                            "description" => "Penerapan Pedoman Teknis Pengolahan Hasil Perkebunan",
                            "sub" => array(
                                1 => "Pengangkutan Tandan Buah Segar (TBS)",
                                2 => "Penerimaan TBS di Unit Pengolahan Kelapa Sawit",
                                3 => "Pengolahan TBS",
                                4 => "Pengolahan Limbah",
                                5 => "Pemanfaatan Limbah",
                            )
                        ),
                    )
                ),
                3 => "Tumpang Tindih dengan Usaha Pertambangan",
                4 => "Rencana dan Realisasi Pembangunan Kebun dan Unit Pengolahan Kelapa Sawit",
                5 => "Penyediaan Data dan Informasi Kepada Instansi Terkait serta Pemangku Kepentingan Lainnya Selain Informasi yang Dikecualikan Sesuai Peraturan ",
            ),
            3 => array(
                1 => "Pelindungan Terhadap Pemanfaatan Hutan Alam Primer dan Lahan Gambut"
            ),
            4 => array(
                1 => "Kewajiban Perusahaan Perkebunan yang Terintegrasi dengan Unit Pengolahan Kelapa Sawit",
                2 => "Kewajiban Terkait Izin Lingkungan",
                3 => "Pengelolaan Bahan Berbahaya dan Beracun Serta Limbah Bahan Berbahaya dan Beracun (B3)",
                4 => "Gangguan dari Sumber yang tidak Bergerak",
                5 => "Pencegahan dan Penanggulangan Kebakaran",
                6 => "Pelestarian keanekaragaman Hayati (biodiversity)",
                7 => "Konservasi Terhadap Sumber dan Kualitas Air",
                8 => "Kawasan Lindung",
                9 => "Konservasi kawasan dengan potensi erosi tinggi",
                10 => "Mitigasi Emisi Gas Rumah Kaca (GRK)",
            ),
            5 => array(
                1 => "Keselamatan dan Kesehatan Kerja (K3)",
                2 => "Kesejahteraan dan Peningkatan Kemampuan Pekerja",
                3 => "Penggunaan Pekerja Anak dan Diskriminasi pekerja (Suku, Ras, Gender dan Agama)",
                4 => "Fasilitasi Pembentukan Serikat Pekerja",
                5 => "Perusahaan Perkebunan mendorong dan memfasilitasi pembentukan koperasi pekerja dan karyawan",
            ),
            6 => array(
                1 => "Tanggung Jawab Sosial dan lingkungan kemasyarakatan",
                2 => "Pemberdayaan Masyarakat Adat / Penduduk Asli",
                3 => "Pengembangan Usaha Lokal",
            ),
            7 => array(
                1 => "Peningkatan Usaha Secara Berkelanjutan",
            ),
        );
    }
}