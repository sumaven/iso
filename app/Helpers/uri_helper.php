<?php

use CodeIgniter\Config\Services;

if (! function_exists('getUriSegment')) {
    function getUriSegment($argVal = -1)
    {
        $uriObject = Services::request()->uri;
        if ($uriObject->getTotalSegments() < $argVal) return;

        return $uriObject->getSegment($argVal);
    }
}
