<?php

if (!function_exists('get_submain')) {
    function get_submain()
    {
        // "value" => "option"
        return array(
            1 => array(
                1 => "Kebijakan K3",
                2 => "Tanggung Jawab dan Wewenang Untuk Bertindak",
                3 => "Tinjauan dan Evaluasi",
                4 => "Keterlibatan dan Konsultasi dengan Tenaga Kerja",
            ),
            2 => array(
                1 => "Rencana Strategi K3",
                2 => "Manual SMK3",
                3 => "Peraturan perundangan dan Persyaratan Lain dibidang K3",
                4 => "Informasi K3",
            ),
            3 => array(
                1 => "Pengendalian Perancangan",
                2 => "Peninjauan Kontrak",
            ),
            4 => array(
                1 => "Persetujuan, Pengeluaran dan Pengendalian Dokumen",
                2 => "Perubahan dan Modifikasi Dokumen",
            ),
            5 => array(
                1 => "Spesifikasi Pembelian Barang dan Jasa",
                2 => "Sistem Verifikasi Barang dan Jasa Yang Telah Dibeli",
                3 => "Pengendalian Barang dan Jasa yang Dipasok Pelanggan",
                4 => "Kemampuan Telusur Produk",
            ),
            6 => array(
                1 => "Sistem Kerja",
                2 => "Pengawasan",
                3 => "Seleksi dan Penempatan Personil",
                4 => "Area Terbatas",
                5 => "Pemeliharaan, Perbaikan dan Perubahan Sarana Produksi",
                6 => "Pelayanan",
                7 => "Kesiapan Untuk Menangani Keadaan Darurat",
                8 => "Pertolongan Pertama Pada Kecelakaan",
                9 => "Rencana dan Pemulihan Keadaan Darurat",
            ),
            7 => array(
                1 => "Pemeriksaan Bahaya",
                2 => "Pemantauan / Pengukuran Lingkungan Kerja",
                3 => "Peralatan Pemeriksaan / Inspeksi, Pengukuran dan Pengujian",
                4 => "Pemantauan Kesehatan Tenaga Kerja",
            ),
            8 => array(
                1 => "Pelaporan Bahaya",
                2 => "Pelaporan Kecelakaan",
                3 => "Pemeriksaan dan Pengkajian Kecelakaan",
                4 => "Penanganan Masalah",
            ),
            9 => array(
                1 => "Penanganan Secara Manual dan Mekanis",
                2 => "Sistem Pengangkutan, Penyimpanan dan Pembuangan",
                3 => "Pengendalian Bahan Kimia Berbahaya (BKB)",
            ),
            10 => array(
                1 => "Catatan K3",
                2 => "Data dan Laporan K3",
            ),
            11 => array(
                1 => "Audit Internal SMK3",
            ),
            12 => array(
                1 => "Strategi Pelatihan",
                2 => "Pelatihan Bagi Manajemen dan Penyelia",
                3 => "Pelatihan Bagi Tenaga Kerja",
                4 => "Pelatihan Pengenalan dan Pelatihan Untuk Pengunjung dan Kontraktor",
                5 => "Pelatihan Keahlian Khusus",
            ),
        );
    }
}