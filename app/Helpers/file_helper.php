<?php

if ( ! function_exists('delete_file_url'))
{
    function delete_file_url($url = '')
    {
        if (empty($url)){
            return true;
        }

        $file_name = str_replace(base_url(), '', $url);
        $file_name = ROOTPATH . "public" . $file_name;

        if (file_exists($file_name)){
            if(unlink($file_name))
            {
                return true;
            } else {
                return false;
            }
        }
    }
}